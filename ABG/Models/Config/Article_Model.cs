﻿using System;
namespace ABG
{
    public class Article_Model
    {
        #region [ Field Name ]
        private string _ArticleKey = "";
        private int _ArticleStatus = 0;
        private string _ArticleID = "";
        private string _ArticleTag = "";
        private string _MetaDescription = "";
        private string _MetaKeyWord = "";
        private string _ArticleName = "";
        private string _Summarize = "";
        private string _ArticleContent = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private string _ImageSmall = "";
        private string _ImageLarge = "";
        private string _FileAttack = "";
        private string _FileName = "";
        private bool _Publish;
        private int _Hits = 0;
        private int _Rank = 0;
        private bool _Activate;
        private int _Slug = 0;
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string ArticleKey
        {
            get { return _ArticleKey; }
            set { _ArticleKey = value; }
        }
        public int ArticleStatus
        {
            get { return _ArticleStatus; }
            set { _ArticleStatus = value; }
        }
        public string ArticleID
        {
            get { return _ArticleID; }
            set { _ArticleID = value; }
        }
        public string ArticleTag
        {
            get { return _ArticleTag; }
            set { _ArticleTag = value; }
        }
        public string MetaDescription
        {
            get { return _MetaDescription; }
            set { _MetaDescription = value; }
        }
        public string MetaKeyWord
        {
            get { return _MetaKeyWord; }
            set { _MetaKeyWord = value; }
        }
        public string ArticleName
        {
            get { return _ArticleName; }
            set { _ArticleName = value; }
        }
        public string Summarize
        {
            get { return _Summarize; }
            set { _Summarize = value; }
        }
        public string ArticleContent
        {
            get { return _ArticleContent; }
            set { _ArticleContent = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string ImageSmall
        {
            get { return _ImageSmall; }
            set { _ImageSmall = value; }
        }
        public string ImageLarge
        {
            get { return _ImageLarge; }
            set { _ImageLarge = value; }
        }
        public string FileAttack
        {
            get { return _FileAttack; }
            set { _FileAttack = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public int Hits
        {
            get { return _Hits; }
            set { _Hits = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public bool Activate
        {
            get { return _Activate; }
            set { _Activate = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
