﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Sample_Data
    {
        public static List<Payroll_Sample_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ItemKey, A.ItemID, A.ItemName, A.UnitName, A.[RANK], A.CategoryName, A.Description, A.Formula, A.Parameter
FROM HRM_Payroll_Sample A 
LEFT JOIN HRM_Payroll_Category B ON A.CategoryKey = B.CategoryKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
ORDER BY A.ItemID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Sample_Model> zList = new List<Payroll_Sample_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Payroll_Sample_Model()
                    {
                        ItemKey = r["ItemKey"].ToInt(),
                        ItemID = r["ItemID"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        CategoryName = r["CategoryName"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                        UnitName = r["UnitName"].ToString(),
                        Formula = r["Formula"].ToString(),
                        Parameter = r["Parameter"].ToFloat(),
                    });
                }
            }
            return zList;
        }
        public static List<Payroll_Sample_Model> ListPlus(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Sample WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ItemType=1  ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Sample_Model> zList = new List<Payroll_Sample_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Payroll_Sample_Model()
                    {
                        ItemKey = r["ItemKey"].ToInt(),
                        ItemID = r["ItemID"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Total = r["Total"].ToFloat(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }
        public static List<Payroll_Sample_Model> ListMinus(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Sample WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ItemType=2  ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Sample_Model> zList = new List<Payroll_Sample_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Payroll_Sample_Model()
                    {
                        ItemKey = r["ItemKey"].ToInt(),
                        ItemID = r["ItemID"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Total = r["Total"].ToFloat(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }
    }
}
