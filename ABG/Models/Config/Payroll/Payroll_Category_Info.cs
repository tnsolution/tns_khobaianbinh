﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Payroll_Category_Info
{
 
public  Payroll_Category_Model Payroll_Category = new Payroll_Category_Model();
 
#region [ Constructor Get Information ]
public Payroll_Category_Info()
{
}
public Payroll_Category_Info(int CategoryKey)
{
string zSQL = "SELECT * FROM HRM_Payroll_Category WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["CategoryKey"]!= DBNull.Value)
Payroll_Category.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
Payroll_Category.CategoryName = zReader["CategoryName"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Payroll_Category.Rank = int.Parse(zReader["Rank"].ToString());
if (zReader["RecordStatus"]!= DBNull.Value)
Payroll_Category.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Payroll_Category.CreatedOn = (DateTime)zReader["CreatedOn"];
Payroll_Category.CreatedBy = zReader["CreatedBy"].ToString();
Payroll_Category.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Payroll_Category.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Payroll_Category.ModifiedBy = zReader["ModifiedBy"].ToString();
Payroll_Category.ModifiedName = zReader["ModifiedName"].ToString();
Payroll_Category.Message = "200 OK";
}
else
{
Payroll_Category.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Payroll_Category.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Payroll_Category (" 
 + " CategoryName , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @CategoryName , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Category.CategoryName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Category.Rank;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Category.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Category.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Category.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Category.Message = "201 Created";
}
catch (Exception Err)
{
Payroll_Category.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Payroll_Category(" 
 + " CategoryKey , CategoryName , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @CategoryKey , @CategoryName , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Category.CategoryKey;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Category.CategoryName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Category.Rank;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Category.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Category.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Category.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Category.Message = "201 Created";
}
catch (Exception Err)
{
Payroll_Category.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_Payroll_Category SET " 
            + " CategoryName = @CategoryName,"
            + " Rank = @Rank,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE CategoryKey = @CategoryKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Category.CategoryKey;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Category.CategoryName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Category.Rank;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Category.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Category.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Category.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Category.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_Payroll_Category SET RecordStatus = 99 WHERE CategoryKey = @CategoryKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Category.CategoryKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Category.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Category.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_Payroll_Category WHERE CategoryKey = @CategoryKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Category.CategoryKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Category.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Category.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
