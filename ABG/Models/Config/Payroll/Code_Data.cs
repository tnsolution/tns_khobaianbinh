﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Code_Data
    {
        
        public static List<Payroll_Code_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Code WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY ItemType, Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Code_Model> zList = new List<Payroll_Code_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Code_Model()
                {
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    ItemType = r["ItemType"].ToInt(),
                    Math = r["Math"].ToString(),
                    MathCaculator = r["MathCaculator"].ToString()
                });
            }
            return zList;
        }
    }
}
