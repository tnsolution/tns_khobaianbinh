﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Increase_Data
    {

        public static List<Payroll_Increase_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Increase WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Increase_Model> zList = new List<Payroll_Increase_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zFromDate = DateTime.MinValue;
                    DateTime.TryParse(r["FromDate"].ToString(), out zFromDate);
                    DateTime zToDate = DateTime.MinValue;
                    DateTime.TryParse(r["ToDate"].ToString(), out zToDate);
                    zList.Add(new Payroll_Increase_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        ID = r["ID"].ToString(),
                        ContractID = r["ContractID"].ToString(),
                        EmployeeName = r["EmployeeName"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        FromDate = zFromDate,
                        ToDate = zToDate,
                        Total = r["Total"].ToFloat(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}
