﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Article_Data
    {
        public static string Top1CateChildKey(string PartnerNumber, out string Message, int Parent)
        {
            string zResult = "";
            string zSQL = @"SELECT TOP 1 CategoryKey FROM PDT_Article_Category WHERE Parent = @Parent AND PartnerNumber = @PartnerNumber AND RecordStatus <> 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zResult;
        }
        public static List<Article_Model> ListImage(string PartnerNumber, out string Message, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 9"; // slug = 9 là các record chỉ chứa hình
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    FileName = r["FileName"].ToString(),
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Article_Model> ListDocument(string PartnerNumber, out string Message, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 8"; // slug = 8 là các record chỉ chứa tài liệu cổ đông
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    FileName = r["FileName"].ToString(),
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static string CateChildKey(string PartnerNumber, out string Message, int Parent)
        {
            string zResult = "";
            string zSQL = @"
SELECT STUFF( (SELECT ',' + CAST(CategoryKey AS NVARCHAR(50)) 
                             FROM PDT_Article_Category
                             WHERE RecordStatus <> 99 AND Parent = @Parent
                             FOR XML PATH('')), 
                            1, 1, '')";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zResult;
        }
        public static List<Article_Model> ListInCate(string PartnerNumber, out string Message, string CateChild)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 0";
            if (CateChild.Length > 0)
            {
                zSQL += " AND CategoryKey IN (" + CateChild + ")";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Model> ListByTag(string PartnerNumber, out string Message, string Tag)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Tag != string.Empty)
            {
                zSQL += " AND ArticleTag = @ArticleTag";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ArticleTag", SqlDbType.NVarChar).Value = Tag;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<string> ListTag(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT DISTINCT ArticleTag FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND LEN(ArticleTag) > 0";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<string> zList = new List<string>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(r["ArticleTag"].ToString());
            }
            return zList;
        }

        public static List<Article_Model> ListNew(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP 5 A.* 
FROM PDT_Article A 
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND A.CategoryKey IN (43,44)
ORDER BY CreatedOn desc ";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Model> List(string PartnerNumber, out string Message, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 0";
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //tìm kiếm danh sách tin tức
        public static List<Article_Model> ListSearch(string PartnerNumber, out string Message, string CategoryKey, string Parent)
        {
            DataTable zTable = new DataTable();
            string zFilter = "";
            if (Parent == "0")
            {
                zFilter += " AND B.Parent = @CategoryKey  ";
            }
            else
            {
                zFilter += " AND A.CategoryKey =@CategoryKey";
            }

            string zSQL = @"SELECT A.* FROM PDT_Article A
 LEFT JOIN[dbo].[PDT_Article_Category] B ON A.CategoryKey=B.CategoryKey
WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber 
AND B.RecordStatus != 99 AND B.PartnerNumber = @PartnerNumber
 @Paramater
 ";
            zSQL = zSQL.Replace("@Paramater", zFilter);
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Model> Listservice(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey=@CategoryKey  ";
            if (Activate == true)
            {
                zSQL += " AND Activate=@Activate ";
            }

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Danh sách kho bãi Admin
        public static List<Article_Model> Listwarehouse(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            if (Activate == true)
            {
                zSQL += " AND Activate=@Activate ";
            }

            zSQL += " AND CategoryKey=@CategoryKey  ORDER BY CreatedOn DESC  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Danh sách kho bãi Index
        public static List<Article_Model> ListwarehouseIndaex(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select TOP 6 * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            zSQL += " AND CategoryKey=@CategoryKey AND Publish=@Publish AND Activate=@Activate ORDER BY CreatedOn DESC  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        //top 3 danh sách tin mới
        public static List<Article_Model> ListArticleNew(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP 3 A.* FROM PDT_Article A 
 LEFT JOIN[dbo].[PDT_Article_Category] B ON A.CategoryKey=B.CategoryKey 
WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber  
AND B.RecordStatus != 99 AND B.PartnerNumber = @PartnerNumber  
AND A.Activate=@Activate  
AND B.Parent = @CategoryKey ORDER BY CreatedOn desc ";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Top 3 danh sách tin mới Index
        public static List<Article_Model> ListArticleNewIndex(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP 3 A.* FROM PDT_Article A
 LEFT JOIN[dbo].[PDT_Article_Category] B ON A.CategoryKey=B.CategoryKey
WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber 
AND B.RecordStatus != 99 AND B.PartnerNumber = @PartnerNumber 
AND A.Publish=@Publish AND B.Publish=@Publish
AND A.Activate=@Activate 
AND B.Parent = @CategoryKey ORDER BY CreatedOn desc ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Danh sách tin giới thiệu admin
        public static List<Article_Model> Listintroduce(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey=@CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Tin giới thiệu hiện ở Index
        public static List<Article_Model> ListintroduceIndex(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 1 * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey=@CategoryKey AND Activate=@Activate AND Publish=@Publish ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Top 6 danh sách tin tức
        public static List<Article_Model> ListTop6Article(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select top 6 * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            zSQL += " AND CategoryKey=@CategoryKey AND Activate=@Activate  ORDER BY CreatedOn DESC  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //Số lượng tin chung
        public static int CountActivatePublish(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            zSQL += " AND CategoryKey=@CategoryKey AND Activate=@Activate  ORDER BY CreatedOn DESC  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            int zlist = zTable.Rows.Count;
            return zlist;
        }

        //Số lượng tin công ty
        public static int CountActivate(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            zSQL += " AND CategoryKey=@CategoryKey AND Activate=@Activate  ORDER BY CreatedOn DESC  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            int zlist = zTable.Rows.Count;
            return zlist;
        }

        public static int KT(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "select * from [dbo].[PDT_Article] where RecordStatus != 99 AND PartnerNumber =@PartnerNumber ";
            zSQL += " AND CategoryKey=@CategoryKey AND Activate=@Activate";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            int zlist = zTable.Rows.Count;
            return zlist;
        }

        public static List<Article_Model> ListintroduceMenu(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey=@CategoryKey AND Activate=@Activate ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        //danh sách dịch vụ
        public static List<Article_Model> ListServiceTop4(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT top 4 * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey=@CategoryKey AND Activate=@Activate ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }


    }
}
