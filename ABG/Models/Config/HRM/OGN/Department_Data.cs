﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Department_Data
    {
        public static List<Department_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Department WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Department_Model> zList = new List<Department_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Department_Model()
                    {
                        BranchKey = r["BranchKey"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        DepartmentKey = r["DepartmentKey"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        DepartmentID = r["DepartmentID"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }

        public static List<Department_Model> ListSearch(string PartnerNumber, string Search)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  * FROM HRM_Department WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( DepartmentID LIKE @Search OR DepartmentName LIKE @Search  )";
            }
            zSQL += "  ORDER BY [Rank]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Department_Model> zList = new List<Department_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Department_Model()
                    {
                        DepartmentKey = r["DepartmentKey"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        DepartmentID = r["DepartmentID"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }
    }
}
