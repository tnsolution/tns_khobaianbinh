﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Position_Info
    {

        public Position_Model ListPosition = new Position_Model();

        #region [ Constructor Get Information ]
        public Position_Info()
        {
        }
        public Position_Info(int? PositionKey)
        {
            string zSQL = "SELECT * FROM HRM_ListPosition WHERE PositionKey = @PositionKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = PositionKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["PositionKey"] != DBNull.Value)
                        ListPosition.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    ListPosition.PositionNameEN = zReader["PositionNameEN"].ToString();
                    ListPosition.PositionNameVN = zReader["PositionNameVN"].ToString();
                    ListPosition.PositionNameCN = zReader["PositionNameCN"].ToString();
                    ListPosition.PositionID = zReader["PositionID"].ToString();
                    ListPosition.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["BusinessKey"] != DBNull.Value)
                        ListPosition.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        ListPosition.ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    ListPosition.Description = zReader["Description"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        ListPosition.Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        ListPosition.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    ListPosition.CreatedBy = zReader["CreatedBy"].ToString();
                    ListPosition.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        ListPosition.CreatedOn = (DateTime)zReader["CreatedOn"];
                    ListPosition.ModifiedBy = zReader["ModifiedBy"].ToString();
                    ListPosition.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        ListPosition.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    ListPosition.Message = "200 OK";
                }
                else
                {
                    ListPosition.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                ListPosition.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ListPosition ("
         + " PositionNameEN , PositionNameVN , PositionNameCN , PositionID , PartnerNumber , BusinessKey , ParentKey , Description , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PositionNameEN , @PositionNameVN , @PositionNameCN , @PositionID , @PartnerNumber , @BusinessKey , @ParentKey , @Description , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
        string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionNameEN", SqlDbType.NVarChar).Value = ListPosition.PositionNameEN;
                zCommand.Parameters.Add("@PositionNameVN", SqlDbType.NVarChar).Value = ListPosition.PositionNameVN;
                zCommand.Parameters.Add("@PositionNameCN", SqlDbType.NVarChar).Value = ListPosition.PositionNameCN;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = ListPosition.PositionID;
                if (ListPosition.PartnerNumber != "" && ListPosition.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ListPosition.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = ListPosition.BusinessKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = ListPosition.ParentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ListPosition.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = ListPosition.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = ListPosition.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = ListPosition.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = ListPosition.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ListPosition.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ListPosition.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListPosition.Message = "201 Created";
            }
            catch (Exception Err)
            {
                ListPosition.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE HRM_ListPosition SET "
                        + " PositionNameEN = @PositionNameEN,"
                        + " PositionNameVN = @PositionNameVN,"
                        + " PositionNameCN = @PositionNameCN,"
                        + " PositionID = @PositionID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " BusinessKey = @BusinessKey,"
                        + " ParentKey = @ParentKey,"
                        + " Description = @Description,"
                        + " Rank = @Rank,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE PositionKey = @PositionKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = ListPosition.PositionKey;
                zCommand.Parameters.Add("@PositionNameEN", SqlDbType.NVarChar).Value = ListPosition.PositionNameEN;
                zCommand.Parameters.Add("@PositionNameVN", SqlDbType.NVarChar).Value = ListPosition.PositionNameVN;
                zCommand.Parameters.Add("@PositionNameCN", SqlDbType.NVarChar).Value = ListPosition.PositionNameCN;
                zCommand.Parameters.Add("@PositionID", SqlDbType.NVarChar).Value = ListPosition.PositionID;
                if (ListPosition.PartnerNumber != "" && ListPosition.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ListPosition.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = ListPosition.BusinessKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = ListPosition.ParentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ListPosition.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = ListPosition.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = ListPosition.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ListPosition.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ListPosition.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListPosition.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListPosition.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_ListPosition SET RecordStatus = 99 WHERE PositionKey = @PositionKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = ListPosition.PositionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListPosition.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListPosition.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_ListPosition WHERE PositionKey = @PositionKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = ListPosition.PositionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListPosition.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListPosition.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
