﻿using System;
namespace ABG
{
    public class Criteria_Model
    {
        #region [ Field Name ]
        private int _CriteriaKey = 0;
        private string _CriteriaID = "";
        private string _CriteriaName = "";
        private int _MaxPoint = 0;
        private int _Rank = 0;
        private string _Description = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int CriteriaKey
        {
            get { return _CriteriaKey; }
            set { _CriteriaKey = value; }
        }
        public string CriteriaID
        {
            get { return _CriteriaID; }
            set { _CriteriaID = value; }
        }
        public string CriteriaName
        {
            get { return _CriteriaName; }
            set { _CriteriaName = value; }
        }
        public int MaxPoint
        {
            get { return _MaxPoint; }
            set { _MaxPoint = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
