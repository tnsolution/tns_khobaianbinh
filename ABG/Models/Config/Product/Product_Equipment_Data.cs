﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Product_Equipment_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM PDT_Product_Equipment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Product_Equipment_Model> ListEquipment(string PartnerNumber, string ProductKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  * FROM PDT_Product_Equipment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ProductKey =@ProductKey";
            zSQL += "  ORDER BY CreatedOn";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Product_Equipment_Model> zList = new List<Product_Equipment_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                  
                    zList.Add(new Product_Equipment_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        ItemKey = r["ItemKey"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Value = r["Value"].ToFloat(),
                        Description = r["Description"].ToString()
                    });
                }
            }
            return zList;
        }
        public static DataTable ListEqu(string PartnerNumber, string ProductKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM PDT_Product_Equipment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ProductKey = @ProductKey ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
