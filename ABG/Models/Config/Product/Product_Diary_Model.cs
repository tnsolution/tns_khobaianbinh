﻿using System;
namespace ABG
{
    public class Product_Diary_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _PhotoPath = "";
        private string _AreaKey = "";
        private string _AreaName = "";
        private string _AssetKey = "";
        private string _AssetName = "";
        private DateTime _DateWrite = DateTime.MinValue;
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private string _Description = "";
        private int _Parent = 0;
        private string _ContractKey = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string AreaKey
        {
            get { return _AreaKey; }
            set { _AreaKey = value; }
        }
        public string AreaName
        {
            get { return _AreaName; }
            set { _AreaName = value; }
        }
        public string AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public string AssetName
        {
            get { return _AssetName; }
            set { _AssetName = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ContractKey
        {
            get
            {
                return _ContractKey;
            }

            set
            {
                _ContractKey = value;
            }
        }

        public string PhotoPath
        {
            get
            {
                return _PhotoPath;
            }

            set
            {
                _PhotoPath = value;
            }
        }
        #endregion
    }
}
