﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Product_Equipment_Info
{
 
public  Product_Equipment_Model Product_Equipment = new Product_Equipment_Model();
 
#region [ Constructor Get Information ]
public Product_Equipment_Info()
{
}
public Product_Equipment_Info(int AutoKey)
{
string zSQL = "SELECT * FROM PDT_Product_Equipment WHERE AutoKey = @AutoKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["AutoKey"]!= DBNull.Value)
Product_Equipment.AutoKey = int.Parse(zReader["AutoKey"].ToString());
Product_Equipment.ProductKey = zReader["ProductKey"].ToString();
Product_Equipment.ItemKey = zReader["ItemKey"].ToString();
Product_Equipment.ItemName = zReader["ItemName"].ToString();
if (zReader["Value"]!= DBNull.Value)
Product_Equipment.Value = float.Parse(zReader["Value"].ToString());
Product_Equipment.Description = zReader["Description"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Product_Equipment.Rank = int.Parse(zReader["Rank"].ToString());
Product_Equipment.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["Publish"]!= DBNull.Value)
Product_Equipment.Publish = (bool)zReader["Publish"];
if (zReader["RecordStatus"]!= DBNull.Value)
Product_Equipment.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Product_Equipment.CreatedOn = (DateTime)zReader["CreatedOn"];
Product_Equipment.CreatedBy = zReader["CreatedBy"].ToString();
Product_Equipment.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Product_Equipment.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Product_Equipment.ModifiedBy = zReader["ModifiedBy"].ToString();
Product_Equipment.ModifiedName = zReader["ModifiedName"].ToString();
Product_Equipment.Message = "200 OK";
}
else
{
Product_Equipment.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Product_Equipment.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Equipment (" 
 + " ProductKey , ItemKey , ItemName , Value , Description , Rank , PartnerNumber , Publish , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ProductKey , @ItemKey , @ItemName , @Value , @Description , @Rank , @PartnerNumber , @Publish , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Equipment.ProductKey;
zCommand.Parameters.Add("@ItemKey", SqlDbType.NChar).Value = Product_Equipment.ItemKey;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Product_Equipment.ItemName;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Equipment.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Equipment.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Equipment.Rank;
if(Product_Equipment.PartnerNumber != "" && Product_Equipment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Equipment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Equipment.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Equipment.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Equipment.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Equipment.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Equipment.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Equipment.Message = "201 Created";
}
catch (Exception Err)
{
Product_Equipment.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Equipment(" 
 + " AutoKey , ProductKey , ItemKey , ItemName , Value , Description , Rank , PartnerNumber , Publish , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @AutoKey , @ProductKey , @ItemKey , @ItemName , @Value , @Description , @Rank , @PartnerNumber , @Publish , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Equipment.AutoKey;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Equipment.ProductKey;
zCommand.Parameters.Add("@ItemKey", SqlDbType.NChar).Value = Product_Equipment.ItemKey;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Product_Equipment.ItemName;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Equipment.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Equipment.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Equipment.Rank;
if(Product_Equipment.PartnerNumber != "" && Product_Equipment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Equipment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Equipment.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Equipment.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Equipment.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Equipment.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Equipment.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Equipment.Message = "201 Created";
}
catch (Exception Err)
{
Product_Equipment.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE PDT_Product_Equipment SET " 
            + " ProductKey = @ProductKey,"
            + " ItemKey = @ItemKey,"
            + " ItemName = @ItemName,"
            + " Value = @Value,"
            + " Description = @Description,"
            + " Rank = @Rank,"
            + " PartnerNumber = @PartnerNumber,"
            + " Publish = @Publish,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE AutoKey = @AutoKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Equipment.AutoKey;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Equipment.ProductKey;
zCommand.Parameters.Add("@ItemKey", SqlDbType.NChar).Value = Product_Equipment.ItemKey;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Product_Equipment.ItemName;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Equipment.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Equipment.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Equipment.Rank;
if(Product_Equipment.PartnerNumber != "" && Product_Equipment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Equipment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Equipment.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Equipment.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Equipment.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Equipment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Equipment.Message = "200 OK";
}
catch (Exception Err)
{
Product_Equipment.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE PDT_Product_Equipment SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Equipment.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Equipment.Message = "200 OK";
}
catch (Exception Err)
{
Product_Equipment.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM PDT_Product_Equipment WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Equipment.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Equipment.Message = "200 OK";
}
catch (Exception Err)
{
Product_Equipment.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
