﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Product_Diary_Data
    {       
        public static List<Product_Diary_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  * FROM PDT_Product_Diary WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            zSQL += "  ORDER BY DateWrite";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Product_Diary_Model> zList = new List<Product_Diary_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zDateWrite = DateTime.MinValue;
                    if (DateTime.TryParse(r["DateWrite"].ToString(), out zDateWrite))
                    {

                    }

                    zList.Add(new Product_Diary_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        AreaName = r["AreaName"].ToString(),
                        AssetName = r["AssetName"].ToString(),
                        DateWrite = zDateWrite,
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        Description = r["Description"].ToString(),

                    });
                }
            }
            return zList;
        }
        public static List<Product_Diary_Model> List(string PartnerNumber, string AssetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PDT_Product_Diary WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND AssetKey = @AssetKey";
            zSQL += "  ORDER BY DateWrite";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Product_Diary_Model> zList = new List<Product_Diary_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zDateWrite = DateTime.MinValue;
                    if (DateTime.TryParse(r["DateWrite"].ToString(), out zDateWrite))
                    {

                    }

                    zList.Add(new Product_Diary_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        AreaName = r["AreaName"].ToString(),
                        AssetName = r["AssetName"].ToString(),
                        DateWrite = zDateWrite,
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        Description = r["Description"].ToString(),
                        PhotoPath = r["PhotoPath"].ToString(),
                        CreatedBy = r["CreatedBy"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}
