﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Product_Feature_Info
{
 
public  Product_Feature_Model Product_Feature = new Product_Feature_Model();
 
#region [ Constructor Get Information ]
public Product_Feature_Info()
{
}
public Product_Feature_Info(int FeatureKey)
{
string zSQL = "SELECT * FROM PDT_Product_Feature WHERE FeatureKey = @FeatureKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@FeatureKey", SqlDbType.Int).Value = FeatureKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["FeatureKey"]!= DBNull.Value)
Product_Feature.FeatureKey = int.Parse(zReader["FeatureKey"].ToString());
Product_Feature.ProductKey = zReader["ProductKey"].ToString();
Product_Feature.Name = zReader["Name"].ToString();
if (zReader["Value"]!= DBNull.Value)
Product_Feature.Value = float.Parse(zReader["Value"].ToString());
Product_Feature.Description = zReader["Description"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Product_Feature.Rank = int.Parse(zReader["Rank"].ToString());
Product_Feature.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["Publish"]!= DBNull.Value)
Product_Feature.Publish = (bool)zReader["Publish"];
if (zReader["RecordStatus"]!= DBNull.Value)
Product_Feature.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Product_Feature.CreatedOn = (DateTime)zReader["CreatedOn"];
Product_Feature.CreatedBy = zReader["CreatedBy"].ToString();
Product_Feature.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Product_Feature.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Product_Feature.ModifiedBy = zReader["ModifiedBy"].ToString();
Product_Feature.ModifiedName = zReader["ModifiedName"].ToString();
Product_Feature.Message = "200 OK";
}
else
{
Product_Feature.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Product_Feature.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Feature (" 
 + " ProductKey , Name , Value , Description , Rank , PartnerNumber , Publish , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ProductKey , @Name , @Value , @Description , @Rank , @PartnerNumber , @Publish , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Feature.ProductKey;
zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Product_Feature.Name;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Feature.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Feature.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Feature.Rank;
if(Product_Feature.PartnerNumber != "" && Product_Feature.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Feature.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Feature.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Feature.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Feature.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Feature.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Feature.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Feature.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Feature.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Feature.Message = "201 Created";
}
catch (Exception Err)
{
Product_Feature.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Feature(" 
 + " FeatureKey , ProductKey , Name , Value , Description , Rank , PartnerNumber , Publish , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @FeatureKey , @ProductKey , @Name , @Value , @Description , @Rank , @PartnerNumber , @Publish , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@FeatureKey", SqlDbType.Int).Value = Product_Feature.FeatureKey;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Feature.ProductKey;
zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Product_Feature.Name;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Feature.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Feature.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Feature.Rank;
if(Product_Feature.PartnerNumber != "" && Product_Feature.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Feature.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Feature.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Feature.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Feature.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Feature.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Feature.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Feature.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Feature.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Feature.Message = "201 Created";
}
catch (Exception Err)
{
Product_Feature.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE PDT_Product_Feature SET " 
            + " ProductKey = @ProductKey,"
            + " Name = @Name,"
            + " Value = @Value,"
            + " Description = @Description,"
            + " Rank = @Rank,"
            + " PartnerNumber = @PartnerNumber,"
            + " Publish = @Publish,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE FeatureKey = @FeatureKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@FeatureKey", SqlDbType.Int).Value = Product_Feature.FeatureKey;
zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Product_Feature.ProductKey;
zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Product_Feature.Name;
zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Product_Feature.Value;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Feature.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Feature.Rank;
if(Product_Feature.PartnerNumber != "" && Product_Feature.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Feature.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
if (Product_Feature.Publish == null) 
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
else
zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Feature.Publish;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Feature.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Feature.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Feature.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Feature.Message = "200 OK";
}
catch (Exception Err)
{
Product_Feature.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE PDT_Product_Feature SET RecordStatus = 99 WHERE FeatureKey = @FeatureKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@FeatureKey", SqlDbType.Int).Value = Product_Feature.FeatureKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Feature.Message = "200 OK";
}
catch (Exception Err)
{
Product_Feature.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM PDT_Product_Feature WHERE FeatureKey = @FeatureKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@FeatureKey", SqlDbType.Int).Value = Product_Feature.FeatureKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Feature.Message = "200 OK";
}
catch (Exception Err)
{
Product_Feature.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
