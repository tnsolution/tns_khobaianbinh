﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Product_Land_Data
    {
        public static List<Product_Land_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT *
FROM PDT_Product_Land 
WHERE RecordStatus != 99 
AND Class = 'TK'
AND PartnerNumber = @PartnerNumber 
AND ParentKey = '00000000-0000-0000-0000-000000000000'
ORDER BY [Rank]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Product_Land_Model> zList = new List<Product_Land_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Product_Land_Model()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        Address = r["Address"].ToString(),
                        Rank = r["Rank"].ToInt()
                    });
                }
            }
            return zList;
        }
        public static List<Product_Land_Model> ListAsset(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT *
FROM PDT_Product_Land 
WHERE RecordStatus != 99 
AND Class != 'TK'
AND PartnerNumber = @PartnerNumber 
AND ParentKey != '00000000-0000-0000-0000-000000000000'
ORDER BY [Rank], ParentKey, Style";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Product_Land_Model> zList = new List<Product_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Product_Land_Model()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        Address = r["Address"].ToString(),
                        Rank = r["Rank"].ToInt()
                    });
                }
            }
            return zList;
        }
        public static List<Product_Land_Model> ListAsset(string PartnerNumber, string ParentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT *
FROM PDT_Product_Land 
WHERE RecordStatus != 99 
AND Class != 'TK'
AND PartnerNumber = @PartnerNumber 
AND ParentKey != '00000000-0000-0000-0000-000000000000'
AND ParentKey = @ParentKey 
ORDER BY [Rank],Style";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Product_Land_Model> zList = new List<Product_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Product_Land_Model()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        Address = r["Address"].ToString(),
                        Rank = r["Rank"].ToInt()
                    });
                }
            }
            return zList;
        }
        public static List<Product_Land_Model> Search(string PartnerNumber, string Parent, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT *
FROM PDT_Product_Land 
WHERE RecordStatus != 99 
AND Class != 'TK'
AND PartnerNumber = @PartnerNumber ";
            if (Parent.Length < 36)
            {
                zSQL += " AND ParentKey != '00000000-0000-0000-0000-000000000000'";
            }
            else
            {
                zSQL += " AND ParentKey = @Parent";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND ProductName LIKE @ProductName";
            }
            zSQL += " ORDER BY [Rank], ParentKey, Style";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Product_Land_Model> zList = new List<Product_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Product_Land_Model()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        Address = r["Address"].ToString(),
                        Rank = r["Rank"].ToInt()
                    });
                }
            }
            return zList;
        }

        public static DataTable GetLand(string PartnerNumber, string ParentKey)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT X.*, 
A.ProductKey, A.ProductName, A.LngLat, A.CodeLine, A.Class
FROM PDT_Product_Land A 
OUTER APPLY dbo.GetContract(A.ProductKey, A.PartnerNumber) X
WHERE 
A.PartnerNumber = @PartnerNumber
AND A.ParentKey = @ParentKey
AND A.RecordStatus <> 99
AND LEN(A.LngLat) > 0
ORDER BY X.ToDate DESC ";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }

            return zTable;
        }
        public static DataTable GetAsset(string PartnerNumber, string ProductKey)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT X.*, A.ProductKey, A.ProductName, A.LngLat
FROM PDT_Product_Land A 
OUTER APPLY dbo.GetContract(A.ProductKey, A.PartnerNumber) X
WHERE A.PartnerNumber = @PartnerNumber
AND A.ProductKey = @ProductKey
AND LEN(A.LngLat) > 0
ORDER BY X.ToDate DESC ";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }

            return zTable;
        }
        public static DataTable SearchAsset(string PartnerNumber, string CustomerKey, string ContractID )
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT X.*, A.ProductKey, A.ProductName, A.LngLat
FROM PDT_Product_Land A 
OUTER APPLY dbo.GetContract(A.ProductKey, A.PartnerNumber) X
WHERE A.PartnerNumber = @PartnerNumber AND LEN(A.LngLat) > 0";
            if (CustomerKey != string.Empty)
            {
                zSQL += " AND X.BuyerKey = @CustomerKey";
            }
            if (ContractID != string.Empty)
            {
                zSQL += " AND (X.ContractID = @ContractID OR X.SubContract = @ContractID)";
            }

            zSQL+=" ORDER BY X.ToDate DESC ";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = ContractID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }

            return zTable;
        }
    }
}
