﻿namespace ABG
{
    public class Customer_Category_Model
    {
        public int CategoryKey { get; set; }
        public string CategoryName { get; set; }
    }
}