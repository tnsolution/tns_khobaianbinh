﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABG
{
    public class Track_Item
    {
        public string ActionName { get; set; } = "";
        public string MethodType { get; set; } = "";
        public string ControllerName { get; set; } = "";
        public string Url { get; set; } = "";
        public string Description { get; set; } = "";
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
    }
}