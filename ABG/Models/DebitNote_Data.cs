﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class DebitNote_Data
    {
        public static List<DebitNote_Model> List(string PartnerNumber, string ContractKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.ContractID, B.SubContract, B.FromDate, B.ToDate, B.BuyerName
FROM FNC_DebitNote A
LEFT JOIN CRM_Contract B ON A.ContractKey = B.ContractKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND A.ContractKey = @ContractKey
ORDER BY A.CreatedOn";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                //zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = ItemKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<DebitNote_Model> zList = new List<DebitNote_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new DebitNote_Model()
                {
                    BuyerName= (r["BuyerName"] == DBNull.Value ? "" : r["BuyerName"].ToString()),
                    FromDate = (r["FromDate"] == DBNull.Value ? "" : ((DateTime)r["FromDate"]).ToString("dd/MM/yyyy")),
                    ToDate = (r["ToDate"] == DBNull.Value ? "" : ((DateTime)r["ToDate"]).ToString("dd/MM/yyyy")),
                    SubContract = (r["SubContract"] == DBNull.Value ? "" : r["SubContract"].ToString()),
                    ContractID = (r["ContractID"] == DBNull.Value ? "" : r["ContractID"].ToString()),
                    PhotoPath = (r["PhotoPath"] == DBNull.Value ? "" : r["PhotoPath"].ToString()),

                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    DebitNote = (r["DebitNote"] == DBNull.Value ? "" : r["DebitNote"].ToString()),
                    DebitDate = (r["DebitDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DebitDate"]),
                    AmountPaid = (r["AmountPaid"] == DBNull.Value ? 0 : double.Parse(r["AmountPaid"].ToString())),
                    AmountLeft = (r["AmountLeft"] == DBNull.Value ? 0 : double.Parse(r["AmountLeft"].ToString())),
                    CustomerKey = (r["CustomerKey"] == DBNull.Value ? "" : r["CustomerKey"].ToString()),
                    ContractKey = (r["ContractKey"] == DBNull.Value ? "" : r["ContractKey"].ToString()),
                    ItemKey = (r["ItemKey"] == DBNull.Value ? "" : r["ItemKey"].ToString()),
                    ProductKey = (r["ProductKey"] == DBNull.Value ? "" : r["ProductKey"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
