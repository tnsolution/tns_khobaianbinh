﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class DebitNote_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _DebitNote = "";
        private DateTime _DebitDate = DateTime.MinValue;
        private double _AmountPaid = 0;
        private double _AmountLeft = 0;
        private string _CustomerKey = "";
        private string _ContractKey = "";
        private string _ItemKey = "";
        private string _ProductKey = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string DebitNote
        {
            get { return _DebitNote; }
            set { _DebitNote = value; }
        }
        public DateTime DebitDate
        {
            get { return _DebitDate; }
            set { _DebitDate = value; }
        }
        public double AmountPaid
        {
            get { return _AmountPaid; }
            set { _AmountPaid = value; }
        }
        public double AmountLeft
        {
            get { return _AmountLeft; }
            set { _AmountLeft = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string ContractKey
        {
            get { return _ContractKey; }
            set { _ContractKey = value; }
        }
        public string ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion

        public string PhotoPath { get; set; } = "";

        public string ContractID { get; set; } = "";
        public string SubContract { get; set; } = "";
        public string FromDate { get; set; } = "";
        public string ToDate { get; set; } = "";
        public string BuyerName { get; set; } = "";
    }
}
