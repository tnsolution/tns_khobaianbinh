﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace ABG
{
    public static class Helper
    {
        public static double SumRow(DataTable zTable, int Index, string Name)
        {
            double total = 0;
            foreach (DataRow r in zTable.Rows)
            {
                if (r[0].ToString().ToUpper() == Name.ToUpper())
                {
                    total += r[Index].ToDouble();
                }
            }
            return total;
        }
        public static string CheckPayroll(string EmployeeKey, string ParentKey)
        {
            string zResult = "";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                string SQL = @"
SELECT [Description]
FROM HRM_Work_Day
WHERE
RecordStatus <> 99
AND EmployeeKey = @EmployeeKey 
AND ParentKey = @ParentKey";
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                var obj = zCommand.ExecuteScalar();
                if (obj != null)
                    zResult = obj.ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static void LogAction(User_Model UserLog, string action = "", string key = "", string content = "")
        {
            var desc = "";
            //tên người thực hiện
            var user = UserLog.EmployeeName + "> ";
            desc += user;
            //nếu có đối tượng xử lý thì diễn giãi [ thao tác đối tượng nào ]
            if (key != string.Empty)
            {
                action += " [" + new Product_Land_Info(key).Product_Land.ProductName + "]";
            }
            desc += action;
            if (content != string.Empty)
            {
                desc += " > Nội dung: " + TN_Utils.GetShortContent(content, 30);
            }

            string SQL = "INSERT INTO SYS_User_Track ("
                + " JsonData , Description , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
                + " VALUES ( "
                + " N'Trong Controller' , N'" + desc + "' , N'" + UserLog.UserKey + "' , N'" + UserLog.EmployeeName + "' , N'" + UserLog.UserKey + "' , N'" + UserLog.EmployeeName + "' ) ";

            Helper.RunSQL(SQL, out _);
        }

        public static bool DeleteSingleFile(string FilePath, out string Message)
        {
            try
            {
                string fullPath = HttpContext.Current.Server.MapPath(FilePath);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                Message = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="HttpFiles"></param>
        /// <param name="FilePath"></param>
        /// <param name="FileSave"></param>
        /// <returns></returns>
        public static bool UploadSingleFile(HttpPostedFileBase file, string FilePath, out string FileAttach)
        {
            try
            {
                string fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                string fileSave = HttpContext.Current.Server.MapPath(FilePath + fileName);
                DirectoryInfo zDir = new DirectoryInfo(HttpContext.Current.Server.MapPath(FilePath));
                if (!zDir.Exists)
                {
                    zDir.Create();
                }
                else
                {
                    if (System.IO.File.Exists(fileSave))
                    {
                        System.IO.File.Delete(fileSave);
                    }
                }

                file.SaveAs(fileSave);

                FileAttach = FilePath + fileName;
                return true;
            }

            catch (Exception)
            {
                FileAttach = string.Empty;
                return false;
            }
        }

        public static string ArticleHeaderWebSite = "background: url('/FileUpload/184FDCA5-CCB4-41EC-BE98-85CD3537989B/ArticlePicture/bab23698-d1af-42b8-b36e-4b6a590040ff_z2385232356077_afd8b414faab356c2b5b5876d7b94f30.jpg');background-size: cover;background-repeat: no-repeat;";
        public static string PartnerNumber = ConfigurationManager.AppSettings["PartnerNumber"];
        public static string UploadPath = "/FileUpload/" + PartnerNumber;

        public static string DefaultImage = "/assets/img/avatar.jpg";
        public static string DefaultFileExcel = "/assets/img/EXL.png";
        public static string DefaultFileThumbnail = "/assets/img/unknow.png";
        public static string DefaultFileDoc = "/assets/img/DOC.png";
        public static string DefaultFilePDF = "/assets/img/PDF.png";

        public static string TableContractEmployee = "HRM_Contract";

        public static string ToStringComma(this IList<string> data)
        {
            return "'" + string.Join("', '", data) + "'";
        }
        public static List<string> ToListString(this string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<string>>(data);
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }
        //
        public static List<Product_Land_Model> SelectLand(string Paramater)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT *
FROM PDT_Product_Land 
WHERE RecordStatus != 99 
AND Class = 'TK'
AND PartnerNumber = @PartnerNumber 
AND ParentKey = '00000000-0000-0000-0000-000000000000'
AND ProductKey IN (" + Paramater + ") ORDER BY [Rank]";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Product_Land_Model> zList = new List<Product_Land_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Product_Land_Model()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        ParentKey = r["ParentKey"].ToString(),
                        Address = r["Address"].ToString(),
                        Rank = r["Rank"].ToInt()
                    });
                }
            }
            return zList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files"></param>
        /// <param name="FolderName">"Employee"</param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string StoreFilePost(HttpPostedFileBase[] files, string FolderName, string Key)
        {
            string zFilePath = "";
            string fileName = "";
            try
            {
                if (files.Length > 0)
                {
                    //Upload hình
                    foreach (HttpPostedFileBase file in files)
                    {
                        //Checking file is available to save.  
                        if (file != null && file.ContentLength > 0)
                        {
                            string strGui = Guid.NewGuid().ToString();
                            fileName = strGui + Path.GetExtension(file.FileName);

                            #region[Upload]   
                            zFilePath = UploadPath + "/" + FolderName + "/" + Key + "/";
                            string ServerPath = HttpContext.Current.Server.MapPath(zFilePath);
                            string zFileSave = Path.Combine(ServerPath, fileName);

                            // Check Foder
                            DirectoryInfo zDir = new DirectoryInfo(ServerPath);
                            if (!zDir.Exists)
                            {
                                zDir.Create();
                            }
                            else
                            {
                                if (File.Exists(zFileSave))
                                {
                                    File.Delete(zFileSave);
                                }
                            }

                            file.SaveAs(zFileSave);
                            #endregion
                        }
                    }
                }

                return zFilePath + fileName;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        /// <summary>
        /// return string split by ;
        /// </summary>
        /// <param name="files"></param>
        /// <param name="FolderName"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string StoreFilePostArray(HttpFileCollectionBase files, string FolderName, string Key)
        {
            string zReturnArray = "";
            string fileName = "";
            try
            {
                if (files.Count > 0)
                {
                    //Upload hình
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        //Checking file is available to save.  
                        if (file != null && file.ContentLength > 0)
                        {
                            string zFilePath = "";

                            string strGui = Guid.NewGuid().ToString();
                            fileName = strGui + Path.GetExtension(file.FileName);

                            #region[Upload]   
                            zFilePath = UploadPath + "/" + FolderName + "/" + Key + "/";
                            string ServerPath = HttpContext.Current.Server.MapPath(zFilePath);
                            string zFileSave = Path.Combine(ServerPath, fileName);

                            // Check Foder
                            DirectoryInfo zDir = new DirectoryInfo(ServerPath);
                            if (!zDir.Exists)
                            {
                                zDir.Create();
                            }
                            else
                            {
                                if (File.Exists(zFileSave))
                                {
                                    File.Delete(zFileSave);
                                }
                            }

                            file.SaveAs(zFileSave);
                            #endregion

                            zFilePath = zFilePath + fileName;
                            zReturnArray += zFilePath + ";";
                        }
                    }
                }

                return zReturnArray;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        public static double GetDouble(string s)
        {
            double result = Convert.ToDouble(s.Replace(",", ";").Replace(".", ",").Replace(";", "."));

            //char systemSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            //double result = 0;
            //try
            //{
            //    if (s != null)
            //    {
            //        if (!s.Contains(","))
            //        {
            //            result = double.Parse(s, CultureInfo.InvariantCulture);
            //        }
            //        else
            //        {
            //            result = Convert.ToDouble(s.Replace(".", systemSeparator.ToString()).Replace(",", systemSeparator.ToString()));
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    try
            //    {
            //        result = Convert.ToDouble(s);
            //    }
            //    catch
            //    {
            //        try
            //        {

            //        }
            //        catch
            //        {
            //            throw new Exception("Wrong string-to-double format");
            //        }
            //    }
            //}
            return result;
        }
        public static DataTable XMLToTable(string XMLData)
        {
            StringReader theReader = new StringReader(XMLData);
            DataSet theDataSet = new DataSet();
            theDataSet.ReadXml(theReader);
            return theDataSet.Tables[0];
        }
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (!seenKeys.Contains(keySelector(element)))
                {
                    seenKeys.Add(keySelector(element));
                    yield return element;
                }
            }
        }
        public static List<Select_Item> SelectData(int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT CategoryKey, CategoryName FROM GOB_Category WHERE RecordStatus <> 99 AND Type = @Type";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.NVarChar).Value = Type;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Select_Item> zList = new List<Select_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Select_Item()
                {
                    Key = r[0].ToString(),
                    Name = r[1].ToString()
                });
            }
            return zList;
        }
        public static List<Select_Item> SelectData(string TypeName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT CategoryKey, CategoryName FROM GOB_Category WHERE RecordStatus <> 99 AND UPPER(TypeName) = UPPER(@TypeName)";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = TypeName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Select_Item> zList = new List<Select_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Select_Item()
                {
                    Key = r[0].ToString(),
                    Name = r[1].ToString()
                });
            }
            return zList;
        }
        public static string AutoEmployeeID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_EmployeeID(@Prefix, @PartnerNumber)";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static void RunSQL(string SQL, out string Message)
        {
            string zResult = "";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = "";
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static string GetSQLString(string SQL, out string Message)
        {
            string zResult = "";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                var obj = zCommand.ExecuteScalar();
                if (obj != null)
                    zResult = obj.ToString();
                zCommand.Dispose();
                Message = "";
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string Calculate(string Math, string Employee, List<Payroll_Item> Data)
        {
            try
            {
                string MathCode = Generate_Math_Code(Math);
                if (MathCode != string.Empty)
                {
                    string[] temp = MathCode.Split(',');

                    double zGetMoney;
                    for (int i = 0; i < temp.Length; i++)
                    {
                        int n;
                        bool isNumeric = int.TryParse(temp[i], out n);
                        if (isNumeric)
                        {
                            string code = "";
                            if (n.ToString().Length <= 1)
                            {
                                code = String.Concat("0", n.ToString());
                            }

                            zGetMoney = Data.SingleOrDefault(d => d.ItemID == code && d.EmployeeKey == Employee).Amount.ToDouble();
                            temp[i] = Convert.ToString(zGetMoney);
                        }
                        else
                        {
                            if (temp[i].Contains("t") && temp[i].Contains("%"))
                            {
                                temp[i] = temp[i].Replace('t', ' ').Replace('%', ' ').Trim();
                                temp[i] = (temp[i].ToDouble() / 100).ToString();
                            }
                            else if (temp[i].Contains("t"))
                            {
                                temp[i] = temp[i].Replace('t', ' ').Trim();
                            }
                        }
                    }
                    string x = string.Join("", temp);
                    DataTable dt = new DataTable();
                    var result = dt.Compute(x, null);
                    double Money = 0;
                    bool isMoney = double.TryParse(result.ToString(), out Money);
                    if (!Double.IsNaN(Money))
                    {
                        return Money.ToString("n0");
                    }
                    else
                    {
                        return "0";
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private static string Generate_Math_Code(string Code)
        {
            string strtemp = Code;
            for (int i = strtemp.Length - 1; i >= 0; i--)
            {
                switch (strtemp[i])
                {
                    case '+':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case '-':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case 'x':
                    case '*':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case '/':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;
                    case '(':
                        //strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 1, ",");
                        break;
                    case ')':
                        strtemp = strtemp.Insert(i, ",");
                        //strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    default:
                        break;
                }
            }

            return strtemp;
        }
        public static Leave_Close_Model LeaveCloseEnd(string EmployeeKey, DateTime FromDate, DateTime ToDate, float Number, out string Message)
        {
            Leave_Close_Model zModel = new Leave_Close_Model();

            DataTable zTable = new DataTable();
            string SQL = @"TINHPHEPANBINH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = Number;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            if (zTable.Rows.Count > 0)
            {
                DataRow r = zTable.Rows[0];
                zModel.CloseYear = r["CloseYear"].ToString();
                zModel.EmployeeKey = r["EmployeeKey"].ToString();
                zModel.EmployeeID = r["EmployeeID"].ToString();
                zModel.EmployeeName = r["EmployeeName"].ToString();
                zModel.Description = r["Description"].ToString();
                zModel.CloseEnd = r["CloseEnd"].ToFloat();
                zModel.Incremental = r["Incremental"].ToFloat();
            }
            else
            {
                Message = "Không tìm thấy dữ liệu, vui lòng liên hệ IT !.";
            }
            return zModel;
        }
    }
    public class PivotTable
    {
        #region Variables

        private DataTable _DataTable;

        #endregion Variables

        #region Constructors

        public PivotTable(DataTable dataTable)
        {
            _DataTable = dataTable;
        }

        #endregion Constructors

        #region Properties

        public DataTable ResultTable
        {
            get { return _DataTable; }
        }
        #endregion Properties

        #region Private Methods

        private string[] FindValues(string xAxisField, string xAxisValue, string yAxisField, string yAxisValue, string[] zAxisFields)
        {
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis++;
            }

            string[] zAxisValues = new string[zAxis];
            //set default values
            for (int i = 0; i <= zAxisValues.GetUpperBound(0); i++)
            {
                zAxisValues[i] = "0";
            }

            foreach (DataRow row in _DataTable.Rows)
            {
                if (Convert.ToString(row[xAxisField]) == xAxisValue && Convert.ToString(row[yAxisField]) == yAxisValue)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        zAxisValues[z] = Convert.ToString(Convert.ToDouble(row[zAxisFields[z]]) + Convert.ToDouble(zAxisValues[z]));
                    }
                    //   break; // If you are sure that you don't have duplicated row of data, uncomment to gain performance
                }
            }

            return zAxisValues;
        }

        private string FindValue(string xAxisField, string xAxisValue, string yAxisField, string yAxisValue, string zAxisField)
        {
            string zAxisValue = "";

            foreach (DataRow row in _DataTable.Rows)
            {
                if (Convert.ToString(row[xAxisField]) == xAxisValue && Convert.ToString(row[yAxisField]) == yAxisValue)
                {
                    zAxisValue = Convert.ToString(row[zAxisField]);
                    break;
                }
            }

            return zAxisValue;
        }
        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Creates an advanced 3D Pivot table.
        /// </summary>
        /// <param name="xAxisField">The main heading at the top of the report.</param>
        /// <param name="yAxisField">The heading on the left of the report.</param>
        /// <param name="zAxisFields">The sub heading at the top of the report.</param>
        /// <param name="mainColumnName">Name of the column in xAxis.</param>
        /// <param name="columnTotalName">Name of the column with the totals.</param>
        /// <param name="rowTotalName">Name of the row with the totals.</param>
        /// <param name="zAxisFieldsNames">Name of the columns in the zAxis.</param>
        /// <returns>HtmlTable Control.</returns>
        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        /// <summary>
        /// Creates an advanced 3D Pivot table.
        /// </summary>
        /// <param name="xAxisField">The main heading at the top of the report.</param>
        /// <param name="yAxisField">The heading on the left of the report.</param>
        /// <param name="zAxisFields">The sub heading at the top of the report.</param>
        /// <param name="mainColumnName">Name of the column in xAxis.</param>
        /// <param name="columnTotalName">Name of the column with the totals.</param>
        /// <param name="rowTotalName">Name of the row with the totals.</param>
        /// <param name="zAxisFieldsNames">Name of the columns in the zAxis.</param>
        /// <param name="Sort">Name of the columns sort before add totalrow</param>
        /// <returns>HtmlTable Control.</returns>
        public DataTable Generate_NoSort(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            //DataView dv = table.AsDataView();
            //dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            //table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        public DataTable Generate_ASC(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    double temp = 0;
                    if (double.TryParse(matrix[col, row], out temp))
                    {

                    }
                    yTotals[col] += temp;
                    //yTotals[col] +=  Convert.ToDouble("0" + );
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    double temp = 0;
                    if (double.TryParse(matrix[z, y], out temp))
                    {

                    }
                    xTotals[zCount, y] += temp;
                    //xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            //for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            //{
            //    DataRow dataRow = table.NewRow();
            //    for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
            //    {
            //        if (z == 0)
            //        {
            //            dataRow[z] = Convert.ToString(yAxis[y]);
            //        }
            //        else
            //        {
            //            dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
            //        }
            //    }

            //    //append x-axis grand totals
            //    for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
            //    {
            //        dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

            //    }
            //    table.Rows.Add(dataRow);
            //}
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        double tem = 0;
                        if (double.TryParse((matrix[(z - 1), y]).ToString(), out tem))
                        {

                        }
                        dataRow[z] = tem;// Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    double tem = 0;
                    if (double.TryParse((xTotals[z - (zAxis * xAxis.Count), y]).ToString(), out tem))
                    {

                    }
                    dataRow[z + 1] = tem; //Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }

            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " ASC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    double tem = 0;
                    if (double.TryParse((yTotals[x - 1]).ToString(), out tem))
                    {

                    }
                    dataRowTotals[x] = tem;//Convert.ToString(yTotals[x - 1]);
                }
            }

            //append x-axis/y-axis totals
            for (int z = 0; z < zAxis; z++)
            {
                double tem = 0;
                if (double.TryParse((xTotals[z, xTotals.GetUpperBound(1)]).ToString(), out tem))
                {

                }
                dataRowTotals[table.Columns.Count - zAxis + z] = tem;//Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        public DataTable Generate(string xAxisField, string yAxisField, string[] zAxisFields, string mainColumnName, string columnTotalName, string rowTotalName, string[] zAxisFieldsNames, int SortIndex, bool isTotal)
        {

            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the y-axis/z-axis fields
            int zAxis = zAxisFields.Length;
            if (zAxis < 1)
            {
                zAxis = 1;
            }

            string[,] matrix = new string[(xAxis.Count * zAxis), yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string[] zAxisValues = FindValues(xAxisField, Convert.ToString(xAxis[x])
                                                      , yAxisField, Convert.ToString(yAxis[y]), zAxisFields);
                    for (int z = 0; z < zAxis; z++) //loop thru z-axis fields
                    {
                        //sub columns
                        matrix[(((x + 1) * zAxis - zAxis) + z), y] = zAxisValues[z];
                    }
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[(xAxis.Count * zAxis)];
            for (int col = 0; col < (xAxis.Count * zAxis); col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[,] xTotals = new double[zAxis, (yAxis.Count + 1)];
            for (int y = 0; y < yAxis.Count; y++) //loop thru the y-axis
            {
                int zCount = 0;
                for (int z = 0; z < (zAxis * xAxis.Count); z++) //loop thru the z-axis
                {
                    xTotals[zCount, y] += Convert.ToDouble("0" + matrix[z, y]);
                    if (zCount == (zAxis - 1))
                    {
                        zCount = 0;
                    }
                    else
                    {
                        zCount++;
                    }
                }
            }
            for (int xx = 0; xx < zAxis; xx++) //Grand Total
            {
                for (int xy = 0; xy < yAxis.Count; xy++)
                {
                    xTotals[xx, yAxis.Count] += xTotals[xx, xy];
                }
            }

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);
            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = Convert.ToString(xAxis[x] + " | " + zAxisFieldsNames[z]);
                        table.Columns.Add(column);
                    }
                }
                else
                {
                    for (int z = 0; z < zAxis; z++)
                    {
                        DataColumn column = new DataColumn();
                        column.ColumnName = columnTotalName + " | " + zAxisFieldsNames[z];
                        table.Columns.Add(column);
                    }
                }
            }


            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= (zAxis * xAxis.Count); z++) //loop thru z-axis + 1
                {
                    if (z == 0)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    else
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1), y]);
                    }
                }

                //append x-axis grand totals
                for (int z = zAxis * xAxis.Count; z < zAxis + (zAxis * xAxis.Count); z++)
                {
                    dataRow[z + 1] = Convert.ToString(xTotals[z - (zAxis * xAxis.Count), y]);

                }
                table.Rows.Add(dataRow);
            }


            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (zAxis * xAxis.Count); x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                else
                {
                    dataRowTotals[x] = Convert.ToString(yTotals[x - 1]);
                }
            }

            if (isTotal)
            {
                //append x-axis/y-axis totals
                for (int z = 0; z < zAxis; z++)
                {
                    dataRowTotals[table.Columns.Count - zAxis + z] = Convert.ToString(xTotals[z, xTotals.GetUpperBound(1)]);
                }
                table.Rows.Add(dataRowTotals);
            }
            return table;
        }

        /// <summary>
        /// Creates a simple 3D Pivot Table.
        /// </summary>
        /// <param name="xAxisField">The heading at the top of the table.</param>
        /// <param name="yAxisField">The heading to the left of the table.</param>
        /// <param name="zAxisField">The item value field.</param>
        /// <param name="mainColumnName">Title of the main column</param>
        /// <param name="columnTotalName">Title of the total column</param>
        /// <param name="rowTotalName">Title of the row column</param>
        /// <returns></returns>
        public DataTable Generate(string xAxisField, string yAxisField, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName)
        {
            return Generate(xAxisField, yAxisField, new string[0], new string[0], zAxisField, mainColumnName, columnTotalName, rowTotalName);
        }

        public DataTable Generate(string xAxisField, string yAxisField, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName, int SortIndex)
        {
            return Generate(xAxisField, yAxisField, new string[0], new string[0], zAxisField, mainColumnName, columnTotalName, rowTotalName, SortIndex);
        }

        /// <summary>
        /// Creates a simple 3D Pivot Table.
        /// </summary>
        /// <param name="xAxisField">The heading at the top of the table.</param>
        /// <param name="yAxisField">The heading to the left of the table.</param>
        /// <param name="yAxisInfoFields">Other columns that we want to show on the y axis.</param>
        /// <param name="yAxisInfoFieldsNames">Title of the additionnal columns on y axis.</param>
        /// <param name="zAxisField">The item value field.</param>
        /// <param name="mainColumnName">Title of the main column</param>
        /// <param name="columnTotalName">Title of the total column</param>
        /// <param name="rowTotalName">Title of the row column</param>
        /// <returns></returns>
        public DataTable Generate(string xAxisField, string yAxisField, string[] yAxisInfoFields, string[] yAxisInfoFieldsNames, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName)
        {
            //style table
            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the x-axis/y-axis fields
            string[,] matrix = new string[xAxis.Count, yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string zAxisValue = FindValue(xAxisField, Convert.ToString(xAxis[x])
                                                  , yAxisField, Convert.ToString(yAxis[y]), zAxisField);



                    matrix[x, y] = zAxisValue;
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[xAxis.Count];
            for (int col = 0; col < xAxis.Count; col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    if (matrix[col, row] != "")
                    {
                        yTotals[col] += Convert.ToDouble(matrix[col, row]);
                    }
                    //yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[] xTotals = new double[(yAxis.Count + 1)];
            for (int row = 0; row < yAxis.Count; row++)
            {
                xTotals[row] = 0;
                for (int col = 0; col < xAxis.Count; col++)
                {
                    if (matrix[col, row] != "")
                    {
                        xTotals[row] += Convert.ToDouble(matrix[col, row]);
                    }
                    //xTotals[row] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }
            xTotals[xTotals.GetUpperBound(0)] = 0; //Grand Total
            for (int i = 0; i < xTotals.GetUpperBound(0); i++)
            {
                xTotals[xTotals.GetUpperBound(0)] += xTotals[i];
            }

            //Build HTML Table

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);

            foreach (string yAxisInfoFieldsName in yAxisInfoFieldsNames)
            {
                table.Columns.Add(yAxisInfoFieldsName);
            }

            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    DataColumn column = new DataColumn();
                    column.ColumnName = Convert.ToString(xAxis[x]);
                    table.Columns.Add(column);
                }
                else
                {
                    DataColumn column = new DataColumn(columnTotalName);
                    table.Columns.Add(column);
                }
            }

            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= xAxis.Count + yAxisInfoFieldsNames.Length; z++) //loop thru z-axis + 1
                {
                    if (z < yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(_DataTable.Rows[y][yAxisInfoFields[z]]);
                    }
                    if (z == yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    if (z > yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1 - yAxisInfoFieldsNames.Length), y]);
                    }
                }


                dataRow[xAxis.Count + yAxisInfoFieldsNames.Length + 1] = Convert.ToString(xTotals[y]);

                table.Rows.Add(dataRow);
            }

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (xAxis.Count + 1) + yAxisInfoFieldsNames.Length; x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                if (x > yAxisInfoFieldsNames.Length)
                {
                    if (x <= xAxis.Count + yAxisInfoFieldsNames.Length)
                    {
                        dataRowTotals[x] = Convert.ToString(yTotals[(x - 1 - yAxisInfoFieldsNames.Length)]);
                    }
                    else
                    {
                        dataRowTotals[x] = Convert.ToString(xTotals[xTotals.GetUpperBound(0)]);
                    }
                }
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }

        public DataTable Generate(string xAxisField, string yAxisField, string[] yAxisInfoFields, string[] yAxisInfoFieldsNames, string zAxisField, string mainColumnName, string columnTotalName, string rowTotalName, int SortIndex)
        {
            //style table
            /*
             * The x-axis is the main horizontal row.
             * The z-axis is the sub horizontal row.
             * The y-axis is the left vertical column.
             */

            //get distinct xAxisFields
            ArrayList xAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!xAxis.Contains(row[xAxisField]))
                {
                    xAxis.Add(row[xAxisField]);
                }
            }

            //get distinct yAxisFields
            ArrayList yAxis = new ArrayList();
            foreach (DataRow row in _DataTable.Rows)
            {
                if (!yAxis.Contains(row[yAxisField]))
                {
                    yAxis.Add(row[yAxisField]);
                }
            }

            //create a 2D array for the x-axis/y-axis fields
            string[,] matrix = new string[xAxis.Count, yAxis.Count];

            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis fields
            {
                //rows
                for (int x = 0; x < xAxis.Count; x++) //loop thru x-axis fields
                {
                    //main columns
                    //get the z-axis values
                    string zAxisValue = FindValue(xAxisField, Convert.ToString(xAxis[x])
                                                  , yAxisField, Convert.ToString(yAxis[y]), zAxisField);



                    matrix[x, y] = zAxisValue;
                }
            }

            //calculate totals for the y-axis
            double[] yTotals = new double[xAxis.Count];
            for (int col = 0; col < xAxis.Count; col++)
            {
                yTotals[col] = 0;
                for (int row = 0; row < yAxis.Count; row++)
                {
                    if (matrix[col, row] != "")
                    {
                        yTotals[col] += Convert.ToDouble(matrix[col, row]);
                    }
                    //yTotals[col] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }

            //calculate totals for the x-axis
            double[] xTotals = new double[(yAxis.Count + 1)];
            for (int row = 0; row < yAxis.Count; row++)
            {
                xTotals[row] = 0;
                for (int col = 0; col < xAxis.Count; col++)
                {
                    if (matrix[col, row] != "")
                    {
                        xTotals[row] += Convert.ToDouble(matrix[col, row]);
                    }
                    //xTotals[row] += Convert.ToDouble("0" + matrix[col, row]);
                }
            }
            xTotals[xTotals.GetUpperBound(0)] = 0; //Grand Total
            for (int i = 0; i < xTotals.GetUpperBound(0); i++)
            {
                xTotals[xTotals.GetUpperBound(0)] += xTotals[i];
            }

            //Build HTML Table

            //Build HTML Table
            //Append main row (x-axis)
            DataTable table = new DataTable();
            DataColumn columnYTitle = new DataColumn(mainColumnName);

            foreach (string yAxisInfoFieldsName in yAxisInfoFieldsNames)
            {
                table.Columns.Add(yAxisInfoFieldsName);
            }

            table.Columns.Add(columnYTitle);

            for (int x = 0; x <= xAxis.Count; x++) //loop thru x-axis + 1
            {
                if (x < xAxis.Count)
                {
                    DataColumn column = new DataColumn();
                    column.ColumnName = Convert.ToString(xAxis[x]);
                    table.Columns.Add(column);
                }
                else
                {
                    DataColumn column = new DataColumn(columnTotalName);
                    table.Columns.Add(column);
                }
            }

            //Append table items from matrix
            for (int y = 0; y < yAxis.Count; y++) //loop thru y-axis
            {
                DataRow dataRow = table.NewRow();
                for (int z = 0; z <= xAxis.Count + yAxisInfoFieldsNames.Length; z++) //loop thru z-axis + 1
                {
                    if (z < yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(_DataTable.Rows[y][yAxisInfoFields[z]]);
                    }
                    if (z == yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(yAxis[y]);
                    }
                    if (z > yAxisInfoFieldsNames.Length)
                    {
                        dataRow[z] = Convert.ToString(matrix[(z - 1 - yAxisInfoFieldsNames.Length), y]);
                    }
                }


                dataRow[xAxis.Count + yAxisInfoFieldsNames.Length + 1] = Convert.ToString(xTotals[y]);

                table.Rows.Add(dataRow);
            }

            DataView dv = table.AsDataView();
            dv.Sort = dv.Table.Columns[SortIndex].ColumnName + " DESC";
            table = dv.ToTable();

            //append y-axis totals
            DataRow dataRowTotals = table.NewRow();
            for (int x = 0; x <= (xAxis.Count + 1) + yAxisInfoFieldsNames.Length; x++)
            {
                if (x == 0)
                {
                    dataRowTotals[0] = rowTotalName;
                }
                if (x > yAxisInfoFieldsNames.Length)
                {
                    if (x <= xAxis.Count + yAxisInfoFieldsNames.Length)
                    {
                        dataRowTotals[x] = Convert.ToString(yTotals[(x - 1 - yAxisInfoFieldsNames.Length)]);
                    }
                    else
                    {
                        dataRowTotals[x] = Convert.ToString(xTotals[xTotals.GetUpperBound(0)]);
                    }
                }
            }
            table.Rows.Add(dataRowTotals);

            return table;
        }
        #endregion Public Methods
    }
    public class Select_Item
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}