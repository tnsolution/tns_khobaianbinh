﻿using ABG.Customer;

using System.Collections.Generic;
namespace ABG
{
    public class Contract_Object
    {
        public Contract_Model Contract { get; set; }
        public List<Contract_Land_Model> ListItem { get; set; }
        public List<Document_Model> ListFile { get; set; }
    }
}