﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Contract_Land_Info
    {

        public Contract_Land_Model Contract_Land = new Contract_Land_Model();

        #region [ Constructor Get Information ]
        public Contract_Land_Info()
        {
            Contract_Land.LandItemKey = Guid.NewGuid().ToString();
        }
        public Contract_Land_Info(string LandItemKey)
        {
            string zSQL = "SELECT * FROM CRM_Contract_Land WHERE LandItemKey = @LandItemKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LandItemKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Contract_Land.LandItemKey = zReader["LandItemKey"].ToString();
                    Contract_Land.ItemKey = zReader["ItemKey"].ToString();
                    Contract_Land.ItemID = zReader["ItemID"].ToString();
                    Contract_Land.ItemName = zReader["ItemName"].ToString();
                    if (zReader["UnitKey"] != DBNull.Value)
                    {
                        Contract_Land.UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    }

                    Contract_Land.UnitName = zReader["UnitName"].ToString();
                    if (zReader["StandardCost"] != DBNull.Value)
                    {
                        Contract_Land.StandardCost = double.Parse(zReader["StandardCost"].ToString());
                    }

                    if (zReader["Quantity"] != DBNull.Value)
                    {
                        Contract_Land.Quantity = float.Parse(zReader["Quantity"].ToString());
                    }

                    if (zReader["Area"] != DBNull.Value)
                    {
                        Contract_Land.Area = float.Parse(zReader["Area"].ToString());
                    }

                    if (zReader["Price"] != DBNull.Value)
                    {
                        Contract_Land.Price = float.Parse(zReader["Price"].ToString());
                    }

                    if (zReader["VAT"] != DBNull.Value)
                    {
                        Contract_Land.VAT = double.Parse(zReader["VAT"].ToString());
                    }

                    if (zReader["VAT_Percent"] != DBNull.Value)
                    {
                        Contract_Land.VAT_Percent = float.Parse(zReader["VAT_Percent"].ToString());
                    }

                    if (zReader["SubTotal"] != DBNull.Value)
                    {
                        Contract_Land.SubTotal = double.Parse(zReader["SubTotal"].ToString());
                    }

                    Contract_Land.ContractKey = zReader["ContractKey"].ToString();
                    Contract_Land.CurrencyID = zReader["CurrencyID"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                    {
                        Contract_Land.CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                    }

                    if (zReader["TotalCurrencyMain"] != DBNull.Value)
                    {
                        Contract_Land.TotalCurrencyMain = double.Parse(zReader["TotalCurrencyMain"].ToString());
                    }

                    Contract_Land.Style = zReader["Style"].ToString();
                    Contract_Land.Class = zReader["Class"].ToString();
                    Contract_Land.CodeLine = zReader["CodeLine"].ToString();
                    Contract_Land.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Contract_Land.Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Contract_Land.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Contract_Land.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Contract_Land.CreatedBy = zReader["CreatedBy"].ToString();
                    Contract_Land.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Contract_Land.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Contract_Land.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Contract_Land.ModifiedName = zReader["ModifiedName"].ToString();
                    Contract_Land.Message = "200 OK";
                }
                else
                {
                    Contract_Land.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Contract_Land ("
         + " ItemKey , ItemID , ItemName , UnitKey , UnitName , StandardCost , Quantity , Area , Price , VAT , VAT_Percent , SubTotal , ContractKey , CurrencyID , CurrencyRate , TotalCurrencyMain , Style , Class , CodeLine , PartnerNumber , Description , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ItemKey , @ItemID , @ItemName , @UnitKey , @UnitName , @StandardCost , @Quantity , @Area , @Price , @VAT , @VAT_Percent , @SubTotal , @ContractKey , @CurrencyID , @CurrencyRate , @TotalCurrencyMain , @Style , @Class , @CodeLine , @PartnerNumber , @Description , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract_Land.ItemKey != "" && Contract_Land.ItemKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ItemKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Contract_Land.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Land.ItemName;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Contract_Land.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Land.UnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Contract_Land.StandardCost;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Land.Quantity;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Contract_Land.Area;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = Contract_Land.Price;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = Contract_Land.VAT;
                zCommand.Parameters.Add("@VAT_Percent", SqlDbType.Float).Value = Contract_Land.VAT_Percent;
                zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = Contract_Land.SubTotal;
                if (Contract_Land.ContractKey != "" && Contract_Land.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CurrencyID", SqlDbType.Char).Value = Contract_Land.CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Contract_Land.CurrencyRate;
                zCommand.Parameters.Add("@TotalCurrencyMain", SqlDbType.Money).Value = Contract_Land.TotalCurrencyMain;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract_Land.CodeLine;
                if (Contract_Land.PartnerNumber != "" && Contract_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Land.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Land.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract_Land.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract_Land.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Land.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Contract_Land("
         + " LandItemKey , ItemKey , ItemID , ItemName , UnitKey , UnitName , StandardCost , Quantity , Area , Price , VAT , VAT_Percent , SubTotal , ContractKey , CurrencyID , CurrencyRate , TotalCurrencyMain , Style , Class , CodeLine , PartnerNumber , Description , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @LandItemKey , @ItemKey , @ItemID , @ItemName , @UnitKey , @UnitName , @StandardCost , @Quantity , @Area , @Price , @VAT , @VAT_Percent , @SubTotal , @ContractKey , @CurrencyID , @CurrencyRate , @TotalCurrencyMain , @Style , @Class , @CodeLine , @PartnerNumber , @Description , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract_Land.LandItemKey != "" && Contract_Land.LandItemKey.Length == 36)
                {
                    zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.LandItemKey);
                }
                else
                {
                    zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Contract_Land.ItemKey != "" && Contract_Land.ItemKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ItemKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Contract_Land.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Land.ItemName;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Contract_Land.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Land.UnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Contract_Land.StandardCost;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Land.Quantity;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Contract_Land.Area;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = Contract_Land.Price;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = Contract_Land.VAT;
                zCommand.Parameters.Add("@VAT_Percent", SqlDbType.Float).Value = Contract_Land.VAT_Percent;
                zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = Contract_Land.SubTotal;
                if (Contract_Land.ContractKey != "" && Contract_Land.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CurrencyID", SqlDbType.Char).Value = Contract_Land.CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Contract_Land.CurrencyRate;
                zCommand.Parameters.Add("@TotalCurrencyMain", SqlDbType.Money).Value = Contract_Land.TotalCurrencyMain;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract_Land.CodeLine;
                if (Contract_Land.PartnerNumber != "" && Contract_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Land.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Land.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract_Land.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract_Land.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Land.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE CRM_Contract_Land SET "
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " StandardCost = @StandardCost,"
                        + " Quantity = @Quantity,"
                        + " Area = @Area,"
                        + " Price = @Price,"
                        + " VAT = @VAT,"
                        + " VAT_Percent = @VAT_Percent,"
                        + " SubTotal = @SubTotal,"
                        + " ContractKey = @ContractKey,"
                        + " CurrencyID = @CurrencyID,"
                        + " CurrencyRate = @CurrencyRate,"
                        + " TotalCurrencyMain = @TotalCurrencyMain,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE LandItemKey = @LandItemKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract_Land.LandItemKey != "" && Contract_Land.LandItemKey.Length == 36)
                {
                    zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.LandItemKey);
                }
                else
                {
                    zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Contract_Land.ItemKey != "" && Contract_Land.ItemKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ItemKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Contract_Land.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Land.ItemName;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Contract_Land.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Land.UnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Contract_Land.StandardCost;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Land.Quantity;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Contract_Land.Area;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = Contract_Land.Price;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = Contract_Land.VAT;
                zCommand.Parameters.Add("@VAT_Percent", SqlDbType.Float).Value = Contract_Land.VAT_Percent;
                zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = Contract_Land.SubTotal;
                if (Contract_Land.ContractKey != "" && Contract_Land.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CurrencyID", SqlDbType.Char).Value = Contract_Land.CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Contract_Land.CurrencyRate;
                zCommand.Parameters.Add("@TotalCurrencyMain", SqlDbType.Money).Value = Contract_Land.TotalCurrencyMain;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract_Land.CodeLine;
                if (Contract_Land.PartnerNumber != "" && Contract_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Land.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Land.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_Contract_Land SET RecordStatus = 99 WHERE LandItemKey = @LandItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.LandItemKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Contract_Land WHERE LandItemKey = @LandItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@LandItemKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Land.LandItemKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Land.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
