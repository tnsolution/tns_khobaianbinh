﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Contract_Land_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Contract_Land WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Contract_Land_Model> List(string PartnerNumber, string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Contract_Land WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ContractKey =@ContractKey ORDER BY ItemName";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Land_Model> zList = new List<Contract_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Contract_Land_Model()
                    {
                        LandItemKey = r["LandItemKey"].ToString(),
                        ItemKey = r["ItemKey"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Area = r["Area"].ToFloat(),
                        Price = r["Price"].ToFloat(),
                        VAT_Percent = r["VAT_Percent"].ToFloat(),
                        TotalCurrencyMain = r["TotalCurrencyMain"].ToFloat(),
                        UnitName = r["UnitName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}
