﻿using ABG.Customer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace ABG
{
    public class Customer_Data
    {
        public static string CheckContractD(string PartnerNumber, string ContractID, string SubContract, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(ContractKey, '') FROM CRM_Contract WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber AND ContractID = @ContractID AND SubContract = @SubContract";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = SubContract;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                var obj = zCommand.ExecuteScalar();
                if (obj != null)
                {
                    zResult = obj.ToString();
                }
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            Message = zResult;
            return zResult;
        }

        public static string CheckContractD(string PartnerNumber, string ContractID, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ContractKey FROM CRM_Contract WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber AND ContractID = @ContractID AND LEN(SubContract) = 0";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = ContractID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                var obj = zCommand.ExecuteScalar();
                if (obj != null)
                {
                    zResult = obj.ToString();
                }

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            Message = zResult;
            return zResult;
        }

        public static List<Customer_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, dbo.HopDongChinh(CustomerKey) AS ContractID FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY ContractID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Customer_Model()
                    {
                        ContractID = r["ContractID"].ToString(),
                        CustomerKey = r["CustomerKey"].ToString(),
                        CustomerID = r["CustomerID"].ToString(),
                        FullName = r["FullName"].ToString(),
                        Aliases = r["Aliases"].ToString(),
                        JobTitle = r["JobTitle"].ToString(),
                        CustomerTypeName = r["CustomerTypeName"].ToString(),
                        TaxNumber = r["TaxNumber"].ToString(),
                        Address = r["Address"].ToString(),
                        Phone = r["Phone"].ToString(),
                        Email = r["Email"].ToString(),
                        Note = r["Note"].ToString(),
                    });
                }
            }
            return zList;
        }
        public static List<Customer_Model> Search(string PartnerNumber, string Search)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
        SELECT * 
        FROM CRM_Customer 
        WHERE RecordStatus != 99 
        AND PartnerNumber = @PartnerNumber
        AND (CustomerID LIKE @Search 
        OR FullName LIKE @Search 
        OR TaxNumber LIKE @Search)
        ORDER BY FullName";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Customer_Model()
                    {
                        CustomerKey = r["CustomerKey"].ToString(),
                        CustomerID = r["CustomerID"].ToString(),
                        FullName = r["FullName"].ToString(),
                        Aliases = r["Aliases"].ToString(),
                        JobTitle = r["JobTitle"].ToString(),
                        CustomerTypeName = r["CustomerTypeName"].ToString(),
                        TaxNumber = r["TaxNumber"].ToString(),
                        Address = r["Address"].ToString(),
                        Phone = r["Phone"].ToString(),
                        Email = r["Email"].ToString(),
                        Note = r["Note"].ToString(),
                    });
                }
            }
            return zList;
        }

        public static List<Contract_Object> ListContract(string PartnerNumber, string CustomerKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*
FROM CRM_Contract A
WHERE A.PartnerNumber = @PartnerNumber
AND A.RecordStatus <> 99
AND A.BuyerKey = @BuyerKey
ORDER BY A.ContractID, A.SubContract, A.FromDate
";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@BuyerKey", SqlDbType.NVarChar).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Object> zList = new List<Contract_Object>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zFromDate = DateTime.MinValue;
                    DateTime.TryParse(r["FromDate"].ToString(), out zFromDate);

                    DateTime zToDate = DateTime.MinValue;
                    DateTime.TryParse(r["ToDate"].ToString(), out zToDate);

                    DateTime zDate = DateTime.MinValue;
                    DateTime.TryParse(r["DateLiquidation"].ToString(), out zDate);

                    DateTime zDateSign = DateTime.MinValue;
                    DateTime.TryParse(r["DateSign"].ToString(), out zDate);

                    Contract_Model zModel = new Contract_Model();
                    zModel.ContractKey = r["ContractKey"].ToString();
                    zModel.ContractID = r["ContractID"].ToString();
                    zModel.SubContract = r["SubContract"].ToString();
                    zModel.Purpose = r["Purpose"].ToString();
                    zModel.Description = r["Description"].ToString();
                    zModel.DateSign = zDateSign;
                    zModel.FromDate = zFromDate;
                    zModel.ToDate = zToDate;
                    zModel.DateLiquidation = zDate;
                    zModel.ReasonLiquidation = r["ReasonLiquidation"].ToString();
                    zModel.Liquidated = r["Liquidated"].ToInt();
                    Contract_Object zObj = new Contract_Object();
                    zObj.Contract = zModel;
                    zObj.ListItem = ListContractItem(PartnerNumber, r["ContractKey"].ToString());

                    zList.Add(zObj);
                }
            }
            return zList;
        }
        private static List<Contract_Land_Model> ListContractItem(string PartnerNumber, string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Contract_Land WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ContractKey = @ContractKey ORDER BY ItemID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Land_Model> zList = new List<Contract_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Contract_Land_Model()
                    {
                        LandItemKey = r["LandItemKey"].ToString(),
                        ItemKey = r["ItemKey"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Area = r["Area"].ToFloat(),
                        Price = r["Price"].ToFloat(),
                        VAT_Percent = r["VAT_Percent"].ToFloat(),
                        TotalCurrencyMain = r["TotalCurrencyMain"].ToFloat(),
                        UnitName = r["UnitName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }                

        public static DataTable HistoryContract(string PartnerNumber, string ProductKey)
        {
            string zSQL = @"SELECT 
	A.ProductID, A.ProductName, A.StandardUnitName, 
    B.Area, B.Price, B.VAT, B.SubTotal, 
    C.BuyerName, C.DateSign, C.FromDate, C.ToDate, 
    C.Purpose, C.ContractID, C.SubContract,
    C.Liquidated, C.DateLiquidation, C.ReasonLiquidation
	FROM PDT_Product_Land A 
	LEFT JOIN CRM_Contract_Land B ON A.ProductKey = B.ItemKey
	LEFT JOIN CRM_Contract C ON C.ContractKey= B.ContractKey
	WHERE 
	A.PartnerNumber = @PartnerNumber
	AND A.ProductKey = @ProductKey
	AND A.RecordStatus <> 99 
	AND B.RecordStatus <> 99
    AND C.RecordStatus <> 99
	ORDER BY C.ToDate";

            DataTable zTable = new DataTable();
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }

        public static List<Customer_Category_Model> ListCategory(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Customer_Category WHERE PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Customer_Category_Model> zList = new List<Customer_Category_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Customer_Category_Model()
                    {
                        CategoryKey = r["CategoryKey"].ToInt(),
                        CategoryName = r["CategoryNameVN"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}