﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG.Customer
{
    public class Contract_Info
    {

        public Contract_Model Contract = new Contract_Model();

        #region [ Constructor Get Information ]
        public Contract_Info()
        {
            Contract.ContractKey = Guid.NewGuid().ToString();
        }
        public Contract_Info(string ContractKey)
        {
            string zSQL = "SELECT A.*, B.ProductName FROM CRM_Contract A LEFT JOIN PDT_Product_Land B ON A.RegionKey  = CAST(B.ProductKey AS NVARCHAR(50))  WHERE A.ContractKey = @ContractKey AND A.RecordStatus != 99 AND B.RecordStatus != 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ContractKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Contract.ContractKey = zReader["ContractKey"].ToString();
                    Contract.ContractID = zReader["ContractID"].ToString();
                    Contract.SubContract = zReader["SubContract"].ToString();
                    Contract.ContractName = zReader["ContractName"].ToString();
                    if (zReader["ContractDate"] != DBNull.Value)
                    {
                        Contract.ContractDate = (DateTime)zReader["ContractDate"];
                    }

                    Contract.BuyerKey = zReader["BuyerKey"].ToString();
                    Contract.BuyerID = zReader["BuyerID"].ToString();
                    Contract.BuyerName = zReader["BuyerName"].ToString();
                    Contract.BuyerAddress = zReader["BuyerAddress"].ToString();
                    Contract.BuyerPhone = zReader["BuyerPhone"].ToString();
                    Contract.BuyerTaxCode = zReader["BuyerTaxCode"].ToString();
                    Contract.BuyerBankCode = zReader["BuyerBankCode"].ToString();
                    Contract.BuyerAddressBank = zReader["BuyerAddressBank"].ToString();
                    Contract.BuyerRepresent = zReader["BuyerRepresent"].ToString();
                    Contract.BuyerPosition = zReader["BuyerPosition"].ToString();
                    Contract.SellerKey = zReader["SellerKey"].ToString();
                    Contract.SellerID = zReader["SellerID"].ToString();
                    Contract.SellerName = zReader["SellerName"].ToString();
                    Contract.SellerAddress = zReader["SellerAddress"].ToString();
                    Contract.SellerPhone = zReader["SellerPhone"].ToString();
                    Contract.SellerTaxCode = zReader["SellerTaxCode"].ToString();
                    Contract.SellerBankCode = zReader["SellerBankCode"].ToString();
                    Contract.SellerAddressBank = zReader["SellerAddressBank"].ToString();
                    Contract.SellerRepresent = zReader["SellerRepresent"].ToString();
                    Contract.SellerPosition = zReader["SellerPosition"].ToString();
                    Contract.Parent = zReader["Parent"].ToString();
                    Contract.Purpose = zReader["Purpose"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Contract.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Contract.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["PercentIncrease"] != DBNull.Value)
                    {
                        Contract.PercentIncrease = int.Parse(zReader["PercentIncrease"].ToString());
                    }

                    Contract.DepositExt = zReader["DepositExt"].ToString();
                    if (zReader["DepositMoney"] != DBNull.Value)
                    {
                        Contract.DepositMoney = double.Parse(zReader["DepositMoney"].ToString());
                    }

                    Contract.PayExt = zReader["PayExt"].ToString();
                    Contract.PayNo = zReader["PayNo"].ToString();
                    if (zReader["WithinNo"] != DBNull.Value)
                    {
                        Contract.WithinNo = int.Parse(zReader["WithinNo"].ToString());
                    }

                    Contract.WithinExt = zReader["WithinExt"].ToString();
                    Contract.Currency = zReader["Currency"].ToString();
                    Contract.SpecialRequirements = zReader["SpecialRequirements"].ToString();
                    if (zReader["PowerFee"] != DBNull.Value)
                    {
                        Contract.PowerFee = double.Parse(zReader["PowerFee"].ToString());
                    }

                    if (zReader["WaterFee"] != DBNull.Value)
                    {
                        Contract.WaterFee = double.Parse(zReader["WaterFee"].ToString());
                    }

                    if (zReader["WashFee"] != DBNull.Value)
                    {
                        Contract.WashFee = double.Parse(zReader["WashFee"].ToString());
                    }

                    if (zReader["DateRenewal"] != DBNull.Value)
                    {
                        Contract.DateRenewal = int.Parse(zReader["DateRenewal"].ToString());
                    }

                    if (zReader["DateExpired"] != DBNull.Value)
                    {
                        Contract.DateExpired = int.Parse(zReader["DateExpired"].ToString());
                    }

                    if (zReader["DateTransfer"] != DBNull.Value)
                    {
                        Contract.DateTransfer = (DateTime)zReader["DateTransfer"];
                    }

                    if (zReader["DateSign"] != DBNull.Value)
                    {
                        Contract.DateSign = (DateTime)zReader["DateSign"];
                    }

                    if (zReader["IsDraft"] != DBNull.Value)
                    {
                        Contract.IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    }

                    if (zReader["Status"] != DBNull.Value)
                    {
                        Contract.Status = int.Parse(zReader["Status"].ToString());
                    }

                    Contract.Organization = zReader["Organization"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Contract.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    Contract.RegionKey = zReader["RegionKey"].ToString();
                    Contract.RegionName = zReader["ProductName"].ToString();

                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Contract.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Contract.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["ContractTypeKey"] != DBNull.Value)
                    {
                        Contract.ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                    }

                    Contract.ContractTypeName = zReader["ContractTypeName"].ToString();
                    if (zReader["PeriodPayKey"] != DBNull.Value)
                    {
                        Contract.PeriodPayKey = int.Parse(zReader["PeriodPayKey"].ToString());
                    }

                    Contract.PeriodPayName = zReader["PeriodPayName"].ToString();
                    if (zReader["ContractStatusKey"] != DBNull.Value)
                    {
                        Contract.ContractStatusKey = int.Parse(zReader["ContractStatusKey"].ToString());
                    }

                    Contract.ContractStatusName = zReader["ContractStatusName"].ToString();
                    if (zReader["SubTotal"] != DBNull.Value)
                    {
                        Contract.SubTotal = double.Parse(zReader["SubTotal"].ToString());
                    }

                    if (zReader["VAT"] != DBNull.Value)
                    {
                        Contract.VAT = double.Parse(zReader["VAT"].ToString());
                    }

                    if (zReader["Total"] != DBNull.Value)
                    {
                        Contract.Total = double.Parse(zReader["Total"].ToString());
                    }

                    Contract.CurrencyID = zReader["CurrencyID"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                    {
                        Contract.CurrencyRate = double.Parse(zReader["CurrencyRate"].ToString());
                    }

                    if (zReader["TotalCurrencyMain"] != DBNull.Value)
                    {
                        Contract.TotalCurrencyMain = double.Parse(zReader["TotalCurrencyMain"].ToString());
                    }

                    if (zReader["StatusContract"] != DBNull.Value)
                    {
                        Contract.StatusContract = int.Parse(zReader["StatusContract"].ToString());
                    }

                    Contract.Description = zReader["Description"].ToString();
                    Contract.FileAttack = zReader["FileAttack"].ToString();
                    Contract.FilePath = zReader["FilePath"].ToString();
                    if (zReader["FileUploadDate"] != DBNull.Value)
                    {
                        Contract.FileUploadDate = (DateTime)zReader["FileUploadDate"];
                    }

                    if (zReader["Liquidated"] != DBNull.Value)
                    {
                        Contract.Liquidated = int.Parse(zReader["Liquidated"].ToString());
                    }

                    if (zReader["DateLiquidation"] != DBNull.Value)
                    {
                        Contract.DateLiquidation = (DateTime)zReader["DateLiquidation"];
                    }

                    Contract.FileLiquidationName = zReader["FileLiquidationName"].ToString();
                    Contract.FileLiquidationPath = zReader["FileLiquidationPath"].ToString();
                    if (zReader["FileLiquidationUploadDate"] != DBNull.Value)
                    {
                        Contract.FileLiquidationUploadDate = (DateTime)zReader["FileLiquidationUploadDate"];
                    }

                    Contract.EmployeeBusinessKey = zReader["EmployeeBusinessKey"].ToString();
                    Contract.EmployeeBusinessName = zReader["EmployeeBusinessName"].ToString();
                    Contract.Presenter = zReader["Presenter"].ToString();
                    if (zReader["RoseMoney"] != DBNull.Value)
                    {
                        Contract.RoseMoney = double.Parse(zReader["RoseMoney"].ToString());
                    }

                    Contract.ReasonLiquidation = zReader["ReasonLiquidation"].ToString();
                    Contract.Drafts = zReader["Drafts"].ToString();
                    Contract.Note = zReader["Note"].ToString();
                    Contract.Style = zReader["Style"].ToString();
                    Contract.Class = zReader["Class"].ToString();
                    Contract.CodeLine = zReader["CodeLine"].ToString();
                    Contract.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Contract.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Contract.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Contract.CreatedBy = zReader["CreatedBy"].ToString();
                    Contract.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Contract.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Contract.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Contract.ModifiedName = zReader["ModifiedName"].ToString();
                    Contract.Message = "200 OK";
                }
                else
                {
                    Contract.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Contract ("
         + " ContractID , SubContract , ContractName , ContractDate , BuyerKey , BuyerID , BuyerName , BuyerAddress , BuyerPhone , BuyerTaxCode , BuyerBankCode , BuyerAddressBank , BuyerRepresent , BuyerPosition , SellerKey , SellerID , SellerName , SellerAddress , SellerPhone , SellerTaxCode , SellerBankCode , SellerAddressBank , SellerRepresent , SellerPosition , Parent , Purpose , FromDate , ToDate , PercentIncrease , DepositExt , DepositMoney , PayExt , PayNo , WithinNo , WithinExt , Currency , SpecialRequirements , PowerFee , WaterFee , WashFee , DateRenewal , DateExpired , DateTransfer , DateSign , IsDraft , Status , Organization , Rank , RegionKey , CategoryKey , CategoryName , ContractTypeKey , ContractTypeName , PeriodPayKey , PeriodPayName , ContractStatusKey , ContractStatusName , SubTotal , VAT , Total , CurrencyID , CurrencyRate , TotalCurrencyMain , StatusContract , Description , FileAttack , FilePath , FileUploadDate , Liquidated , DateLiquidation , FileLiquidationName , FileLiquidationPath , FileLiquidationUploadDate , EmployeeBusinessKey , EmployeeBusinessName , Presenter , RoseMoney , ReasonLiquidation , Drafts , Note , Style , Class , CodeLine , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractID , @SubContract , @ContractName , @ContractDate , @BuyerKey , @BuyerID , @BuyerName , @BuyerAddress , @BuyerPhone , @BuyerTaxCode , @BuyerBankCode , @BuyerAddressBank , @BuyerRepresent , @BuyerPosition , @SellerKey , @SellerID , @SellerName , @SellerAddress , @SellerPhone , @SellerTaxCode , @SellerBankCode , @SellerAddressBank , @SellerRepresent , @SellerPosition , @Parent , @Purpose , @FromDate , @ToDate , @PercentIncrease , @DepositExt , @DepositMoney , @PayExt , @PayNo , @WithinNo , @WithinExt , @Currency , @SpecialRequirements , @PowerFee , @WaterFee , @WashFee , @DateRenewal , @DateExpired , @DateTransfer , @DateSign , @IsDraft , @Status , @Organization , @Rank , @RegionKey , @CategoryKey , @CategoryName , @ContractTypeKey , @ContractTypeName , @PeriodPayKey , @PeriodPayName , @ContractStatusKey , @ContractStatusName , @SubTotal , @VAT , @Total , @CurrencyID , @CurrencyRate , @TotalCurrencyMain , @StatusContract , @Description , @FileAttack , @FilePath , @FileUploadDate , @Liquidated , @DateLiquidation , @FileLiquidationName , @FileLiquidationPath , @FileLiquidationUploadDate , @EmployeeBusinessKey , @EmployeeBusinessName , @Presenter , @RoseMoney , @ReasonLiquidation , @Drafts , @Note , @Style , @Class , @CodeLine , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                if (Contract.ContractDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = Contract.ContractDate;
                }

                if (Contract.BuyerKey != "" && Contract.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerID", SqlDbType.NVarChar).Value = Contract.BuyerID;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Contract.BuyerName;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Contract.BuyerAddress;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Contract.BuyerPhone;
                zCommand.Parameters.Add("@BuyerTaxCode", SqlDbType.NVarChar).Value = Contract.BuyerTaxCode;
                zCommand.Parameters.Add("@BuyerBankCode", SqlDbType.NVarChar).Value = Contract.BuyerBankCode;
                zCommand.Parameters.Add("@BuyerAddressBank", SqlDbType.NVarChar).Value = Contract.BuyerAddressBank;
                zCommand.Parameters.Add("@BuyerRepresent", SqlDbType.NVarChar).Value = Contract.BuyerRepresent;
                zCommand.Parameters.Add("@BuyerPosition", SqlDbType.NVarChar).Value = Contract.BuyerPosition;
                if (Contract.SellerKey != "" && Contract.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerID", SqlDbType.NVarChar).Value = Contract.SellerID;
                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Contract.SellerName;
                zCommand.Parameters.Add("@SellerAddress", SqlDbType.NVarChar).Value = Contract.SellerAddress;
                zCommand.Parameters.Add("@SellerPhone", SqlDbType.NVarChar).Value = Contract.SellerPhone;
                zCommand.Parameters.Add("@SellerTaxCode", SqlDbType.NVarChar).Value = Contract.SellerTaxCode;
                zCommand.Parameters.Add("@SellerBankCode", SqlDbType.NVarChar).Value = Contract.SellerBankCode;
                zCommand.Parameters.Add("@SellerAddressBank", SqlDbType.NVarChar).Value = Contract.SellerAddressBank;
                zCommand.Parameters.Add("@SellerRepresent", SqlDbType.NVarChar).Value = Contract.SellerRepresent;
                zCommand.Parameters.Add("@SellerPosition", SqlDbType.NVarChar).Value = Contract.SellerPosition;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                zCommand.Parameters.Add("@Purpose", SqlDbType.NVarChar).Value = Contract.Purpose;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract.ToDate;
                }

                zCommand.Parameters.Add("@PercentIncrease", SqlDbType.Int).Value = Contract.PercentIncrease;
                zCommand.Parameters.Add("@DepositExt", SqlDbType.NVarChar).Value = Contract.DepositExt;
                zCommand.Parameters.Add("@DepositMoney", SqlDbType.Money).Value = Contract.DepositMoney;
                zCommand.Parameters.Add("@PayExt", SqlDbType.NVarChar).Value = Contract.PayExt;
                zCommand.Parameters.Add("@PayNo", SqlDbType.NVarChar).Value = Contract.PayNo;
                zCommand.Parameters.Add("@WithinNo", SqlDbType.Int).Value = Contract.WithinNo;
                zCommand.Parameters.Add("@WithinExt", SqlDbType.NVarChar).Value = Contract.WithinExt;
                zCommand.Parameters.Add("@Currency", SqlDbType.NVarChar).Value = Contract.Currency;
                zCommand.Parameters.Add("@SpecialRequirements", SqlDbType.NVarChar).Value = Contract.SpecialRequirements;
                zCommand.Parameters.Add("@PowerFee", SqlDbType.Money).Value = Contract.PowerFee;
                zCommand.Parameters.Add("@WaterFee", SqlDbType.Money).Value = Contract.WaterFee;
                zCommand.Parameters.Add("@WashFee", SqlDbType.Money).Value = Contract.WashFee;
                zCommand.Parameters.Add("@DateRenewal", SqlDbType.Int).Value = Contract.DateRenewal;
                zCommand.Parameters.Add("@DateExpired", SqlDbType.Int).Value = Contract.DateExpired;
                if (Contract.DateTransfer == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = Contract.DateTransfer;
                }

                if (Contract.DateSign == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = Contract.DateSign;
                }

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = Contract.IsDraft;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Contract.Status;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Contract.Organization;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Contract.Rank;
                zCommand.Parameters.Add("@RegionKey", SqlDbType.NVarChar).Value = Contract.RegionKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Contract.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Contract.CategoryName;
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = Contract.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Contract.ContractTypeName;
                zCommand.Parameters.Add("@PeriodPayKey", SqlDbType.Int).Value = Contract.PeriodPayKey;
                zCommand.Parameters.Add("@PeriodPayName", SqlDbType.NVarChar).Value = Contract.PeriodPayName;
                zCommand.Parameters.Add("@ContractStatusKey", SqlDbType.Int).Value = Contract.ContractStatusKey;
                zCommand.Parameters.Add("@ContractStatusName", SqlDbType.NVarChar).Value = Contract.ContractStatusName;
                zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = Contract.SubTotal;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = Contract.VAT;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Contract.Total;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.Char).Value = Contract.CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Contract.CurrencyRate;
                zCommand.Parameters.Add("@TotalCurrencyMain", SqlDbType.Money).Value = Contract.TotalCurrencyMain;
                zCommand.Parameters.Add("@StatusContract", SqlDbType.Int).Value = Contract.StatusContract;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = Contract.Description;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Contract.FileAttack;
                zCommand.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = Contract.FilePath;
                if (Contract.FileUploadDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FileUploadDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FileUploadDate", SqlDbType.DateTime).Value = Contract.FileUploadDate;
                }

                zCommand.Parameters.Add("@Liquidated", SqlDbType.Int).Value = Contract.Liquidated;
                if (Contract.DateLiquidation == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = Contract.DateLiquidation;
                }

                zCommand.Parameters.Add("@FileLiquidationName", SqlDbType.NVarChar).Value = Contract.FileLiquidationName;
                zCommand.Parameters.Add("@FileLiquidationPath", SqlDbType.NVarChar).Value = Contract.FileLiquidationPath;
                if (Contract.FileLiquidationUploadDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FileLiquidationUploadDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FileLiquidationUploadDate", SqlDbType.DateTime).Value = Contract.FileLiquidationUploadDate;
                }

                if (Contract.EmployeeBusinessKey != "" && Contract.EmployeeBusinessKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeBusinessKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.EmployeeBusinessKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeBusinessKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeBusinessName", SqlDbType.NVarChar).Value = Contract.EmployeeBusinessName;
                zCommand.Parameters.Add("@Presenter", SqlDbType.NVarChar).Value = Contract.Presenter;
                zCommand.Parameters.Add("@RoseMoney", SqlDbType.Money).Value = Contract.RoseMoney;
                zCommand.Parameters.Add("@ReasonLiquidation", SqlDbType.NVarChar).Value = Contract.ReasonLiquidation;
                zCommand.Parameters.Add("@Drafts", SqlDbType.NVarChar).Value = Contract.Drafts;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Contract.Note;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract.CodeLine;
                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Contract("
         + " ContractKey , ContractID , SubContract , ContractName , ContractDate , BuyerKey , BuyerID , BuyerName , BuyerAddress , BuyerPhone , BuyerTaxCode , BuyerBankCode , BuyerAddressBank , BuyerRepresent , BuyerPosition , SellerKey , SellerID , SellerName , SellerAddress , SellerPhone , SellerTaxCode , SellerBankCode , SellerAddressBank , SellerRepresent , SellerPosition , Parent , Purpose , FromDate , ToDate , PercentIncrease , DepositExt , DepositMoney , PayExt , PayNo , WithinNo , WithinExt , Currency , SpecialRequirements , PowerFee , WaterFee , WashFee , DateRenewal , DateExpired , DateTransfer , DateSign , IsDraft , Status , Organization , Rank , RegionKey , CategoryKey , CategoryName , ContractTypeKey , ContractTypeName , PeriodPayKey , PeriodPayName , ContractStatusKey , ContractStatusName , SubTotal , VAT , Total , CurrencyID , CurrencyRate , TotalCurrencyMain , StatusContract , Description , FileAttack , FilePath , FileUploadDate , Liquidated , DateLiquidation , FileLiquidationName , FileLiquidationPath , FileLiquidationUploadDate , EmployeeBusinessKey , EmployeeBusinessName , Presenter , RoseMoney , ReasonLiquidation , Drafts , Note , Style , Class , CodeLine , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractKey , @ContractID , @SubContract , @ContractName , @ContractDate , @BuyerKey , @BuyerID , @BuyerName , @BuyerAddress , @BuyerPhone , @BuyerTaxCode , @BuyerBankCode , @BuyerAddressBank , @BuyerRepresent , @BuyerPosition , @SellerKey , @SellerID , @SellerName , @SellerAddress , @SellerPhone , @SellerTaxCode , @SellerBankCode , @SellerAddressBank , @SellerRepresent , @SellerPosition , @Parent , @Purpose , @FromDate , @ToDate , @PercentIncrease , @DepositExt , @DepositMoney , @PayExt , @PayNo , @WithinNo , @WithinExt , @Currency , @SpecialRequirements , @PowerFee , @WaterFee , @WashFee , @DateRenewal , @DateExpired , @DateTransfer , @DateSign , @IsDraft , @Status , @Organization , @Rank , @RegionKey , @CategoryKey , @CategoryName , @ContractTypeKey , @ContractTypeName , @PeriodPayKey , @PeriodPayName , @ContractStatusKey , @ContractStatusName , @SubTotal , @VAT , @Total , @CurrencyID , @CurrencyRate , @TotalCurrencyMain , @StatusContract , @Description , @FileAttack , @FilePath , @FileUploadDate , @Liquidated , @DateLiquidation , @FileLiquidationName , @FileLiquidationPath , @FileLiquidationUploadDate , @EmployeeBusinessKey , @EmployeeBusinessName , @Presenter , @RoseMoney , @ReasonLiquidation , @Drafts , @Note , @Style , @Class , @CodeLine , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;                
                if (Contract.ContractKey != "" && Contract.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = new Guid(Contract.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                if (Contract.ContractDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = Contract.ContractDate;
                }

                if (Contract.BuyerKey != "" && Contract.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerID", SqlDbType.NVarChar).Value = Contract.BuyerID;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Contract.BuyerName;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Contract.BuyerAddress;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Contract.BuyerPhone;
                zCommand.Parameters.Add("@BuyerTaxCode", SqlDbType.NVarChar).Value = Contract.BuyerTaxCode;
                zCommand.Parameters.Add("@BuyerBankCode", SqlDbType.NVarChar).Value = Contract.BuyerBankCode;
                zCommand.Parameters.Add("@BuyerAddressBank", SqlDbType.NVarChar).Value = Contract.BuyerAddressBank;
                zCommand.Parameters.Add("@BuyerRepresent", SqlDbType.NVarChar).Value = Contract.BuyerRepresent;
                zCommand.Parameters.Add("@BuyerPosition", SqlDbType.NVarChar).Value = Contract.BuyerPosition;
                if (Contract.SellerKey != "" && Contract.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerID", SqlDbType.NVarChar).Value = Contract.SellerID;
                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Contract.SellerName;
                zCommand.Parameters.Add("@SellerAddress", SqlDbType.NVarChar).Value = Contract.SellerAddress;
                zCommand.Parameters.Add("@SellerPhone", SqlDbType.NVarChar).Value = Contract.SellerPhone;
                zCommand.Parameters.Add("@SellerTaxCode", SqlDbType.NVarChar).Value = Contract.SellerTaxCode;
                zCommand.Parameters.Add("@SellerBankCode", SqlDbType.NVarChar).Value = Contract.SellerBankCode;
                zCommand.Parameters.Add("@SellerAddressBank", SqlDbType.NVarChar).Value = Contract.SellerAddressBank;
                zCommand.Parameters.Add("@SellerRepresent", SqlDbType.NVarChar).Value = Contract.SellerRepresent;
                zCommand.Parameters.Add("@SellerPosition", SqlDbType.NVarChar).Value = Contract.SellerPosition;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                zCommand.Parameters.Add("@Purpose", SqlDbType.NVarChar).Value = Contract.Purpose;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract.ToDate;
                }

                zCommand.Parameters.Add("@PercentIncrease", SqlDbType.Int).Value = Contract.PercentIncrease;
                zCommand.Parameters.Add("@DepositExt", SqlDbType.NVarChar).Value = Contract.DepositExt;
                zCommand.Parameters.Add("@DepositMoney", SqlDbType.Money).Value = Contract.DepositMoney;
                zCommand.Parameters.Add("@PayExt", SqlDbType.NVarChar).Value = Contract.PayExt;
                zCommand.Parameters.Add("@PayNo", SqlDbType.NVarChar).Value = Contract.PayNo;
                zCommand.Parameters.Add("@WithinNo", SqlDbType.Int).Value = Contract.WithinNo;
                zCommand.Parameters.Add("@WithinExt", SqlDbType.NVarChar).Value = Contract.WithinExt;
                zCommand.Parameters.Add("@Currency", SqlDbType.NVarChar).Value = Contract.Currency;
                zCommand.Parameters.Add("@SpecialRequirements", SqlDbType.NVarChar).Value = Contract.SpecialRequirements;
                zCommand.Parameters.Add("@PowerFee", SqlDbType.Money).Value = Contract.PowerFee;
                zCommand.Parameters.Add("@WaterFee", SqlDbType.Money).Value = Contract.WaterFee;
                zCommand.Parameters.Add("@WashFee", SqlDbType.Money).Value = Contract.WashFee;
                zCommand.Parameters.Add("@DateRenewal", SqlDbType.Int).Value = Contract.DateRenewal;
                zCommand.Parameters.Add("@DateExpired", SqlDbType.Int).Value = Contract.DateExpired;
                if (Contract.DateTransfer == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = Contract.DateTransfer;
                }

                if (Contract.DateSign == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = Contract.DateSign;
                }

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = Contract.IsDraft;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Contract.Status;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Contract.Organization;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Contract.Rank;
                zCommand.Parameters.Add("@RegionKey", SqlDbType.NVarChar).Value = Contract.RegionKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Contract.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Contract.CategoryName;
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = Contract.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Contract.ContractTypeName;
                zCommand.Parameters.Add("@PeriodPayKey", SqlDbType.Int).Value = Contract.PeriodPayKey;
                zCommand.Parameters.Add("@PeriodPayName", SqlDbType.NVarChar).Value = Contract.PeriodPayName;
                zCommand.Parameters.Add("@ContractStatusKey", SqlDbType.Int).Value = Contract.ContractStatusKey;
                zCommand.Parameters.Add("@ContractStatusName", SqlDbType.NVarChar).Value = Contract.ContractStatusName;
                zCommand.Parameters.Add("@SubTotal", SqlDbType.Money).Value = Contract.SubTotal;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = Contract.VAT;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Contract.Total;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.Char).Value = Contract.CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Contract.CurrencyRate;
                zCommand.Parameters.Add("@TotalCurrencyMain", SqlDbType.Money).Value = Contract.TotalCurrencyMain;
                zCommand.Parameters.Add("@StatusContract", SqlDbType.Int).Value = Contract.StatusContract;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = Contract.Description;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Contract.FileAttack;
                zCommand.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = Contract.FilePath;
                if (Contract.FileUploadDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FileUploadDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FileUploadDate", SqlDbType.DateTime).Value = Contract.FileUploadDate;
                }

                zCommand.Parameters.Add("@Liquidated", SqlDbType.Int).Value = Contract.Liquidated;
                if (Contract.DateLiquidation == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = Contract.DateLiquidation;
                }

                zCommand.Parameters.Add("@FileLiquidationName", SqlDbType.NVarChar).Value = Contract.FileLiquidationName;
                zCommand.Parameters.Add("@FileLiquidationPath", SqlDbType.NVarChar).Value = Contract.FileLiquidationPath;
                if (Contract.FileLiquidationUploadDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FileLiquidationUploadDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FileLiquidationUploadDate", SqlDbType.DateTime).Value = Contract.FileLiquidationUploadDate;
                }

                if (Contract.EmployeeBusinessKey != "" && Contract.EmployeeBusinessKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeBusinessKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.EmployeeBusinessKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeBusinessKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeBusinessName", SqlDbType.NVarChar).Value = Contract.EmployeeBusinessName;
                zCommand.Parameters.Add("@Presenter", SqlDbType.NVarChar).Value = Contract.Presenter;
                zCommand.Parameters.Add("@RoseMoney", SqlDbType.Money).Value = Contract.RoseMoney;
                zCommand.Parameters.Add("@ReasonLiquidation", SqlDbType.NVarChar).Value = Contract.ReasonLiquidation;
                zCommand.Parameters.Add("@Drafts", SqlDbType.NVarChar).Value = Contract.Drafts;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Contract.Note;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract.CodeLine;
                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE CRM_Contract SET "
                        + " ContractID = @ContractID, Style = @Style,"
                        + " SubContract = @SubContract,"
                        + " ContractName = @ContractName,"
                        + " ContractDate = @ContractDate,"
                        + " BuyerKey = @BuyerKey,"
                        + " BuyerID = @BuyerID,"
                        + " BuyerName = @BuyerName,"
                        + " BuyerAddress = @BuyerAddress,"
                        + " BuyerPhone = @BuyerPhone,"
                        + " BuyerTaxCode = @BuyerTaxCode,"
                        + " BuyerBankCode = @BuyerBankCode,"
                        + " BuyerAddressBank = @BuyerAddressBank,"
                        + " BuyerRepresent = @BuyerRepresent,"
                        + " BuyerPosition = @BuyerPosition,"
                        + " SellerKey = @SellerKey,"
                        + " SellerID = @SellerID,"
                        + " SellerName = @SellerName,"
                        + " SellerAddress = @SellerAddress,"
                        + " SellerPhone = @SellerPhone,"
                        + " SellerTaxCode = @SellerTaxCode,"
                        + " SellerBankCode = @SellerBankCode,"
                        + " SellerAddressBank = @SellerAddressBank,"
                        + " SellerRepresent = @SellerRepresent,"
                        + " SellerPosition = @SellerPosition,"
                        + " Parent = @Parent,"
                        + " Purpose = @Purpose,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " DateSign = @DateSign,"
                        + " Organization = @Organization,"
                        + " RegionKey = @RegionKey,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ContractKey = @ContractKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                if (Contract.ContractKey != "" && Contract.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                if (Contract.ContractDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContractDate", SqlDbType.Date).Value = Contract.ContractDate;
                }

                if (Contract.BuyerKey != "" && Contract.BuyerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.BuyerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BuyerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BuyerID", SqlDbType.NVarChar).Value = Contract.BuyerID;
                zCommand.Parameters.Add("@BuyerName", SqlDbType.NVarChar).Value = Contract.BuyerName;
                zCommand.Parameters.Add("@BuyerAddress", SqlDbType.NVarChar).Value = Contract.BuyerAddress;
                zCommand.Parameters.Add("@BuyerPhone", SqlDbType.NVarChar).Value = Contract.BuyerPhone;
                zCommand.Parameters.Add("@BuyerTaxCode", SqlDbType.NVarChar).Value = Contract.BuyerTaxCode;
                zCommand.Parameters.Add("@BuyerBankCode", SqlDbType.NVarChar).Value = Contract.BuyerBankCode;
                zCommand.Parameters.Add("@BuyerAddressBank", SqlDbType.NVarChar).Value = Contract.BuyerAddressBank;
                zCommand.Parameters.Add("@BuyerRepresent", SqlDbType.NVarChar).Value = Contract.BuyerRepresent;
                zCommand.Parameters.Add("@BuyerPosition", SqlDbType.NVarChar).Value = Contract.BuyerPosition;
                if (Contract.SellerKey != "" && Contract.SellerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SellerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@SellerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SellerID", SqlDbType.NVarChar).Value = Contract.SellerID;
                zCommand.Parameters.Add("@SellerName", SqlDbType.NVarChar).Value = Contract.SellerName;
                zCommand.Parameters.Add("@SellerAddress", SqlDbType.NVarChar).Value = Contract.SellerAddress;
                zCommand.Parameters.Add("@SellerPhone", SqlDbType.NVarChar).Value = Contract.SellerPhone;
                zCommand.Parameters.Add("@SellerTaxCode", SqlDbType.NVarChar).Value = Contract.SellerTaxCode;
                zCommand.Parameters.Add("@SellerBankCode", SqlDbType.NVarChar).Value = Contract.SellerBankCode;
                zCommand.Parameters.Add("@SellerAddressBank", SqlDbType.NVarChar).Value = Contract.SellerAddressBank;
                zCommand.Parameters.Add("@SellerRepresent", SqlDbType.NVarChar).Value = Contract.SellerRepresent;
                zCommand.Parameters.Add("@SellerPosition", SqlDbType.NVarChar).Value = Contract.SellerPosition;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                zCommand.Parameters.Add("@Purpose", SqlDbType.NVarChar).Value = Contract.Purpose;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract.ToDate;
                }

                if (Contract.DateSign == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = Contract.DateSign;
                }

                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = Contract.Organization;
                zCommand.Parameters.Add("@RegionKey", SqlDbType.NVarChar).Value = Contract.RegionKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = Contract.Description;

                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_Contract SET RecordStatus = 99 WHERE ContractKey = @ContractKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Contract WHERE ContractKey = @ContractKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Terminate()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_Contract SET Liquidated = 1,"
                        + " DateLiquidation = @DateLiquidation,"
                        + " ReasonLiquidation = @ReasonLiquidation,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ContractKey = @ContractKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                zCommand.Parameters.Add("@ReasonLiquidation", SqlDbType.NVarChar).Value = Contract.ReasonLiquidation;
                if (Contract.DateLiquidation == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateLiquidation", SqlDbType.DateTime).Value = Contract.DateLiquidation;
                }
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
