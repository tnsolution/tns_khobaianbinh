﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG.Customer
{
    public class Contract_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Contract WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Contract_Model> ListContract(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  * FROM CRM_Contract WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            zSQL += "  ORDER BY DateWrite";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Model> zList = new List<Contract_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zDateSign = DateTime.MinValue;
                    DateTime.TryParse(r["DateSign"].ToString(), out zDateSign);


                    zList.Add(new Contract_Model()
                    {
                        ContractKey = r["ContractKey"].ToString(),
                        ContractID = r["ContractID"].ToString(),
                        SubContract = r["SubContract"].ToString(),
                        DateSign = zDateSign,
                        Description = r["Description"].ToString(),

                    });
                }
            }
            return zList;
        }        
    }
}
