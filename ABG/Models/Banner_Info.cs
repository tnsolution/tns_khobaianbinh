﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Banner_Info
    {

        public Banner_Model Banner = new Banner_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Banner_Info()
        {
        }
        public Banner_Info(int BannerKey)
        {
            string zSQL = "SELECT * FROM PDT_Banner WHERE BannerKey = @BannerKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = BannerKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["BannerKey"] != DBNull.Value)
                    {
                        Banner.BannerKey = int.Parse(zReader["BannerKey"].ToString());
                    }

                    Banner.Url = zReader["Url"].ToString();
                    Banner.Description = zReader["Description"].ToString();
                    Banner.Title = zReader["Title"].ToString();
                    Banner.Partnernumber = zReader["Partnernumber"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Banner.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["Publish"] != DBNull.Value)
                    {
                        Banner.Publish = (bool)zReader["Publish"];
                    }

                    if (zReader["ContentPublish"] != DBNull.Value)
                    {
                        Banner.ContentPublish = (bool)zReader["ContentPublish"];
                    }

                    Banner.ContentTemplate = zReader["ContentTemplate"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Banner.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Banner.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Banner.CreatedBy = zReader["CreatedBy"].ToString();
                    Banner.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Banner.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Banner.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Banner.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }


        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Banner ("
         + " Url , Description , Title , Partnernumber , Rank , Publish , ContentPublish , ContentTemplate , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @Url , @Description , @Title , @Partnernumber , @Rank , @Publish , @ContentPublish , @ContentTemplate , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = Banner.Url;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Banner.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Banner.Title;
                zCommand.Parameters.Add("@Partnernumber", SqlDbType.NVarChar).Value = Banner.Partnernumber;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Banner.Rank;
                if (Banner.Publish == null)
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Banner.Publish;
                }

                if (Banner.ContentPublish == null)
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = Banner.ContentPublish;
                }

                zCommand.Parameters.Add("@ContentTemplate", SqlDbType.NVarChar).Value = Banner.ContentTemplate;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Banner.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Banner.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Banner.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Banner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Banner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Banner("
         + " BannerKey , Url , Description , Title , Partnernumber , Rank , Publish , ContentPublish , ContentTemplate , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @BannerKey , @Url , @Description , @Title , @Partnernumber , @Rank , @Publish , @ContentPublish , @ContentTemplate , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = Banner.BannerKey;
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = Banner.Url;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Banner.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Banner.Title;
                zCommand.Parameters.Add("@Partnernumber", SqlDbType.NVarChar).Value = Banner.Partnernumber;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Banner.Rank;
                if (Banner.Publish == null)
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Banner.Publish;
                }

                if (Banner.ContentPublish == null)
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = Banner.ContentPublish;
                }

                zCommand.Parameters.Add("@ContentTemplate", SqlDbType.NVarChar).Value = Banner.ContentTemplate;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Banner.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Banner.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Banner.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Banner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Banner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Banner SET "
                        + " Url = @Url,"
                        + " Description = @Description,"
                        + " Title = @Title,"
                        + " Partnernumber = @Partnernumber,"
                        + " Rank = @Rank,"
                        + " Publish = @Publish,"
                        + " ContentPublish = @ContentPublish,"
                        + " ContentTemplate = @ContentTemplate,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE BannerKey = @BannerKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = Banner.BannerKey;
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = Banner.Url;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Banner.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Banner.Title;
                zCommand.Parameters.Add("@Partnernumber", SqlDbType.NVarChar).Value = Banner.Partnernumber;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Banner.Rank;
                if (Banner.Publish == null)
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Banner.Publish;
                }

                if (Banner.ContentPublish == null)
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ContentPublish", SqlDbType.Bit).Value = Banner.ContentPublish;
                }

                zCommand.Parameters.Add("@ContentTemplate", SqlDbType.NVarChar).Value = Banner.ContentTemplate;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Banner.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Banner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Banner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Banner SET RecordStatus = 99 WHERE BannerKey = @BannerKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = Banner.BannerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString().GetFirstLine();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Banner WHERE BannerKey = @BannerKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = Banner.BannerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string SetActivate(int BannerKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Banner SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE BannerKey = @BannerKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BannerKey", SqlDbType.Int).Value = BannerKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Banner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Banner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }

        #endregion
    }
}
