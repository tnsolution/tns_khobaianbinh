﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class DebitNote_Info
    {

        public DebitNote_Model DebitNote = new DebitNote_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public DebitNote_Info()
        {
        }
        public DebitNote_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_DebitNote WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        DebitNote.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    DebitNote.DebitNote = zReader["DebitNote"].ToString();
                    if (zReader["DebitDate"] != DBNull.Value)
                        DebitNote.DebitDate = (DateTime)zReader["DebitDate"];
                    if (zReader["AmountPaid"] != DBNull.Value)
                        DebitNote.AmountPaid = double.Parse(zReader["AmountPaid"].ToString());
                    if (zReader["AmountLeft"] != DBNull.Value)
                        DebitNote.AmountLeft = double.Parse(zReader["AmountLeft"].ToString());
                    DebitNote.CustomerKey = zReader["CustomerKey"].ToString();
                    DebitNote.ContractKey = zReader["ContractKey"].ToString();
                    DebitNote.ItemKey = zReader["ItemKey"].ToString();
                    DebitNote.ProductKey = zReader["ProductKey"].ToString();
                    DebitNote.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        DebitNote.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        DebitNote.CreatedOn = (DateTime)zReader["CreatedOn"];
                    DebitNote.CreatedBy = zReader["CreatedBy"].ToString();
                    DebitNote.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        DebitNote.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    DebitNote.ModifiedBy = zReader["ModifiedBy"].ToString();
                    DebitNote.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_DebitNote ("
         + " DebitNote , DebitDate , AmountPaid , AmountLeft , CustomerKey , ContractKey , ItemKey , ProductKey , PhotoPath, PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @DebitNote , @DebitDate , @AmountPaid , @AmountLeft , @CustomerKey , @ContractKey , @ItemKey , @ProductKey , @PhotoPath, @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DebitNote", SqlDbType.NVarChar).Value = DebitNote.DebitNote;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = DebitNote.PhotoPath;
                if (DebitNote.DebitDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DebitNote.DebitDate;
                zCommand.Parameters.Add("@AmountPaid", SqlDbType.Money).Value = DebitNote.AmountPaid;
                zCommand.Parameters.Add("@AmountLeft", SqlDbType.Money).Value = DebitNote.AmountLeft;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = DebitNote.CustomerKey;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = DebitNote.ContractKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = DebitNote.ItemKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = DebitNote.ProductKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = DebitNote.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = DebitNote.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = DebitNote.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = DebitNote.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = DebitNote.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = DebitNote.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_DebitNote("
         + " AutoKey , DebitNote , DebitDate , AmountPaid , AmountLeft , CustomerKey , ContractKey , ItemKey , ProductKey , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @DebitNote , @DebitDate , @AmountPaid , @AmountLeft , @CustomerKey , @ContractKey , @ItemKey , @ProductKey , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DebitNote.AutoKey;
                zCommand.Parameters.Add("@DebitNote", SqlDbType.NVarChar).Value = DebitNote.DebitNote;
                if (DebitNote.DebitDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DebitNote.DebitDate;
                zCommand.Parameters.Add("@AmountPaid", SqlDbType.Money).Value = DebitNote.AmountPaid;
                zCommand.Parameters.Add("@AmountLeft", SqlDbType.Money).Value = DebitNote.AmountLeft;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = DebitNote.CustomerKey;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = DebitNote.ContractKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = DebitNote.ItemKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = DebitNote.ProductKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = DebitNote.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = DebitNote.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = DebitNote.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = DebitNote.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = DebitNote.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = DebitNote.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_DebitNote SET "
                        + " DebitNote = @DebitNote,"
                        + " DebitDate = @DebitDate,"
                        + " AmountPaid = @AmountPaid,"
                        + " AmountLeft = @AmountLeft,"
                        + " CustomerKey = @CustomerKey,"
                        + " ContractKey = @ContractKey,"
                        + " ItemKey = @ItemKey,"
                        + " ProductKey = @ProductKey,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DebitNote.AutoKey;
                zCommand.Parameters.Add("@DebitNote", SqlDbType.NVarChar).Value = DebitNote.DebitNote;
                if (DebitNote.DebitDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DebitDate", SqlDbType.DateTime).Value = DebitNote.DebitDate;
                zCommand.Parameters.Add("@AmountPaid", SqlDbType.Money).Value = DebitNote.AmountPaid;
                zCommand.Parameters.Add("@AmountLeft", SqlDbType.Money).Value = DebitNote.AmountLeft;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = DebitNote.CustomerKey;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = DebitNote.ContractKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.NVarChar).Value = DebitNote.ItemKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = DebitNote.ProductKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = DebitNote.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = DebitNote.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = DebitNote.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = DebitNote.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_DebitNote SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DebitNote.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_DebitNote WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DebitNote.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
