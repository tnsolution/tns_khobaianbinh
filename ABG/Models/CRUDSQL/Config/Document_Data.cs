﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Document_Data
    {
        //TableKey KEY LIÊN KẾT Ở BẢNG KHÁC
        public static List<Document_Model> List(string PartnerNumber, string TableKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM GOB_Document WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND TableKey = @TableKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@TableKey", SqlDbType.NVarChar).Value = TableKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Document_Model> zList = new List<Document_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Document_Model() {
                    AutoKey = r["AutoKey"].ToInt(),
                    FilePath = r["FilePath"].ToString(),
                    FileName = r["FileName"].ToString(),
                    FileExt = r["FileExt"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                });
            }

            return zList;
        }
    }
}
