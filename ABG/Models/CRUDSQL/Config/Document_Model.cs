﻿using System;
namespace ABG
{
    public class Document_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _FilePath = "";
        private string _FileName = "";
        private string _FileExt = "";
        private string _Base64 = "";
        private string _Description = "";
        private string _Title = "";
        private int _Type = 0;
        private int _Category = 0;
        private int _Parent = 0;
        private int _Slug = 0;
        private string _TableJoin = "";
        private string _TableKey = "";
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public string FileExt
        {
            get { return _FileExt; }
            set { _FileExt = value; }
        }
        public string Base64
        {
            get { return _Base64; }
            set { _Base64 = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string TableJoin
        {
            get { return _TableJoin; }
            set { _TableJoin = value; }
        }
        public string TableKey
        {
            get { return _TableKey; }
            set { _TableKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
