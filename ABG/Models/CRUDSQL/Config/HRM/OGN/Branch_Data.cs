﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Branch_Data
    {
        public static List<Branch_Model> ListChoPhep(string branchKey, string year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, 
dbo.SoNhanVienLamViec(A.BranchKey) SLCB,
dbo.TongNSDungPhep(A.BranchKey, @year) SLCBDungPhep
FROM HRM_Branch A
WHERE 
RecordStatus != 99 
AND PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' 
ORDER BY [RANK]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@branchKey", SqlDbType.NVarChar).Value = branchKey;
                zCommand.Parameters.Add("@year", SqlDbType.NVarChar).Value = year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Branch_Model> zList = new List<Branch_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Branch_Model()
                    {
                        BranchKey = r["BranchKey"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        SLCB = r["SLCB"].ToString(),
                        SLCBDungPhep = r["SLCBDungPhep"].ToString(),
                    });
                }
            }
            return zList;
        }
        public static List<Branch_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Branch WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Branch_Model> zList = new List<Branch_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Branch_Model()
                    {
                        BranchKey = r["BranchKey"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        BranchID = r["BranchID"].ToString(),
                        Address = r["Address"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }
    }
}
