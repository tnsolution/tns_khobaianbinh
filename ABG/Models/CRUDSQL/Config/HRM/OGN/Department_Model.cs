﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Department_Model
{
#region [ Field Name ]
private string _DepartmentKey = "";
private string _DepartmentName = "";
private string _DepartmentID = "";
private string _Address = "";
private string _Parent = "";
private string _FullPath = "";
private int _Rank = 0;
private int _LevelOrganization = 0;
private int _CategoryKey = 0;
private string _CategoryName = "";
private string _BranchKey = "";
private string _BranchName = "";
private string _Description = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public string DepartmentKey
{
get { return _DepartmentKey; }
set { _DepartmentKey = value; }
}
public string DepartmentName
{
get { return _DepartmentName; }
set { _DepartmentName = value; }
}
public string DepartmentID
{
get { return _DepartmentID; }
set { _DepartmentID = value; }
}
public string Address
{
get { return _Address; }
set { _Address = value; }
}
public string Parent
{
get { return _Parent; }
set { _Parent = value; }
}
public string FullPath
{
get { return _FullPath; }
set { _FullPath = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public int LevelOrganization
{
get { return _LevelOrganization; }
set { _LevelOrganization = value; }
}
public int CategoryKey
{
get { return _CategoryKey; }
set { _CategoryKey = value; }
}
public string CategoryName
{
get { return _CategoryName; }
set { _CategoryName = value; }
}
public string BranchKey
{
get { return _BranchKey; }
set { _BranchKey = value; }
}
public string BranchName
{
get { return _BranchName; }
set { _BranchName = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
