﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Branch_Info
{
 
public  Branch_Model Branch = new Branch_Model();
 
#region [ Constructor Get Information ]
public Branch_Info()
{
Branch.BranchKey = Guid.NewGuid().ToString();
}
public Branch_Info(string BranchKey)
{
string zSQL = "SELECT * FROM HRM_Branch WHERE BranchKey = @BranchKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BranchKey);
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
Branch.BranchKey = zReader["BranchKey"].ToString();
Branch.BranchName = zReader["BranchName"].ToString();
Branch.BranchID = zReader["BranchID"].ToString();
Branch.Address = zReader["Address"].ToString();
Branch.Ward = zReader["Ward"].ToString();
Branch.District = zReader["District"].ToString();
Branch.Province = zReader["Province"].ToString();
Branch.Country = zReader["Country"].ToString();
Branch.Description = zReader["Description"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Branch.Rank = int.Parse(zReader["Rank"].ToString());
Branch.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Branch.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Branch.CreatedOn = (DateTime)zReader["CreatedOn"];
Branch.CreatedBy = zReader["CreatedBy"].ToString();
Branch.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Branch.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Branch.ModifiedBy = zReader["ModifiedBy"].ToString();
Branch.ModifiedName = zReader["ModifiedName"].ToString();
Branch.Message = "200 OK";
}
else
{
Branch.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Branch.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Branch (" 
 + " BranchName , BranchID , Address , Ward , District , Province , Country , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @BranchName , @BranchID , @Address , @Ward , @District , @Province , @Country , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Branch.BranchName;
zCommand.Parameters.Add("@BranchID", SqlDbType.NVarChar).Value = Branch.BranchID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Branch.Address;
zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = Branch.Ward;
zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = Branch.District;
zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = Branch.Province;
zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Branch.Country;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Branch.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Branch.Rank;
if(Branch.PartnerNumber != "" && Branch.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Branch.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Branch.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Branch.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Branch.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Branch.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Branch.Message = "201 Created";
}
catch (Exception Err)
{
Branch.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Branch(" 
 + " BranchKey , BranchName , BranchID , Address , Ward , District , Province , Country , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @BranchKey , @BranchName , @BranchID , @Address , @Ward , @District , @Province , @Country , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Branch.BranchKey != "" && Branch.BranchKey.Length == 36)
{
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.BranchKey);
}
else
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Branch.BranchName;
zCommand.Parameters.Add("@BranchID", SqlDbType.NVarChar).Value = Branch.BranchID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Branch.Address;
zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = Branch.Ward;
zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = Branch.District;
zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = Branch.Province;
zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Branch.Country;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Branch.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Branch.Rank;
if(Branch.PartnerNumber != "" && Branch.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Branch.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Branch.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Branch.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Branch.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Branch.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Branch.Message = "201 Created";
}
catch (Exception Err)
{
Branch.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_Branch SET " 
            + " BranchName = @BranchName,"
            + " BranchID = @BranchID,"
            + " Address = @Address,"
            + " Ward = @Ward,"
            + " District = @District,"
            + " Province = @Province,"
            + " Country = @Country,"
            + " Description = @Description,"
            + " Rank = @Rank,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE BranchKey = @BranchKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Branch.BranchKey != "" && Branch.BranchKey.Length == 36)
{
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.BranchKey);
}
else
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Branch.BranchName;
zCommand.Parameters.Add("@BranchID", SqlDbType.NVarChar).Value = Branch.BranchID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Branch.Address;
zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = Branch.Ward;
zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = Branch.District;
zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = Branch.Province;
zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = Branch.Country;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Branch.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Branch.Rank;
if(Branch.PartnerNumber != "" && Branch.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Branch.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Branch.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Branch.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Branch.Message = "200 OK";
}
catch (Exception Err)
{
Branch.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_Branch SET RecordStatus = 99 WHERE BranchKey = @BranchKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.BranchKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Branch.Message = "200 OK";
}
catch (Exception Err)
{
Branch.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_Branch WHERE BranchKey = @BranchKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Branch.BranchKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Branch.Message = "200 OK";
}
catch (Exception Err)
{
Branch.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
