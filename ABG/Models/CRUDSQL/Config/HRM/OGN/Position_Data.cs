﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Position_Data
    {
        public static List<Position_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_ListPosition WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY [Rank]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Position_Model> zList = new List<Position_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Position_Model()
                    {
                        PositionKey = r["PositionKey"].ToInt(),
                        PositionID = r["PositionID"].ToString(),
                        PositionNameVN = r["PositionNameVN"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }

        public static List<Position_Model> ListSearch(string PartnerNumber, string Search)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  * FROM HRM_ListPosition WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Search.Trim().Length > 0)
            {
                zSQL += " AND ( PositionID LIKE @Search OR PositionNameVN LIKE @Search  )";
            }
            zSQL += "  ORDER BY [Rank]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Position_Model> zList = new List<Position_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Position_Model()
                    {
                        PositionKey = r["PositionKey"].ToInt(),
                        PositionID = r["PositionID"].ToString(),
                        PositionNameVN = r["PositionNameVN"].ToString(),
                        Description = r["Description"].ToString(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }
            return zList;
        }
    }
}
