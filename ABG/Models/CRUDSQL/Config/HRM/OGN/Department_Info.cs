﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Department_Info
{
 
public  Department_Model Department = new Department_Model();
 
#region [ Constructor Get Information ]
public Department_Info()
{
Department.DepartmentKey = Guid.NewGuid().ToString();
}
public Department_Info(string DepartmentKey)
{
string zSQL = "SELECT * FROM HRM_Department WHERE DepartmentKey = @DepartmentKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(DepartmentKey);
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
Department.DepartmentKey = zReader["DepartmentKey"].ToString();
Department.DepartmentName = zReader["DepartmentName"].ToString();
Department.DepartmentID = zReader["DepartmentID"].ToString();
Department.Address = zReader["Address"].ToString();
Department.Parent = zReader["Parent"].ToString();
Department.FullPath = zReader["FullPath"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Department.Rank = int.Parse(zReader["Rank"].ToString());
if (zReader["LevelOrganization"]!= DBNull.Value)
Department.LevelOrganization = int.Parse(zReader["LevelOrganization"].ToString());
if (zReader["CategoryKey"]!= DBNull.Value)
Department.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
Department.CategoryName = zReader["CategoryName"].ToString();
Department.BranchKey = zReader["BranchKey"].ToString();
Department.BranchName = zReader["BranchName"].ToString();
Department.Description = zReader["Description"].ToString();
Department.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Department.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Department.CreatedOn = (DateTime)zReader["CreatedOn"];
Department.CreatedBy = zReader["CreatedBy"].ToString();
Department.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Department.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Department.ModifiedBy = zReader["ModifiedBy"].ToString();
Department.ModifiedName = zReader["ModifiedName"].ToString();
Department.Message = "200 OK";
}
else
{
Department.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Department.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Department (" 
 + " DepartmentName , DepartmentID , Address , Parent , FullPath , Rank , LevelOrganization , CategoryKey , CategoryName , BranchKey , BranchName , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @DepartmentName , @DepartmentID , @Address , @Parent , @FullPath , @Rank , @LevelOrganization , @CategoryKey , @CategoryName , @BranchKey , @BranchName , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Department.DepartmentName;
zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = Department.DepartmentID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Department.Address;
zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Department.Parent;
zCommand.Parameters.Add("@FullPath", SqlDbType.NVarChar).Value = Department.FullPath;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Department.Rank;
zCommand.Parameters.Add("@LevelOrganization", SqlDbType.Int).Value = Department.LevelOrganization;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Department.CategoryKey;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Department.CategoryName;
zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Department.BranchKey;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Department.BranchName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Department.Description;
if(Department.PartnerNumber != "" && Department.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Department.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Department.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Department.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Department.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Department.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Department.Message = "201 Created";
}
catch (Exception Err)
{
Department.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Department(" 
 + " DepartmentKey , DepartmentName , DepartmentID , Address , Parent , FullPath , Rank , LevelOrganization , CategoryKey , CategoryName , BranchKey , BranchName , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @DepartmentKey , @DepartmentName , @DepartmentID , @Address , @Parent , @FullPath , @Rank , @LevelOrganization , @CategoryKey , @CategoryName , @BranchKey , @BranchName , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Department.DepartmentKey != "" && Department.DepartmentKey.Length == 36)
{
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.DepartmentKey);
}
else
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Department.DepartmentName;
zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = Department.DepartmentID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Department.Address;
zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Department.Parent;
zCommand.Parameters.Add("@FullPath", SqlDbType.NVarChar).Value = Department.FullPath;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Department.Rank;
zCommand.Parameters.Add("@LevelOrganization", SqlDbType.Int).Value = Department.LevelOrganization;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Department.CategoryKey;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Department.CategoryName;
zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Department.BranchKey;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Department.BranchName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Department.Description;
if(Department.PartnerNumber != "" && Department.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Department.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Department.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Department.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Department.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Department.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Department.Message = "201 Created";
}
catch (Exception Err)
{
Department.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_Department SET " 
            + " DepartmentName = @DepartmentName,"
            + " DepartmentID = @DepartmentID,"
            + " Address = @Address,"
            + " Parent = @Parent,"
            + " FullPath = @FullPath,"
            + " Rank = @Rank,"
            + " LevelOrganization = @LevelOrganization,"
            + " CategoryKey = @CategoryKey,"
            + " CategoryName = @CategoryName,"
            + " BranchKey = @BranchKey,"
            + " BranchName = @BranchName,"
            + " Description = @Description,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE DepartmentKey = @DepartmentKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Department.DepartmentKey != "" && Department.DepartmentKey.Length == 36)
{
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.DepartmentKey);
}
else
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Department.DepartmentName;
zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = Department.DepartmentID;
zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Department.Address;
zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Department.Parent;
zCommand.Parameters.Add("@FullPath", SqlDbType.NVarChar).Value = Department.FullPath;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Department.Rank;
zCommand.Parameters.Add("@LevelOrganization", SqlDbType.Int).Value = Department.LevelOrganization;
zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Department.CategoryKey;
zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Department.CategoryName;
zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Department.BranchKey;
zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Department.BranchName;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Department.Description;
if(Department.PartnerNumber != "" && Department.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Department.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Department.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Department.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Department.Message = "200 OK";
}
catch (Exception Err)
{
Department.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_Department SET RecordStatus = 99 WHERE DepartmentKey = @DepartmentKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.DepartmentKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Department.Message = "200 OK";
}
catch (Exception Err)
{
Department.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_Department WHERE DepartmentKey = @DepartmentKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Department.DepartmentKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Department.Message = "200 OK";
}
catch (Exception Err)
{
Department.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
