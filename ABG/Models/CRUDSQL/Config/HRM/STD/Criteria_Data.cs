﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Criteria_Data
    {
        
        public static List<Criteria_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Criteria WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Criteria_Model> zList = new List<Criteria_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Criteria_Model()
                {
                    CriteriaKey = r["CriteriaKey"].ToInt(),
                    CriteriaID = r["CriteriaID"].ToString(),
                    CriteriaName = r["CriteriaName"].ToString(),
                    MaxPoint = r["MaxPoint"].ToInt(),
                    Description = r["Description"].ToString(),
                    Rank = r["Rank"].ToInt()
                });
            }
            return zList;
        }
    }
}
