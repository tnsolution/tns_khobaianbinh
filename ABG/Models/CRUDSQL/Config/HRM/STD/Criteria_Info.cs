﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Criteria_Info
    {

        public Criteria_Model Criteria = new Criteria_Model();

        #region [ Constructor Get Information ]
        public Criteria_Info()
        {
        }
        public Criteria_Info(int CriteriaKey)
        {
            string zSQL = "SELECT * FROM HRM_Criteria WHERE CriteriaKey = @CriteriaKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = CriteriaKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["CriteriaKey"] != DBNull.Value)
                    {
                        Criteria.CriteriaKey = int.Parse(zReader["CriteriaKey"].ToString());
                    }

                    Criteria.CriteriaID = zReader["CriteriaID"].ToString();
                    Criteria.CriteriaName = zReader["CriteriaName"].ToString();
                    if (zReader["MaxPoint"] != DBNull.Value)
                    {
                        Criteria.MaxPoint = int.Parse(zReader["MaxPoint"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Criteria.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    Criteria.Description = zReader["Description"].ToString();
                    Criteria.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Criteria.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Criteria.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Criteria.CreatedBy = zReader["CreatedBy"].ToString();
                    Criteria.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Criteria.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Criteria.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Criteria.ModifiedName = zReader["ModifiedName"].ToString();
                    Criteria.Message = "200 OK";
                }
                else
                {
                    Criteria.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Criteria.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Criteria ("
         + " CriteriaID , CriteriaName , MaxPoint , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CriteriaID , @CriteriaName , @MaxPoint , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CriteriaID", SqlDbType.NVarChar).Value = Criteria.CriteriaID;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Criteria.CriteriaName;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Criteria.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Criteria.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Criteria.Description;
                if (Criteria.PartnerNumber != "" && Criteria.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Criteria.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Criteria.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Criteria.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Criteria.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Criteria.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Criteria.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Criteria.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Criteria.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Criteria("
         + " CriteriaKey , CriteriaID , CriteriaName , MaxPoint , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CriteriaKey , @CriteriaID , @CriteriaName , @MaxPoint , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Criteria.CriteriaKey;
                zCommand.Parameters.Add("@CriteriaID", SqlDbType.NVarChar).Value = Criteria.CriteriaID;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Criteria.CriteriaName;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Criteria.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Criteria.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Criteria.Description;
                if (Criteria.PartnerNumber != "" && Criteria.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Criteria.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Criteria.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Criteria.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Criteria.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Criteria.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Criteria.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Criteria.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Criteria.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Criteria SET "
                        + " CriteriaID = @CriteriaID,"
                        + " CriteriaName = @CriteriaName,"
                        + " MaxPoint = @MaxPoint,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CriteriaKey = @CriteriaKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Criteria.CriteriaKey;
                zCommand.Parameters.Add("@CriteriaID", SqlDbType.NVarChar).Value = Criteria.CriteriaID;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Criteria.CriteriaName;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Criteria.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Criteria.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Criteria.Description;
                if (Criteria.PartnerNumber != "" && Criteria.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Criteria.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Criteria.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Criteria.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Criteria.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Criteria.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Criteria.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Criteria SET RecordStatus = 99 WHERE CriteriaKey = @CriteriaKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Criteria.CriteriaKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Criteria.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Criteria.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Criteria WHERE CriteriaKey = @CriteriaKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Criteria.CriteriaKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Criteria.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Criteria.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
