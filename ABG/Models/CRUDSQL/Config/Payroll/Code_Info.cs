﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Payroll_Code_Info
{
 
public  Payroll_Code_Model Payroll_Code = new Payroll_Code_Model();
 
#region [ Constructor Get Information ]
public Payroll_Code_Info()
{
}
public Payroll_Code_Info(int ItemKey)
{
string zSQL = "SELECT * FROM HRM_Payroll_Code WHERE ItemKey = @ItemKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = ItemKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["ItemKey"]!= DBNull.Value)
Payroll_Code.ItemKey = int.Parse(zReader["ItemKey"].ToString());
Payroll_Code.ItemName = zReader["ItemName"].ToString();
if (zReader["ItemType"]!= DBNull.Value)
Payroll_Code.ItemType = int.Parse(zReader["ItemType"].ToString());
Payroll_Code.ItemID = zReader["ItemID"].ToString();
Payroll_Code.Math = zReader["Math"].ToString();
Payroll_Code.MathCaculator = zReader["MathCaculator"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Payroll_Code.Rank = int.Parse(zReader["Rank"].ToString());
Payroll_Code.Description = zReader["Description"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Payroll_Code.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
Payroll_Code.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["CreatedOn"]!= DBNull.Value)
Payroll_Code.CreatedOn = (DateTime)zReader["CreatedOn"];
Payroll_Code.CreatedBy = zReader["CreatedBy"].ToString();
Payroll_Code.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Payroll_Code.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Payroll_Code.ModifiedBy = zReader["ModifiedBy"].ToString();
Payroll_Code.ModifiedName = zReader["ModifiedName"].ToString();
Payroll_Code.Message = "200 OK";
}
else
{
Payroll_Code.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Payroll_Code.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Payroll_Code (" 
 + " ItemName , ItemType , ItemID , Math , MathCaculator , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ItemName , @ItemType , @ItemID , @Math , @MathCaculator , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Code.ItemName;
zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Code.ItemType;
zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Code.ItemID;
zCommand.Parameters.Add("@Math", SqlDbType.NVarChar).Value = Payroll_Code.Math;
zCommand.Parameters.Add("@MathCaculator", SqlDbType.NVarChar).Value = Payroll_Code.MathCaculator;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Code.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Code.Description;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Code.RecordStatus;
if(Payroll_Code.PartnerNumber != "" && Payroll_Code.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Code.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Code.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Code.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Code.Message = "201 Created";
}
catch (Exception Err)
{
Payroll_Code.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Payroll_Code(" 
 + " ItemKey , ItemName , ItemType , ItemID , Math , MathCaculator , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ItemKey , @ItemName , @ItemType , @ItemID , @Math , @MathCaculator , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Code.ItemKey;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Code.ItemName;
zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Code.ItemType;
zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Code.ItemID;
zCommand.Parameters.Add("@Math", SqlDbType.NVarChar).Value = Payroll_Code.Math;
zCommand.Parameters.Add("@MathCaculator", SqlDbType.NVarChar).Value = Payroll_Code.MathCaculator;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Code.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Code.Description;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Code.RecordStatus;
if(Payroll_Code.PartnerNumber != "" && Payroll_Code.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Code.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Code.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Code.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Code.Message = "201 Created";
}
catch (Exception Err)
{
Payroll_Code.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_Payroll_Code SET " 
            + " ItemName = @ItemName,"
            + " ItemType = @ItemType,"
            + " ItemID = @ItemID,"
            + " Math = @Math,"
            + " MathCaculator = @MathCaculator,"
            + " Rank = @Rank,"
            + " Description = @Description,"
            + " RecordStatus = @RecordStatus,"
            + " PartnerNumber = @PartnerNumber,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE ItemKey = @ItemKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Code.ItemKey;
zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Code.ItemName;
zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Code.ItemType;
zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Code.ItemID;
zCommand.Parameters.Add("@Math", SqlDbType.NVarChar).Value = Payroll_Code.Math;
zCommand.Parameters.Add("@MathCaculator", SqlDbType.NVarChar).Value = Payroll_Code.MathCaculator;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Code.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Code.Description;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Code.RecordStatus;
if(Payroll_Code.PartnerNumber != "" && Payroll_Code.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Code.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Code.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Code.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Code.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_Payroll_Code SET RecordStatus = 99 WHERE ItemKey = @ItemKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Code.ItemKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Code.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Code.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_Payroll_Code WHERE ItemKey = @ItemKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Code.ItemKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Payroll_Code.Message = "200 OK";
}
catch (Exception Err)
{
Payroll_Code.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
