﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Sample_Info
    {

        public Payroll_Sample_Model Payroll_Sample = new Payroll_Sample_Model();

        #region [ Constructor Get Information ]
        public Payroll_Sample_Info()
        {
        }
        public Payroll_Sample_Info(int ItemKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Sample WHERE ItemKey = @ItemKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = ItemKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ItemKey"] != DBNull.Value)
                        Payroll_Sample.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    Payroll_Sample.ItemName = zReader["ItemName"].ToString();
                    if (zReader["ItemType"] != DBNull.Value)
                        Payroll_Sample.ItemType = int.Parse(zReader["ItemType"].ToString());
                    Payroll_Sample.ItemID = zReader["ItemID"].ToString();
                    if (zReader["Quantity"] != DBNull.Value)
                        Payroll_Sample.Quantity = float.Parse(zReader["Quantity"].ToString());
                    Payroll_Sample.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Total"] != DBNull.Value)
                        Payroll_Sample.Total = double.Parse(zReader["Total"].ToString());
                    if (zReader["IsContract"] != DBNull.Value)
                        Payroll_Sample.IsContract = int.Parse(zReader["IsContract"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        Payroll_Sample.Rank = int.Parse(zReader["Rank"].ToString());
                    Payroll_Sample.Description = zReader["Description"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Payroll_Sample.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Payroll_Sample.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["Parameter"] != DBNull.Value)
                        Payroll_Sample.Parameter = float.Parse(zReader["Parameter"].ToString());
                    Payroll_Sample.Formula = zReader["Formula"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Payroll_Sample.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Payroll_Sample.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Payroll_Sample.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Payroll_Sample.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Sample.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Payroll_Sample.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Payroll_Sample.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Sample.ModifiedName = zReader["ModifiedName"].ToString();
                    Payroll_Sample.Message = "200 OK";
                }
                else
                {
                    Payroll_Sample.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Sample ("
         + " ItemName , ItemType , ItemID , Quantity , UnitName , Total , IsContract , Rank , Description , CategoryKey , CategoryName , Parameter , Formula , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ItemName , @ItemType , @ItemID , @Quantity , @UnitName , @Total , @IsContract , @Rank , @Description , @CategoryKey , @CategoryName , @Parameter , @Formula , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Sample.ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Sample.ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Sample.ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Sample.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Sample.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Sample.Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = Payroll_Sample.IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Sample.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Sample.Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Sample.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Sample.CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = Payroll_Sample.Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = Payroll_Sample.Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Sample.RecordStatus;
                if (Payroll_Sample.PartnerNumber != "" && Payroll_Sample.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Sample.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Sample.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Sample.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Sample("
         + " ItemKey , ItemName , ItemType , ItemID , Quantity , UnitName , Total , IsContract , Rank , Description , CategoryKey , CategoryName , Parameter , Formula , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ItemKey , @ItemName , @ItemType , @ItemID , @Quantity , @UnitName , @Total , @IsContract , @Rank , @Description , @CategoryKey , @CategoryName , @Parameter , @Formula , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Sample.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Sample.ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Sample.ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Sample.ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Sample.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Sample.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Sample.Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = Payroll_Sample.IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Sample.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Sample.Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Sample.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Sample.CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = Payroll_Sample.Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = Payroll_Sample.Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Sample.RecordStatus;
                if (Payroll_Sample.PartnerNumber != "" && Payroll_Sample.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Sample.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Sample.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Sample.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Sample SET "
                        + " ItemName = @ItemName,"
                        + " ItemType = @ItemType,"
                        + " ItemID = @ItemID,"
                        + " Quantity = @Quantity,"
                        + " UnitName = @UnitName,"
                        + " Total = @Total,"
                        + " IsContract = @IsContract,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Parameter = @Parameter,"
                        + " Formula = @Formula,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ItemKey = @ItemKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Sample.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Sample.ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Sample.ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Sample.ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Sample.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Sample.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Sample.Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = Payroll_Sample.IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Sample.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Sample.Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Payroll_Sample.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Payroll_Sample.CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = Payroll_Sample.Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = Payroll_Sample.Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Sample.RecordStatus;
                if (Payroll_Sample.PartnerNumber != "" && Payroll_Sample.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Sample.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Sample.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Sample SET RecordStatus = 99 WHERE ItemKey = @ItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Sample.ItemKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Sample WHERE ItemKey = @ItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Sample.ItemKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string CapNhatHeSo(float HeSo, int maso)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";

            if (maso == 100)
                zSQL = "UPDATE HRM_Payroll_Sample SET Parameter = " + HeSo + " WHERE ItemKey = 101";
            if (maso == 102)
                zSQL = "UPDATE HRM_Payroll_Sample SET Parameter = " + HeSo + " WHERE ItemKey = 103";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);                
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Sample.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Sample.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
