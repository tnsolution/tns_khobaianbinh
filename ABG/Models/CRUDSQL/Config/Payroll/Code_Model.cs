﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Payroll_Code_Model
{
#region [ Field Name ]
private int _ItemKey = 0;
private string _ItemName = "";
private int _ItemType = 0;
private string _ItemID = "";
private string _Math = "";
private string _MathCaculator = "";
private int _Rank = 0;
private string _Description = "";
private int _RecordStatus = 0;
private string _PartnerNumber = "";
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int ItemKey
{
get { return _ItemKey; }
set { _ItemKey = value; }
}
public string ItemName
{
get { return _ItemName; }
set { _ItemName = value; }
}
public int ItemType
{
get { return _ItemType; }
set { _ItemType = value; }
}
public string ItemID
{
get { return _ItemID; }
set { _ItemID = value; }
}
public string Math
{
get { return _Math; }
set { _Math = value; }
}
public string MathCaculator
{
get { return _MathCaculator; }
set { _MathCaculator = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
