﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Increase_Info
    {

        public Payroll_Increase_Model Payroll_Increase = new Payroll_Increase_Model();

        #region [ Constructor Get Information ]
        public Payroll_Increase_Info()
        {
        }
        public Payroll_Increase_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Increase WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Payroll_Increase.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Payroll_Increase.ID = zReader["ID"].ToString();
                    Payroll_Increase.ContractKey = zReader["ContractKey"].ToString();
                    Payroll_Increase.ContractID = zReader["ContractID"].ToString();
                    Payroll_Increase.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Increase.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                    {
                        Payroll_Increase.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    }

                    Payroll_Increase.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Quantity"] != DBNull.Value)
                    {
                        Payroll_Increase.Quantity = float.Parse(zReader["Quantity"].ToString());
                    }

                    Payroll_Increase.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Total"] != DBNull.Value)
                    {
                        Payroll_Increase.Total = double.Parse(zReader["Total"].ToString());
                    }

                    if (zReader["WriteDate"] != DBNull.Value)
                    {
                        Payroll_Increase.WriteDate = (DateTime)zReader["WriteDate"];
                    }

                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Payroll_Increase.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Payroll_Increase.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["ItemType"] != DBNull.Value)
                    {
                        Payroll_Increase.ItemType = int.Parse(zReader["ItemType"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Payroll_Increase.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    Payroll_Increase.Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Increase.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Payroll_Increase.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Increase.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Increase.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Increase.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Increase.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Increase.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Increase.ModifiedName = zReader["ModifiedName"].ToString();
                    Payroll_Increase.Message = "200 OK";
                }
                else
                {
                    Payroll_Increase.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Increase ("
         + " ID , ContractKey , ContractID , EmployeeKey , EmployeeName , ItemKey , ItemName , Quantity , UnitName , Total , WriteDate , FromDate , ToDate , ItemType , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ID , @ContractKey , @ContractID , @EmployeeKey , @EmployeeName , @ItemKey , @ItemName , @Quantity , @UnitName , @Total , @WriteDate , @FromDate , @ToDate , @ItemType , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = Payroll_Increase.ID;
                if (Payroll_Increase.ContractKey != "" && Payroll_Increase.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Payroll_Increase.ContractID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Increase.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Increase.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Increase.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Increase.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Increase.Total;
                if (Payroll_Increase.WriteDate == null)
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = Payroll_Increase.WriteDate;
                }

                if (Payroll_Increase.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Payroll_Increase.FromDate;
                }

                if (Payroll_Increase.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Payroll_Increase.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Increase.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Increase.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Increase.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Increase.RecordStatus;
                if (Payroll_Increase.PartnerNumber != "" && Payroll_Increase.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Increase.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Increase.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Increase.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Increase("
         + " AutoKey , ID , ContractKey , ContractID , EmployeeKey , EmployeeName , ItemKey , ItemName , Quantity , UnitName , Total , WriteDate , FromDate , ToDate , ItemType , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ID , @ContractKey , @ContractID , @EmployeeKey , @EmployeeName , @ItemKey , @ItemName , @Quantity , @UnitName , @Total , @WriteDate , @FromDate , @ToDate , @ItemType , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Increase.AutoKey;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = Payroll_Increase.ID;
                if (Payroll_Increase.ContractKey != "" && Payroll_Increase.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Payroll_Increase.ContractID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Increase.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Increase.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Increase.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Increase.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Increase.Total;
                if (Payroll_Increase.WriteDate == null)
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = Payroll_Increase.WriteDate;
                }

                if (Payroll_Increase.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Payroll_Increase.FromDate;
                }

                if (Payroll_Increase.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Payroll_Increase.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Increase.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Increase.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Increase.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Increase.RecordStatus;
                if (Payroll_Increase.PartnerNumber != "" && Payroll_Increase.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Increase.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Increase.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Increase.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Increase SET "
                        + " ID = @ID,"
                        + " ContractKey = @ContractKey,"
                        + " ContractID = @ContractID,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " ItemKey = @ItemKey,"
                        + " ItemName = @ItemName,"
                        + " Quantity = @Quantity,"
                        + " UnitName = @UnitName,"
                        + " Total = @Total,"
                        + " WriteDate = @WriteDate,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " ItemType = @ItemType,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Increase.AutoKey;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = Payroll_Increase.ID;
                if (Payroll_Increase.ContractKey != "" && Payroll_Increase.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Payroll_Increase.ContractID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Increase.EmployeeName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Increase.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Increase.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Increase.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Increase.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Increase.Total;
                if (Payroll_Increase.WriteDate == null)
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@WriteDate", SqlDbType.Date).Value = Payroll_Increase.WriteDate;
                }

                if (Payroll_Increase.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Payroll_Increase.FromDate;
                }

                if (Payroll_Increase.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Payroll_Increase.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Increase.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Payroll_Increase.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Increase.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Increase.RecordStatus;
                if (Payroll_Increase.PartnerNumber != "" && Payroll_Increase.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Increase.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Increase.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Increase.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Increase SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Increase.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Increase.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Increase WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Increase.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Increase.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Increase.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
