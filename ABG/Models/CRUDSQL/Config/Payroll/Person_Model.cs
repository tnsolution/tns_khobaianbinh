﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Payroll_Person_Model
{
#region [ Field Name ]
private string _PayrollKey = "";
private string _EmployeeKey = "";
private string _EmployeeID = "";
private string _EmployeeName = "";
private string _BranchKey = "";
private string _BranchName = "";
private string _DepartmentKey = "";
private string _DepartmentName = "";
private int _PositionKey = 0;
private string _PositionName = "";
private int _Month = 0;
private int _Year = 0;
private double _TotalMoney = 0;
private int _TotalWorkTime = 0;
private float _TotalDateWorking= 0;
private float _TotalDateLeaving= 0;
private float _TotalDatePaidLeave= 0;
private float _TotalDateUnpaidLeave= 0;
private float _TotalDateAnnualLeave= 0;
private float _TotalDateDayOff= 0;
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private string _Style = "";
private string _Class = "";
private string _CodeLine = "";
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public string PayrollKey
{
get { return _PayrollKey; }
set { _PayrollKey = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string EmployeeID
{
get { return _EmployeeID; }
set { _EmployeeID = value; }
}
public string EmployeeName
{
get { return _EmployeeName; }
set { _EmployeeName = value; }
}
public string BranchKey
{
get { return _BranchKey; }
set { _BranchKey = value; }
}
public string BranchName
{
get { return _BranchName; }
set { _BranchName = value; }
}
public string DepartmentKey
{
get { return _DepartmentKey; }
set { _DepartmentKey = value; }
}
public string DepartmentName
{
get { return _DepartmentName; }
set { _DepartmentName = value; }
}
public int PositionKey
{
get { return _PositionKey; }
set { _PositionKey = value; }
}
public string PositionName
{
get { return _PositionName; }
set { _PositionName = value; }
}
public int Month
{
get { return _Month; }
set { _Month = value; }
}
public int Year
{
get { return _Year; }
set { _Year = value; }
}
public double TotalMoney
{
get { return _TotalMoney; }
set { _TotalMoney = value; }
}
public int TotalWorkTime
{
get { return _TotalWorkTime; }
set { _TotalWorkTime = value; }
}
public float TotalDateWorking
{
get { return _TotalDateWorking; }
set { _TotalDateWorking = value; }
}
public float TotalDateLeaving
{
get { return _TotalDateLeaving; }
set { _TotalDateLeaving = value; }
}
public float TotalDatePaidLeave
{
get { return _TotalDatePaidLeave; }
set { _TotalDatePaidLeave = value; }
}
public float TotalDateUnpaidLeave
{
get { return _TotalDateUnpaidLeave; }
set { _TotalDateUnpaidLeave = value; }
}
public float TotalDateAnnualLeave
{
get { return _TotalDateAnnualLeave; }
set { _TotalDateAnnualLeave = value; }
}
public float TotalDateDayOff
{
get { return _TotalDateDayOff; }
set { _TotalDateDayOff = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public string Style
{
get { return _Style; }
set { _Style = value; }
}
public string Class
{
get { return _Class; }
set { _Class = value; }
}
public string CodeLine
{
get { return _CodeLine; }
set { _CodeLine = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
