﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Product_Land_Info
    {

        public Product_Land_Model Product_Land = new Product_Land_Model();

        #region [ Constructor Get Information ]
        public Product_Land_Info()
        {
            Product_Land.ProductKey = Guid.NewGuid().ToString();
        }
        public Product_Land_Info(string ProductKey)
        {
            string zSQL = "SELECT * FROM PDT_Product_Land WHERE ProductKey = @ProductKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ProductKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Product_Land.ProductKey = zReader["ProductKey"].ToString();
                    Product_Land.ProductID = zReader["ProductID"].ToString();
                    Product_Land.ProductName = zReader["ProductName"].ToString();
                    Product_Land.ProductNumber = zReader["ProductNumber"].ToString();
                    Product_Land.ProductSerial = zReader["ProductSerial"].ToString();
                    Product_Land.ProductModel = zReader["ProductModel"].ToString();
                    Product_Land.ParentKey = zReader["ParentKey"].ToString();
                    if (zReader["StandardUnitKey"] != DBNull.Value)
                    {
                        Product_Land.StandardUnitKey = int.Parse(zReader["StandardUnitKey"].ToString());
                    }

                    Product_Land.StandardUnitName = zReader["StandardUnitName"].ToString();
                    if (zReader["StandardCost"] != DBNull.Value)
                    {
                        Product_Land.StandardCost = double.Parse(zReader["StandardCost"].ToString());
                    }

                    if (zReader["DiscontinuedDate"] != DBNull.Value)
                    {
                        Product_Land.DiscontinuedDate = (DateTime)zReader["DiscontinuedDate"];
                    }

                    if (zReader["SafetyInStock"] != DBNull.Value)
                    {
                        Product_Land.SafetyInStock = float.Parse(zReader["SafetyInStock"].ToString());
                    }

                    Product_Land.Style = zReader["Style"].ToString();
                    Product_Land.Class = zReader["Class"].ToString();
                    Product_Land.CodeLine = zReader["CodeLine"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Product_Land.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Product_Land.CategoryName = zReader["CategoryName"].ToString();
                    Product_Land.PhotoPath = zReader["PhotoPath"].ToString();
                    if (zReader["Width"] != DBNull.Value)
                    {
                        Product_Land.Width = float.Parse(zReader["Width"].ToString());
                    }

                    if (zReader["Length"] != DBNull.Value)
                    {
                        Product_Land.Length = float.Parse(zReader["Length"].ToString());
                    }

                    if (zReader["Height"] != DBNull.Value)
                    {
                        Product_Land.Height = float.Parse(zReader["Height"].ToString());
                    }

                    if (zReader["Campus"] != DBNull.Value)
                    {
                        Product_Land.Campus = float.Parse(zReader["Campus"].ToString());
                    }

                    if (zReader["Area"] != DBNull.Value)
                    {
                        Product_Land.Area = float.Parse(zReader["Area"].ToString());
                    }

                    if (zReader["AreaBuild"] != DBNull.Value)
                    {
                        Product_Land.AreaBuild = float.Parse(zReader["AreaBuild"].ToString());
                    }

                    if (zReader["AreaUsing"] != DBNull.Value)
                    {
                        Product_Land.AreaUsing = float.Parse(zReader["AreaUsing"].ToString());
                    }

                    Product_Land.Address = zReader["Address"].ToString();
                    Product_Land.CityName = zReader["CityName"].ToString();
                    Product_Land.DistrictName = zReader["DistrictName"].ToString();
                    Product_Land.WardName = zReader["WardName"].ToString();
                    if (zReader["PostCode"] != DBNull.Value)
                    {
                        Product_Land.PostCode = int.Parse(zReader["PostCode"].ToString());
                    }

                    Product_Land.LngLat = zReader["LngLat"].ToString();
                    Product_Land.Kml = zReader["Kml"].ToString();
                    Product_Land.Map = zReader["Map"].ToString();
                    Product_Land.Description = zReader["Description"].ToString();
                    if (zReader["ProjectKey"] != DBNull.Value)
                    {
                        Product_Land.ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    }

                    Product_Land.ProjectName = zReader["ProjectName"].ToString();
                    Product_Land.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Product_Land.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Product_Land.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Product_Land.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Product_Land.CreatedBy = zReader["CreatedBy"].ToString();
                    Product_Land.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Product_Land.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Product_Land.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product_Land.ModifiedName = zReader["ModifiedName"].ToString();
                    Product_Land.Message = "200 OK";
                }
                else
                {
                    Product_Land.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Land ("
         + " ProductID , ProductName , ProductNumber , ProductSerial , ProductModel , ParentKey , StandardUnitKey , StandardUnitName , StandardCost , DiscontinuedDate , SafetyInStock , Style , Class , CodeLine , CategoryKey , CategoryName , PhotoPath , Width , Length , Height , Campus , Area , AreaBuild , AreaUsing , Address , CityName , DistrictName , WardName , PostCode , LngLat , Kml , Map , Description , ProjectKey , ProjectName , PartnerNumber , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductID , @ProductName , @ProductNumber , @ProductSerial , @ProductModel , @ParentKey , @StandardUnitKey , @StandardUnitName , @StandardCost , @DiscontinuedDate , @SafetyInStock , @Style , @Class , @CodeLine , @CategoryKey , @CategoryName , @PhotoPath , @Width , @Length , @Height , @Campus , @Area , @AreaBuild , @AreaUsing , @Address , @CityName , @DistrictName , @WardName , @PostCode , @LngLat , @Kml , @Map , @Description , @ProjectKey , @ProjectName , @PartnerNumber , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product_Land.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product_Land.ProductName;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product_Land.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product_Land.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product_Land.ProductModel;
                if (Product_Land.ParentKey != "" && Product_Land.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product_Land.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product_Land.StandardUnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product_Land.StandardCost;
                if (Product_Land.DiscontinuedDate == null)
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product_Land.DiscontinuedDate;
                }

                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product_Land.SafetyInStock;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Product_Land.CodeLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Land.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Land.CategoryName;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Land.PhotoPath;
                zCommand.Parameters.Add("@Width", SqlDbType.Float).Value = Product_Land.Width;
                zCommand.Parameters.Add("@Length", SqlDbType.Float).Value = Product_Land.Length;
                zCommand.Parameters.Add("@Height", SqlDbType.Float).Value = Product_Land.Height;
                zCommand.Parameters.Add("@Campus", SqlDbType.Float).Value = Product_Land.Campus;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Product_Land.Area;
                zCommand.Parameters.Add("@AreaBuild", SqlDbType.Float).Value = Product_Land.AreaBuild;
                zCommand.Parameters.Add("@AreaUsing", SqlDbType.Float).Value = Product_Land.AreaUsing;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Product_Land.Address;
                zCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = Product_Land.CityName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Product_Land.DistrictName;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Product_Land.WardName;
                zCommand.Parameters.Add("@PostCode", SqlDbType.Int).Value = Product_Land.PostCode;
                zCommand.Parameters.Add("@LngLat", SqlDbType.NVarChar).Value = Product_Land.LngLat;
                zCommand.Parameters.Add("@Kml", SqlDbType.NVarChar).Value = Product_Land.Kml;
                zCommand.Parameters.Add("@Map", SqlDbType.NVarChar).Value = Product_Land.Map;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land.Description;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Product_Land.ProjectKey;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = Product_Land.ProjectName;
                if (Product_Land.PartnerNumber != "" && Product_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Land.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Land.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Land.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Land("
         + " ProductKey , ProductID , ProductName , ProductNumber , ProductSerial , ProductModel , ParentKey , StandardUnitKey , StandardUnitName , StandardCost , DiscontinuedDate , SafetyInStock , Style , Class , CodeLine , CategoryKey , CategoryName , PhotoPath , Width , Length , Height , Campus , Area , AreaBuild , AreaUsing , Address , CityName , DistrictName , WardName , PostCode , LngLat , Kml , Map , Description , ProjectKey , ProjectName , PartnerNumber , Rank , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductKey , @ProductID , @ProductName , @ProductNumber , @ProductSerial , @ProductModel , @ParentKey , @StandardUnitKey , @StandardUnitName , @StandardCost , @DiscontinuedDate , @SafetyInStock , @Style , @Class , @CodeLine , @CategoryKey , @CategoryName , @PhotoPath , @Width , @Length , @Height , @Campus , @Area , @AreaBuild , @AreaUsing , @Address , @CityName , @DistrictName , @WardName , @PostCode , @LngLat , @Kml , @Map , @Description , @ProjectKey , @ProjectName , @PartnerNumber , @Rank , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Product_Land.ProductKey != "" && Product_Land.ProductKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ProductKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product_Land.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product_Land.ProductName;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product_Land.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product_Land.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product_Land.ProductModel;
                if (Product_Land.ParentKey != "" && Product_Land.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product_Land.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product_Land.StandardUnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product_Land.StandardCost;
                if (Product_Land.DiscontinuedDate == null)
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product_Land.DiscontinuedDate;
                }

                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product_Land.SafetyInStock;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Product_Land.CodeLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Land.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Land.CategoryName;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Land.PhotoPath;
                zCommand.Parameters.Add("@Width", SqlDbType.Float).Value = Product_Land.Width;
                zCommand.Parameters.Add("@Length", SqlDbType.Float).Value = Product_Land.Length;
                zCommand.Parameters.Add("@Height", SqlDbType.Float).Value = Product_Land.Height;
                zCommand.Parameters.Add("@Campus", SqlDbType.Float).Value = Product_Land.Campus;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Product_Land.Area;
                zCommand.Parameters.Add("@AreaBuild", SqlDbType.Float).Value = Product_Land.AreaBuild;
                zCommand.Parameters.Add("@AreaUsing", SqlDbType.Float).Value = Product_Land.AreaUsing;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Product_Land.Address;
                zCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = Product_Land.CityName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Product_Land.DistrictName;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Product_Land.WardName;
                zCommand.Parameters.Add("@PostCode", SqlDbType.Int).Value = Product_Land.PostCode;
                zCommand.Parameters.Add("@LngLat", SqlDbType.NVarChar).Value = Product_Land.LngLat;
                zCommand.Parameters.Add("@Kml", SqlDbType.NVarChar).Value = Product_Land.Kml;
                zCommand.Parameters.Add("@Map", SqlDbType.NVarChar).Value = Product_Land.Map;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land.Description;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Product_Land.ProjectKey;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = Product_Land.ProjectName;
                if (Product_Land.PartnerNumber != "" && Product_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Land.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Land.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Land.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Product_Land SET "
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " ProductNumber = @ProductNumber,"
                        + " ProductSerial = @ProductSerial,"
                        + " ProductModel = @ProductModel,"
                        + " ParentKey = @ParentKey,"
                        + " StandardUnitKey = @StandardUnitKey,"
                        + " StandardUnitName = @StandardUnitName,"
                        + " StandardCost = @StandardCost,"
                        + " DiscontinuedDate = @DiscontinuedDate,"
                        + " SafetyInStock = @SafetyInStock,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " PhotoPath = @PhotoPath,"
                        + " Width = @Width,"
                        + " Length = @Length,"
                        + " Height = @Height,"
                        + " Campus = @Campus,"
                        + " Area = @Area,"
                        + " AreaBuild = @AreaBuild,"
                        + " AreaUsing = @AreaUsing,"
                        + " Address = @Address,"
                        + " CityName = @CityName,"
                        + " DistrictName = @DistrictName,"
                        + " WardName = @WardName,"
                        + " PostCode = @PostCode,"
                        + " LngLat = @LngLat,"
                        + " Kml = @Kml,"
                        + " Map = @Map,"
                        + " Description = @Description,"
                        + " ProjectKey = @ProjectKey,"
                        + " ProjectName = @ProjectName,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Rank = @Rank,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ProductKey = @ProductKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Product_Land.ProductKey != "" && Product_Land.ProductKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ProductKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Product_Land.ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Product_Land.ProductName;
                zCommand.Parameters.Add("@ProductNumber", SqlDbType.NVarChar).Value = Product_Land.ProductNumber;
                zCommand.Parameters.Add("@ProductSerial", SqlDbType.NVarChar).Value = Product_Land.ProductSerial;
                zCommand.Parameters.Add("@ProductModel", SqlDbType.NVarChar).Value = Product_Land.ProductModel;
                if (Product_Land.ParentKey != "" && Product_Land.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@StandardUnitKey", SqlDbType.Int).Value = Product_Land.StandardUnitKey;
                zCommand.Parameters.Add("@StandardUnitName", SqlDbType.NVarChar).Value = Product_Land.StandardUnitName;
                zCommand.Parameters.Add("@StandardCost", SqlDbType.Money).Value = Product_Land.StandardCost;
                if (Product_Land.DiscontinuedDate == null)
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DiscontinuedDate", SqlDbType.DateTime).Value = Product_Land.DiscontinuedDate;
                }

                zCommand.Parameters.Add("@SafetyInStock", SqlDbType.Float).Value = Product_Land.SafetyInStock;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Product_Land.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Product_Land.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Product_Land.CodeLine;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Product_Land.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Product_Land.CategoryName;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Land.PhotoPath;
                zCommand.Parameters.Add("@Width", SqlDbType.Float).Value = Product_Land.Width;
                zCommand.Parameters.Add("@Length", SqlDbType.Float).Value = Product_Land.Length;
                zCommand.Parameters.Add("@Height", SqlDbType.Float).Value = Product_Land.Height;
                zCommand.Parameters.Add("@Campus", SqlDbType.Float).Value = Product_Land.Campus;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = Product_Land.Area;
                zCommand.Parameters.Add("@AreaBuild", SqlDbType.Float).Value = Product_Land.AreaBuild;
                zCommand.Parameters.Add("@AreaUsing", SqlDbType.Float).Value = Product_Land.AreaUsing;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Product_Land.Address;
                zCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = Product_Land.CityName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Product_Land.DistrictName;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Product_Land.WardName;
                zCommand.Parameters.Add("@PostCode", SqlDbType.Int).Value = Product_Land.PostCode;
                zCommand.Parameters.Add("@LngLat", SqlDbType.NVarChar).Value = Product_Land.LngLat;
                zCommand.Parameters.Add("@Kml", SqlDbType.NVarChar).Value = Product_Land.Kml;
                zCommand.Parameters.Add("@Map", SqlDbType.NVarChar).Value = Product_Land.Map;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land.Description;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Product_Land.ProjectKey;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = Product_Land.ProjectName;
                if (Product_Land.PartnerNumber != "" && Product_Land.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land.Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product_Land SET RecordStatus = 99 WHERE ProductKey = @ProductKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ProductKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product_Land WHERE ProductKey = @ProductKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land.ProductKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Land.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Land.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
