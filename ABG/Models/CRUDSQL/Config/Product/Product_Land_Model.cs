﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Product_Land_Model
{
#region [ Field Name ]
private string _ProductKey = "";
private string _ProductID = "";
private string _ProductName = "";
private string _ProductNumber = "";
private string _ProductSerial = "";
private string _ProductModel = "";
private string _ParentKey = "";
private int? _StandardUnitKey = 0;
private string _StandardUnitName = "";
private double? _StandardCost = 0;
private DateTime? _DiscontinuedDate = null;
private float? _SafetyInStock= 0;
private string _Style = "";
private string _Class = "";
private string _CodeLine = "";
private int? _CategoryKey = 0;
private string _CategoryName = "";
private string _PhotoPath = "";
private float? _Width= 0;
private float? _Length= 0;
private float? _Height= 0;
private float? _Campus= 0;
private float? _Area= 0;
private float? _AreaBuild= 0;
private float? _AreaUsing= 0;
private string _Address = "";
private string _CityName = "";
private string _DistrictName = "";
private string _WardName = "";
private int? _PostCode = 0;
private string _LngLat = "";
private string _Kml = "";
private string _Map = "";
private string _Description = "";
private int? _ProjectKey = 0;
private string _ProjectName = "";
private string _PartnerNumber = "";
private int? _Rank = 0;
private int? _RecordStatus = 0;
private DateTime? _CreatedOn = null;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime? _ModifiedOn = null;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public string ProductKey
{
get { return _ProductKey; }
set { _ProductKey = value; }
}
public string ProductID
{
get { return _ProductID; }
set { _ProductID = value; }
}
public string ProductName
{
get { return _ProductName; }
set { _ProductName = value; }
}
public string ProductNumber
{
get { return _ProductNumber; }
set { _ProductNumber = value; }
}
public string ProductSerial
{
get { return _ProductSerial; }
set { _ProductSerial = value; }
}
public string ProductModel
{
get { return _ProductModel; }
set { _ProductModel = value; }
}
public string ParentKey
{
get { return _ParentKey; }
set { _ParentKey = value; }
}
public int? StandardUnitKey
{
get { return _StandardUnitKey; }
set { _StandardUnitKey = value; }
}
public string StandardUnitName
{
get { return _StandardUnitName; }
set { _StandardUnitName = value; }
}
public double? StandardCost
{
get { return _StandardCost; }
set { _StandardCost = value; }
}
public DateTime? DiscontinuedDate
{
get { return _DiscontinuedDate; }
set { _DiscontinuedDate = value; }
}
public float? SafetyInStock
{
get { return _SafetyInStock; }
set { _SafetyInStock = value; }
}
public string Style
{
get { return _Style; }
set { _Style = value; }
}
public string Class
{
get { return _Class; }
set { _Class = value; }
}
public string CodeLine
{
get { return _CodeLine; }
set { _CodeLine = value; }
}
public int? CategoryKey
{
get { return _CategoryKey; }
set { _CategoryKey = value; }
}
public string CategoryName
{
get { return _CategoryName; }
set { _CategoryName = value; }
}
public string PhotoPath
{
get { return _PhotoPath; }
set { _PhotoPath = value; }
}
public float? Width
{
get { return _Width; }
set { _Width = value; }
}
public float? Length
{
get { return _Length; }
set { _Length = value; }
}
public float? Height
{
get { return _Height; }
set { _Height = value; }
}
public float? Campus
{
get { return _Campus; }
set { _Campus = value; }
}
public float? Area
{
get { return _Area; }
set { _Area = value; }
}
public float? AreaBuild
{
get { return _AreaBuild; }
set { _AreaBuild = value; }
}
public float? AreaUsing
{
get { return _AreaUsing; }
set { _AreaUsing = value; }
}
public string Address
{
get { return _Address; }
set { _Address = value; }
}
public string CityName
{
get { return _CityName; }
set { _CityName = value; }
}
public string DistrictName
{
get { return _DistrictName; }
set { _DistrictName = value; }
}
public string WardName
{
get { return _WardName; }
set { _WardName = value; }
}
public int? PostCode
{
get { return _PostCode; }
set { _PostCode = value; }
}
public string LngLat
{
get { return _LngLat; }
set { _LngLat = value; }
}
public string Kml
{
get { return _Kml; }
set { _Kml = value; }
}
public string Map
{
get { return _Map; }
set { _Map = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int? ProjectKey
{
get { return _ProjectKey; }
set { _ProjectKey = value; }
}
public string ProjectName
{
get { return _ProjectName; }
set { _ProjectName = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int? Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public int? RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime? CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime? ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
