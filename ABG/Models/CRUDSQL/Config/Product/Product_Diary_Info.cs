﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Product_Diary_Info
    {

        public Product_Diary_Model Product_Diary = new Product_Diary_Model();

        #region [ Constructor Get Information ]
        public Product_Diary_Info()
        {
        }
        public Product_Diary_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM PDT_Product_Diary WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Product_Diary.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Product_Diary.AreaKey = zReader["AreaKey"].ToString();
                    Product_Diary.AreaName = zReader["AreaName"].ToString();
                    Product_Diary.AssetKey = zReader["AssetKey"].ToString();
                    Product_Diary.AssetName = zReader["AssetName"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                    {
                        Product_Diary.DateWrite = (DateTime)zReader["DateWrite"];
                    }

                    Product_Diary.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Product_Diary.EmployeeName = zReader["EmployeeName"].ToString();
                    Product_Diary.BranchKey = zReader["BranchKey"].ToString();
                    Product_Diary.BranchName = zReader["BranchName"].ToString();
                    Product_Diary.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Product_Diary.DepartmentName = zReader["DepartmentName"].ToString();
                    Product_Diary.Description = zReader["Description"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                    {
                        Product_Diary.Parent = int.Parse(zReader["Parent"].ToString());
                    }

                    Product_Diary.ContractKey = zReader["ContractKey"].ToString();
                    Product_Diary.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Product_Diary.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Product_Diary.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Product_Diary.CreatedBy = zReader["CreatedBy"].ToString();
                    Product_Diary.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Product_Diary.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Product_Diary.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product_Diary.ModifiedName = zReader["ModifiedName"].ToString();
                    Product_Diary.Message = "200 OK";
                }
                else
                {
                    Product_Diary.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Diary ("
         + " PhotoPath, AreaKey , AreaName , AssetKey , AssetName , DateWrite , EmployeeKey , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , Description , Parent , ContractKey, PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PhotoPath, @AreaKey , @AreaName , @AssetKey , @AssetName , @DateWrite , @EmployeeKey , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @Description , @Parent, @ContractKey , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Diary.PhotoPath;
                zCommand.Parameters.Add("@AreaKey", SqlDbType.NVarChar).Value = Product_Diary.AreaKey;
                zCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = Product_Diary.AreaName;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = Product_Diary.AssetKey;
                zCommand.Parameters.Add("@AssetName", SqlDbType.NVarChar).Value = Product_Diary.AssetName;
                if (Product_Diary.DateWrite == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Product_Diary.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Product_Diary.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Product_Diary.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Product_Diary.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Product_Diary.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Product_Diary.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Product_Diary.DepartmentName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Diary.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Diary.Parent;
                if (Product_Diary.PartnerNumber != "" && Product_Diary.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Product_Diary.ContractKey != "" && Product_Diary.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Diary.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Diary.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Diary.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Diary.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Diary.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Diary.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Diary("
         + " PhotoPath, AutoKey , AreaKey , AreaName , AssetKey , AssetName , DateWrite , EmployeeKey , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , Description , Parent , ContractKey,PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PhotoPath, @AutoKey , @AreaKey , @AreaName , @AssetKey , @AssetName , @DateWrite , @EmployeeKey , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @Description , @Parent, @ContractKey, @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Diary.PhotoPath;

                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Diary.AutoKey;
                zCommand.Parameters.Add("@AreaKey", SqlDbType.NVarChar).Value = Product_Diary.AreaKey;
                zCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = Product_Diary.AreaName;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = Product_Diary.AssetKey;
                zCommand.Parameters.Add("@AssetName", SqlDbType.NVarChar).Value = Product_Diary.AssetName;
                if (Product_Diary.DateWrite == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Product_Diary.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Product_Diary.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Product_Diary.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Product_Diary.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Product_Diary.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Product_Diary.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Product_Diary.DepartmentName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Diary.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Diary.Parent;
                if (Product_Diary.PartnerNumber != "" && Product_Diary.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                if (Product_Diary.ContractKey != "" && Product_Diary.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Diary.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Diary.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Diary.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Diary.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Diary.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Diary.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PDT_Product_Diary SET "
                        + " AreaKey = @AreaKey,"
                        + " AreaName = @AreaName,"
                        + " AssetKey = @AssetKey,"
                        + " AssetName = @AssetName,"
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " Description = @Description,"
                        + " Parent = @Parent, ContractKey = @ContractKey, PhotoPath = @PhotoPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Diary.PhotoPath;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Diary.AutoKey;
                zCommand.Parameters.Add("@AreaKey", SqlDbType.NVarChar).Value = Product_Diary.AreaKey;
                zCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = Product_Diary.AreaName;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = Product_Diary.AssetKey;
                zCommand.Parameters.Add("@AssetName", SqlDbType.NVarChar).Value = Product_Diary.AssetName;
                if (Product_Diary.DateWrite == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Product_Diary.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Product_Diary.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Product_Diary.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Product_Diary.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Product_Diary.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Product_Diary.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Product_Diary.DepartmentName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Diary.Description;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Product_Diary.Parent;
                if (Product_Diary.PartnerNumber != "" && Product_Diary.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                if (Product_Diary.ContractKey != "" && Product_Diary.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Diary.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Diary.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Diary.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Diary.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Diary.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product_Diary SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Diary.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Diary.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product_Diary WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Product_Diary.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Product_Diary.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Product_Diary.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
