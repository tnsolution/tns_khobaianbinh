﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Product_Equipment_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _ProductKey = "";
private string _ItemKey = "";
private string _ItemName = "";
private float _Value= 0;
private string _Description = "";
private int _Rank = 0;
private string _PartnerNumber = "";
private bool _Publish;
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string ProductKey
{
get { return _ProductKey; }
set { _ProductKey = value; }
}
public string ItemKey
{
get { return _ItemKey; }
set { _ItemKey = value; }
}
public string ItemName
{
get { return _ItemName; }
set { _ItemName = value; }
}
public float Value
{
get { return _Value; }
set { _Value = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public bool Publish
{
get { return _Publish; }
set { _Publish = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
