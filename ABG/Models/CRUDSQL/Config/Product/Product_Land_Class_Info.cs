﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Product_Land_Class_Info
{
 
public  Product_Land_Class_Model Product_Land_Class = new Product_Land_Class_Model();
 
#region [ Constructor Get Information ]
public Product_Land_Class_Info()
{
}
public Product_Land_Class_Info(int ClassKey)
{
string zSQL = "SELECT * FROM PDT_Product_Land_Class WHERE ClassKey = @ClassKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = ClassKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["ClassKey"]!= DBNull.Value)
Product_Land_Class.ClassKey = int.Parse(zReader["ClassKey"].ToString());
Product_Land_Class.ClassID = zReader["ClassID"].ToString();
Product_Land_Class.ClassName = zReader["ClassName"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Product_Land_Class.Rank = int.Parse(zReader["Rank"].ToString());
Product_Land_Class.Description = zReader["Description"].ToString();
Product_Land_Class.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Product_Land_Class.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Product_Land_Class.CreatedOn = (DateTime)zReader["CreatedOn"];
Product_Land_Class.CreatedBy = zReader["CreatedBy"].ToString();
Product_Land_Class.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Product_Land_Class.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Product_Land_Class.ModifiedBy = zReader["ModifiedBy"].ToString();
Product_Land_Class.ModifiedName = zReader["ModifiedName"].ToString();
Product_Land_Class.Message = "200 OK";
}
else
{
Product_Land_Class.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Product_Land_Class.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Land_Class (" 
 + " ClassID , ClassName , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ClassID , @ClassName , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ClassID", SqlDbType.NChar).Value = Product_Land_Class.ClassID;
zCommand.Parameters.Add("@ClassName", SqlDbType.NVarChar).Value = Product_Land_Class.ClassName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land_Class.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land_Class.Description;
if(Product_Land_Class.PartnerNumber != "" && Product_Land_Class.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land_Class.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land_Class.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Land_Class.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Land_Class.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Land_Class.Message = "201 Created";
}
catch (Exception Err)
{
Product_Land_Class.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Product_Land_Class(" 
 + " ClassKey , ClassID , ClassName , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @ClassKey , @ClassID , @ClassName , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = Product_Land_Class.ClassKey;
zCommand.Parameters.Add("@ClassID", SqlDbType.NChar).Value = Product_Land_Class.ClassID;
zCommand.Parameters.Add("@ClassName", SqlDbType.NVarChar).Value = Product_Land_Class.ClassName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land_Class.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land_Class.Description;
if(Product_Land_Class.PartnerNumber != "" && Product_Land_Class.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land_Class.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land_Class.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Land_Class.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Land_Class.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Land_Class.Message = "201 Created";
}
catch (Exception Err)
{
Product_Land_Class.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE PDT_Product_Land_Class SET " 
            + " ClassID = @ClassID,"
            + " ClassName = @ClassName,"
            + " Rank = @Rank,"
            + " Description = @Description,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE ClassKey = @ClassKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = Product_Land_Class.ClassKey;
zCommand.Parameters.Add("@ClassID", SqlDbType.NChar).Value = Product_Land_Class.ClassID;
zCommand.Parameters.Add("@ClassName", SqlDbType.NVarChar).Value = Product_Land_Class.ClassName;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Land_Class.Rank;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Product_Land_Class.Description;
if(Product_Land_Class.PartnerNumber != "" && Product_Land_Class.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Land_Class.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Land_Class.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Land_Class.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Land_Class.Message = "200 OK";
}
catch (Exception Err)
{
Product_Land_Class.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE PDT_Product_Land_Class SET RecordStatus = 99 WHERE ClassKey = @ClassKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = Product_Land_Class.ClassKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Land_Class.Message = "200 OK";
}
catch (Exception Err)
{
Product_Land_Class.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM PDT_Product_Land_Class WHERE ClassKey = @ClassKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = Product_Land_Class.ClassKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Product_Land_Class.Message = "200 OK";
}
catch (Exception Err)
{
Product_Land_Class.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
