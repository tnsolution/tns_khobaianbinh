﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Document_Info
    {

        public Document_Model Document = new Document_Model();

        #region [ Constructor Get Information ]
        public Document_Info()
        {
        }
        public Document_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM GOB_Document WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Document.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Document.FilePath = zReader["FilePath"].ToString();
                    Document.FileName = zReader["FileName"].ToString();
                    Document.FileExt = zReader["FileExt"].ToString();
                    Document.Base64 = zReader["Base64"].ToString();
                    Document.Description = zReader["Description"].ToString();
                    Document.Title = zReader["Title"].ToString();
                    if (zReader["Type"] != DBNull.Value)
                    {
                        Document.Type = int.Parse(zReader["Type"].ToString());
                    }

                    if (zReader["Category"] != DBNull.Value)
                    {
                        Document.Category = int.Parse(zReader["Category"].ToString());
                    }

                    if (zReader["Parent"] != DBNull.Value)
                    {
                        Document.Parent = int.Parse(zReader["Parent"].ToString());
                    }

                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Document.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Document.TableJoin = zReader["TableJoin"].ToString();
                    Document.TableKey = zReader["TableKey"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Document.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Document.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Document.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Document.CreatedBy = zReader["CreatedBy"].ToString();
                    Document.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Document.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Document.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Document.ModifiedName = zReader["ModifiedName"].ToString();
                    Document.Message = "200 OK";
                }
                else
                {
                    Document.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Document.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO GOB_Document ("
         + " FilePath , FileName , FileExt , Base64 , Description , Title , Type , Category , Parent , Slug , TableJoin , TableKey , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @FilePath , @FileName , @FileExt , @Base64 , @Description , @Title , @Type , @Category , @Parent , @Slug , @TableJoin , @TableKey , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = Document.FilePath;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = Document.FileName;
                zCommand.Parameters.Add("@FileExt", SqlDbType.NVarChar).Value = Document.FileExt;
                zCommand.Parameters.Add("@Base64", SqlDbType.NVarChar).Value = Document.Base64;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Document.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Document.Title;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Document.Type;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Document.Category;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Document.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Document.Slug;
                zCommand.Parameters.Add("@TableJoin", SqlDbType.NVarChar).Value = Document.TableJoin;
                zCommand.Parameters.Add("@TableKey", SqlDbType.NVarChar).Value = Document.TableKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Document.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Document.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Document.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Document.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Document.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Document.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Document.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Document.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO GOB_Document("
         + " AutoKey , FilePath , FileName , FileExt , Base64 , Description , Title , Type , Category , Parent , Slug , TableJoin , TableKey , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @FilePath , @FileName , @FileExt , @Base64 , @Description , @Title , @Type , @Category , @Parent , @Slug , @TableJoin , @TableKey , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Document.AutoKey;
                zCommand.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = Document.FilePath;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = Document.FileName;
                zCommand.Parameters.Add("@FileExt", SqlDbType.NVarChar).Value = Document.FileExt;
                zCommand.Parameters.Add("@Base64", SqlDbType.NVarChar).Value = Document.Base64;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Document.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Document.Title;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Document.Type;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Document.Category;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Document.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Document.Slug;
                zCommand.Parameters.Add("@TableJoin", SqlDbType.NVarChar).Value = Document.TableJoin;
                zCommand.Parameters.Add("@TableKey", SqlDbType.NVarChar).Value = Document.TableKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Document.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Document.PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Document.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Document.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Document.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Document.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Document.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Document.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE GOB_Document SET "
                        + " FilePath = @FilePath,"
                        + " FileName = @FileName,"
                        + " FileExt = @FileExt,"
                        + " Base64 = @Base64,"
                        + " Description = @Description,"
                        + " Title = @Title,"
                        + " Type = @Type,"
                        + " Category = @Category,"
                        + " Parent = @Parent,"
                        + " Slug = @Slug,"
                        + " TableJoin = @TableJoin,"
                        + " TableKey = @TableKey,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Document.AutoKey;
                zCommand.Parameters.Add("@FilePath", SqlDbType.NVarChar).Value = Document.FilePath;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = Document.FileName;
                zCommand.Parameters.Add("@FileExt", SqlDbType.NVarChar).Value = Document.FileExt;
                zCommand.Parameters.Add("@Base64", SqlDbType.NVarChar).Value = Document.Base64;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Document.Description;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Document.Title;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Document.Type;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Document.Category;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Document.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Document.Slug;
                zCommand.Parameters.Add("@TableJoin", SqlDbType.NVarChar).Value = Document.TableJoin;
                zCommand.Parameters.Add("@TableKey", SqlDbType.NVarChar).Value = Document.TableKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Document.RecordStatus;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Document.PartnerNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Document.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Document.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Document.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Document.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE GOB_Document SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Document.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Document.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Document.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM GOB_Document WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Document.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Document.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Document.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
