﻿using System;
using System.Collections.Generic;

namespace ABG
{
    public class User_Model
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private string _UserAPI = "";
        private string _UserName = "";
        private string _Password = "";
        private string _PIN = "";
        private string _Description = "";
        private string _GroupName = "";
        private string _PartnerNumber = "";
        private string _PartnerID = "";
        private int? _BusinessKey = 0;
        private bool? _Activate;
        private DateTime? _ExpireDate = null;
        private DateTime _LastLoginDate = DateTime.MinValue;
        private int? _FailedPasswordCount = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _EmployeePhone = "";
        private string _EmployeeEmail = "";
        private string _PhotoPath = "";
        private int? _Slug = 0;
        private int? _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string UserAPI
        {
            get { return _UserAPI; }
            set { _UserAPI = value; }
        }
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string PIN
        {
            get { return _PIN; }
            set { _PIN = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int? BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public bool? Activate
        {
            get { return _Activate; }
            set { _Activate = value; }
        }
        public DateTime? ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public DateTime LastLoginDate
        {
            get { return _LastLoginDate; }
            set { _LastLoginDate = value; }
        }
        public int? FailedPasswordCount
        {
            get { return _FailedPasswordCount; }
            set { _FailedPasswordCount = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int? Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int? RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        public List<User_Role> ListRole { get; set; }
        public string DataAccess { get; set; }

        public string EmployeePhone
        {
            get
            {
                return _EmployeePhone;
            }

            set
            {
                _EmployeePhone = value;
            }
        }

        public string EmployeeEmail
        {
            get
            {
                return _EmployeeEmail;
            }

            set
            {
                _EmployeeEmail = value;
            }
        }

        public string PhotoPath
        {
            get
            {
                return _PhotoPath;
            }

            set
            {
                _PhotoPath = value;
            }
        }

        public string PartnerID
        {
            get
            {
                return _PartnerID;
            }

            set
            {
                _PartnerID = value;
            }
        }
    }
}
