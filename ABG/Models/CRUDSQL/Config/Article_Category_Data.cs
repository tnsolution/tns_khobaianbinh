﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Article_Category_Data
    {
        public static List<Article_Category_Model> List(string PartnerNumber, out string Message, int Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Parent = @Parent ORDER BY [RANK]";        
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    WebUrl= r["WebUrl"].ToString(),
                    Slug= r["Slug"].ToInt(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Image = r["Image"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Activate = r["Activate"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Category_Model> ListCategory(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "WITH R AS ( SELECT CategoryKey, CategoryName AS NAME1, Description, Publish, PARENT, DEPTH = 0, SORT = CAST(CategoryKey AS VARCHAR(MAX)) ";
            zSQL += " FROM PDT_Article_Category WHERE RecordStatus <> 99 AND Parent = 0 AND PartnerNumber = @PartnerNumber ";
            zSQL += " UNION ALL ";
            zSQL += " SELECT Sub.CategoryKey, Sub.CategoryName AS NAME1, Sub.Description, Sub.Publish, Sub.PARENT, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)) ";
            zSQL += " FROM R INNER JOIN PDT_Article_Category Sub ON R.CategoryKey = Sub.Parent WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber) ";
            zSQL += " SELECT CategoryName = REPLICATE('---', R.DEPTH * 1) + R.[NAME1], R.CategoryKey, R.Description, R.Publish, R.SORT, R.DEPTH, R.Parent ";
            zSQL += " FROM R ORDER BY SORT  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Level = r["SORT"].ToInt(),
                });
            }
            return zList;
        }

        public static List<Article_Category_Model> Search(string PartnerNumber, out string Message, int Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Parent == 0)
                zSQL += " AND Parent != 0";
            else
                zSQL += " AND Parent = @Parent ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Image = r["Image"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Activate = r["Activate"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Category_Model> ListserviceIndex(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 3 * FROM PDT_Article_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Publish=@Publish AND Activate=@Activate AND Parent=@CategoryKey  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Image = r["Image"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Activate = r["Activate"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Category_Model> ListCategoryService(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Publish=@Publish AND Activate=@Activate AND Parent=@CategoryKey  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Image = r["Image"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Activate = r["Activate"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Article_Category_Model> ListInfo(string PartnerNumber, out string Message, bool Publish, bool Activate, int CategoryKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article_Category WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Publish=@Publish AND Activate=@Activate AND CategoryKey=@CategoryKey  ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Publish;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Image = r["Image"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Activate = r["Activate"].ToBool(),
                    Level = r["Level"].ToInt(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
