﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Article_Category_Info
    {

        public Article_Category_Model Article_Category = new Article_Category_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Article_Category_Info()
        {
        }
        public Article_Category_Info(int CategoryKey)
        {
            string zSQL = "SELECT * FROM PDT_Article_Category WHERE CategoryKey = @CategoryKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        Article_Category.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    Article_Category.CategoryName = zReader["CategoryName"].ToString();
                    Article_Category.Description = zReader["Description"].ToString();
                    Article_Category.Image = zReader["Image"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        Article_Category.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        Article_Category.Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Publish"] != DBNull.Value)
                        Article_Category.Publish = (bool)zReader["Publish"];
                    if (zReader["Activate"] != DBNull.Value)
                        Article_Category.Activate = (bool)zReader["Activate"];
                    if (zReader["Level"] != DBNull.Value)
                        Article_Category.Level = int.Parse(zReader["Level"].ToString());
                    Article_Category.CategoryPath = zReader["CategoryPath"].ToString();
                    Article_Category.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Article_Category.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Article_Category.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Article_Category.CreatedBy = zReader["CreatedBy"].ToString();
                    Article_Category.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Article_Category.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Article_Category.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Article_Category.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Article_Category ("
         + " CategoryName , Description , Image , Parent , Rank , Publish , Activate , Level , CategoryPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CategoryName , @Description , @Image , @Parent , @Rank , @Publish , @Activate , @Level , @CategoryPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Article_Category.Description;
                zCommand.Parameters.Add("@Image", SqlDbType.NVarChar).Value = Article_Category.Image;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Article_Category.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article_Category.Rank;
                if (Article_Category.Publish == null)
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article_Category.Publish;
                if (Article_Category.Activate == null)
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article_Category.Activate;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Article_Category.Level;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Article_Category.CategoryPath;
                if (Article_Category.PartnerNumber != "" && Article_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article_Category.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Article_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Article_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Article_Category("
         + " CategoryKey , CategoryName , Description , Image , Parent , Rank , Publish , Activate , Level , CategoryPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CategoryKey , @CategoryName , @Description , @Image , @Parent , @Rank , @Publish , @Activate , @Level , @CategoryPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article_Category.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Article_Category.Description;
                zCommand.Parameters.Add("@Image", SqlDbType.NVarChar).Value = Article_Category.Image;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Article_Category.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article_Category.Rank;
                if (Article_Category.Publish == null)
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article_Category.Publish;
                if (Article_Category.Activate == null)
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article_Category.Activate;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Article_Category.Level;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Article_Category.CategoryPath;
                if (Article_Category.PartnerNumber != "" && Article_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article_Category.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article_Category.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Article_Category.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Article_Category.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Article_Category SET "
                        + " CategoryName = @CategoryName,"
                        + " Description = @Description,"
                        + " Image = @Image,"
                        + " Parent = @Parent,"
                        + " Rank = @Rank,"
                        + " Publish = @Publish,"
                        + " Activate = @Activate,"
                        + " Level = @Level,"
                        + " CategoryPath = @CategoryPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CategoryKey = @CategoryKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article_Category.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article_Category.CategoryName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Article_Category.Description;
                zCommand.Parameters.Add("@Image", SqlDbType.NVarChar).Value = Article_Category.Image;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Article_Category.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article_Category.Rank;
                if (Article_Category.Publish == null)
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article_Category.Publish;
                if (Article_Category.Activate == null)
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article_Category.Activate;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Article_Category.Level;
                zCommand.Parameters.Add("@CategoryPath", SqlDbType.NVarChar).Value = Article_Category.CategoryPath;
                if (Article_Category.PartnerNumber != "" && Article_Category.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article_Category.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article_Category.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Article_Category SET RecordStatus = 99 WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Article_Category WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article_Category.CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string SetPublish(int CategoryKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Article_Category SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE CategoryKey = @CategoryKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article_Category.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article_Category.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        #endregion
    }
}
