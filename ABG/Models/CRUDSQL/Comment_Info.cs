﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Comment_Info
{
 
public  Comment_Model Comment = new Comment_Model();
private string _Message = "";
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
 
#region [ Constructor Get Information ]
public Comment_Info()
{
Comment.CommentKey = Guid.NewGuid().ToString();
}
public Comment_Info(string CommentKey)
{
string zSQL = "SELECT * FROM PDT_Comment WHERE CommentKey = @CommentKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(CommentKey);
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
Comment.CommentKey = zReader["CommentKey"].ToString();
Comment.CommentID = zReader["CommentID"].ToString();
Comment.ProductKey = zReader["ProductKey"].ToString();
Comment.ProductName = zReader["ProductName"].ToString();
if (zReader["CommentDate"]!= DBNull.Value)
Comment.CommentDate = (DateTime)zReader["CommentDate"];
Comment.Description = zReader["Description"].ToString();
if (zReader["Rate"]!= DBNull.Value)
Comment.Rate = int.Parse(zReader["Rate"].ToString());
Comment.CustomerKey = zReader["CustomerKey"].ToString();
Comment.CustomerName = zReader["CustomerName"].ToString();
Comment.CustomerEmail = zReader["CustomerEmail"].ToString();
Comment.Referrer = zReader["Referrer"].ToString();
Comment.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Comment.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Comment.CreatedOn = (DateTime)zReader["CreatedOn"];
Comment.CreatedBy = zReader["CreatedBy"].ToString();
Comment.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Comment.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Comment.ModifiedBy = zReader["ModifiedBy"].ToString();
Comment.ModifiedName = zReader["ModifiedName"].ToString();
_Message = "200 OK";
}
else
{
_Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Comment (" 
 + " CommentID , ProductKey , ProductName , CommentDate , Description , Rate , CustomerKey , CustomerName , CustomerEmail , Referrer , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @CommentID , @ProductKey , @ProductName , @CommentDate , @Description , @Rate , @CustomerKey , @CustomerName , @CustomerEmail , @Referrer , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@CommentID", SqlDbType.NVarChar).Value = Comment.CommentID;
if(Comment.ProductKey != "" && Comment.ProductKey.Length == 36)
{
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.ProductKey);
}
else
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Comment.ProductName;
if (Comment.CommentDate == DateTime.MinValue) 
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = Comment.CommentDate;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Comment.Description;
zCommand.Parameters.Add("@Rate", SqlDbType.Int).Value = Comment.Rate;
zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Comment.CustomerKey;
zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Comment.CustomerName;
zCommand.Parameters.Add("@CustomerEmail", SqlDbType.NVarChar).Value = Comment.CustomerEmail;
zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Comment.Referrer;
if(Comment.PartnerNumber != "" && Comment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Comment.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Comment.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Comment.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Comment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Comment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "201 Created";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO PDT_Comment(" 
 + " CommentKey , CommentID , ProductKey , ProductName , CommentDate , Description , Rate , CustomerKey , CustomerName , CustomerEmail , Referrer , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @CommentKey , @CommentID , @ProductKey , @ProductName , @CommentDate , @Description , @Rate , @CustomerKey , @CustomerName , @CustomerEmail , @Referrer , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Comment.CommentKey != "" && Comment.CommentKey.Length == 36)
{
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.CommentKey);
}
else
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@CommentID", SqlDbType.NVarChar).Value = Comment.CommentID;
if(Comment.ProductKey != "" && Comment.ProductKey.Length == 36)
{
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.ProductKey);
}
else
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Comment.ProductName;
if (Comment.CommentDate == DateTime.MinValue) 
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = Comment.CommentDate;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Comment.Description;
zCommand.Parameters.Add("@Rate", SqlDbType.Int).Value = Comment.Rate;
zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Comment.CustomerKey;
zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Comment.CustomerName;
zCommand.Parameters.Add("@CustomerEmail", SqlDbType.NVarChar).Value = Comment.CustomerEmail;
zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Comment.Referrer;
if(Comment.PartnerNumber != "" && Comment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Comment.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Comment.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Comment.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Comment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Comment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "201 Created";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE PDT_Comment SET " 
            + " CommentID = @CommentID,"
            + " ProductKey = @ProductKey,"
            + " ProductName = @ProductName,"
            + " CommentDate = @CommentDate,"
            + " Description = @Description,"
            + " Rate = @Rate,"
            + " CustomerKey = @CustomerKey,"
            + " CustomerName = @CustomerName,"
            + " CustomerEmail = @CustomerEmail,"
            + " Referrer = @Referrer,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE CommentKey = @CommentKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
if(Comment.CommentKey != "" && Comment.CommentKey.Length == 36)
{
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.CommentKey);
}
else
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@CommentID", SqlDbType.NVarChar).Value = Comment.CommentID;
if(Comment.ProductKey != "" && Comment.ProductKey.Length == 36)
{
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.ProductKey);
}
else
zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Comment.ProductName;
if (Comment.CommentDate == DateTime.MinValue) 
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@CommentDate", SqlDbType.DateTime).Value = Comment.CommentDate;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Comment.Description;
zCommand.Parameters.Add("@Rate", SqlDbType.Int).Value = Comment.Rate;
zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = Comment.CustomerKey;
zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Comment.CustomerName;
zCommand.Parameters.Add("@CustomerEmail", SqlDbType.NVarChar).Value = Comment.CustomerEmail;
zCommand.Parameters.Add("@Referrer", SqlDbType.NVarChar).Value = Comment.Referrer;
if(Comment.PartnerNumber != "" && Comment.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Comment.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Comment.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Comment.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE PDT_Comment SET RecordStatus = 99 WHERE CommentKey = @CommentKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.CommentKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM PDT_Comment WHERE CommentKey = @CommentKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@CommentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Comment.CommentKey);
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
_Message = "200 OK";
}
catch (Exception Err)
{
_Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
