﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABG
{
    public class LeaveView
    {
        public int Year { get; set; }
        public int STD { get; set; }
        public int TN { get; set; }
        public int Begin { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public float TotalDate { get; set; }
        public float END { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
    }
}