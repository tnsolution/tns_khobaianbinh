﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABG
{
    public class LeaveIncurred
    {
        public string NoteKey { get; set; } = "";
        public DateTime NoteDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; } = "";
        public float TotalDate { get; set; }
    }
}