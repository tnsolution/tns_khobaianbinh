﻿namespace ABG
{
    public class Leave_Item
    {
        public string EmployeeKey { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeID { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
        public string Sort { get; set; }
        public string BeginYear { get; set; }
        public string BeginNo { get; set; }
        public string BeginPlus { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string TotalDate { get; set; }
        public string CategoryName { get; set; }
        public string CloseYear { get; set; }
        public string CloseEnd { get; set; }
        public string Incremental { get; set; }
        public string Description { get; set; }
        
    }
}