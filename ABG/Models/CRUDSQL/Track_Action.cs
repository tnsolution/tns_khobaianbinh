﻿
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace ABG
{
    public class TrackingActionFilter : ActionFilterAttribute
    {
        public static T GetAttributeFrom<T>(object instance, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = instance.GetType().GetMethod(propertyName);
            return (T)property.GetCustomAttributes(attrType, false).First();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var Session = filterContext.HttpContext.Session["User_Model"];

            if (Session != null)
            {
                var User = Session as User_Model;

                Track_Item zTrack = new Track_Item();
                var url = filterContext.HttpContext.Request.Url;
                zTrack.Url = url.ToString();

                var actionName = filterContext.ActionDescriptor.ActionName;
                zTrack.ActionName = actionName;

                var method = filterContext.HttpContext.Request.HttpMethod;
                zTrack.MethodType = method;

                var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                zTrack.ControllerName = controllerName;

                Assembly asm = Assembly.GetExecutingAssembly();

                var listcontrol = asm.GetTypes()
                       .Where(type => typeof(Controller).IsAssignableFrom(type)) //filter controllers
                       .SelectMany(type => type.GetMethods())
                       .Where(m => m.IsPublic && !m.IsDefined(typeof(NonActionAttribute)));

                foreach (var c in listcontrol)
                {
                    var dname = c.GetCustomAttribute<DisplayNameAttribute>();
                    var cName = c.DeclaringType?.Name.Replace("Controller", "");
                    var aName = c.Name;
                    var p = c.GetParameters();

                    if (actionName == aName &&
                        controllerName == cName &&
                        dname != null)
                    {
                        string store = JsonConvert.SerializeObject(zTrack);
                        string desc = dname.DisplayName;

                        if (url.Query.Contains(p[0].Name))
                        {
                            var key = url.Query.Split('=')[1];
                            desc += " [" + new Product_Land_Info(key).Product_Land.ProductName + "]";
                        }

                        string SQL = "INSERT INTO SYS_User_Track ("
             + " JsonData , Description , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
             + " VALUES ( "
             + " N'" + store + "' , N'" + desc + "' , N'" + User.UserKey + "' , N'" + User.EmployeeName + "' , N'" + User.UserKey + "' , N'" + User.EmployeeName + "' ) ";

                        Helper.RunSQL(SQL, out string Message);
                        break;
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}