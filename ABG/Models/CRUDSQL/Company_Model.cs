﻿using System;
namespace ABG
{
    public partial class Company_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _Time = "";
        private string _Title1 = "";
        private string _Title2 = "";
        private string _Title3 = "";
        private string _Description = "";
        private string _FileAttack = "";
        private int _Parent = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
        public string Title1
        {
            get { return _Title1; }
            set { _Title1 = value; }
        }
        public string Title2
        {
            get { return _Title2; }
            set { _Title2 = value; }
        }
        public string Title3
        {
            get { return _Title3; }
            set { _Title3 = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string FileAttack
        {
            get { return _FileAttack; }
            set { _FileAttack = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}