﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Banner_Data
    {
        public static List<Banner_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Banner WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY RANK ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Banner_Model> zList = new List<Banner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Banner_Model()
                {
                    BannerKey = r["BannerKey"].ToInt(),
                    Url = r["Url"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                    Partnernumber = r["Partnernumber"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    ContentPublish = r["ContentPublish"].ToBool(),
                    ContentTemplate = r["ContentTemplate"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Banner_Model> List(string PartnerNumber, out string Message, bool Publish)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Banner WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Publish=@Publish ORDER BY RANK ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Publish", SqlDbType.NVarChar).Value = Publish;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Banner_Model> zList = new List<Banner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Banner_Model()
                {
                    BannerKey = r["BannerKey"].ToInt(),
                    Url = r["Url"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                    Partnernumber = r["Partnernumber"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    ContentPublish = r["ContentPublish"].ToBool(),
                    ContentTemplate = r["ContentTemplate"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
