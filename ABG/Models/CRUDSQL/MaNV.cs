﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    public class MaNV
    {
        public List<string> MaNVHetHan30 { get; set; }
        public List<string> MaNVVoHan { get; set; }
        public List<string> MaNVCoHan { get; set; }
    }