﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Company_Data
    {
        public static List<Company_Model> List(out string Message, int slug)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM INF_Company WHERE RecordStatus != 99 AND Slug = " + slug;
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = zTable.ToList<Company_Model>();
            return zList;
        }
    }
}
