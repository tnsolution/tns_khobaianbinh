﻿using System.Collections.Generic;
using ABG.Employee;

namespace ABG.Models.Employee
{
    public class General
    {
        public Employee_Object Employee { get; set; }
        public List<Experience_Model> ListExp { get; set; }
        public List<Education_Model> ListEdu { get; set; }
        public List<Skill_Model> ListSkill { get; set; }
    }

    public class History
    {
        public List<Contract_Model> ListContract { get; set; }
    }
}