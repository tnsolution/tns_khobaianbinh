﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG.Employee
{
    public class Contract_Data
    {
        string PartnerNumber = "184FDCA5-CCB4-41EC-BE98-85CD3537989B";
        public static List<Contract_Model> ChiLayHopDong(string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  
A.ContractKey, A.ContractType, A.EmployeeKey,
A.SignDate, A.FromDate, A.ToDate
FROM HRM_Contract A 
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' 
AND A.EmployeeKey = @EmployeeKey
";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    ContractKey = r["ContractKey"].ToString(),
                    ContractType = r["ContractType"].ToInt(),

                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
        public static List<Employee_Model> ListContract()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD, 
A.LastName, A.FirstName, A.EmployeeKey, A.WorkingStatusKey, A.BranchName
FROM HRM_Employee A 
WHERE 
A.RecordStatus <> 99 
AND A.WorkingStatusKey = 1 
AND PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
ORDER BY BranchName DESC, PositionKey";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            var zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    BranchName = r["BranchName"].ToString(),
                    WorkingStatusKey = r["WorkingStatusKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    LastName = r["LastName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    HDLD = r["HDLD"].ToString(),
                });
            }
            return zList;
        }
        public static List<Employee_Model> ListContractBranch(string Branch)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD, 
A.LastName, A.FirstName, A.EmployeeKey, A.WorkingStatusKey, A.BranchName
FROM HRM_Employee A 
WHERE 
A.RecordStatus <> 99 
AND A.WorkingStatusKey = 1 ";
            if (Branch != "")
            {
                zSQL += " AND A.BranchKey = @Branch";
            }

            zSQL += " AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' ORDER BY PositionKey";
            zSQL += " ORDER BY BranchName DESC, PositionKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Branch", SqlDbType.NVarChar).Value = Branch;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            var zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    BranchName = r["BranchName"].ToString(),
                    WorkingStatusKey = r["WorkingStatusKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    LastName = r["LastName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    HDLD = r["HDLD"].ToString(),
                });
            }
            return zList;
        }
        public static List<Employee_Model> ListContractEmployee(string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD, 
A.LastName, A.FirstName, A.EmployeeKey, A.WorkingStatusKey
FROM HRM_Employee A 
WHERE 
A.RecordStatus <> 99 
AND A.WorkingStatusKey = 1 
AND A.EmployeeKey = @EmployeeKey
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
ORDER BY PositionKey";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            var zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    WorkingStatusKey = r["WorkingStatusKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    LastName = r["LastName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    HDLD = r["HDLD"].ToString(),
                });
            }
            return zList;
        }
        public static List<Employee_Model> ListContractEmployeeIN(string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD, 
A.LastName, A.FirstName, A.EmployeeKey, A.WorkingStatusKey
FROM HRM_Employee A 
WHERE 
A.RecordStatus <> 99 
AND A.WorkingStatusKey = 1 
AND A.EmployeeKey IN (" + Employee + @")
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
ORDER BY PositionKey";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            var zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    WorkingStatusKey = r["WorkingStatusKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    LastName = r["LastName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    HDLD = r["HDLD"].ToString(),
                });
            }
            return zList;
        }
        public static List<Contract_Model> ListContract(string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  
A.ContractKey, A.ContractID, A.SubContract, C.ContractTypeNameVN AS ContractTypeName, 
A.SignDate, A.FromDate, A.ToDate, A.EmployeeName, A.Note,
B.EmployeeID, B.BranchName, B.PositionName 
FROM HRM_Contract A 
LEFT JOIN HRM_ListContractType C ON A.ContractType = C.ContractTypeKey
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
WHERE 
A.RecordStatus != 99 
AND B.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND A.EmployeeKey = @EmployeeKey";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    Note = r["Note"].ToString(),
                    SubContract = r["SubContract"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }
            return zList;
        }


        public static List<Contract_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  
A.ContractKey, A.ContractID, C.ContractTypeNameVN AS ContractTypeName, SubContract,
A.SignDate, A.FromDate, A.ToDate, A.EmployeeName,
B.EmployeeID, B.BranchName, B.PositionName 
FROM HRM_Contract A 
LEFT JOIN HRM_ListContractType C ON A.ContractType = C.ContractTypeKey
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    SubContract = r["SubContract"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
        public static List<Contract_Model> ListBranch(string Branch)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  
A.ContractKey, A.ContractID, A.ContractTypeName, 
A.SignDate, A.FromDate, A.ToDate, A.EmployeeName,
B.EmployeeID, B.BranchName, B.PositionName 
FROM HRM_Contract A 
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' 
AND A.Branch = @Branch";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Branch", SqlDbType.NVarChar).Value = Branch;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
        public static List<Contract_Model> List(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ContractKey, A.ContractID, A.SubContract, A.ContractTypeName, A.SignDate, 
A.PositionName, A.EmployeeName, A.BranchAdress, A.DepartmentName, A.ReportToName, 
A.FromDate, A.ToDate, A.Note, X.FilePath, B.Total
FROM HRM_Contract A 
LEFT JOIN HRM_Contract_Detail B ON A.ContractKey = B.ContractKey
CROSS APPLY (SELECT TOP 1 B.FilePath FROM GOB_Document B WHERE B.TableKey = A.ContractKey) 
X
WHERE 
A.RecordStatus != 99 
AND B.ItemKey = 81
AND B.RecordStatus != 99 
AND A.employeekey = @EmployeeKey
ORDER BY A.ContractID, SubContract";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                    DepartmentName = r["DepartmentName"].ToString(),
                    Note = r["Note"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    SubContract = r["SubContract"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    ReportToName = r["ReportToName"].ToString(),
                    BranchAdress = r["BranchAdress"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    SalaryGross = r["Total"].ToDouble(),
                    FilePath = r["FilePath"].ToString(),
                });
            }

            return zList;
        }
        public static List<Contract_Model> Search(
            string PartnerNumber, string SearchName, string Department,
            DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender,
            string Edu, string Position, int Status, string Social)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.ContractKey, A.ContractID, A.ContractTypeName, SubContract,
A.SignDate, A.FromDate, A.ToDate, A.EmployeeName,
B.EmployeeID, B.BranchName, B.PositionName
FROM HRM_Contract A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_ListPosition C ON C.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department D ON D.DepartmentKey = B.DepartmentKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Status != 0)
            {
                zSQL += " AND B.WorkingStatusKey = @Status";
            }
            if (Social != string.Empty)
                zSQL += " AND B.Style = @Social";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND B.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND B.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND B.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND EmployeeName LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, B.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND B.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND B.PositionKey = @Position";
            }
            zSQL += " ORDER BY C.[Rank] ASC, D.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@Social", SqlDbType.NVarChar).Value = Social;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }
                FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }
                ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    SubContract = r["SubContract"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
    }
}
