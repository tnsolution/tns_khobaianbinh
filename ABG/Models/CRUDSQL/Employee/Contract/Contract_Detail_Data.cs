﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Contract_Detail_Data
    {
        public static List<Contract_Detail_Model> List(string PartnerNumber, string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Contract_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ContractKey = @ContractKey AND ItemType = 1";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Detail_Model> zList = new List<Contract_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Contract_Detail_Model() {
                    AutoKey = r["AutoKey"].ToInt(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ContractKey = r["ContractKey"].ToString(),
                    ItemName = r["ItemName"].ToString(),                    
                    Description = r["Description"].ToString(),
                    Total = r["Total"].ToDouble(),
                    Rank = r["Rank"].ToInt(),
                });
            }

            return zList;
        }
        public static List<Contract_Detail_Model> ListMinus(string PartnerNumber, string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Contract_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ContractKey = @ContractKey AND ItemType=2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Detail_Model> zList = new List<Contract_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Contract_Detail_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ContractKey = r["ContractKey"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Description = r["Description"].ToString(),
                    Quantity = r["Quantity"].ToFloat(),
                    Total = r["Total"].ToDouble(),
                    Rank = r["Rank"].ToInt(),
                });
            }

            return zList;
        }
    }
}
