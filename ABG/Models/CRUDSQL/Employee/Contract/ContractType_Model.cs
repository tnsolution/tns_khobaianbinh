﻿using System;
namespace ABG
{
    public class ContractType_Model
    {
        #region [ Field Name ]
        private int _ContractTypeKey = 0;
        private string _ContractTypeNameEN = "";
        private string _ContractTypeNameVN = "";
        private string _ContractTypeNameCN = "";
        private string _ContractTypeID = "";
        private int _Parent = 0;
        private string _Description = "";
        private int _Rank = 0;
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int ContractTypeKey
        {
            get { return _ContractTypeKey; }
            set { _ContractTypeKey = value; }
        }
        public string ContractTypeNameEN
        {
            get { return _ContractTypeNameEN; }
            set { _ContractTypeNameEN = value; }
        }
        public string ContractTypeNameVN
        {
            get { return _ContractTypeNameVN; }
            set { _ContractTypeNameVN = value; }
        }
        public string ContractTypeNameCN
        {
            get { return _ContractTypeNameCN; }
            set { _ContractTypeNameCN = value; }
        }
        public string ContractTypeID
        {
            get { return _ContractTypeID; }
            set { _ContractTypeID = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
