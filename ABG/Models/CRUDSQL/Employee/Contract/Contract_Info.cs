﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG.Employee
{
    public class Contract_Info
    {
        public Contract_Model Contract = new Contract_Model();

        #region [ Constructor Get Information ]
        public Contract_Info()
        {
            Contract.ContractKey = Guid.NewGuid().ToString();
        }
        public Contract_Info(string ContractKey)
        {
            string zSQL = "SELECT * FROM HRM_Contract WHERE ContractKey = @ContractKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ContractKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Contract.ContractKey = zReader["ContractKey"].ToString();
                    Contract.ContractID = zReader["ContractID"].ToString();
                    Contract.SubContract = zReader["SubContract"].ToString();
                    Contract.ContractName = zReader["ContractName"].ToString();
                    if (zReader["ContractType"] != DBNull.Value)
                    {
                        Contract.ContractType = int.Parse(zReader["ContractType"].ToString());
                    }

                    Contract.ContractTypeName = zReader["ContractTypeName"].ToString();
                    Contract.Parent = zReader["Parent"].ToString();
                    if (zReader["SignDate"] != DBNull.Value)
                    {
                        Contract.SignDate = (DateTime)zReader["SignDate"];
                    }

                    Contract.SignBy = zReader["SignBy"].ToString();
                    Contract.SignName = zReader["SignName"].ToString();
                    if (zReader["SignPositionKey"] != DBNull.Value)
                    {
                        Contract.SignPositionKey = int.Parse(zReader["SignPositionKey"].ToString());
                    }

                    Contract.SignPositionName = zReader["SignPositionName"].ToString();
                    Contract.SignAddress = zReader["SignAddress"].ToString();
                    Contract.SignCompany = zReader["SignCompany"].ToString();
                    Contract.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Contract.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["EmployeeBirthDay"] != DBNull.Value)
                    {
                        Contract.EmployeeBirthDay = (DateTime)zReader["EmployeeBirthDay"];
                    }

                    Contract.EmployeeBirthPlace = zReader["EmployeeBirthPlace"].ToString();
                    Contract.EmployeeGender = zReader["EmployeeGender"].ToString();
                    Contract.EmployeePassport = zReader["EmployeePassport"].ToString();
                    if (zReader["EmployeeIssueDate"] != DBNull.Value)
                    {
                        Contract.EmployeeIssueDate = (DateTime)zReader["EmployeeIssueDate"];
                    }

                    Contract.EmployeeIssuePlace = zReader["EmployeeIssuePlace"].ToString();
                    Contract.EmployeeAdress = zReader["EmployeeAdress"].ToString();
                    Contract.EmployeeInsurceNumber = zReader["EmployeeInsurceNumber"].ToString();
                    Contract.BranchKey = zReader["BranchKey"].ToString();
                    Contract.BranchName = zReader["BranchName"].ToString();
                    Contract.BranchAdress = zReader["BranchAdress"].ToString();
                    Contract.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Contract.DepartmentName = zReader["DepartmentName"].ToString();
                    Contract.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Contract.ReportToKey = zReader["ReportToKey"].ToString();
                    Contract.ReportToName = zReader["ReportToName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Contract.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Contract.PositionName = zReader["PositionName"].ToString();
                    Contract.JobName = zReader["JobName"].ToString();
                    if (zReader["ProbationFromDate"] != DBNull.Value)
                    {
                        Contract.ProbationFromDate = (DateTime)zReader["ProbationFromDate"];
                    }

                    if (zReader["ProbationToDate"] != DBNull.Value)
                    {
                        Contract.ProbationToDate = (DateTime)zReader["ProbationToDate"];
                    }

                    if (zReader["ProbationSalary"] != DBNull.Value)
                    {
                        Contract.ProbationSalary = double.Parse(zReader["ProbationSalary"].ToString());
                    }

                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Contract.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Contract.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["DateBeginSalary"] != DBNull.Value)
                    {
                        Contract.DateBeginSalary = (DateTime)zReader["DateBeginSalary"];
                    }

                    if (zReader["SalaryBasic"] != DBNull.Value)
                    {
                        Contract.SalaryBasic = double.Parse(zReader["SalaryBasic"].ToString());
                    }

                    if (zReader["SalaryNet"] != DBNull.Value)
                    {
                        Contract.SalaryNet = double.Parse(zReader["SalaryNet"].ToString());
                    }

                    if (zReader["SalaryGross"] != DBNull.Value)
                    {
                        Contract.SalaryGross = double.Parse(zReader["SalaryGross"].ToString());
                    }

                    if (zReader["SalaryInsurce"] != DBNull.Value)
                    {
                        Contract.SalaryInsurce = double.Parse(zReader["SalaryInsurce"].ToString());
                    }

                    if (zReader["DateJoinInsurce"] != DBNull.Value)
                    {
                        Contract.DateJoinInsurce = (DateTime)zReader["DateJoinInsurce"];
                    }

                    if (zReader["DateSeniority"] != DBNull.Value)
                    {
                        Contract.DateSeniority = (DateTime)zReader["DateSeniority"];
                    }

                    if (zReader["DateBeginPaidLeave"] != DBNull.Value)
                    {
                        Contract.DateBeginPaidLeave = (DateTime)zReader["DateBeginPaidLeave"];
                    }

                    if (zReader["Activate"] != DBNull.Value)
                    {
                        Contract.Activate = (bool)zReader["Activate"];
                    }

                    Contract.Note = zReader["Note"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Contract.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Contract.Style = zReader["Style"].ToString();
                    Contract.Class = zReader["Class"].ToString();
                    Contract.CodeLine = zReader["CodeLine"].ToString();
                    Contract.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Contract.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Contract.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Contract.CreatedBy = zReader["CreatedBy"].ToString();
                    Contract.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Contract.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Contract.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Contract.ModifiedName = zReader["ModifiedName"].ToString();
                    Contract.Message = "200 OK";
                }
                else
                {
                    Contract.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Contract ("
         + " ContractID , SubContract , ContractName , ContractType , ContractTypeName , Parent , SignDate , SignBy , SignName , SignPositionKey , SignPositionName , SignAddress , SignCompany , EmployeeKey , EmployeeName , EmployeeBirthDay , EmployeeBirthPlace , EmployeeGender , EmployeePassport , EmployeeIssueDate , EmployeeIssuePlace , EmployeeAdress , EmployeeInsurceNumber , BranchKey , BranchName , BranchAdress , DepartmentKey , DepartmentName , OrganizationPath , ReportToKey , ReportToName , PositionKey , PositionName , JobName , ProbationFromDate , ProbationToDate , ProbationSalary , FromDate , ToDate , DateBeginSalary , SalaryBasic , SalaryNet , SalaryGross , SalaryInsurce , DateJoinInsurce , DateSeniority , DateBeginPaidLeave , Activate , Note , Slug , Style , Class , CodeLine , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractID , @SubContract , @ContractName , @ContractType , @ContractTypeName , @Parent , @SignDate , @SignBy , @SignName , @SignPositionKey , @SignPositionName , @SignAddress , @SignCompany , @EmployeeKey , @EmployeeName , @EmployeeBirthDay , @EmployeeBirthPlace , @EmployeeGender , @EmployeePassport , @EmployeeIssueDate , @EmployeeIssuePlace , @EmployeeAdress , @EmployeeInsurceNumber , @BranchKey , @BranchName , @BranchAdress , @DepartmentKey , @DepartmentName , @OrganizationPath , @ReportToKey , @ReportToName , @PositionKey , @PositionName , @JobName , @ProbationFromDate , @ProbationToDate , @ProbationSalary , @FromDate , @ToDate , @DateBeginSalary , @SalaryBasic , @SalaryNet , @SalaryGross , @SalaryInsurce , @DateJoinInsurce , @DateSeniority , @DateBeginPaidLeave , @Activate , @Note , @Slug , @Style , @Class , @CodeLine , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                zCommand.Parameters.Add("@ContractType", SqlDbType.Int).Value = Contract.ContractType;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Contract.ContractTypeName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                if (Contract.SignDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = Contract.SignDate;
                }

                if (Contract.SignBy != "" && Contract.SignBy.Length == 36)
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SignBy);
                }
                else
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SignName", SqlDbType.NVarChar).Value = Contract.SignName;
                zCommand.Parameters.Add("@SignPositionKey", SqlDbType.Int).Value = Contract.SignPositionKey;
                zCommand.Parameters.Add("@SignPositionName", SqlDbType.NVarChar).Value = Contract.SignPositionName;
                zCommand.Parameters.Add("@SignAddress", SqlDbType.NVarChar).Value = Contract.SignAddress;
                zCommand.Parameters.Add("@SignCompany", SqlDbType.NVarChar).Value = Contract.SignCompany;
                if (Contract.EmployeeKey != "" && Contract.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Contract.EmployeeName;
                if (Contract.EmployeeBirthDay == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = Contract.EmployeeBirthDay;
                }

                zCommand.Parameters.Add("@EmployeeBirthPlace", SqlDbType.NVarChar).Value = Contract.EmployeeBirthPlace;
                zCommand.Parameters.Add("@EmployeeGender", SqlDbType.NVarChar).Value = Contract.EmployeeGender;
                zCommand.Parameters.Add("@EmployeePassport", SqlDbType.NVarChar).Value = Contract.EmployeePassport;
                if (Contract.EmployeeIssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = Contract.EmployeeIssueDate;
                }

                zCommand.Parameters.Add("@EmployeeIssuePlace", SqlDbType.NVarChar).Value = Contract.EmployeeIssuePlace;
                zCommand.Parameters.Add("@EmployeeAdress", SqlDbType.NVarChar).Value = Contract.EmployeeAdress;
                zCommand.Parameters.Add("@EmployeeInsurceNumber", SqlDbType.NVarChar).Value = Contract.EmployeeInsurceNumber;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Contract.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Contract.BranchName;
                zCommand.Parameters.Add("@BranchAdress", SqlDbType.NVarChar).Value = Contract.BranchAdress;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Contract.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Contract.DepartmentName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Contract.OrganizationPath;
                if (Contract.ReportToKey != "" && Contract.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Contract.ReportToName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Contract.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Contract.PositionName;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Contract.JobName;
                if (Contract.ProbationFromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = Contract.ProbationFromDate;
                }

                if (Contract.ProbationToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = Contract.ProbationToDate;
                }

                zCommand.Parameters.Add("@ProbationSalary", SqlDbType.Money).Value = Contract.ProbationSalary;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Contract.ToDate;
                }

                if (Contract.DateBeginSalary == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = Contract.DateBeginSalary;
                }

                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = Contract.SalaryBasic;
                zCommand.Parameters.Add("@SalaryNet", SqlDbType.Money).Value = Contract.SalaryNet;
                zCommand.Parameters.Add("@SalaryGross", SqlDbType.Money).Value = Contract.SalaryGross;
                zCommand.Parameters.Add("@SalaryInsurce", SqlDbType.Money).Value = Contract.SalaryInsurce;
                if (Contract.DateJoinInsurce == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = Contract.DateJoinInsurce;
                }

                if (Contract.DateSeniority == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = Contract.DateSeniority;
                }

                if (Contract.DateBeginPaidLeave == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = Contract.DateBeginPaidLeave;
                }

                if (Contract.Activate == null)
                {
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Contract.Activate;
                }

                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Contract.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Contract.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract.CodeLine;
                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Contract("
         + " ContractKey , ContractID , SubContract , ContractName , ContractType , ContractTypeName , Parent , SignDate , SignBy , SignName , SignPositionKey , SignPositionName , SignAddress , SignCompany , EmployeeKey , EmployeeName , EmployeeBirthDay , EmployeeBirthPlace , EmployeeGender , EmployeePassport , EmployeeIssueDate , EmployeeIssuePlace , EmployeeAdress , EmployeeInsurceNumber , BranchKey , BranchName , BranchAdress , DepartmentKey , DepartmentName , OrganizationPath , ReportToKey , ReportToName , PositionKey , PositionName , JobName , ProbationFromDate , ProbationToDate , ProbationSalary , FromDate , ToDate , DateBeginSalary , SalaryBasic , SalaryNet , SalaryGross , SalaryInsurce , DateJoinInsurce , DateSeniority , DateBeginPaidLeave , Activate , Note , Slug , Style , Class , CodeLine , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractKey , @ContractID , @SubContract , @ContractName , @ContractType , @ContractTypeName , @Parent , @SignDate , @SignBy , @SignName , @SignPositionKey , @SignPositionName , @SignAddress , @SignCompany , @EmployeeKey , @EmployeeName , @EmployeeBirthDay , @EmployeeBirthPlace , @EmployeeGender , @EmployeePassport , @EmployeeIssueDate , @EmployeeIssuePlace , @EmployeeAdress , @EmployeeInsurceNumber , @BranchKey , @BranchName , @BranchAdress , @DepartmentKey , @DepartmentName , @OrganizationPath , @ReportToKey , @ReportToName , @PositionKey , @PositionName , @JobName , @ProbationFromDate , @ProbationToDate , @ProbationSalary , @FromDate , @ToDate , @DateBeginSalary , @SalaryBasic , @SalaryNet , @SalaryGross , @SalaryInsurce , @DateJoinInsurce , @DateSeniority , @DateBeginPaidLeave , @Activate , @Note , @Slug , @Style , @Class , @CodeLine , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract.ContractKey != "" && Contract.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                zCommand.Parameters.Add("@ContractType", SqlDbType.Int).Value = Contract.ContractType;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Contract.ContractTypeName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                if (Contract.SignDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = Contract.SignDate;
                }

                if (Contract.SignBy != "" && Contract.SignBy.Length == 36)
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SignBy);
                }
                else
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SignName", SqlDbType.NVarChar).Value = Contract.SignName;
                zCommand.Parameters.Add("@SignPositionKey", SqlDbType.Int).Value = Contract.SignPositionKey;
                zCommand.Parameters.Add("@SignPositionName", SqlDbType.NVarChar).Value = Contract.SignPositionName;
                zCommand.Parameters.Add("@SignAddress", SqlDbType.NVarChar).Value = Contract.SignAddress;
                zCommand.Parameters.Add("@SignCompany", SqlDbType.NVarChar).Value = Contract.SignCompany;
                if (Contract.EmployeeKey != "" && Contract.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Contract.EmployeeName;
                if (Contract.EmployeeBirthDay == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = Contract.EmployeeBirthDay;
                }

                zCommand.Parameters.Add("@EmployeeBirthPlace", SqlDbType.NVarChar).Value = Contract.EmployeeBirthPlace;
                zCommand.Parameters.Add("@EmployeeGender", SqlDbType.NVarChar).Value = Contract.EmployeeGender;
                zCommand.Parameters.Add("@EmployeePassport", SqlDbType.NVarChar).Value = Contract.EmployeePassport;
                if (Contract.EmployeeIssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = Contract.EmployeeIssueDate;
                }

                zCommand.Parameters.Add("@EmployeeIssuePlace", SqlDbType.NVarChar).Value = Contract.EmployeeIssuePlace;
                zCommand.Parameters.Add("@EmployeeAdress", SqlDbType.NVarChar).Value = Contract.EmployeeAdress;
                zCommand.Parameters.Add("@EmployeeInsurceNumber", SqlDbType.NVarChar).Value = Contract.EmployeeInsurceNumber;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Contract.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Contract.BranchName;
                zCommand.Parameters.Add("@BranchAdress", SqlDbType.NVarChar).Value = Contract.BranchAdress;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Contract.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Contract.DepartmentName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Contract.OrganizationPath;
                if (Contract.ReportToKey != "" && Contract.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Contract.ReportToName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Contract.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Contract.PositionName;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Contract.JobName;
                if (Contract.ProbationFromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = Contract.ProbationFromDate;
                }

                if (Contract.ProbationToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = Contract.ProbationToDate;
                }

                zCommand.Parameters.Add("@ProbationSalary", SqlDbType.Money).Value = Contract.ProbationSalary;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Contract.ToDate;
                }

                if (Contract.DateBeginSalary == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = Contract.DateBeginSalary;
                }

                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = Contract.SalaryBasic;
                zCommand.Parameters.Add("@SalaryNet", SqlDbType.Money).Value = Contract.SalaryNet;
                zCommand.Parameters.Add("@SalaryGross", SqlDbType.Money).Value = Contract.SalaryGross;
                zCommand.Parameters.Add("@SalaryInsurce", SqlDbType.Money).Value = Contract.SalaryInsurce;
                if (Contract.DateJoinInsurce == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = Contract.DateJoinInsurce;
                }

                if (Contract.DateSeniority == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = Contract.DateSeniority;
                }

                if (Contract.DateBeginPaidLeave == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = Contract.DateBeginPaidLeave;
                }

                if (Contract.Activate == null)
                {
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Contract.Activate;
                }

                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Contract.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Contract.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract.CodeLine;
                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Contract SET "
                        + " ContractID = @ContractID,"
                        + " SubContract = @SubContract,"
                        + " ContractName = @ContractName,"
                        + " ContractType = @ContractType,"
                        + " ContractTypeName = @ContractTypeName,"
                        + " Parent = @Parent,"
                        + " SignDate = @SignDate,"
                        + " SignBy = @SignBy,"
                        + " SignName = @SignName,"
                        + " SignPositionKey = @SignPositionKey,"
                        + " SignPositionName = @SignPositionName,"
                        + " SignAddress = @SignAddress,"
                        + " SignCompany = @SignCompany,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " EmployeeBirthDay = @EmployeeBirthDay,"
                        + " EmployeeBirthPlace = @EmployeeBirthPlace,"
                        + " EmployeeGender = @EmployeeGender,"
                        + " EmployeePassport = @EmployeePassport,"
                        + " EmployeeIssueDate = @EmployeeIssueDate,"
                        + " EmployeeIssuePlace = @EmployeeIssuePlace,"
                        + " EmployeeAdress = @EmployeeAdress,"
                        + " EmployeeInsurceNumber = @EmployeeInsurceNumber,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " BranchAdress = @BranchAdress,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " ReportToKey = @ReportToKey,"
                        + " ReportToName = @ReportToName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " JobName = @JobName,"
                        + " ProbationFromDate = @ProbationFromDate,"
                        + " ProbationToDate = @ProbationToDate,"
                        + " ProbationSalary = @ProbationSalary,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " DateBeginSalary = @DateBeginSalary,"
                        + " SalaryBasic = @SalaryBasic,"
                        + " SalaryNet = @SalaryNet,"
                        + " SalaryGross = @SalaryGross,"
                        + " SalaryInsurce = @SalaryInsurce,"
                        + " DateJoinInsurce = @DateJoinInsurce,"
                        + " DateSeniority = @DateSeniority,"
                        + " DateBeginPaidLeave = @DateBeginPaidLeave,"
                        + " Activate = @Activate,"
                        + " Note = @Note,"
                        + " Slug = @Slug,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ContractKey = @ContractKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract.ContractKey != "" && Contract.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = Contract.ContractID;
                zCommand.Parameters.Add("@SubContract", SqlDbType.NVarChar).Value = Contract.SubContract;
                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Contract.ContractName;
                zCommand.Parameters.Add("@ContractType", SqlDbType.Int).Value = Contract.ContractType;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Contract.ContractTypeName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Contract.Parent;
                if (Contract.SignDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@SignDate", SqlDbType.Date).Value = Contract.SignDate;
                }

                if (Contract.SignBy != "" && Contract.SignBy.Length == 36)
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.SignBy);
                }
                else
                {
                    zCommand.Parameters.Add("@SignBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@SignName", SqlDbType.NVarChar).Value = Contract.SignName;
                zCommand.Parameters.Add("@SignPositionKey", SqlDbType.Int).Value = Contract.SignPositionKey;
                zCommand.Parameters.Add("@SignPositionName", SqlDbType.NVarChar).Value = Contract.SignPositionName;
                zCommand.Parameters.Add("@SignAddress", SqlDbType.NVarChar).Value = Contract.SignAddress;
                zCommand.Parameters.Add("@SignCompany", SqlDbType.NVarChar).Value = Contract.SignCompany;
                if (Contract.EmployeeKey != "" && Contract.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Contract.EmployeeName;
                if (Contract.EmployeeBirthDay == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeBirthDay", SqlDbType.DateTime).Value = Contract.EmployeeBirthDay;
                }

                zCommand.Parameters.Add("@EmployeeBirthPlace", SqlDbType.NVarChar).Value = Contract.EmployeeBirthPlace;
                zCommand.Parameters.Add("@EmployeeGender", SqlDbType.NVarChar).Value = Contract.EmployeeGender;
                zCommand.Parameters.Add("@EmployeePassport", SqlDbType.NVarChar).Value = Contract.EmployeePassport;
                if (Contract.EmployeeIssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeIssueDate", SqlDbType.DateTime).Value = Contract.EmployeeIssueDate;
                }

                zCommand.Parameters.Add("@EmployeeIssuePlace", SqlDbType.NVarChar).Value = Contract.EmployeeIssuePlace;
                zCommand.Parameters.Add("@EmployeeAdress", SqlDbType.NVarChar).Value = Contract.EmployeeAdress;
                zCommand.Parameters.Add("@EmployeeInsurceNumber", SqlDbType.NVarChar).Value = Contract.EmployeeInsurceNumber;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Contract.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Contract.BranchName;
                zCommand.Parameters.Add("@BranchAdress", SqlDbType.NVarChar).Value = Contract.BranchAdress;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Contract.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Contract.DepartmentName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Contract.OrganizationPath;
                if (Contract.ReportToKey != "" && Contract.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Contract.ReportToName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Contract.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Contract.PositionName;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Contract.JobName;
                if (Contract.ProbationFromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationFromDate", SqlDbType.DateTime).Value = Contract.ProbationFromDate;
                }

                if (Contract.ProbationToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ProbationToDate", SqlDbType.DateTime).Value = Contract.ProbationToDate;
                }

                zCommand.Parameters.Add("@ProbationSalary", SqlDbType.Money).Value = Contract.ProbationSalary;
                if (Contract.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Contract.FromDate;
                }

                if (Contract.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Contract.ToDate;
                }

                if (Contract.DateBeginSalary == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginSalary", SqlDbType.DateTime).Value = Contract.DateBeginSalary;
                }

                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = Contract.SalaryBasic;
                zCommand.Parameters.Add("@SalaryNet", SqlDbType.Money).Value = Contract.SalaryNet;
                zCommand.Parameters.Add("@SalaryGross", SqlDbType.Money).Value = Contract.SalaryGross;
                zCommand.Parameters.Add("@SalaryInsurce", SqlDbType.Money).Value = Contract.SalaryInsurce;
                if (Contract.DateJoinInsurce == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateJoinInsurce", SqlDbType.DateTime).Value = Contract.DateJoinInsurce;
                }

                if (Contract.DateSeniority == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateSeniority", SqlDbType.DateTime).Value = Contract.DateSeniority;
                }

                if (Contract.DateBeginPaidLeave == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateBeginPaidLeave", SqlDbType.DateTime).Value = Contract.DateBeginPaidLeave;
                }

                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Contract.Activate;

                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Contract.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Contract.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Contract.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Contract.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Contract.CodeLine;
                if (Contract.PartnerNumber != "" && Contract.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Contract SET RecordStatus = 99 WHERE ContractKey = @ContractKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Contract WHERE ContractKey = @ContractKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract.ContractKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
