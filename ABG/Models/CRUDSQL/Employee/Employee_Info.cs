﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

namespace ABG
{
    public class Employee_Info
    {

        public Employee_Model Employee = new Employee_Model();
        public Family_Model Family = new Family_Model();

        #region [ Constructor Get Information ]
        public Employee_Info()
        {
            Employee.EmployeeKey = Guid.NewGuid().ToString();
        }
        public Employee_Info(string EmployeeKey)
        {
            string zSQL = @"
SELECT A.*, B.*
FROM HRM_Employee A 
LEFT JOIN HRM_Family B ON A.EmployeeKey = B.EmployeeKey
WHERE A.EmployeeKey = @EmployeeKey 
AND A.RecordStatus != 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    #region Family
                    if (zReader["AutoKey"] != DBNull.Value)
                        Family.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Family.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Family.FatherName = zReader["FatherName"].ToString();
                    if (zReader["FatherBirthday"] != DBNull.Value)
                        Family.FatherBirthday = (DateTime)zReader["FatherBirthday"];
                    Family.FatherWork = zReader["FatherWork"].ToString();
                    Family.FatherAddress = zReader["FatherAddress"].ToString();
                    Family.MotherName = zReader["MotherName"].ToString();
                    if (zReader["MotherBirthday"] != DBNull.Value)
                        Family.MotherBirthday = (DateTime)zReader["MotherBirthday"];
                    Family.MotherWork = zReader["MotherWork"].ToString();
                    Family.MotherAddress = zReader["MotherAddress"].ToString();
                    Family.PartnersName = zReader["PartnersName"].ToString();
                    if (zReader["PartnersBirthday"] != DBNull.Value)
                        Family.PartnersBirthday = (DateTime)zReader["PartnersBirthday"];
                    Family.PartnersWork = zReader["PartnersWork"].ToString();
                    Family.PartnersAddress = zReader["PartnersAddress"].ToString();
                    Family.ChildExtend = zReader["ChildExtend"].ToString();
                    Family.OrtherExtend = zReader["OrtherExtend"].ToString();
                    Family.Description = zReader["Description"].ToString();
                    #endregion

                    #region Employee
                    Employee.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Employee.DepartmentName = zReader["DepartmentName"].ToString();

                    Employee.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Employee.EmployeeID = zReader["EmployeeID"].ToString();
                    Employee.LastName = zReader["LastName"].ToString();
                    Employee.FirstName = zReader["FirstName"].ToString();
                    Employee.ReportToKey = zReader["ReportToKey"].ToString();
                    Employee.ReportToName = zReader["ReportToName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                    {
                        Employee.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    }

                    Employee.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Employee.OrganizationName = zReader["OrganizationName"].ToString();
                    Employee.BranchKey = zReader["BranchKey"].ToString();
                    Employee.BranchName = zReader["BranchName"].ToString();
                    if (zReader["JobKey"] != DBNull.Value)
                    {
                        Employee.JobKey = int.Parse(zReader["JobKey"].ToString());
                    }

                    Employee.JobName = zReader["JobName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Employee.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Employee.PositionName = zReader["PositionName"].ToString();
                    if (zReader["WorkingStatusKey"] != DBNull.Value)
                    {
                        Employee.WorkingStatusKey = int.Parse(zReader["WorkingStatusKey"].ToString());
                    }

                    Employee.WorkingStatusName = zReader["WorkingStatusName"].ToString();
                    if (zReader["StartingDate"] != DBNull.Value)
                    {
                        Employee.StartingDate = (DateTime)zReader["StartingDate"];
                    }

                    if (zReader["LeavingDate"] != DBNull.Value)
                    {
                        Employee.LeavingDate = (DateTime)zReader["LeavingDate"];
                    }

                    if (zReader["ContractTypeKey"] != DBNull.Value)
                    {
                        Employee.ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                    }

                    Employee.ContractTypeName = zReader["ContractTypeName"].ToString();
                    Employee.WorkingAddress = zReader["WorkingAddress"].ToString();
                    Employee.WorkingLocation = zReader["WorkingLocation"].ToString();
                    Employee.CompanyPhone = zReader["CompanyPhone"].ToString();
                    Employee.CompanyEmail = zReader["CompanyEmail"].ToString();
                    if (zReader["Gender"] != DBNull.Value)
                    {
                        Employee.Gender = int.Parse(zReader["Gender"].ToString());
                    }

                    if (zReader["Birthday"] != DBNull.Value)
                    {
                        Employee.Birthday = (DateTime)zReader["Birthday"];
                    }

                    Employee.BirthPlace = zReader["BirthPlace"].ToString();
                    if (zReader["MaritalStatusKey"] != DBNull.Value)
                    {
                        Employee.MaritalStatusKey = int.Parse(zReader["MaritalStatusKey"].ToString());
                    }

                    Employee.Nationality = zReader["Nationality"].ToString();
                    Employee.Ethnicity = zReader["Ethnicity"].ToString();
                    Employee.PassportNumber = zReader["PassportNumber"].ToString();
                    if (zReader["IssueDate"] != DBNull.Value)
                    {
                        Employee.IssueDate = (DateTime)zReader["IssueDate"];
                    }

                    if (zReader["ExpireDate"] != DBNull.Value)
                    {
                        Employee.ExpireDate = (DateTime)zReader["ExpireDate"];
                    }

                    Employee.IssuePlace = zReader["IssuePlace"].ToString();
                    Employee.AddressRegister = zReader["AddressRegister"].ToString();
                    Employee.CityRegister = zReader["CityRegister"].ToString();
                    Employee.AddressContact = zReader["AddressContact"].ToString();
                    Employee.CityContact = zReader["CityContact"].ToString();
                    Employee.EmergencyContact = zReader["EmergencyContact"].ToString();
                    Employee.MobiPhone = zReader["MobiPhone"].ToString();
                    Employee.Email = zReader["Email"].ToString();
                    if (zReader["Photo"] != DBNull.Value)
                    {
                        Employee.Photo = (Image)zReader["Photo"];
                    }

                    Employee.PhotoPath = zReader["PhotoPath"].ToString();
                    Employee.TaxNumber = zReader["TaxNumber"].ToString();
                    Employee.BankAccount = zReader["BankAccount"].ToString();
                    Employee.BankName = zReader["BankName"].ToString();
                    Employee.Note = zReader["Note"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Employee.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Employee.Style = zReader["Style"].ToString();
                    Employee.Class = zReader["Class"].ToString();
                    Employee.CodeLine = zReader["CodeLine"].ToString();
                    Employee.NickName = zReader["NickName"].ToString();
                    Employee.Password = zReader["Password"].ToString();
                    Employee.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Employee.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Employee.CreatedBy = zReader["CreatedBy"].ToString();
                    Employee.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Employee.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Employee.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Employee.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Employee.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }
                    #endregion


                    Employee.Message = "200 OK";
                }
                else
                {
                    Employee.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Employee.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Employee (" +
            " EmployeeID , LastName , FirstName , ReportToKey , ReportToName , OrganizationKey , OrganizationPath , OrganizationName, DepartmentKey , DepartmentName , " +
            " BranchKey , BranchName , JobKey , JobName , PositionKey , PositionName , WorkingStatusKey , WorkingStatusName , StartingDate , LeavingDate , ContractTypeKey , " +
            " ContractTypeName , WorkingAddress , WorkingLocation , CompanyPhone , CompanyEmail , Gender , Birthday , BirthPlace , MaritalStatusKey , Nationality , Ethnicity , " +
            " PassportNumber , IssueDate , ExpireDate , IssuePlace , AddressRegister , CityRegister , AddressContact , CityContact , " +
            " EmergencyContact , MobiPhone , Email , Photo , PhotoPath , TaxNumber , BankAccount , BankName , Note , Slug , Style , Class , CodeLine, NickName , Password , " +
            " PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )" +
            " VALUES ( " +
            " @EmployeeID , @LastName , @FirstName , @ReportToKey , @ReportToName , @OrganizationKey , @OrganizationPath , @OrganizationName , @DepartmentKey , @DepartmentName, " +
            " @BranchKey , @BranchName , @JobKey , @JobName , @PositionKey , @PositionName , @WorkingStatusKey , @WorkingStatusName , @StartingDate, @LeavingDate, " +
            " @ContractTypeKey , @ContractTypeName , @WorkingAddress , @WorkingLocation , @CompanyPhone , @CompanyEmail , @Gender , @Birthday , @BirthPlace, @MaritalStatusKey, @Nationality, @Ethnicity, " +
            " @PassportNumber , @IssueDate , @ExpireDate , @IssuePlace , @AddressRegister , @CityRegister , @AddressContact , @CityContact , " +
            " @EmergencyContact , @MobiPhone , @Email , @Photo , @PhotoPath , @TaxNumber , @BankAccount , @BankName , @Note , @Slug , @Style , @Class , @CodeLine , @NickName , @Password, " +
            " @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName)";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Employee.EmployeeID;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = Employee.LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = Employee.FirstName;
                if (Employee.ReportToKey != "" && Employee.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Employee.ReportToName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Employee.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Employee.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Employee.OrganizationName;
                if (Employee.DepartmentKey != "" && Employee.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.DepartmentKey);
                }
                else
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Employee.DepartmentName;
                if (Employee.BranchKey != "" && Employee.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Employee.BranchName;
                zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = Employee.JobKey;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Employee.JobName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Employee.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Employee.PositionName;
                zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = Employee.WorkingStatusKey;
                zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = Employee.WorkingStatusName;
                if (Employee.StartingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = Employee.StartingDate;
                }

                if (Employee.LeavingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = Employee.LeavingDate;
                }

                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = Employee.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Employee.ContractTypeName;
                zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = Employee.WorkingAddress;
                zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = Employee.WorkingLocation;
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = Employee.CompanyPhone;
                zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = Employee.CompanyEmail;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Employee.Gender;
                if (Employee.Birthday == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Employee.Birthday;
                }

                zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = Employee.BirthPlace;
                zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = Employee.MaritalStatusKey;
                zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = Employee.Nationality;
                zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = Employee.Ethnicity;
                zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = Employee.PassportNumber;
                if (Employee.IssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = Employee.IssueDate;
                }

                if (Employee.ExpireDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Employee.ExpireDate;
                }

                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = Employee.IssuePlace;
                zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = Employee.AddressRegister;
                zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = Employee.CityRegister;
                zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = Employee.AddressContact;
                zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = Employee.CityContact;
                zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = Employee.EmergencyContact;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = Employee.MobiPhone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Employee.Email;
                if (Employee.Photo == null)
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = Employee.Photo;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Employee.PhotoPath;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Employee.TaxNumber;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Employee.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Employee.BankName;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Employee.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Employee.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Employee.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Employee.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Employee.CodeLine;
                zCommand.Parameters.Add("@NickName", SqlDbType.NVarChar).Value = Employee.NickName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Employee.Password;
                if (Employee.PartnerNumber != "" && Employee.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Employee.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Employee.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Employee.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Employee.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Employee.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Employee.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Employee.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Employee(" +
                " EmployeeKey , EmployeeID , LastName , FirstName , ReportToKey , ReportToName , OrganizationKey , OrganizationPath , OrganizationName , DepartmentKey , DepartmentName , " +
                " BranchKey , BranchName , JobKey , JobName , PositionKey , PositionName , WorkingStatusKey , WorkingStatusName , StartingDate , LeavingDate , " +
                " ContractTypeKey , ContractTypeName , WorkingAddress , WorkingLocation , CompanyPhone , CompanyEmail , Gender , Birthday , BirthPlace , MaritalStatusKey , " +
                " Nationality , Ethnicity , PassportNumber , IssueDate , ExpireDate , IssuePlace , AddressRegister , CityRegister , AddressContact , CityContact , " +
                " EmergencyContact , MobiPhone , Email , Photo , PhotoPath , TaxNumber , BankAccount , BankName , Note , Slug , Style , Class , CodeLine , NickName , Password , " +
                " PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName) " +
                " VALUES ( " +
                " @EmployeeKey , @EmployeeID , @LastName , @FirstName , @ReportToKey , @ReportToName , @OrganizationKey , @OrganizationPath , @OrganizationName ,  @DepartmentKey , @DepartmentName ," +
                " @BranchKey , @BranchName , @JobKey , @JobName , @PositionKey , @PositionName , @WorkingStatusKey , @WorkingStatusName , @StartingDate , @LeavingDate , " +
                " @ContractTypeKey , @ContractTypeName , @WorkingAddress , @WorkingLocation , @CompanyPhone , @CompanyEmail , @Gender , @Birthday , @BirthPlace , @MaritalStatusKey , " +
                " @Nationality , @Ethnicity , @PassportNumber , @IssueDate , @ExpireDate , @IssuePlace , @AddressRegister , @CityRegister , @AddressContact , @CityContact , " +
                " @EmergencyContact , @MobiPhone , @Email , @Photo , @PhotoPath , @TaxNumber , @BankAccount , @BankName , @Note , @Slug , @Style , @Class , @CodeLine , @NickName , @Password , " +
                " @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName) ";

            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Employee.EmployeeKey != "" && Employee.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Employee.EmployeeID;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = Employee.LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = Employee.FirstName;
                if (Employee.ReportToKey != "" && Employee.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Employee.ReportToName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Employee.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Employee.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Employee.OrganizationName;
                if (Employee.DepartmentKey != "" && Employee.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.DepartmentKey);
                }
                else
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Employee.DepartmentName;
                if (Employee.BranchKey != "" && Employee.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Employee.BranchName;
                zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = Employee.JobKey;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Employee.JobName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Employee.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Employee.PositionName;
                zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = Employee.WorkingStatusKey;
                zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = Employee.WorkingStatusName;
                if (Employee.StartingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = Employee.StartingDate;
                }

                if (Employee.LeavingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = Employee.LeavingDate;
                }

                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = Employee.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Employee.ContractTypeName;
                zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = Employee.WorkingAddress;
                zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = Employee.WorkingLocation;
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = Employee.CompanyPhone;
                zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = Employee.CompanyEmail;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Employee.Gender;
                if (Employee.Birthday == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Employee.Birthday;
                }

                zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = Employee.BirthPlace;
                zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = Employee.MaritalStatusKey;
                zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = Employee.Nationality;
                zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = Employee.Ethnicity;
                zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = Employee.PassportNumber;
                if (Employee.IssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = Employee.IssueDate;
                }

                if (Employee.ExpireDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Employee.ExpireDate;
                }

                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = Employee.IssuePlace;
                zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = Employee.AddressRegister;
                zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = Employee.CityRegister;
                zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = Employee.AddressContact;
                zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = Employee.CityContact;
                zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = Employee.EmergencyContact;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = Employee.MobiPhone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Employee.Email;
                if (Employee.Photo == null)
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = Employee.Photo;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Employee.PhotoPath;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Employee.TaxNumber;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Employee.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Employee.BankName;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Employee.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Employee.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Employee.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Employee.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Employee.CodeLine;
                zCommand.Parameters.Add("@NickName", SqlDbType.NVarChar).Value = Employee.NickName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Employee.Password;
                if (Employee.PartnerNumber != "" && Employee.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Employee.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Employee.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Employee.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Employee.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Employee.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Employee.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Employee.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Employee SET "
                        + " EmployeeID = @EmployeeID,"
                        + " LastName = @LastName,"
                        + " FirstName = @FirstName,"
                        + " ReportToKey = @ReportToKey,"
                        + " ReportToName = @ReportToName,"
                        + " OrganizationKey = @OrganizationKey,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " OrganizationName = @OrganizationName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " JobKey = @JobKey,"
                        + " JobName = @JobName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " WorkingStatusKey = @WorkingStatusKey,"
                        + " WorkingStatusName = @WorkingStatusName,"
                        + " StartingDate = @StartingDate,"
                        + " LeavingDate = @LeavingDate,"
                        + " ContractTypeKey = @ContractTypeKey,"
                        + " ContractTypeName = @ContractTypeName,"
                        + " WorkingAddress = @WorkingAddress,"
                        + " WorkingLocation = @WorkingLocation,"
                        + " CompanyPhone = @CompanyPhone,"
                        + " CompanyEmail = @CompanyEmail,"
                        + " Gender = @Gender,"
                        + " Birthday = @Birthday,"
                        + " BirthPlace = @BirthPlace,"
                        + " MaritalStatusKey = @MaritalStatusKey,"
                        + " Nationality = @Nationality,"
                        + " Ethnicity = @Ethnicity,"
                        + " PassportNumber = @PassportNumber,"
                        + " IssueDate = @IssueDate,"
                        + " ExpireDate = @ExpireDate,"
                        + " IssuePlace = @IssuePlace,"
                        + " AddressRegister = @AddressRegister,"
                        + " CityRegister = @CityRegister,"
                        + " AddressContact = @AddressContact,"
                        + " CityContact = @CityContact,"
                        + " EmergencyContact = @EmergencyContact,"
                        + " MobiPhone = @MobiPhone,"
                        + " Email = @Email,"
                        + " TaxNumber = @TaxNumber,"
                        + " BankAccount = @BankAccount,"
                        + " BankName = @BankName,"
                        + " Note = @Note,"
                        + " Slug = @Slug,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " NickName = @NickName,"
                        + " Password = @Password,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Employee.EmployeeKey != "" && Employee.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Employee.EmployeeID;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = Employee.LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = Employee.FirstName;
                if (Employee.ReportToKey != "" && Employee.ReportToKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.ReportToKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ReportToName", SqlDbType.NVarChar).Value = Employee.ReportToName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Employee.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Employee.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Employee.OrganizationName;
                if (Employee.DepartmentKey != "" && Employee.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.DepartmentKey);
                }
                else
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Employee.DepartmentName;
                if (Employee.BranchKey != "" && Employee.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Employee.BranchName;
                zCommand.Parameters.Add("@JobKey", SqlDbType.Int).Value = Employee.JobKey;
                zCommand.Parameters.Add("@JobName", SqlDbType.NVarChar).Value = Employee.JobName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Employee.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Employee.PositionName;
                zCommand.Parameters.Add("@WorkingStatusKey", SqlDbType.Int).Value = Employee.WorkingStatusKey;
                zCommand.Parameters.Add("@WorkingStatusName", SqlDbType.NVarChar).Value = Employee.WorkingStatusName;
                if (Employee.StartingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.Date).Value = Employee.StartingDate;
                }

                if (Employee.LeavingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.Date).Value = Employee.LeavingDate;
                }

                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = Employee.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeName", SqlDbType.NVarChar).Value = Employee.ContractTypeName;
                zCommand.Parameters.Add("@WorkingAddress", SqlDbType.NVarChar).Value = Employee.WorkingAddress;
                zCommand.Parameters.Add("@WorkingLocation", SqlDbType.NVarChar).Value = Employee.WorkingLocation;
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = Employee.CompanyPhone;
                zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = Employee.CompanyEmail;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Employee.Gender;
                if (Employee.Birthday == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = Employee.Birthday;
                }

                zCommand.Parameters.Add("@BirthPlace", SqlDbType.NVarChar).Value = Employee.BirthPlace;
                zCommand.Parameters.Add("@MaritalStatusKey", SqlDbType.Int).Value = Employee.MaritalStatusKey;
                zCommand.Parameters.Add("@Nationality", SqlDbType.NVarChar).Value = Employee.Nationality;
                zCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar).Value = Employee.Ethnicity;
                zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = Employee.PassportNumber;
                if (Employee.IssueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = Employee.IssueDate;
                }

                if (Employee.ExpireDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Employee.ExpireDate;
                }

                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = Employee.IssuePlace;
                zCommand.Parameters.Add("@AddressRegister", SqlDbType.NVarChar).Value = Employee.AddressRegister;
                zCommand.Parameters.Add("@CityRegister", SqlDbType.NVarChar).Value = Employee.CityRegister;
                zCommand.Parameters.Add("@AddressContact", SqlDbType.NVarChar).Value = Employee.AddressContact;
                zCommand.Parameters.Add("@CityContact", SqlDbType.NVarChar).Value = Employee.CityContact;
                zCommand.Parameters.Add("@EmergencyContact", SqlDbType.NVarChar).Value = Employee.EmergencyContact;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = Employee.MobiPhone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Employee.Email;
                if (Employee.Photo == null)
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = Employee.Photo;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Employee.PhotoPath;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = Employee.TaxNumber;
                zCommand.Parameters.Add("@BankAccount", SqlDbType.NVarChar).Value = Employee.BankAccount;
                zCommand.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = Employee.BankName;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = Employee.Note;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Employee.Slug;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Employee.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Employee.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Employee.CodeLine;
                zCommand.Parameters.Add("@NickName", SqlDbType.NVarChar).Value = Employee.NickName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = Employee.Password;
                if (Employee.PartnerNumber != "" && Employee.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Employee.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Employee.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Employee.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Employee.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Employee.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Employee SET RecordStatus = 99 WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.EmployeeKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Employee.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Employee.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Employee.EmployeeKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Employee.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Employee.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
