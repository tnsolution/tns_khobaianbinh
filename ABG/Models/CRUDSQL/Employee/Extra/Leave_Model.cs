﻿using System;
namespace ABG
{
    public class LeaveApplication_Model
    {
        #region [ Field Name ]
        private string _LeaveKey = "";
        private string _LeaveName = "";
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private string _OrganizationPath = "";
        private DateTime _FromDate = DateTime.MinValue;
        private DateTime _ToDate = DateTime.MinValue;
        private float _TotalLeaveDate = 0;
        private int _TypeOfLeaveID = 0;
        private string _TypeOfLeaveName = "";
        private string _Reason = "";
        private DateTime _DateWrite = DateTime.MinValue;
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string LeaveKey
        {
            get { return _LeaveKey; }
            set { _LeaveKey = value; }
        }
        public string LeaveName
        {
            get { return _LeaveName; }
            set { _LeaveName = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public string OrganizationPath
        {
            get { return _OrganizationPath; }
            set { _OrganizationPath = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public float TotalLeaveDate
        {
            get { return _TotalLeaveDate; }
            set { _TotalLeaveDate = value; }
        }
        public int TypeOfLeaveID
        {
            get { return _TypeOfLeaveID; }
            set { _TypeOfLeaveID = value; }
        }
        public string TypeOfLeaveName
        {
            get { return _TypeOfLeaveName; }
            set { _TypeOfLeaveName = value; }
        }
        public string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
