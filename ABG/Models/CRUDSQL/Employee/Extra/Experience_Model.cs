﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Experience_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _EmployeeKey = "";
private string _UnitWork = "";
private string _UnitPosition = "";
private DateTime _FromDate = DateTime.MinValue;
private DateTime _ToDate = DateTime.MinValue;
private string _Description = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string UnitWork
{
get { return _UnitWork; }
set { _UnitWork = value; }
}
public string UnitPosition
{
get { return _UnitPosition; }
set { _UnitPosition = value; }
}
public DateTime FromDate
{
get { return _FromDate; }
set { _FromDate = value; }
}
public DateTime ToDate
{
get { return _ToDate; }
set { _ToDate = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
