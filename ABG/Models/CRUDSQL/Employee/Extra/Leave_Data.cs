﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class LeaveApplication_Data
    {
        public static List<LeaveApplication_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_LeaveApplication WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY ToDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<LeaveApplication_Model> zList = new List<LeaveApplication_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime DateWrite = DateTime.MinValue;
                DateTime.TryParse(r["DateWrite"].ToString(), out DateWrite);

                DateTime FromDate = DateTime.MinValue;
                DateTime.TryParse(r["FromDate"].ToString(), out FromDate);

                DateTime ToDate = DateTime.MinValue;
                DateTime.TryParse(r["ToDate"].ToString(), out ToDate);

                zList.Add(new LeaveApplication_Model()
                {
                    TotalLeaveDate = r["TotalLeaveDate"].ToFloat(),
                    LeaveKey = r["LeaveKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    Reason = r["Reason"].ToString(),
                    FromDate = FromDate,
                    ToDate = ToDate,
                    DateWrite = DateWrite,
                    TypeOfLeaveName = r["TypeOfLeaveName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                });
            }
            return zList;
        }
    }
}
