﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class LeaveApplication_Info
    {

        public LeaveApplication_Model LeaveApplication = new LeaveApplication_Model();

        #region [ Constructor Get Information ]
        public LeaveApplication_Info()
        {
            LeaveApplication.LeaveKey = Guid.NewGuid().ToString();
        }
        public LeaveApplication_Info(string LeaveKey)
        {
            string zSQL = "SELECT * FROM HRM_LeaveApplication WHERE LeaveKey = @LeaveKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    LeaveApplication.LeaveKey = zReader["LeaveKey"].ToString();
                    LeaveApplication.LeaveName = zReader["LeaveName"].ToString();
                    LeaveApplication.EmployeeKey = zReader["EmployeeKey"].ToString();
                    LeaveApplication.EmployeeID = zReader["EmployeeID"].ToString();
                    LeaveApplication.EmployeeName = zReader["EmployeeName"].ToString();
                    LeaveApplication.BranchKey = zReader["BranchKey"].ToString();
                    LeaveApplication.BranchName = zReader["BranchName"].ToString();
                    LeaveApplication.DepartmentKey = zReader["DepartmentKey"].ToString();
                    LeaveApplication.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        LeaveApplication.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    LeaveApplication.PositionName = zReader["PositionName"].ToString();
                    LeaveApplication.OrganizationPath = zReader["OrganizationPath"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        LeaveApplication.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        LeaveApplication.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["TotalLeaveDate"] != DBNull.Value)
                    {
                        LeaveApplication.TotalLeaveDate = float.Parse(zReader["TotalLeaveDate"].ToString());
                    }

                    if (zReader["TypeOfLeaveID"] != DBNull.Value)
                    {
                        LeaveApplication.TypeOfLeaveID = int.Parse(zReader["TypeOfLeaveID"].ToString());
                    }

                    LeaveApplication.TypeOfLeaveName = zReader["TypeOfLeaveName"].ToString();
                    LeaveApplication.Reason = zReader["Reason"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                    {
                        LeaveApplication.DateWrite = (DateTime)zReader["DateWrite"];
                    }

                    LeaveApplication.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        LeaveApplication.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    LeaveApplication.Style = zReader["Style"].ToString();
                    LeaveApplication.Class = zReader["Class"].ToString();
                    LeaveApplication.CodeLine = zReader["CodeLine"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        LeaveApplication.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    LeaveApplication.CreatedBy = zReader["CreatedBy"].ToString();
                    LeaveApplication.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        LeaveApplication.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    LeaveApplication.ModifiedBy = zReader["ModifiedBy"].ToString();
                    LeaveApplication.ModifiedName = zReader["ModifiedName"].ToString();
                    LeaveApplication.Message = "200 OK";
                }
                else
                {
                    LeaveApplication.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_LeaveApplication ("
         + " LeaveName , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , FromDate , ToDate , TotalLeaveDate , TypeOfLeaveID , TypeOfLeaveName , Reason , DateWrite , PartnerNumber , RecordStatus , Style , Class , CodeLine , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @LeaveName , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @FromDate , @ToDate , @TotalLeaveDate , @TypeOfLeaveID , @TypeOfLeaveName , @Reason , @DateWrite , @PartnerNumber , @RecordStatus , @Style , @Class , @CodeLine , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@LeaveName", SqlDbType.NVarChar).Value = LeaveApplication.LeaveName;
                if (LeaveApplication.EmployeeKey != "" && LeaveApplication.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeName;
                if (LeaveApplication.BranchKey != "" && LeaveApplication.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = LeaveApplication.BranchName;
                if (LeaveApplication.DepartmentKey != "" && LeaveApplication.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = LeaveApplication.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = LeaveApplication.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = LeaveApplication.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = LeaveApplication.OrganizationPath;
                if (LeaveApplication.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = LeaveApplication.FromDate;
                }

                if (LeaveApplication.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = LeaveApplication.ToDate;
                }

                zCommand.Parameters.Add("@TotalLeaveDate", SqlDbType.Float).Value = LeaveApplication.TotalLeaveDate;
                zCommand.Parameters.Add("@TypeOfLeaveID", SqlDbType.Int).Value = LeaveApplication.TypeOfLeaveID;
                zCommand.Parameters.Add("@TypeOfLeaveName", SqlDbType.NVarChar).Value = LeaveApplication.TypeOfLeaveName;
                zCommand.Parameters.Add("@Reason", SqlDbType.NText).Value = LeaveApplication.Reason;
                if (LeaveApplication.DateWrite == null)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = LeaveApplication.DateWrite;
                }

                if (LeaveApplication.PartnerNumber != "" && LeaveApplication.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = LeaveApplication.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = LeaveApplication.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = LeaveApplication.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = LeaveApplication.CodeLine;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = LeaveApplication.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = LeaveApplication.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                LeaveApplication.Message = "201 Created";
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_LeaveApplication("
         + " LeaveKey , LeaveName , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , FromDate , ToDate , TotalLeaveDate , TypeOfLeaveID , TypeOfLeaveName , Reason , DateWrite , PartnerNumber , RecordStatus , Style , Class , CodeLine , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @LeaveKey , @LeaveName , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @FromDate , @ToDate , @TotalLeaveDate , @TypeOfLeaveID , @TypeOfLeaveName , @Reason , @DateWrite , @PartnerNumber , @RecordStatus , @Style , @Class , @CodeLine , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (LeaveApplication.LeaveKey != "" && LeaveApplication.LeaveKey.Length == 36)
                {
                    zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.LeaveKey);
                }
                else
                {
                    zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@LeaveName", SqlDbType.NVarChar).Value = LeaveApplication.LeaveName;
                if (LeaveApplication.EmployeeKey != "" && LeaveApplication.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeName;
                if (LeaveApplication.BranchKey != "" && LeaveApplication.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = LeaveApplication.BranchName;
                if (LeaveApplication.DepartmentKey != "" && LeaveApplication.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = LeaveApplication.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = LeaveApplication.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = LeaveApplication.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = LeaveApplication.OrganizationPath;
                if (LeaveApplication.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = LeaveApplication.FromDate;
                }

                if (LeaveApplication.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = LeaveApplication.ToDate;
                }

                zCommand.Parameters.Add("@TotalLeaveDate", SqlDbType.Float).Value = LeaveApplication.TotalLeaveDate;
                zCommand.Parameters.Add("@TypeOfLeaveID", SqlDbType.Int).Value = LeaveApplication.TypeOfLeaveID;
                zCommand.Parameters.Add("@TypeOfLeaveName", SqlDbType.NVarChar).Value = LeaveApplication.TypeOfLeaveName;
                zCommand.Parameters.Add("@Reason", SqlDbType.NText).Value = LeaveApplication.Reason;
                if (LeaveApplication.DateWrite == null)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = LeaveApplication.DateWrite;
                }

                if (LeaveApplication.PartnerNumber != "" && LeaveApplication.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = LeaveApplication.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = LeaveApplication.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = LeaveApplication.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = LeaveApplication.CodeLine;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = LeaveApplication.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = LeaveApplication.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                LeaveApplication.Message = "201 Created";
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_LeaveApplication SET "
                        + " LeaveName = @LeaveName,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " TotalLeaveDate = @TotalLeaveDate,"
                        + " TypeOfLeaveID = @TypeOfLeaveID,"
                        + " TypeOfLeaveName = @TypeOfLeaveName,"
                        + " Reason = @Reason,"
                        + " DateWrite = @DateWrite,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE LeaveKey = @LeaveKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (LeaveApplication.LeaveKey != "" && LeaveApplication.LeaveKey.Length == 36)
                {
                    zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.LeaveKey);
                }
                else
                {
                    zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@LeaveName", SqlDbType.NVarChar).Value = LeaveApplication.LeaveName;
                if (LeaveApplication.EmployeeKey != "" && LeaveApplication.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = LeaveApplication.EmployeeName;
                if (LeaveApplication.BranchKey != "" && LeaveApplication.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = LeaveApplication.BranchName;
                if (LeaveApplication.DepartmentKey != "" && LeaveApplication.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = LeaveApplication.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = LeaveApplication.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = LeaveApplication.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = LeaveApplication.OrganizationPath;
                if (LeaveApplication.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = LeaveApplication.FromDate;
                }

                if (LeaveApplication.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = LeaveApplication.ToDate;
                }

                zCommand.Parameters.Add("@TotalLeaveDate", SqlDbType.Float).Value = LeaveApplication.TotalLeaveDate;
                zCommand.Parameters.Add("@TypeOfLeaveID", SqlDbType.Int).Value = LeaveApplication.TypeOfLeaveID;
                zCommand.Parameters.Add("@TypeOfLeaveName", SqlDbType.NVarChar).Value = LeaveApplication.TypeOfLeaveName;
                zCommand.Parameters.Add("@Reason", SqlDbType.NText).Value = LeaveApplication.Reason;
                if (LeaveApplication.DateWrite == null)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = LeaveApplication.DateWrite;
                }

                if (LeaveApplication.PartnerNumber != "" && LeaveApplication.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = LeaveApplication.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = LeaveApplication.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = LeaveApplication.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = LeaveApplication.CodeLine;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = LeaveApplication.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                LeaveApplication.Message = "200 OK";
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_LeaveApplication SET RecordStatus = 99 WHERE LeaveKey = @LeaveKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.LeaveKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                LeaveApplication.Message = "200 OK";
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_LeaveApplication WHERE LeaveKey = @LeaveKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@LeaveKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(LeaveApplication.LeaveKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                LeaveApplication.Message = "200 OK";
            }
            catch (Exception Err)
            {
                LeaveApplication.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
