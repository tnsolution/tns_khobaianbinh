﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Family_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _EmployeeKey = "";
private string _FatherName = "";
private DateTime _FatherBirthday = DateTime.MinValue;
private string _FatherWork = "";
private string _FatherAddress = "";
private string _MotherName = "";
private DateTime _MotherBirthday = DateTime.MinValue;
private string _MotherWork = "";
private string _MotherAddress = "";
private string _PartnersName = "";
private DateTime _PartnersBirthday = DateTime.MinValue;
private string _PartnersWork = "";
private string _PartnersAddress = "";
private string _ChildExtend = "";
private string _OrtherExtend = "";
private string _Description = "";
private int _RecordStatus = 0;
private string _PartnerNumber = "";
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string FatherName
{
get { return _FatherName; }
set { _FatherName = value; }
}
public DateTime FatherBirthday
{
get { return _FatherBirthday; }
set { _FatherBirthday = value; }
}
public string FatherWork
{
get { return _FatherWork; }
set { _FatherWork = value; }
}
public string FatherAddress
{
get { return _FatherAddress; }
set { _FatherAddress = value; }
}
public string MotherName
{
get { return _MotherName; }
set { _MotherName = value; }
}
public DateTime MotherBirthday
{
get { return _MotherBirthday; }
set { _MotherBirthday = value; }
}
public string MotherWork
{
get { return _MotherWork; }
set { _MotherWork = value; }
}
public string MotherAddress
{
get { return _MotherAddress; }
set { _MotherAddress = value; }
}
public string PartnersName
{
get { return _PartnersName; }
set { _PartnersName = value; }
}
public DateTime PartnersBirthday
{
get { return _PartnersBirthday; }
set { _PartnersBirthday = value; }
}
public string PartnersWork
{
get { return _PartnersWork; }
set { _PartnersWork = value; }
}
public string PartnersAddress
{
get { return _PartnersAddress; }
set { _PartnersAddress = value; }
}
public string ChildExtend
{
get { return _ChildExtend; }
set { _ChildExtend = value; }
}
public string OrtherExtend
{
get { return _OrtherExtend; }
set { _OrtherExtend = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
