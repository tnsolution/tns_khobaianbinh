﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Experience_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Experience WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Experience_Model> ListExperience(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT 
A.AutoKey, A.EmployeeKey, 
A.UnitWork, A.UnitPosition, 
A.FromDate, ISNULL( A.ToDate, GETDATE()) ToDate, A.[Description]
FROM HRM_Experience A 
WHERE RecordStatus != 99 
AND PartnerNumber = @PartnerNumber 
AND EmployeeKey = @EmployeeKey 
ORDER BY ToDate ";           
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Experience_Model> zList = new List<Experience_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zFromDate = DateTime.MinValue;
                    if (DateTime.TryParse(r["FromDate"].ToString(), out zFromDate))
                    {

                    }

                    DateTime zToDate = DateTime.MinValue;
                    if (DateTime.TryParse(r["ToDate"].ToString(), out zToDate))
                    {

                    }


                    zList.Add(new Experience_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        UnitWork = r["UnitWork"].ToString(),
                        UnitPosition = r["UnitPosition"].ToString(),
                        FromDate = zFromDate,
                        ToDate = zToDate,
                        Description = r["Description"].ToString()
                    });
                }
            }
            return zList;
        }
    }
}
