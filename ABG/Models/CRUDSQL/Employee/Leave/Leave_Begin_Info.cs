﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Begin_Info
    {

        public Leave_Begin_Model Leave_Begin = new Leave_Begin_Model();

        #region [ Constructor Get Information ]
        public Leave_Begin_Info()
        {
            Leave_Begin.BeginKey = Guid.NewGuid().ToString();
        }
        public Leave_Begin_Info(string BeginKey)
        {
            string zSQL = "SELECT * FROM HRM_Leave_Begin WHERE BeginKey = @BeginKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(BeginKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Leave_Begin.BeginKey = zReader["BeginKey"].ToString();
                    Leave_Begin.BeginYear = zReader["BeginYear"].ToString();
                    if (zReader["BeginNo"] != DBNull.Value)
                    {
                        Leave_Begin.BeginNo = float.Parse(zReader["BeginNo"].ToString());
                    }

                    if (zReader["BeginPlus"] != DBNull.Value)
                    {
                        Leave_Begin.BeginPlus = float.Parse(zReader["BeginPlus"].ToString());
                    }

                    Leave_Begin.Description = zReader["Description"].ToString();
                    Leave_Begin.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Leave_Begin.EmployeeID = zReader["EmployeeID"].ToString();
                    Leave_Begin.EmployeeName = zReader["EmployeeName"].ToString();
                    Leave_Begin.BranchKey = zReader["BranchKey"].ToString();
                    Leave_Begin.BranchName = zReader["BranchName"].ToString();
                    Leave_Begin.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Leave_Begin.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Leave_Begin.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Leave_Begin.PositionName = zReader["PositionName"].ToString();
                    Leave_Begin.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Leave_Begin.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Leave_Begin.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Leave_Begin.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Leave_Begin.CreatedBy = zReader["CreatedBy"].ToString();
                    Leave_Begin.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Leave_Begin.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Leave_Begin.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Leave_Begin.ModifiedName = zReader["ModifiedName"].ToString();
                    Leave_Begin.Message = "200 OK";
                }
                else
                {
                    Leave_Begin.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Begin ("
         + " BeginYear , BeginNo , BeginPlus , Description , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @BeginYear , @BeginNo , @BeginPlus , @Description , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BeginYear", SqlDbType.NVarChar).Value = Leave_Begin.BeginYear;
                zCommand.Parameters.Add("@BeginNo", SqlDbType.Float).Value = Leave_Begin.BeginNo;
                zCommand.Parameters.Add("@BeginPlus", SqlDbType.Float).Value = Leave_Begin.BeginPlus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Begin.Description;
                if (Leave_Begin.EmployeeKey != "" && Leave_Begin.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeName;
                if (Leave_Begin.BranchKey != "" && Leave_Begin.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Begin.BranchName;
                if (Leave_Begin.DepartmentKey != "" && Leave_Begin.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Begin.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Begin.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Begin.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Begin.OrganizationPath;
                if (Leave_Begin.PartnerNumber != "" && Leave_Begin.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Begin.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Begin.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Begin.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Begin.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Begin("
         + " BeginKey , BeginYear , BeginNo , BeginPlus , Description , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @BeginKey , @BeginYear , @BeginNo , @BeginPlus , @Description , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Leave_Begin.BeginKey != "" && Leave_Begin.BeginKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BeginKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BeginYear", SqlDbType.NVarChar).Value = Leave_Begin.BeginYear;
                zCommand.Parameters.Add("@BeginNo", SqlDbType.Float).Value = Leave_Begin.BeginNo;
                zCommand.Parameters.Add("@BeginPlus", SqlDbType.Float).Value = Leave_Begin.BeginPlus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Begin.Description;
                if (Leave_Begin.EmployeeKey != "" && Leave_Begin.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeName;
                if (Leave_Begin.BranchKey != "" && Leave_Begin.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Begin.BranchName;
                if (Leave_Begin.DepartmentKey != "" && Leave_Begin.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Begin.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Begin.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Begin.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Begin.OrganizationPath;
                if (Leave_Begin.PartnerNumber != "" && Leave_Begin.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Begin.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Begin.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Begin.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Begin.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Leave_Begin SET "
                        + " BeginYear = @BeginYear,"
                        + " BeginNo = @BeginNo,"
                        + " BeginPlus = @BeginPlus,"
                        + " Description = @Description,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE BeginKey = @BeginKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Leave_Begin.BeginKey != "" && Leave_Begin.BeginKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BeginKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BeginYear", SqlDbType.NVarChar).Value = Leave_Begin.BeginYear;
                zCommand.Parameters.Add("@BeginNo", SqlDbType.Float).Value = Leave_Begin.BeginNo;
                zCommand.Parameters.Add("@BeginPlus", SqlDbType.Float).Value = Leave_Begin.BeginPlus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Begin.Description;
                if (Leave_Begin.EmployeeKey != "" && Leave_Begin.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Begin.EmployeeName;
                if (Leave_Begin.BranchKey != "" && Leave_Begin.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Begin.BranchName;
                if (Leave_Begin.DepartmentKey != "" && Leave_Begin.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Begin.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Begin.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Begin.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Begin.OrganizationPath;
                if (Leave_Begin.PartnerNumber != "" && Leave_Begin.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Begin.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Begin.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Begin.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Leave_Begin SET RecordStatus = 99 WHERE BeginKey = @BeginKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BeginKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Begin.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Leave_Begin WHERE BeginKey = @BeginKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BeginKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Begin.BeginKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Begin.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Begin.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
