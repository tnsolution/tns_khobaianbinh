﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Note_Data
    {
        public static List<Leave_Note_Model> List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT Top 100 * FROM HRM_Leave_Note WHERE RecordStatus != 99 AND PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' ORDER BY FromDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Leave_Note_Model> zList = new List<Leave_Note_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Leave_Note_Model()
                    {
                        NoteKey = r["NoteKey"].ToString(),
                        NoteDate = r["NoteDate"].ToDate(),
                        Description = r["Description"].ToString(),
                        FromDate = Convert.ToDateTime(r["FromDate"]),
                        ToDate = Convert.ToDateTime(r["ToDate"]),
                        TotalDate = r["TotalDate"].ToInt(),
                        CategoryName = r["CategoryName"].ToString(),
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        PositionName = r["PositionName"].ToString(),
                    });
                }
            }
            return zList;
        }
        public static List<Leave_Note_Model> Search(string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.* 
FROM HRM_Leave_Note A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_ListPosition C ON B.PositionKey = C.PositionKey 
LEFT JOIN HRM_Department D ON B.DepartmentKey = D.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B' ";

            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND D.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND D.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.NoteDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0 && SearchName != "0")
            {
                //zSQL += " AND (LastName + ' ' + FirstName) LIKE @SearchName";
                zSQL += " AND CAST(A.EmployeeKey AS NVARCHAR(50)) = @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, B.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender > 0)
            {
                zSQL += " AND B.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND B.PositionKey = @Position";
            }
            if (Status != 0)
            {
                zSQL += " AND B.WorkingStatusKey = @Status";
            }

            zSQL += " ORDER BY FromDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = SearchName;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Leave_Note_Model> zList = new List<Leave_Note_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Leave_Note_Model()
                    {
                        NoteKey = r["NoteKey"].ToString(),
                        NoteDate = r["NoteDate"].ToDate(),
                        Description = r["Description"].ToString(),
                        FromDate = Convert.ToDateTime(r["FromDate"]),
                        ToDate = Convert.ToDateTime(r["ToDate"]),
                        TotalDate = r["TotalDate"].ToInt(),
                        CategoryName = r["CategoryName"].ToString(),
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        PositionName = r["PositionName"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}
