﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Begin_Data
    {
        public static List<Leave_Begin_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Leave_Begin WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY EmployeeKey, RIGHT(BeginYear,4)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Begin_Model> zList = new List<Leave_Begin_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Begin_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    BeginKey = r["BeginKey"].ToString(),
                    BeginYear = r["BeginYear"].ToString(),
                    BeginNo = r["BeginNo"].ToFloat(),
                    BeginPlus = r["BeginPlus"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                });
            }

            return zList;
        }
    }
}
