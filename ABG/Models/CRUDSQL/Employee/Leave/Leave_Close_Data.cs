﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Close_Data
    {
        public static List<Leave_Close_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.EmployeeKey, A.EmployeeName, C.EmployeeID, C.PositionName, C.DepartmentName,
A.CloseYear, A.CloseEnd, A.[Description], A.Reference, dbo.LyDoNghiPhep(A.Reference) AS LyDo,
B.FromDate, B.ToDate, B.TotalDate
FROM HRM_Leave_Close A
LEFT JOIN HRM_Leave_Note B ON A.Reference = CONVERT(NVARCHAR(50), B.NoteKey)
LEFT JOIN HRM_Employee C ON C.EmployeeKey = A.EmployeeKey
WHERE 
A.RecordStatus <> 99";

            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            }
            zSQL += " ORDER BY A.CloseYear, A.CloseEnd DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Reference = r["Reference"].ToString(),
                    Message = r["LyDo"].ToString(),
                });
            }
            return zList;
        }
        public static List<Leave_Close_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.EmployeeKey, A.EmployeeName, B.EmployeeID, A.CloseKey, A.CloseYear, A.CloseEnd, A.Incremental, A.Description, A.DepartmentName, A.PositionName,
dbo.LyDoNghiPhep(A.Reference) AS LyDo
FROM HRM_Leave_Close A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_ListPosition C ON B.PositionKey = C.PositionKey 
LEFT JOIN HRM_Department D ON A.DepartmentKey = B.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.Slug = 0 
AND A.PartnerNumber = @PartnerNumber 
ORDER BY D.[Rank] ASC, C.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    CloseKey = r["CloseKey"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Message = r["LyDo"].ToString(),
                });
            }

            return zList;
        }
        public static List<Leave_Close_Model> Search(string PartnerNumber, string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, dbo.LyDoNghiPhep(A.Reference) AS LyDo
FROM HRM_Leave_Close A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_ListPosition C ON B.PositionKey = C.PositionKey 
LEFT JOIN HRM_Department D ON B.DepartmentKey = D.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.Slug = 0
AND A.PartnerNumber = @PartnerNumber";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND D.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND D.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND B.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND (LastName + ' ' + FirstName) LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, B.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND B.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND B.PositionKey = @Position";
            }
            if (Status != 0)
            {
                zSQL += " AND B.WorkingStatusKey = @Status";
            }
            zSQL += " ORDER BY B.EmployeeID, C.[Rank] ASC, D.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    CloseKey = r["CloseKey"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Message = r["LyDo"].ToString(),
                });
            }

            return zList;
        }
        public static List<Leave_Close_Model> Track(string PartnerNumber, string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.EmployeeKey, A.EmployeeName, C.EmployeeID, C.PositionName, C.DepartmentName,
A.CloseYear, A.CloseEnd, A.[Description], A.Reference, dbo.LyDoNghiPhep(A.Reference) AS LyDo,
B.FromDate, B.ToDate, B.TotalDate
FROM HRM_Leave_Close A
LEFT JOIN HRM_Leave_Note B ON A.Reference = CONVERT(NVARCHAR(50), B.NoteKey)
LEFT JOIN HRM_Employee C ON C.EmployeeKey = A.EmployeeKey
WHERE 
A.RecordStatus <> 99";

            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND C.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND C.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND C.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND A.EmployeeName LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, C.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND C.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND C.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND C.PositionKey = @Position";
            }
            if (Status != 0)
            {
                zSQL += " AND C.WorkingStatusKey = @Status";
            }
            zSQL += " ORDER BY A.CloseYear, A.CloseEnd DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Reference = r["Reference"].ToString(),
                    Message = r["LyDo"].ToString(),
                });
            }
            return zList;
        }
        public static List<Leave_Close_Model> Track(string PartnerNumber, string SearchName, int Year)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.EmployeeKey, A.EmployeeName, C.EmployeeID, C.PositionName, C.DepartmentName,
A.CloseYear, A.CloseEnd, A.[Description], A.Reference, dbo.LyDoNghiPhep(A.Reference) AS LyDo,
B.FromDate, B.ToDate, B.TotalDate
FROM HRM_Leave_Close A
LEFT JOIN HRM_Leave_Note B ON A.Reference = CONVERT(NVARCHAR(50), B.NoteKey)
LEFT JOIN HRM_Employee C ON C.EmployeeKey = A.EmployeeKey
WHERE 
A.RecordStatus <> 99 AND Right(A.CloseYear,4) = @Year ";


            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND A.EmployeeName LIKE @SearchName";
            }
            zSQL += " ORDER BY A.CloseYear, A.CloseEnd DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    Description = r["Description"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Reference = r["Reference"].ToString(),
                    Message = r["LyDo"].ToString(),
                });
            }
            return zList;
        }
    }
}
