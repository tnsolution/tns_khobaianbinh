﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Close_Info
    {

        public Leave_Close_Model Leave_Close = new Leave_Close_Model();

        #region [ Constructor Get Information ]
        public Leave_Close_Info()
        {
            Leave_Close.CloseKey = Guid.NewGuid().ToString();
        }
        public Leave_Close_Info(string CloseKey, string Employee)
        {
            string zSQL = "SELECT * FROM HRM_Leave_Close WHERE CloseKey = @CloseKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = CloseKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Leave_Close.CloseKey = zReader["CloseKey"].ToString();
                    Leave_Close.CloseYear = zReader["CloseYear"].ToString();
                    Leave_Close.Reference = zReader["Reference"].ToString();
                    if (zReader["CloseEnd"] != DBNull.Value)
                    {
                        Leave_Close.CloseEnd = float.Parse(zReader["CloseEnd"].ToString());
                    }
                    Leave_Close.Incremental = zReader["Incremental"].ToFloat();
                    Leave_Close.Description = zReader["Description"].ToString();
                    Leave_Close.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Leave_Close.EmployeeID = zReader["EmployeeID"].ToString();
                    Leave_Close.EmployeeName = zReader["EmployeeName"].ToString();
                    Leave_Close.BranchKey = zReader["BranchKey"].ToString();
                    Leave_Close.BranchName = zReader["BranchName"].ToString();
                    Leave_Close.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Leave_Close.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Leave_Close.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Leave_Close.PositionName = zReader["PositionName"].ToString();
                    Leave_Close.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Leave_Close.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Leave_Close.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Leave_Close.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Leave_Close.CreatedBy = zReader["CreatedBy"].ToString();
                    Leave_Close.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Leave_Close.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Leave_Close.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Leave_Close.ModifiedName = zReader["ModifiedName"].ToString();
                    Leave_Close.Message = "200 OK";
                }
                else
                {
                    Leave_Close.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Leave_Close_Info(string Reference)
        {
            string zSQL = "SELECT * FROM HRM_Leave_Close WHERE Reference = @Reference AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Reference;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Leave_Close.Reference = zReader["Reference"].ToString();
                    Leave_Close.CloseKey = zReader["CloseKey"].ToString();
                    Leave_Close.CloseYear = zReader["CloseYear"].ToString();
                    if (zReader["CloseEnd"] != DBNull.Value)
                    {
                        Leave_Close.CloseEnd = float.Parse(zReader["CloseEnd"].ToString());
                    }
                    Leave_Close.Incremental = zReader["Incremental"].ToFloat();
                    Leave_Close.Description = zReader["Description"].ToString();
                    Leave_Close.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Leave_Close.EmployeeID = zReader["EmployeeID"].ToString();
                    Leave_Close.EmployeeName = zReader["EmployeeName"].ToString();
                    Leave_Close.BranchKey = zReader["BranchKey"].ToString();
                    Leave_Close.BranchName = zReader["BranchName"].ToString();
                    Leave_Close.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Leave_Close.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Leave_Close.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Leave_Close.PositionName = zReader["PositionName"].ToString();
                    Leave_Close.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Leave_Close.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Leave_Close.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Leave_Close.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Leave_Close.CreatedBy = zReader["CreatedBy"].ToString();
                    Leave_Close.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Leave_Close.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Leave_Close.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Leave_Close.ModifiedName = zReader["ModifiedName"].ToString();
                    Leave_Close.Message = "200 OK";
                }
                else
                {
                    Leave_Close.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Close ("
         + "STD, TN, Slug, Reference, CloseYear , CloseEnd, Incremental , Description , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + "@STD, @TN, @Slug, @Reference, @CloseYear , @CloseEnd, @Incremental , @Description , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@STD", SqlDbType.Float).Value = Leave_Close.STD;
                zCommand.Parameters.Add("@TN", SqlDbType.Float).Value = Leave_Close.TN;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Leave_Close.Slug;
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Leave_Close.Reference;
                zCommand.Parameters.Add("@Incremental", SqlDbType.NVarChar).Value = Leave_Close.Incremental;
                zCommand.Parameters.Add("@CloseYear", SqlDbType.NVarChar).Value = Leave_Close.CloseYear;
                zCommand.Parameters.Add("@CloseEnd", SqlDbType.Float).Value = Leave_Close.CloseEnd;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Close.Description;
                if (Leave_Close.EmployeeKey != "" && Leave_Close.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Close.EmployeeName;
                if (Leave_Close.BranchKey != "" && Leave_Close.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Close.BranchName;
                if (Leave_Close.DepartmentKey != "" && Leave_Close.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Close.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Close.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Close.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Close.OrganizationPath;
                if (Leave_Close.PartnerNumber != "" && Leave_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Close.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Close("
         + " Reference, CloseKey , CloseYear , CloseEnd , Incremental, Description , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @Reference, @CloseKey , @CloseYear , @CloseEnd , @Incremental, @Description , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Leave_Close.Reference;
                if (Leave_Close.CloseKey != "" && Leave_Close.CloseKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.CloseKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@Incremental", SqlDbType.NVarChar).Value = Leave_Close.Incremental;

                zCommand.Parameters.Add("@CloseYear", SqlDbType.NVarChar).Value = Leave_Close.CloseYear;
                zCommand.Parameters.Add("@CloseEnd", SqlDbType.Float).Value = Leave_Close.CloseEnd;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Close.Description;
                if (Leave_Close.EmployeeKey != "" && Leave_Close.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Close.EmployeeName;
                if (Leave_Close.BranchKey != "" && Leave_Close.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Close.BranchName;
                if (Leave_Close.DepartmentKey != "" && Leave_Close.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Close.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Close.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Close.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Close.OrganizationPath;
                if (Leave_Close.PartnerNumber != "" && Leave_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Close.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Leave_Close SET "
                        + " CloseYear = @CloseYear,"
                        + " CloseEnd = @CloseEnd, Incremental = @Incremental, Reference = @Reference,"
                        + " Description = @Description,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CloseKey = @CloseKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Leave_Close.Reference;
                if (Leave_Close.CloseKey != "" && Leave_Close.CloseKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.CloseKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@Incremental", SqlDbType.NVarChar).Value = Leave_Close.Incremental;
                zCommand.Parameters.Add("@CloseYear", SqlDbType.NVarChar).Value = Leave_Close.CloseYear;
                zCommand.Parameters.Add("@CloseEnd", SqlDbType.Float).Value = Leave_Close.CloseEnd;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Close.Description;
                if (Leave_Close.EmployeeKey != "" && Leave_Close.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Close.EmployeeName;
                if (Leave_Close.BranchKey != "" && Leave_Close.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Close.BranchName;
                if (Leave_Close.DepartmentKey != "" && Leave_Close.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Close.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Close.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Close.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Close.OrganizationPath;
                if (Leave_Close.PartnerNumber != "" && Leave_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Close.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Leave_Close SET RecordStatus = 99 WHERE CloseKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.CloseKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Leave_Close WHERE CloseKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Close.CloseKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string Reference)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Leave_Close SET RecordStatus = 99 WHERE Reference = @Reference";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Reference;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteByEmployeeKey(string Reference)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE HRM_Leave_Close WHERE Slug = 1 AND EmployeeKey = @Reference";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Reference;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Close.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Close.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
