﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Employee_Data
    {
        string PartnerNumber = "184FDCA5-CCB4-41EC-BE98-85CD3537989B";
        public static string SoThamNien(string employee, string year)
        {
            string ketqua = "";
            string zSQL = @"
DECLARE @FromDate DATETIME;
SELECT @FromDate = StartingDate FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey
SELECT dbo.SoThamNienTinhPhep(@FromDate, @Todate)";

            //lay ngay cuoi cùng của năm hiện tại
            var tmp = new DateTime(year.ToInt(), DateTime.Now.Month, DateTime.Now.Day);
            tmp = new DateTime(tmp.AddYears(1).Year, 1, 1);
            tmp = tmp.AddDays(-1);

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = employee;
                zCommand.Parameters.Add("@Todate", SqlDbType.DateTime).Value = tmp;
                ketqua = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return ketqua;
        }
        public static List<Leave_Note_Model> CacDonPhepPhatSinh(string Branch, string Employee, string year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
DISTINCT A.EmployeeKey, A.EmployeeName, A.BranchName, B.PositionName
FROM HRM_Leave_Note A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'";
            if (Employee != "0" && Employee != String.Empty)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            if (Branch != "0" && Branch != String.Empty)
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            if (year != "0" && year != String.Empty)
            {
                zSQL += " AND YEAR(A.NoteDate) = @YEAR";
            }
            zSQL += " ORDER BY A.BranchName DESC, PositionName DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Branch;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@YEAR", SqlDbType.Int).Value = year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            var zList = new List<Leave_Note_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Note_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Leave_Note_Model> CacDonPhep(string employeekey, string year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.NoteDate, A.[Description], A.FromDate, A.ToDate, A.TotalDate, 
A.CategoryName, A.EmployeeName, A.BranchName, A.EmployeeKey, A.NoteKey
FROM HRM_Leave_Note A
WHERE 
RecordStatus <> 99
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND A.EmployeeKey = @employeekey
AND YEAR(A.NoteDate) = @year";
            zSQL += " ORDER BY A.BranchName DESC, A.EmployeeName, A.ToDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = employeekey;
                zCommand.Parameters.Add("@year", SqlDbType.NVarChar).Value = year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            var zList = new List<Leave_Note_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Note_Model()
                {
                    NoteKey = r["NoteKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    TotalDate = r["TotalDate"].ToInt(),
                    ToDate = Convert.ToDateTime(r["ToDate"]),
                    FromDate = Convert.ToDateTime(r["FromDate"]),
                    NoteDate = Convert.ToDateTime(r["NoteDate"]),
                    Description = r["Description"].ToString()
                });
            }

            return zList;
        }
        public static List<Employee_Model> CacKeyNhanVien(string Branch)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.EmployeeKey
FROM HRM_Employee A 
WHERE 
A.RecordStatus != 99 
AND A.WorkingStatusKey = 1
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'";
            if (Branch != "0" && Branch != String.Empty)
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Branch;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Employee_Model> zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable Report_Index_SUM_V2(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_INDEX_AllBranch_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable Report_Index_V2(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_INDEX_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static List<Document_Model> ListContract(string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.*
FROM HRM_Contract A LEFT JOIN GOB_Document B ON CONVERT(NVARCHAR(50), A.ContractKey) = B.TableKey
WHERE 
A.RecordStatus <> 99
AND EmployeeKey = @EmployeeKey
ORDER BY EmployeeName";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Document_Model> zList = new List<Document_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Document_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    FilePath = r["FilePath"].ToString(),
                    FileName = r["FileName"].ToString(),
                    FileExt = r["FileExt"].ToString(),
                    Description = r["Description"].ToString(),
                    Title = r["Title"].ToString(),
                });
            }

            return zList;
        }

        public static List<Employee_Model> SearchBranch(string Branch)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
dbo.TinhSoNgayThamNien (A.StartingDate,GETDATE()) AS TN,
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.WorkingStatusKey = 1
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'";
            if (Branch != "0")
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Branch;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    TN = r["TN"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                    WorkingStatusName = r["WorkingStatusName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> SearchFilter(string Branch, string type)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
dbo.TinhSoNgayThamNien (A.StartingDate,GETDATE()) AS TN,
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'";

            switch (type)
            {
                default:
                    break;
                //
                case "contract":
                    zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
dbo.TinhSoNgayThamNien (A.StartingDate,GETDATE()) AS TN,
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
OUTER APPLY dbo.GetContractEmployee(A.EmployeeKey) X
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND A.WorkingStatusKey = 1
AND X.ToDate < GETDATE()";
                    break;
                //
                case "off":
                    zSQL += " AND A.WorkingStatusKey = 2";
                    break;
                case "work":
                    zSQL += " AND A.WorkingStatusKey = 1";
                    break;
                case "years":
                    zSQL += " AND A.WorkingStatusKey = 1 AND dbo.SoThamNien(A.StartingDate) < 2";
                    break;
                case "party":
                    zSQL += " AND A.WorkingStatusKey = 1 AND MONTH(Birthday) = " + DateTime.Now.Month;
                    break;
                case "noinsure":
                    zSQL += " AND A.WorkingStatusKey = 1 AND A.Style = 'KO'";
                    break;
                case "insure":
                    zSQL += " AND A.WorkingStatusKey = 1 AND A.Style = 'BH'";
                    break;
            }
            if (Branch != null && Branch != "0")
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (Branch != null && Branch != string.Empty)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Branch;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    TN = r["TN"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                    WorkingStatusName = r["WorkingStatusName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> SearchName(string Name)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
dbo.TinhSoNgayThamNien (A.StartingDate,GETDATE()) AS TN,
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (Name != null)
                {
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    TN = r["TN"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                    WorkingStatusName = r["WorkingStatusName"].ToString(),
                });
            }

            return zList;
        }

        public static List<Leave_Close_Model> XemKyPhep(string EmployeeKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string SQL = @"SELECT 
B.EmployeeKey, B.LastName + ' ' + B.FirstName AS EmployeeName, B.PositionName,
A.CloseYear, A.STD, A.TN, 
A.Incremental, A.[Description], A.Reference, A.CloseEnd, 
C.FromDate, C.ToDate, C.[Description] AS LyDo, C.TotalDate
FROM HRM_Leave_Close A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_Leave_Note C ON A.Reference = CAST(C.NoteKey AS NVARCHAR(50))
WHERE 
A.EmployeeKey = @EmployeeKey
AND A.SLUG = 1
AND A.RecordStatus <> 99
AND C.RecordStatus <> 99
ORDER BY A.[rank]";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            var zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    STD = r["STD"].ToFloat(),
                    TN = r["TN"].ToFloat(),
                    LyDo = r["LyDo"].ToString(),
                    Reference = r["Reference"].ToString(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    TotalDate = r["TotalDate"].ToFloat(),
                    FromDate = Convert.ToDateTime(r["FromDate"]),
                    ToDate = Convert.ToDateTime(r["ToDate"])
                });
            }
            return zList;
        }
        public static List<Leave_Close_Model> XemKyPhepChiNhanh(string ChiNhanh, out string Message)
        {
            DataTable zTable = new DataTable();
            string SQL = @"SELECT 
B.EmployeeKey, B.LastName + ' ' + B.FirstName AS EmployeeName, B.PositionName,
A.CloseYear, A.STD, A.TN, 
A.Incremental, A.[Description], A.Reference, A.CloseEnd, 
C.FromDate, C.ToDate, C.[Description] AS LyDo, C.TotalDate, C.BranchKey, C.BranchName
FROM HRM_Leave_Close A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_Leave_Note C ON A.Reference = CAST(C.NoteKey AS NVARCHAR(50))
WHERE 
A.RecordStatus <> 99
AND C.RecordStatus <> 99
AND A.SLUG = 1";
            if (ChiNhanh != "")
            {
                SQL += " AND B.BranchKey = @BranchKey";
            }
            SQL += " ORDER BY A.[RANK]";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = ChiNhanh;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            var zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    STD = r["STD"].ToFloat(),
                    TN = r["TN"].ToFloat(),
                    LyDo = r["LyDo"].ToString(),
                    Reference = r["Reference"].ToString(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    TotalDate = r["TotalDate"].ToFloat(),
                    FromDate = Convert.ToDateTime(r["FromDate"]),
                    ToDate = Convert.ToDateTime(r["ToDate"])
                });
            }
            return zList;
        }
        public static DataTable NoteIncurred(string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
        SELECT A.NoteKey, A.NoteDate, A.FromDate, A.ToDate, A.[Description], A.TotalDate
        FROM HRM_Leave_Note A
        WHERE A.EmployeeKey = @EmployeeKey
        AND RecordStatus <> 99
        ORDER BY NoteDate ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static List<Leave_Close_Model> TINHPHEP(string EmployeeKey, DateTime FromDate, DateTime ToDate, float Number, string NoteKey, out string Message)
        {

            DataTable zTable = new DataTable();
            string SQL = @"TINHPHEPANBINH_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = Number;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.NVarChar).Value = NoteKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            //var zList = zTable.ToList<Leave_Close_Model>();
            var zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    CloseYear = r["CloseYear"].ToString(),
                    CloseEnd = r["CloseEnd"].ToFloat(),
                    STD = r["STD"].ToFloat(),
                    TN = r["TN"].ToFloat(),
                    Incremental = r["Incremental"].ToFloat(),
                    Description = r["Description"].ToString(),
                    Reference = r["Reference"].ToString(),
                });
            }
            return zList;
        }

        public static DataTable PayrollIndex_Employee(string PartnerNumber, int Year, int Month, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string MonthParam = "";
            if (Month == 0)
            {
                for (int i = 1; i <= 12; i++)
                {
                    MonthParam += i.ToString("00") + ",";
                }
                MonthParam = MonthParam.Remove(MonthParam.LastIndexOf(","));
            }
            else
            {
                MonthParam = Month.ToString();
            }

            string zSQL = @"
SELECT A.CategoryName, A.ItemName, A.Amount, LEFT(B.CloseDate, 2) AS [MONTH]
FROM HRM_Payroll_Index A
LEFT JOIN HRM_Payroll_Close B ON A.ParentKey = B.CloseKey
WHERE 
A.EmployeeKey = @EmployeeKey
AND A.RecordStatus <> 99
AND B.RecordStatus <> 99
AND RIGHT(B.CloseDate, 4) = @Year
AND LEFT(B.CloseDate, 2) IN (" + MonthParam + ") ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable PayrollIndex_Branch(string PartnerNumber, int Year, int Month, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string Branch = "";
            if (BranchKey == "0")
            {
                var ListBranch = Branch_Data.List(PartnerNumber);
                foreach (var item in ListBranch)
                {
                    Branch += item.BranchKey + ",";
                }
                Branch = Branch.Remove(Branch.LastIndexOf(","));
            }
            else
            {
                Branch = BranchKey;
            }

            string MonthParam = "";
            if (Month == 0)
            {
                for (int i = 1; i <= 12; i++)
                {
                    MonthParam += i.ToString("00") + ",";
                }
                MonthParam = MonthParam.Remove(MonthParam.LastIndexOf(","));
            }
            else
            {
                MonthParam = Month.ToString("00");
            }

            string zSQL = @"TONGHOPLUONG_CHINNHANH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Branch;
                zCommand.Parameters.Add("@MONTH", SqlDbType.NVarChar).Value = MonthParam;
                zCommand.Parameters.Add("@YEAR", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
        public static DataTable PayrollIndex_Branch_V2(string PartnerNumber, int Year)
        {
            DataTable zTable = new DataTable();
            string MonthParam = "";
            for (int i = 1; i <= 12; i++)
            {
                MonthParam += i.ToString("00") + ",";
            }
            MonthParam = MonthParam.Remove(MonthParam.LastIndexOf(","));
            string zSQL = @"TONGHOPLUONG_CHINNHANH_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;

                zCommand.Parameters.Add("@MONTH", SqlDbType.NVarChar).Value = MonthParam;
                zCommand.Parameters.Add("@YEAR", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }

        public static DataTable BranchSummary()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, 
dbo.SoNhanVienLamViec(A.BranchKey) AS NSLV, 
dbo.SoNhanVienNghiViec(A.BranchKey) AS NSNV,
dbo.SoNhanVienCoBHXH(A.BranchKey) AS COBHXH,
dbo.SoNhanVienKoCoXH(A.BranchKey) AS KOBHXH,
dbo.SoNhanVienMoiVaoLam(A.BranchKey) AS MOIVO,
dbo.SoNhanVienCoHDHetHan(A.BranchKey) AS HDDH
FROM HRM_Branch A
WHERE PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND A.RECORDSTATUS <> 99
ORDER BY BranchName DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable BranchSummary(string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, 
dbo.SoNhanVienLamViec(A.BranchKey) AS NSLV, 
dbo.SoNhanVienNghiViec(A.BranchKey) AS NSNV,
dbo.SoNhanVienCoBHXH(A.BranchKey) AS COBHXH,
dbo.SoNhanVienKoCoXH(A.BranchKey) AS KOBHXH,
dbo.SoNhanVienMoiVaoLam(A.BranchKey) AS MOIVO,
dbo.SoNhanVienCoHDHetHan(A.BranchKey) AS HDDH
FROM HRM_Branch A
WHERE PartnerNumber = '184FDCA5-CCB4-41EC-BE98-85CD3537989B'
AND A.RECORDSTATUS <> 99";

            if (BranchKey != null && BranchKey != "0")
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            zSQL += " ORDER BY BranchName DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (BranchKey != null)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static List<Employee_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> ListRECURSIVE(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
; WITH R AS (
	-- ANCHOR PART
      SELECT 
		  A.EmployeeKey, 
		  A.LastName,
		  A.FirstName,
		  A.ReportToKey,
          A.PhotoPath,
		  B.PositionKey,
		  DEPTH = 0 , 
		  SORT = CAST(A.EmployeeKey AS VARCHAR(MAX))
      FROM HRM_Employee A LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey
      WHERE A.RecordStatus <> 99
      AND ReportToKey IS NULL	  
	  AND A.PartnerNumber = @PartnerNumber
      UNION ALL

	-- RECURSIVE PART
      SELECT 		
		Sub.EmployeeKey, 
		Sub.LastName, 
		Sub.FirstName,
		Sub.ReportToKey,
        Sub.PhotoPath,
		Sub.PositionKey,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '>' + CAST(Sub.EmployeeKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN HRM_Employee Sub ON R.EmployeeKey=Sub.ReportToKey	  
	  WHERE Sub.RecordStatus <> 99	  
	  AND Sub.PartnerNumber = @PartnerNumber
)

SELECT R.LastName, R.FirstName, R.SORT, R.DEPTH, R.EmployeeKey, ReportToKey, PhotoPath
FROM R 
ORDER BY SORT";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    ReportToKey = r["ReportToKey"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Slug = r["DEPTH"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                });
            }

            return zList;
        }

        public static List<Employee_Model> Search(
            string PartnerNumber, string SearchName, string Department,
            DateTime FromDate, DateTime ToDate, int FromAge, int ToAge,
            int Gender, string Edu, string Position, int Status, string Social)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Social != string.Empty)
                zSQL += " AND A.Style = @Social";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND A.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND (LastName + ' ' + FirstName) LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, A.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND A.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND A.PositionKey = @Position";
            }
            if (Status != 0)
            {
                zSQL += " AND A.WorkingStatusKey = @Status";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Social", SqlDbType.NVarChar).Value = Social;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Employee_Model> zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);
                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    WorkingStatusName = r["WorkingStatusName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Employee_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
dbo.TinhSoNgayThamNien (A.StartingDate, GETDATE()) AS TN,
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.WorkingStatusKey = 1
AND A.PartnerNumber = @PartnerNumber
ORDER BY C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    TN = r["TN"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                    WorkingStatusName = r["WorkingStatusName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> ListReportTo(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT EmployeeKey, LastName, FirstName 
FROM HRM_Employee A
LEFT JOIN HRM_ListPosition B ON B.PositionKey=A.PositionKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND B.[TypeKey] = 1  
ORDER BY B.Rank ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                });
            }

            return zList;
        }

        public static List<Payroll_Item> TINHLUONG(string PartnerNumber, string Employee, DateTime Date, string Id)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TINHLUONGANBINH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                zCommand.Parameters.Add("@Id", SqlDbType.NVarChar).Value = Id;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Item> zList = new List<Payroll_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Item()
                {
                    ItemKey = r["ItemKey"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Param = r["Param"].ToString(),
                    Formula = r["Formula"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Amount = r["Amount"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable ListPayrollClose(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT JsonData FROM HRM_Payroll_Close_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Payroll_Item> GetPayrollClose(string PartnerNumber, string CloseDate, string EmployeeKey)
        {
            string zJSON = "";
            //string zDate = Date.ToString("MM/yyyy");
            string zSQL = "SELECT JsonData FROM HRM_Payroll_Close_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey AND CloseDate = @CloseDate";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = CloseDate;
                zJSON = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Item> zList = JsonConvert.DeserializeObject<List<Payroll_Item>>(zJSON);

            return zList;
        }

        public static List<Leave_Item> TINHPHEP(string PartnerNumber, string Employee, string YearView, string Name, string ID, string Posittion, string Department, DateTime YearWork)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TINHPHEPANBINH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@YearView", SqlDbType.NVarChar).Value = YearView;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Item> zList = new List<Leave_Item>();
            if (zTable.Rows.Count > 0)
            {
                float Total = 0;
                float CloseIn = 0;
                int Index = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    Index++;

                    float CloseEnd = r["CloseEnd"].ToFloat();
                    float BeginNo = r["BeginNo"].ToFloat();
                    float BeginPlus = r["BeginPlus"].ToFloat();
                    float Incurred = r["TotalDate"].ToFloat();
                    CloseIn += BeginNo + BeginPlus - Incurred;
                    Total += CloseEnd + BeginNo + BeginPlus - Incurred;

                    zList.Add(new Leave_Item()
                    {
                        EmployeeID = ID,
                        EmployeeKey = Employee,
                        EmployeeName = Name,
                        PositionName = Posittion,
                        DepartmentName = Department,
                        Sort = r["Sort"].ToString(),
                        BeginYear = r["BeginYear"].ToString(),
                        BeginNo = BeginNo.ToString(),
                        BeginPlus = BeginPlus.ToString(),
                        FromDate = r["FromDate"].ToDateString(),
                        ToDate = r["ToDate"].ToDateString(),
                        TotalDate = Incurred.ToString(),
                        CloseEnd = CloseIn.ToString(),
                        CloseYear = r["CloseYear"].ToString(),
                        Incremental = Total.ToString(),
                        CategoryName = r["CategoryName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }

                TN_Utils.CalculateYourTime(YearWork, DateTime.Now, out int Plus);
                if (Plus - 5 >= 0)
                {
                    Plus = Plus / 5;
                }
                else
                {
                    Plus = 0;
                }

                Total += Plus + 12;

                string messg = "Số phép đắt đầu năm " + YearView + " !. <br />";
                messg += " 12 tiêu chuẩn, " + Plus + " thâm niên !. <br />";
                messg += " Số phép tích lũy tính đến hết năm " + YearView + " là " + Total + " !. <br />";

                zList.Add(new Leave_Item()
                {
                    EmployeeID = ID,
                    EmployeeKey = Employee,
                    EmployeeName = Name,
                    PositionName = Posittion,
                    DepartmentName = Department,
                    Sort = YearView,
                    BeginYear = YearView,
                    BeginNo = "12",
                    BeginPlus = Plus.ToString(),
                    FromDate = "",
                    ToDate = "",
                    TotalDate = "",
                    CloseEnd = CloseIn.ToString(),
                    CloseYear = YearView,
                    Incremental = Total.ToString(),
                    CategoryName = "",
                    Description = messg,
                });

            }
            return zList;
        }

        public static List<Employee_Model> Ready_Payroll(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
D.*, B.[Rank] 
FROM HRM_Payroll_Close_Detail A
LEFT JOIN HRM_Employee D ON A.EmployeeKey = D.EmployeeKey
LEFT JOIN HRM_ListPosition B ON D.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON D.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus <> 99
AND D.PartnerNumber = @PartnerNumber
AND D.WorkingStatusKey <> 2
AND A.ParentKey = @Parent
ORDER BY D.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent.ToUpper();
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable Report_BHXH(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_BHXH_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            if (zTable.Rows.Count > 0)
            {
                zTable.Columns[0].ColumnName = "NỘI DUNG";
                DataColumn Col = zTable.Columns.Add("TOÀN CÔNG TY", typeof(double));
                Col.SetOrdinal(1);

                foreach (DataRow r in zTable.Rows)
                {
                    double Total = 0;
                    for (int i = 1; i < zTable.Columns.Count - 1; i++)
                    {
                        Total += r[i].ToDouble();
                    }
                    r[1] = Total;
                }

            }

            return zTable;
        }

        public static List<Payroll_Item> PayrollData(string PartnerNumber, string Parent, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, dbo.Get_PhotoPath(EmployeeKey) AS PhotoPath FROM HRM_Payroll_Index WHERE PartnerNumber = @PartnerNumber AND ParentKey = @ParentKey";
            if (BranchKey != string.Empty)
                zSQL += " AND BranchKey = @BranchKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Item> zList = new List<Payroll_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Item()
                {
                    PhotoPath = r["PhotoPath"].ToString(),
                    Style = r["Style"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    Branch = r["BranchName"].ToString(),
                    ItemKey = r["ItemKey"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Param = r["Param"].ToString(),
                    Formula = r["Formula"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Amount = r["Amount"].ToString(),
                });
            }

            return zList;
        }
        public static List<Payroll_Item> PayrollData(string PartnerNumber, string Month, int Year, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT 
dbo.Get_PhotoPath(A.EmployeeKey) AS PhotoPath,
A.EmployeeKey, A.BranchKey, A.BranchName,
A.BranchName, A.ItemName, A.EmployeeName, A.CategoryName, 
A.ItemKey, A.ItemID, A.PositionName, 
A.Formula, A.CategoryKey, A.DepartmentKey, A.DepartmentName,
ISNULL(SUM(A.Amount),0) AS Amount
FROM HRM_Payroll_Index A LEFT JOIN HRM_Payroll_Close B ON A.ParentKey = B.CloseKey
WHERE
A.PartnerNumber = @PartnerNumber
		AND A.RecordStatus <> 99
		AND B.RecordStatus <> 99
		AND A.BranchKey IN (SELECT [Value] FROM dbo.TOL_split_string_to_column(@KEY,',')) 
		AND LEFT(B.CloseDate,2) IN (SELECT [Value] FROM dbo.TOL_split_string_to_column(@MONTH,','))
		AND RIGHT(B.CloseDate,4) = @YEAR
		GROUP BY 
A.EmployeeKey, A.BranchName, A.ItemName, A.EmployeeName, A.CategoryName, 
A.ItemKey, A.ItemID, A.PositionName, A.BranchKey, A.BranchName,
A.Formula, A.CategoryKey, A.DepartmentKey, A.DepartmentName
ORDER BY A.BranchName";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@MONTH", SqlDbType.NVarChar).Value = Month;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = BranchKey;
                zCommand.Parameters.Add("@YEAR", SqlDbType.NVarChar).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Item> zList = new List<Payroll_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Item()
                {
                    PhotoPath = r["PhotoPath"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    Branch = r["BranchName"].ToString(),
                    ItemKey = r["ItemKey"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Formula = r["Formula"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Amount = r["Amount"].ToString(),
                });
            }

            return zList;
        }

        public static string BranchName(string Key)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.AB_GetBranchName(@BranchKey)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Key;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static string GetPhoto(string Employee)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT PhotoPath FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static DataTable Report_Index(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_INDEX";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            if (zTable.Rows.Count > 0)
            {
                zTable.Columns[0].ColumnName = "NỘI DUNG";
                DataColumn Col = zTable.Columns.Add("0", typeof(double));
                Col.SetOrdinal(1);

                foreach (DataRow r in zTable.Rows)
                {
                    double Total = 0;
                    for (int i = 1; i < zTable.Columns.Count - 1; i++)
                    {
                        Total += r[i].ToDouble();
                    }
                    r[1] = Total;
                }
            }
            return zTable;
        }
        public static DataTable Report_Index_Branch(string PartnerNumber, string Parent, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL;
            if (BranchKey != string.Empty)
                zSQL = "TONGHOP_INDEX_Branch";
            else
                zSQL = "TONGHOP_INDEX_AllBranch";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                if (BranchKey != string.Empty)
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static List<Employee_Model> List_Seniority(string PartnerNumber, string Employee, int Year, int Status)
        {
            DateTime zDate = new DateTime(Year, 12, 31, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey,D.BranchName,A.EmployeeKey,A.EmployeeID,A.LastName,A.FirstName,B.PositionNameVN AS PositionName,A.Gender,A.Birthday,A.StartingDate,
[dbo].[TinhSoNgayThamNien] (A.StartingDate,@Year) AS Seniority
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
LEFT JOIN HRM_Branch D ON A.BranchKey = D.BranchKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber= @PartnerNumber ";
            if (Status != 0)
            {
                zSQL += " AND A.WorkingStatusKey = @StatusKey";
            }
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY D.BranchName DESC, A.EmployeeID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Year", SqlDbType.DateTime).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    Note = r["Seniority"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable Table_Seniority(string PartnerNumber, string Employee, int Year, int Status)
        {
            DateTime zDate = new DateTime(Year, 12, 31, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey,D.BranchName,A.EmployeeKey,A.EmployeeID,
A.LastName,A.FirstName,
B.PositionNameVN AS PositionName,A.Gender,A.Birthday,A.StartingDate,
[dbo].[TinhSoNgayThamNien] (A.StartingDate,@Year) AS Seniority,
A.WorkingStatusName
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
LEFT JOIN HRM_Branch D ON A.BranchKey = D.BranchKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber= @PartnerNumber ";
            if (Status != 0)
            {
                zSQL += " AND A.WorkingStatusKey = @StatusKey";
            }
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY D.BranchName DESC, A.EmployeeID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Year", SqlDbType.DateTime).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
    }
}