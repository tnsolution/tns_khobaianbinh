﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Review_Info
    {

        public Review_Model Review = new Review_Model();

        #region [ Constructor Get Information ]
        public Review_Info()
        {
            Review.ReviewKey = Guid.NewGuid().ToString();
        }
        public Review_Info(string ReviewKey)
        {
            string zSQL = "SELECT * FROM HRM_Review WHERE ReviewKey = @ReviewKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ReviewKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Review.ReviewKey = zReader["ReviewKey"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                    {
                        Review.DateWrite = (DateTime)zReader["DateWrite"];
                    }

                    Review.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Review.EmployeeName = zReader["EmployeeName"].ToString();
                    Review.BranchKey = zReader["BranchKey"].ToString();
                    Review.BranchName = zReader["BranchName"].ToString();
                    Review.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Review.DepartmentName = zReader["DepartmentName"].ToString();
                    Review.ValuerKey = zReader["ValuerKey"].ToString();
                    Review.ValuerName = zReader["ValuerName"].ToString();
                    Review.Description = zReader["Description"].ToString();
                    Review.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Review.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Review.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Review.CreatedBy = zReader["CreatedBy"].ToString();
                    Review.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Review.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Review.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Review.ModifiedName = zReader["ModifiedName"].ToString();
                    Review.Message = "200 OK";
                }
                else
                {
                    Review.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Review.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Review ("
         + " DateWrite , EmployeeKey , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , ValuerKey , ValuerName , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @DateWrite , @EmployeeKey , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @ValuerKey , @ValuerName , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Review.DateWrite == null)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Review.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Review.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Review.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Review.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Review.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Review.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Review.DepartmentName;
                zCommand.Parameters.Add("@ValuerKey", SqlDbType.NVarChar).Value = Review.ValuerKey;
                zCommand.Parameters.Add("@ValuerName", SqlDbType.NVarChar).Value = Review.ValuerName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review.Description;
                if (Review.PartnerNumber != "" && Review.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Review.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Review.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Review.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Review("
         + " ReviewKey , DateWrite , EmployeeKey , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , ValuerKey , ValuerName , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ReviewKey , @DateWrite , @EmployeeKey , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @ValuerKey , @ValuerName , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Review.ReviewKey = Guid.NewGuid().ToString();
                if (Review.ReviewKey != "" && Review.ReviewKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.ReviewKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Review.DateWrite == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Review.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Review.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Review.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Review.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Review.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Review.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Review.DepartmentName;
                zCommand.Parameters.Add("@ValuerKey", SqlDbType.NVarChar).Value = Review.ValuerKey;
                zCommand.Parameters.Add("@ValuerName", SqlDbType.NVarChar).Value = Review.ValuerName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review.Description;
                if (Review.PartnerNumber != "" && Review.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Review.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Review.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Review.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Review SET "
                        + " DateWrite = @DateWrite,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " ValuerKey = @ValuerKey,"
                        + " ValuerName = @ValuerName,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ReviewKey = @ReviewKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Review.ReviewKey != "" && Review.ReviewKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.ReviewKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Review.DateWrite == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Review.DateWrite;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Review.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Review.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Review.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Review.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Review.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Review.DepartmentName;
                zCommand.Parameters.Add("@ValuerKey", SqlDbType.NVarChar).Value = Review.ValuerKey;
                zCommand.Parameters.Add("@ValuerName", SqlDbType.NVarChar).Value = Review.ValuerName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review.Description;
                if (Review.PartnerNumber != "" && Review.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Review SET RecordStatus = 99 WHERE ReviewKey = @ReviewKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.ReviewKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Review WHERE ReviewKey = @ReviewKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review.ReviewKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
