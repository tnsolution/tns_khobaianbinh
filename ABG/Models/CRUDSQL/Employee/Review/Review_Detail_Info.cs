﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Review_Detail_Info
    {

        public Review_Detail_Model Review_Detail = new Review_Detail_Model();

        #region [ Constructor Get Information ]
        public Review_Detail_Info()
        {
        }
        public Review_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Review_Detail WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Review_Detail.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Review_Detail.ReviewKey = zReader["ReviewKey"].ToString();
                    if (zReader["CriteriaKey"] != DBNull.Value)
                    {
                        Review_Detail.CriteriaKey = int.Parse(zReader["CriteriaKey"].ToString());
                    }

                    Review_Detail.CriteriaName = zReader["CriteriaName"].ToString();
                    if (zReader["Point"] != DBNull.Value)
                    {
                        Review_Detail.Point = int.Parse(zReader["Point"].ToString());
                    }

                    if (zReader["MaxPoint"] != DBNull.Value)
                    {
                        Review_Detail.MaxPoint = int.Parse(zReader["MaxPoint"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Review_Detail.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    Review_Detail.Description = zReader["Description"].ToString();
                    Review_Detail.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Review_Detail.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Review_Detail.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Review_Detail.CreatedBy = zReader["CreatedBy"].ToString();
                    Review_Detail.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Review_Detail.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Review_Detail.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Review_Detail.ModifiedName = zReader["ModifiedName"].ToString();
                    Review_Detail.Message = "200 OK";
                }
                else
                {
                    Review_Detail.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Review_Detail ("
         + " ReviewKey , CriteriaKey , CriteriaName , Point , MaxPoint , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ReviewKey , @CriteriaKey , @CriteriaName , @Point , @MaxPoint , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.NVarChar).Value = Review_Detail.ReviewKey;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Review_Detail.CriteriaKey;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Review_Detail.CriteriaName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Review_Detail.Point;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Review_Detail.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Review_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review_Detail.Description;
                if (Review_Detail.PartnerNumber != "" && Review_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Review_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Review_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Review_Detail("
         + " AutoKey , ReviewKey , CriteriaKey , CriteriaName , Point , MaxPoint , Rank , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ReviewKey , @CriteriaKey , @CriteriaName , @Point , @MaxPoint , @Rank , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Review_Detail.AutoKey;
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.NVarChar).Value = Review_Detail.ReviewKey;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Review_Detail.CriteriaKey;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Review_Detail.CriteriaName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Review_Detail.Point;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Review_Detail.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Review_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review_Detail.Description;
                if (Review_Detail.PartnerNumber != "" && Review_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Review_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Review_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Review_Detail SET "
                        + " ReviewKey = @ReviewKey,"
                        + " CriteriaKey = @CriteriaKey,"
                        + " CriteriaName = @CriteriaName,"
                        + " Point = @Point,"
                        + " MaxPoint = @MaxPoint,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Review_Detail.AutoKey;
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.NVarChar).Value = Review_Detail.ReviewKey;
                zCommand.Parameters.Add("@CriteriaKey", SqlDbType.Int).Value = Review_Detail.CriteriaKey;
                zCommand.Parameters.Add("@CriteriaName", SqlDbType.NVarChar).Value = Review_Detail.CriteriaName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Review_Detail.Point;
                zCommand.Parameters.Add("@MaxPoint", SqlDbType.Int).Value = Review_Detail.MaxPoint;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Review_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Review_Detail.Description;
                if (Review_Detail.PartnerNumber != "" && Review_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Review_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Review_Detail.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Review_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Review_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Review_Detail SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Review_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Review_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Review_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Review_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Review_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
