﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Person_Detail_Data
    {
        public static List<Payroll_Person_Detail_Model> List(string PartnerNumber,string PayrollKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Person_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND PayrollKey =@PayrollKey ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.NVarChar).Value = PayrollKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Person_Detail_Model> zList = new List<Payroll_Person_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Person_Detail_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    ItemType = r["ItemType"].ToInt(),
                    Total = r["Total"].ToFloat(),
                    Description = r["Description"].ToString()
                });
            }
            return zList;
        }
    }
}
