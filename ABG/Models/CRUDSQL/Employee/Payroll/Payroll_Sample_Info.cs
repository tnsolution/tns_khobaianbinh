﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Payroll_Sample_Info
    {
        #region [ Field Name ]
        private int _ItemKey = 0;
        private string _ItemName = "";
        private int _ItemType = 0;
        private string _ItemID = "";
        private float _Quantity = 0;
        private string _UnitName = "";
        private double _Total = 0;
        private int _IsContract = 0;
        private int _Rank = 0;
        private string _Description = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private float _Parameter = 0;
        private string _Formula = "";
        private int _RecordStatus = 0;
        private string _PartnerNumber = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion
        #region [ Properties ]
        public int ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public int ItemType
        {
            get { return _ItemType; }
            set { _ItemType = value; }
        }
        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public double Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
        public int IsContract
        {
            get { return _IsContract; }
            set { _IsContract = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public float Parameter
        {
            get { return _Parameter; }
            set { _Parameter = value; }
        }
        public string Formula
        {
            get { return _Formula; }
            set { _Formula = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
        #region [ ExtraInfo ]
        private string _Message = "";
        public int Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return int.Parse(_Message.Substring(0, 3));
                else return -1;
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Payroll_Sample_Info()
        {
        }
        public Payroll_Sample_Info(int ItemKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Sample WHERE ItemKey = @ItemKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = ItemKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ItemKey"] != DBNull.Value)
                        _ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    _ItemName = zReader["ItemName"].ToString();
                    if (zReader["ItemType"] != DBNull.Value)
                        _ItemType = int.Parse(zReader["ItemType"].ToString());
                    _ItemID = zReader["ItemID"].ToString();
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    _UnitName = zReader["UnitName"].ToString();
                    if (zReader["Total"] != DBNull.Value)
                        _Total = double.Parse(zReader["Total"].ToString());
                    if (zReader["IsContract"] != DBNull.Value)
                        _IsContract = int.Parse(zReader["IsContract"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["Parameter"] != DBNull.Value)
                        _Parameter = float.Parse(zReader["Parameter"].ToString());
                    _Formula = zReader["Formula"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Sample ("
         + " ItemName , ItemType , ItemID , Quantity , UnitName , Total , IsContract , Rank , Description , CategoryKey , CategoryName , Parameter , Formula , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ItemName , @ItemType , @ItemID , @Quantity , @UnitName , @Total , @IsContract , @Rank , @Description , @CategoryKey , @CategoryName , @Parameter , @Formula , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            zSQL += "SELECT ItemKey FROM HRM_Payroll_Sample WHERE ItemKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = _ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = _Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = _IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = _Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = _Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_PartnerNumber != "" && _PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _ItemKey = Convert.ToInt64(zCommand.ExecuteScalar());
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Sample("
         + " ItemKey , ItemName , ItemType , ItemID , Quantity , UnitName , Total , IsContract , Rank , Description , CategoryKey , CategoryName , Parameter , Formula , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ItemKey , @ItemName , @ItemType , @ItemID , @Quantity , @UnitName , @Total , @IsContract , @Rank , @Description , @CategoryKey , @CategoryName , @Parameter , @Formula , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = _ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = _Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = _IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = _Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = _Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_PartnerNumber != "" && _PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Sample SET "
                        + " ItemName = @ItemName,"
                        + " ItemType = @ItemType,"
                        + " ItemID = @ItemID,"
                        + " Quantity = @Quantity,"
                        + " UnitName = @UnitName,"
                        + " Total = @Total,"
                        + " IsContract = @IsContract,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Parameter = @Parameter,"
                        + " Formula = @Formula,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ItemKey = @ItemKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = _ItemType;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = _Total;
                zCommand.Parameters.Add("@IsContract", SqlDbType.Int).Value = _IsContract;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@Parameter", SqlDbType.Float).Value = _Parameter;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = _Formula;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_PartnerNumber != "" && _PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Sample SET RecordStatus = 99 WHERE ItemKey = @ItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Sample WHERE ItemKey = @ItemKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
