﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Over_Time_Detail_Info
    {

        public Over_Time_Detail_Model Over_Time_Detail = new Over_Time_Detail_Model();

        #region [ Constructor Get Information ]
        public Over_Time_Detail_Info()
        {
        }
        public Over_Time_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Over_Time_Detail WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Over_Time_Detail.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Over_Time_Detail.ParentKey = zReader["ParentKey"].ToString();
                    Over_Time_Detail.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Over_Time_Detail.EmployeeID = zReader["EmployeeID"].ToString();
                    Over_Time_Detail.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                    {
                        Over_Time_Detail.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    }

                    Over_Time_Detail.OrganizationID = zReader["OrganizationID"].ToString();
                    Over_Time_Detail.OrganizationName = zReader["OrganizationName"].ToString();
                    Over_Time_Detail.BranchKey = zReader["BranchKey"].ToString();
                    Over_Time_Detail.BranchName = zReader["BranchName"].ToString();
                    Over_Time_Detail.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Over_Time_Detail.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Over_Time_Detail.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Over_Time_Detail.PositionName = zReader["PositionName"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                    {
                        Over_Time_Detail.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    }

                    Over_Time_Detail.ItemID = zReader["ItemID"].ToString();
                    Over_Time_Detail.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Paramater"] != DBNull.Value)
                    {
                        Over_Time_Detail.Paramater = float.Parse(zReader["Paramater"].ToString());
                    }

                    if (zReader["Hours"] != DBNull.Value)
                    {
                        Over_Time_Detail.Hours = float.Parse(zReader["Hours"].ToString());
                    }

                    if (zReader["OverTimeDate"] != DBNull.Value)
                    {
                        Over_Time_Detail.OverTimeDate = (DateTime)zReader["OverTimeDate"];
                    }

                    Over_Time_Detail.Description = zReader["Description"].ToString();
                    Over_Time_Detail.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Over_Time_Detail.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Over_Time_Detail.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Over_Time_Detail.CreatedBy = zReader["CreatedBy"].ToString();
                    Over_Time_Detail.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Over_Time_Detail.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Over_Time_Detail.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Over_Time_Detail.ModifiedName = zReader["ModifiedName"].ToString();
                    Over_Time_Detail.Message = "200 OK";
                }
                else
                {
                    Over_Time_Detail.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Over_Time_Detail ("
         + " ParentKey , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , ItemKey , ItemID , ItemName , Paramater , Hours , OverTimeDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ParentKey , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @ItemKey , @ItemID , @ItemName , @Paramater , @Hours , @OverTimeDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time_Detail.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time_Detail.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time_Detail.PositionName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time_Detail.Paramater;
                zCommand.Parameters.Add("@Hours", SqlDbType.Float).Value = Over_Time_Detail.Hours;
                if (Over_Time_Detail.OverTimeDate == null)
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time_Detail.OverTimeDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time_Detail.Description;
                if (Over_Time_Detail.PartnerNumber != "" && Over_Time_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Over_Time_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Over_Time_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Over_Time_Detail("
         + " AutoKey , ParentKey , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , ItemKey , ItemID , ItemName , Paramater , Hours , OverTimeDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ParentKey , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @ItemKey , @ItemID , @ItemName , @Paramater , @Hours , @OverTimeDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Over_Time_Detail.AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time_Detail.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time_Detail.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time_Detail.PositionName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time_Detail.Paramater;
                zCommand.Parameters.Add("@Hours", SqlDbType.Float).Value = Over_Time_Detail.Hours;
                if (Over_Time_Detail.OverTimeDate == null)
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time_Detail.OverTimeDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time_Detail.Description;
                if (Over_Time_Detail.PartnerNumber != "" && Over_Time_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Over_Time_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Over_Time_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Over_Time_Detail SET "
                        + " ParentKey = @ParentKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " OrganizationKey = @OrganizationKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " OrganizationName = @OrganizationName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " Paramater = @Paramater,"
                        + " Hours = @Hours,"
                        + " OverTimeDate = @OverTimeDate,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Over_Time_Detail.AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time_Detail.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time_Detail.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time_Detail.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time_Detail.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time_Detail.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time_Detail.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time_Detail.PositionName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time_Detail.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time_Detail.Paramater;
                zCommand.Parameters.Add("@Hours", SqlDbType.Float).Value = Over_Time_Detail.Hours;
                if (Over_Time_Detail.OverTimeDate == null)
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time_Detail.OverTimeDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time_Detail.Description;
                if (Over_Time_Detail.PartnerNumber != "" && Over_Time_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time_Detail.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Over_Time_Detail SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Over_Time_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Over_Time_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Over_Time_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
