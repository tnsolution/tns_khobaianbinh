﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Over_Time_Data
    {
        public static List<Over_Time_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Over_Time WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {

            }
            List<Over_Time_Model> zList = new List<Over_Time_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime.TryParse(r["OverTimeDate"].ToString(), out DateTime zDate);
                zList.Add(new Over_Time_Model()
                {
                    OvertimeKey = r["OvertimeKey"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    OverTimeDate = zDate,
                    Paramater = r["Paramater"].ToFloat(),
                    Description = r["Description"].ToString(),
                });
            }
            return zList;
        }
        public static List<Over_Time_Model> List(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Over_Time WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ParentKey = @Parent";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {

            }
            List<Over_Time_Model> zList = new List<Over_Time_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime.TryParse(r["OverTimeDate"].ToString(), out DateTime zDate);
                zList.Add(new Over_Time_Model()
                {
                    OvertimeKey = r["OvertimeKey"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    OverTimeDate = zDate,
                    Paramater = r["Paramater"].ToFloat(),
                    Description = r["Description"].ToString(),
                });
            }
            return zList;
        }

        public static List<Over_Time_Detail_Model> ListDetail(string PartnerNumber, string ParentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Over_Time_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ParentKey = @ParentKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Over_Time_Detail_Model> zList = new List<Over_Time_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime.TryParse(r["OverTimeDate"].ToString(), out DateTime zDate);
                zList.Add(new Over_Time_Detail_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Paramater = r["Paramater"].ToFloat(),
                    Hours = r["Hours"].ToFloat(),
                    OverTimeDate = zDate
                });
            }
            return zList;
        }
    }
}
