﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Over_Time_Info
    {

        public Over_Time_Model Over_Time = new Over_Time_Model();

        #region [ Constructor Get Information ]
        public Over_Time_Info()
        {
            Over_Time.OvertimeKey = Guid.NewGuid().ToString();
        }
        public Over_Time_Info(string OvertimeKey)
        {
            string zSQL = "SELECT * FROM HRM_Over_Time WHERE OvertimeKey = @OvertimeKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OvertimeKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Over_Time.OvertimeKey = zReader["OvertimeKey"].ToString();
                    if (zReader["OverTimeDate"] != DBNull.Value)
                        Over_Time.OverTimeDate = (DateTime)zReader["OverTimeDate"];
                    Over_Time.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Over_Time.EmployeeID = zReader["EmployeeID"].ToString();
                    Over_Time.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                        Over_Time.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    Over_Time.OrganizationID = zReader["OrganizationID"].ToString();
                    Over_Time.OrganizationName = zReader["OrganizationName"].ToString();
                    Over_Time.BranchKey = zReader["BranchKey"].ToString();
                    Over_Time.BranchName = zReader["BranchName"].ToString();
                    Over_Time.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Over_Time.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        Over_Time.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    Over_Time.PositionName = zReader["PositionName"].ToString();
                    Over_Time.FileAttach = zReader["FileAttach"].ToString();
                    Over_Time.Description = zReader["Description"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                        Over_Time.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    Over_Time.ItemID = zReader["ItemID"].ToString();
                    Over_Time.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Paramater"] != DBNull.Value)
                        Over_Time.Paramater = float.Parse(zReader["Paramater"].ToString());
                    Over_Time.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Over_Time.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Over_Time.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Over_Time.CreatedBy = zReader["CreatedBy"].ToString();
                    Over_Time.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Over_Time.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Over_Time.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Over_Time.ModifiedName = zReader["ModifiedName"].ToString();
                    Over_Time.Message = "200 OK";
                }
                else
                {
                    Over_Time.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Over_Time ("
         + "ParentKey, OverTimeDate , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , FileAttach , Description , ItemKey , ItemID , ItemName , Paramater , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + "@ParentKey, @OverTimeDate , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @FileAttach , @Description , @ItemKey , @ItemID , @ItemName , @Paramater , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time.ParentKey;
                if (Over_Time.OverTimeDate == null)
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time.OverTimeDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time.PositionName;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Over_Time.FileAttach;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time.Description;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time.Paramater;
                if (Over_Time.PartnerNumber != "" && Over_Time.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Over_Time.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Over_Time.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Over_Time("
         + "ParentKey ,OvertimeKey , OverTimeDate , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , FileAttach , Description , ItemKey , ItemID , ItemName , Paramater , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + "@ParentKey, @OvertimeKey , @OverTimeDate , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @FileAttach , @Description , @ItemKey , @ItemID , @ItemName , @Paramater , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time.ParentKey;

                if (Over_Time.OvertimeKey != "" && Over_Time.OvertimeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.OvertimeKey);
                }
                else
                    zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (Over_Time.OverTimeDate == null)
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time.OverTimeDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time.PositionName;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Over_Time.FileAttach;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time.Description;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time.Paramater;
                if (Over_Time.PartnerNumber != "" && Over_Time.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Over_Time.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Over_Time.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Over_Time SET "
                        + " OverTimeDate = @OverTimeDate, ParentKey = @ParentKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " OrganizationKey = @OrganizationKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " OrganizationName = @OrganizationName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " FileAttach = @FileAttach,"
                        + " Description = @Description,"
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " Paramater = @Paramater,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE OvertimeKey = @OvertimeKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Over_Time.ParentKey;
                if (Over_Time.OvertimeKey != "" && Over_Time.OvertimeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.OvertimeKey);
                }
                else
                    zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (Over_Time.OverTimeDate == null)
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeDate", SqlDbType.DateTime).Value = Over_Time.OverTimeDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Over_Time.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Over_Time.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Over_Time.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Over_Time.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Over_Time.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Over_Time.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Over_Time.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Over_Time.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Over_Time.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Over_Time.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Over_Time.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Over_Time.PositionName;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.NVarChar).Value = Over_Time.FileAttach;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Over_Time.Description;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Over_Time.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Over_Time.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Over_Time.ItemName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Over_Time.Paramater;
                if (Over_Time.PartnerNumber != "" && Over_Time.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Over_Time.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Over_Time.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Over_Time.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE HRM_Over_Time SET RecordStatus = 99 WHERE CAST(OvertimeKey AS NVARCHAR(50))= @OvertimeKey
                            UPDATE HRM_Over_Time_Detail SET RecordStatus = 99 WHERE ParentKey = @OvertimeKey
";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OvertimeKey", SqlDbType.NVarChar).Value = Over_Time.OvertimeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Over_Time WHERE OvertimeKey = @OvertimeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.OvertimeKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Over_Time_Detail SET RecordStatus = 99 WHERE ParentKey = @OvertimeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OvertimeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Over_Time.OvertimeKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Over_Time.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Over_Time.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
