﻿using System;
using System.Collections.Generic;

namespace ABG
{
    public class Over_Time_Model
    {
        #region [ Field Name ]
        private string _OvertimeKey = "";
        private DateTime _OverTimeDate = DateTime.MinValue;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _OrganizationKey = 0;
        private string _OrganizationID = "";
        private string _OrganizationName = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private string _FileAttach = "";
        private string _Description = "";
        private int _ItemKey = 0;
        private string _ItemID = "";
        private string _ItemName = "";
        private float _Paramater = 0;
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string OvertimeKey
        {
            get { return _OvertimeKey; }
            set { _OvertimeKey = value; }
        }
        public DateTime OverTimeDate
        {
            get { return _OverTimeDate; }
            set { _OverTimeDate = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int OrganizationKey
        {
            get { return _OrganizationKey; }
            set { _OrganizationKey = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public string OrganizationName
        {
            get { return _OrganizationName; }
            set { _OrganizationName = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public string FileAttach
        {
            get { return _FileAttach; }
            set { _FileAttach = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public float Paramater
        {
            get { return _Paramater; }
            set { _Paramater = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        public string ParentKey { get; set; } = "";
        public List<Over_Time_Detail_Model> ListItem = new List<Over_Time_Detail_Model>();
    }
}
