﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Person_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Person WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Payroll_Person_Model> List(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Person WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Person_Model> zList = new List<Payroll_Person_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime FromDate = DateTime.MinValue;
                DateTime.TryParse(r["FromDate"].ToString(), out FromDate);
                DateTime ToDate = DateTime.MinValue;
                DateTime.TryParse(r["ToDate"].ToString(), out ToDate);
                DateTime ActivatedDate = DateTime.MinValue;
                DateTime.TryParse(r["ActivatedDate"].ToString(), out ActivatedDate);

                zList.Add(new Payroll_Person_Model()
                {
                    PayrollKey = r["PayrollKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    Activated = r["Activated"].ToBool(),
                    Description = r["Description"].ToString(),
                    ActivatedDate = ActivatedDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
        public static List<Payroll_Person_Detail_Model> ListDetail(string PayrollKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Person_Detail WHERE RecordStatus != 99 AND PayrollKey = @PayrollKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.NVarChar).Value = PayrollKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Person_Detail_Model> zList = new List<Payroll_Person_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Person_Detail_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    PayrollKey = r["PayrollKey"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemName = r["ItemName"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    Total = r["Total"].ToDouble(),
                    ItemType = r["ItemType"].ToInt(),
                    MoneyPay = r["MoneyPay"].ToDouble(),
                    Description = r["Description"].ToString(),
                    Quantity = r["Quantity"].ToFloat(),
                    UnitName = r["UnitName"].ToString(),
                });
            }

            return zList;
        }
    }
}
