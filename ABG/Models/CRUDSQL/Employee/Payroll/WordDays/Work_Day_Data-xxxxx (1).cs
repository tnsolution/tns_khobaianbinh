﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Work_Day_Data
    {
        public static List<Work_Day_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Work_Day WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Work_Day_Model> zList = new List<Work_Day_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Work_Day_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    ParentKey = (r["ParentKey"] == DBNull.Value ? "" : r["ParentKey"].ToString()),
                    EmployeeKey = (r["EmployeeKey"] == DBNull.Value ? "" : r["EmployeeKey"].ToString()),
                    EmployeeID = (r["EmployeeID"] == DBNull.Value ? "" : r["EmployeeID"].ToString()),
                    EmployeeName = (r["EmployeeName"] == DBNull.Value ? "" : r["EmployeeName"].ToString()),
                    OrganizationKey = (r["OrganizationKey"] == DBNull.Value ? 0 : int.Parse(r["OrganizationKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    OrganizationName = (r["OrganizationName"] == DBNull.Value ? "" : r["OrganizationName"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? "" : r["BranchKey"].ToString()),
                    BranchName = (r["BranchName"] == DBNull.Value ? "" : r["BranchName"].ToString()),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? "" : r["DepartmentKey"].ToString()),
                    DepartmentName = (r["DepartmentName"] == DBNull.Value ? "" : r["DepartmentName"].ToString()),
                    PositionKey = (r["PositionKey"] == DBNull.Value ? 0 : int.Parse(r["PositionKey"].ToString())),
                    PositionName = (r["PositionName"] == DBNull.Value ? "" : r["PositionName"].ToString()),
                    DateWrite = (r["DateWrite"] == DBNull.Value ? "" : r["DateWrite"].ToString()),
                    Days = (r["Days"] == DBNull.Value ? 0 : float.Parse(r["Days"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }

        public static List<Work_Day_Model> List(string PartnerNumber, string Id)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Work_Day WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ParentKey = @ParentKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Id;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); 
            }
            catch (Exception ex)
            {
                
            }
            List<Work_Day_Model> zList = new List<Work_Day_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Work_Day_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    ParentKey = (r["ParentKey"] == DBNull.Value ? "" : r["ParentKey"].ToString()),
                    EmployeeKey = (r["EmployeeKey"] == DBNull.Value ? "" : r["EmployeeKey"].ToString()),
                    EmployeeID = (r["EmployeeID"] == DBNull.Value ? "" : r["EmployeeID"].ToString()),
                    EmployeeName = (r["EmployeeName"] == DBNull.Value ? "" : r["EmployeeName"].ToString()),
                    OrganizationKey = (r["OrganizationKey"] == DBNull.Value ? 0 : int.Parse(r["OrganizationKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    OrganizationName = (r["OrganizationName"] == DBNull.Value ? "" : r["OrganizationName"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? "" : r["BranchKey"].ToString()),
                    BranchName = (r["BranchName"] == DBNull.Value ? "" : r["BranchName"].ToString()),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? "" : r["DepartmentKey"].ToString()),
                    DepartmentName = (r["DepartmentName"] == DBNull.Value ? "" : r["DepartmentName"].ToString()),
                    PositionKey = (r["PositionKey"] == DBNull.Value ? 0 : int.Parse(r["PositionKey"].ToString())),
                    PositionName = (r["PositionName"] == DBNull.Value ? "" : r["PositionName"].ToString()),
                    DateWrite = (r["DateWrite"] == DBNull.Value ? "" : r["DateWrite"].ToString()),
                    Days = (r["Days"] == DBNull.Value ? 0 : float.Parse(r["Days"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
