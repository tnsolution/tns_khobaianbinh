﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public partial class SalaryTemp_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private DateTime _DateStart = DateTime.MinValue;
private string _Parent = "";
private string _BranchKey = "";
private string _BranchName = "";
private string _DepartmentKey = "";
private string _DepartmentName = "";
private string _EmployeeKey = "";
private string _EmployeeID = "";
private string _EmployeeName = "";
private int _PositionKey = 0;
private string _PositionName = "";
private int _CategoryKey = 0;
private string _CategoryName = "";
private int _ItemKey = 0;
private string _ItemID = "";
private string _ItemName = "";
private float _Amount= 0;
private float _Param= 0;
private string _Formula = "";
private string _Style = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public DateTime DateStart
{
get { return _DateStart; }
set { _DateStart = value; }
}
public string Parent
{
get { return _Parent; }
set { _Parent = value; }
}
public string BranchKey
{
get { return _BranchKey; }
set { _BranchKey = value; }
}
public string BranchName
{
get { return _BranchName; }
set { _BranchName = value; }
}
public string DepartmentKey
{
get { return _DepartmentKey; }
set { _DepartmentKey = value; }
}
public string DepartmentName
{
get { return _DepartmentName; }
set { _DepartmentName = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string EmployeeID
{
get { return _EmployeeID; }
set { _EmployeeID = value; }
}
public string EmployeeName
{
get { return _EmployeeName; }
set { _EmployeeName = value; }
}
public int PositionKey
{
get { return _PositionKey; }
set { _PositionKey = value; }
}
public string PositionName
{
get { return _PositionName; }
set { _PositionName = value; }
}
public int CategoryKey
{
get { return _CategoryKey; }
set { _CategoryKey = value; }
}
public string CategoryName
{
get { return _CategoryName; }
set { _CategoryName = value; }
}
public int ItemKey
{
get { return _ItemKey; }
set { _ItemKey = value; }
}
public string ItemID
{
get { return _ItemID; }
set { _ItemID = value; }
}
public string ItemName
{
get { return _ItemName; }
set { _ItemName = value; }
}
public float Amount
{
get { return _Amount; }
set { _Amount = value; }
}
public float Param
{
get { return _Param; }
set { _Param = value; }
}
public string Formula
{
get { return _Formula; }
set { _Formula = value; }
}
public string Style
{
get { return _Style; }
set { _Style = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
#endregion
}
}
