﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class SalaryTemp_Info
    {

        public SalaryTemp_Model SalaryTemp = new SalaryTemp_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public SalaryTemp_Info()
        {
        }
        public SalaryTemp_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_SalaryTemp WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        SalaryTemp.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["DateStart"] != DBNull.Value)
                        SalaryTemp.DateStart = (DateTime)zReader["DateStart"];
                    SalaryTemp.Parent = zReader["Parent"].ToString();
                    SalaryTemp.BranchKey = zReader["BranchKey"].ToString();
                    SalaryTemp.BranchName = zReader["BranchName"].ToString();
                    SalaryTemp.DepartmentKey = zReader["DepartmentKey"].ToString();
                    SalaryTemp.DepartmentName = zReader["DepartmentName"].ToString();
                    SalaryTemp.EmployeeKey = zReader["EmployeeKey"].ToString();
                    SalaryTemp.EmployeeID = zReader["EmployeeID"].ToString();
                    SalaryTemp.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        SalaryTemp.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    SalaryTemp.PositionName = zReader["PositionName"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        SalaryTemp.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    SalaryTemp.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                        SalaryTemp.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    SalaryTemp.ItemID = zReader["ItemID"].ToString();
                    SalaryTemp.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        SalaryTemp.Amount = float.Parse(zReader["Amount"].ToString());
                    if (zReader["Param"] != DBNull.Value)
                        SalaryTemp.Param = float.Parse(zReader["Param"].ToString());
                    SalaryTemp.Formula = zReader["Formula"].ToString();
                    SalaryTemp.Style = zReader["Style"].ToString();
                    SalaryTemp.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        SalaryTemp.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        SalaryTemp.CreatedOn = (DateTime)zReader["CreatedOn"];
                    SalaryTemp.CreatedBy = zReader["CreatedBy"].ToString();
                    SalaryTemp.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        SalaryTemp.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    SalaryTemp.ModifiedBy = zReader["ModifiedBy"].ToString();
                    SalaryTemp.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_SalaryTemp ("
         + " DateStart , Parent , BranchKey , BranchName , DepartmentKey , DepartmentName , EmployeeKey , EmployeeID , EmployeeName , PositionKey , PositionName , CategoryKey , CategoryName , ItemKey , ItemID , ItemName , Amount , Param , Formula , Style , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @DateStart , @Parent , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @EmployeeKey , @EmployeeID , @EmployeeName , @PositionKey , @PositionName , @CategoryKey , @CategoryName , @ItemKey , @ItemID , @ItemName , @Amount , @Param , @Formula , @Style , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (SalaryTemp.DateStart == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = SalaryTemp.DateStart;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = SalaryTemp.Parent;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = SalaryTemp.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = SalaryTemp.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentName;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = SalaryTemp.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = SalaryTemp.PositionName;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = SalaryTemp.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = SalaryTemp.CategoryName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = SalaryTemp.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = SalaryTemp.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = SalaryTemp.ItemName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = SalaryTemp.Amount;
                zCommand.Parameters.Add("@Param", SqlDbType.Float).Value = SalaryTemp.Param;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = SalaryTemp.Formula;
                zCommand.Parameters.Add("@Style", SqlDbType.NVarChar).Value = SalaryTemp.Style;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = SalaryTemp.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = SalaryTemp.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SalaryTemp.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SalaryTemp.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_SalaryTemp("
         + " AutoKey , DateStart , Parent , BranchKey , BranchName , DepartmentKey , DepartmentName , EmployeeKey , EmployeeID , EmployeeName , PositionKey , PositionName , CategoryKey , CategoryName , ItemKey , ItemID , ItemName , Amount , Param , Formula , Style , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @DateStart , @Parent , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @EmployeeKey , @EmployeeID , @EmployeeName , @PositionKey , @PositionName , @CategoryKey , @CategoryName , @ItemKey , @ItemID , @ItemName , @Amount , @Param , @Formula , @Style , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = SalaryTemp.AutoKey;
                if (SalaryTemp.DateStart == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = SalaryTemp.DateStart;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = SalaryTemp.Parent;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = SalaryTemp.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = SalaryTemp.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentName;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = SalaryTemp.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = SalaryTemp.PositionName;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = SalaryTemp.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = SalaryTemp.CategoryName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = SalaryTemp.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = SalaryTemp.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = SalaryTemp.ItemName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = SalaryTemp.Amount;
                zCommand.Parameters.Add("@Param", SqlDbType.Float).Value = SalaryTemp.Param;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = SalaryTemp.Formula;
                zCommand.Parameters.Add("@Style", SqlDbType.NVarChar).Value = SalaryTemp.Style;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = SalaryTemp.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = SalaryTemp.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SalaryTemp.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SalaryTemp.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_SalaryTemp SET "
                        + " DateStart = @DateStart,"
                        + " Parent = @Parent,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " Amount = @Amount,"
                        + " Param = @Param,"
                        + " Formula = @Formula,"
                        + " Style = @Style,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = SalaryTemp.AutoKey;
                if (SalaryTemp.DateStart == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = SalaryTemp.DateStart;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = SalaryTemp.Parent;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = SalaryTemp.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = SalaryTemp.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = SalaryTemp.DepartmentName;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = SalaryTemp.EmployeeName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = SalaryTemp.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = SalaryTemp.PositionName;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = SalaryTemp.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = SalaryTemp.CategoryName;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = SalaryTemp.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = SalaryTemp.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = SalaryTemp.ItemName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = SalaryTemp.Amount;
                zCommand.Parameters.Add("@Param", SqlDbType.Float).Value = SalaryTemp.Param;
                zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = SalaryTemp.Formula;
                zCommand.Parameters.Add("@Style", SqlDbType.NVarChar).Value = SalaryTemp.Style;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = SalaryTemp.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = SalaryTemp.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SalaryTemp.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_SalaryTemp SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = SalaryTemp.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_SalaryTemp WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = SalaryTemp.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty(string Parent)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_SalaryTemp WHERE Parent = @Parent";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        public string InsertBulk(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_SalaryTemp WHERE Parent = @Parent";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
