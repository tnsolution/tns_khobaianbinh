﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Payroll_Sample_Data
    {
        public static List<Payroll_Sample_Info> List(out string Message)
        {
            var zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Sample WHERE RecordStatus != 99";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            var zList = new List<Payroll_Sample_Info>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Sample_Info()
                {
                    ItemKey = (r["ItemKey"] == DBNull.Value ? 0 : int.Parse(r["ItemKey"].ToString())),
                    ItemName = (r["ItemName"] == DBNull.Value ? "" : r["ItemName"].ToString()),
                    ItemType = (r["ItemType"] == DBNull.Value ? 0 : int.Parse(r["ItemType"].ToString())),
                    ItemID = (r["ItemID"] == DBNull.Value ? "" : r["ItemID"].ToString()),
                    Quantity = (r["Quantity"] == DBNull.Value ? 0 : float.Parse(r["Quantity"].ToString())),
                    UnitName = (r["UnitName"] == DBNull.Value ? "" : r["UnitName"].ToString()),
                    Total = (r["Total"] == DBNull.Value ? 0 : double.Parse(r["Total"].ToString())),
                    IsContract = (r["IsContract"] == DBNull.Value ? 0 : int.Parse(r["IsContract"].ToString())),
                    Rank = (r["Rank"] == DBNull.Value ? 0 : int.Parse(r["Rank"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    CategoryKey = (r["CategoryKey"] == DBNull.Value ? 0 : int.Parse(r["CategoryKey"].ToString())),
                    CategoryName = (r["CategoryName"] == DBNull.Value ? "" : r["CategoryName"].ToString()),
                    Parameter = (r["Parameter"] == DBNull.Value ? 0 : float.Parse(r["Parameter"].ToString())),
                    Formula = (r["Formula"] == DBNull.Value ? "" : r["Formula"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
