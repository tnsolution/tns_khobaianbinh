﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Close_Data
    {
        public static List<Payroll_Close_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Close WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY CloseDate DESC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                
            }
            List<Payroll_Close_Model> zList = new List<Payroll_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Close_Model() {
                    CloseKey = r["CloseKey"].ToString(),
                    CloseDate = r["CloseDate"].ToString(),
                    Title = r["Title"].ToString(),
                    Description = r["Description"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                });
            }
            return zList;
        }
    }
}
