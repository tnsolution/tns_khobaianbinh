﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Close_Detail_Info
    {
        public string Message { get; set; } = "";
        public string Code
        {
            get
            {
                if (Message.Length >= 3)
                    return Message.Substring(0, 3);
                else return "";
            }
        }

        public Payroll_Close_Detail_Model Payroll_Close_Detail = new Payroll_Close_Detail_Model();

        #region [ Constructor Get Information ]
        public Payroll_Close_Detail_Info()
        {
        }
        public Payroll_Close_Detail_Info(string ParentKey, string EmployeeKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Close_Detail WHERE ParentKey = @ParentKey AND EmployeeKey = @EmployeeKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Payroll_Close_Detail.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Payroll_Close_Detail.ParentKey = zReader["ParentKey"].ToString();
                    Payroll_Close_Detail.CloseDate = zReader["CloseDate"].ToString();
                    Payroll_Close_Detail.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Close_Detail.JsonData = zReader["JsonData"].ToString();
                    Payroll_Close_Detail.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Close_Detail.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Close_Detail.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Close_Detail.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Close_Detail.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Close_Detail.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Close_Detail.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Close_Detail.ModifiedName = zReader["ModifiedName"].ToString();
                    Payroll_Close_Detail.Message = "200 OK";
                }
                else
                {
                    Payroll_Close_Detail.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Payroll_Close_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Close_Detail SET RecordStatus = 99 WHERE ParentKey = @ParentKey AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Payroll_Close_Detail.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Close_Detail.EmployeeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = "200 OK";
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion


    }
}