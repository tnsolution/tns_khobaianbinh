﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public partial class Payroll_Support_Model
{
#region [ Field Name ]
private int _SupportKey = 0;
private string _ParentKey = "";
private int _ItemKey = 0;
private string _ItemID = "";
private DateTime _SupportDate = DateTime.MinValue;
private string _EmployeeKey = "";
private string _EmployeeID = "";
private string _EmployeeName = "";
private string _Description = "";
private int _OrganizationKey = 0;
private string _OrganizationPath = "";
private string _OrganizationName = "";
private string _DepartmentKey = "";
private string _DepartmentName = "";
private string _BranchKey = "";
private string _BranchName = "";
private int _PositionKey = 0;
private string _PositionName = "";
private float _Paramater= 0;
private float _Value= 0;
private double _Money = 0;
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _CreatedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
#endregion
 
#region [ Properties ]
public int SupportKey
{
get { return _SupportKey; }
set { _SupportKey = value; }
}
public string ParentKey
{
get { return _ParentKey; }
set { _ParentKey = value; }
}
public int ItemKey
{
get { return _ItemKey; }
set { _ItemKey = value; }
}
public string ItemID
{
get { return _ItemID; }
set { _ItemID = value; }
}
public DateTime SupportDate
{
get { return _SupportDate; }
set { _SupportDate = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string EmployeeID
{
get { return _EmployeeID; }
set { _EmployeeID = value; }
}
public string EmployeeName
{
get { return _EmployeeName; }
set { _EmployeeName = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public int OrganizationKey
{
get { return _OrganizationKey; }
set { _OrganizationKey = value; }
}
public string OrganizationPath
{
get { return _OrganizationPath; }
set { _OrganizationPath = value; }
}
public string OrganizationName
{
get { return _OrganizationName; }
set { _OrganizationName = value; }
}
public string DepartmentKey
{
get { return _DepartmentKey; }
set { _DepartmentKey = value; }
}
public string DepartmentName
{
get { return _DepartmentName; }
set { _DepartmentName = value; }
}
public string BranchKey
{
get { return _BranchKey; }
set { _BranchKey = value; }
}
public string BranchName
{
get { return _BranchName; }
set { _BranchName = value; }
}
public int PositionKey
{
get { return _PositionKey; }
set { _PositionKey = value; }
}
public string PositionName
{
get { return _PositionName; }
set { _PositionName = value; }
}
public float Paramater
{
get { return _Paramater; }
set { _Paramater = value; }
}
public float Value
{
get { return _Value; }
set { _Value = value; }
}
public double Money
{
get { return _Money; }
set { _Money = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
#endregion
}
}
