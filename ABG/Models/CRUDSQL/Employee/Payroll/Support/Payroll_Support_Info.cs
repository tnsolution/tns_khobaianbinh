﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Payroll_Support_Info
    {

        public Payroll_Support_Model Payroll_Support = new Payroll_Support_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Payroll_Support_Info()
        {
        }
        public Payroll_Support_Info(int SupportKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Support WHERE SupportKey = @SupportKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SupportKey", SqlDbType.Int).Value = SupportKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["SupportKey"] != DBNull.Value)
                        Payroll_Support.SupportKey = int.Parse(zReader["SupportKey"].ToString());
                    Payroll_Support.ParentKey = zReader["ParentKey"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                        Payroll_Support.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    Payroll_Support.ItemID = zReader["ItemID"].ToString();
                    if (zReader["SupportDate"] != DBNull.Value)
                        Payroll_Support.SupportDate = (DateTime)zReader["SupportDate"];
                    Payroll_Support.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Support.EmployeeID = zReader["EmployeeID"].ToString();
                    Payroll_Support.EmployeeName = zReader["EmployeeName"].ToString();
                    Payroll_Support.Description = zReader["Description"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                        Payroll_Support.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    Payroll_Support.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Payroll_Support.OrganizationName = zReader["OrganizationName"].ToString();
                    Payroll_Support.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Payroll_Support.DepartmentName = zReader["DepartmentName"].ToString();
                    Payroll_Support.BranchKey = zReader["BranchKey"].ToString();
                    Payroll_Support.BranchName = zReader["BranchName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        Payroll_Support.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    Payroll_Support.PositionName = zReader["PositionName"].ToString();
                    if (zReader["Paramater"] != DBNull.Value)
                        Payroll_Support.Paramater = float.Parse(zReader["Paramater"].ToString());
                    if (zReader["Value"] != DBNull.Value)
                        Payroll_Support.Value = float.Parse(zReader["Value"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        Payroll_Support.Money = double.Parse(zReader["Money"].ToString());
                    Payroll_Support.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Payroll_Support.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Payroll_Support.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Support.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Payroll_Support.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Payroll_Support.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Support.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Payroll_Support.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _Message = "200";
                }
                else
                {
                    _Message = "404";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Support ("
            + " ParentKey , ItemKey , ItemID , SupportDate , EmployeeKey , EmployeeID , EmployeeName , Description , OrganizationKey , OrganizationPath , OrganizationName , DepartmentKey , DepartmentName , BranchKey , BranchName , PositionKey , PositionName , Paramater , Value , Money , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )"
            + " VALUES ( "
            + " @ParentKey , @ItemKey , @ItemID , @SupportDate , @EmployeeKey , @EmployeeID , @EmployeeName , @Description , @OrganizationKey , @OrganizationPath , @OrganizationName , @DepartmentKey , @DepartmentName , @BranchKey , @BranchName , @PositionKey , @PositionName , @Paramater , @Value , @Money , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Payroll_Support.ParentKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Support.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Support.ItemID;
                if (Payroll_Support.SupportDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = Payroll_Support.SupportDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Support.Description;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Payroll_Support.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Support.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Support.BranchName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Support.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Support.PositionName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Payroll_Support.Paramater;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Payroll_Support.Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Payroll_Support.Money;
                if (Payroll_Support.PartnerNumber != "" && Payroll_Support.PartnerNumber.Length == 36)
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Support.PartnerNumber);
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Support.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Support.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Support.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Support("
         + " SupportKey , ParentKey , ItemKey , ItemID , SupportDate , EmployeeKey , EmployeeID , EmployeeName , Description , OrganizationKey , OrganizationPath , OrganizationName , DepartmentKey , DepartmentName , BranchKey , BranchName , PositionKey , PositionName , Paramater , Value , Money , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )"
         + " VALUES ( "
         + " @SupportKey , @ParentKey , @ItemKey , @ItemID , @SupportDate , @EmployeeKey , @EmployeeID , @EmployeeName , @Description , @OrganizationKey , @OrganizationPath , @OrganizationName , @DepartmentKey , @DepartmentName , @BranchKey , @BranchName , @PositionKey , @PositionName , @Paramater , @Value , @Money , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SupportKey", SqlDbType.Int).Value = Payroll_Support.SupportKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Payroll_Support.ParentKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Support.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Support.ItemID;
                if (Payroll_Support.SupportDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = Payroll_Support.SupportDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Support.Description;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Payroll_Support.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Support.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Support.BranchName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Support.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Support.PositionName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Payroll_Support.Paramater;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Payroll_Support.Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Payroll_Support.Money;
                if (Payroll_Support.PartnerNumber != "" && Payroll_Support.PartnerNumber.Length == 36)
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Support.PartnerNumber);
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Support.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Support.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Support.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Support SET "
                        + " ParentKey = @ParentKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " SupportDate = @SupportDate,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " Description = @Description,"
                        + " OrganizationKey = @OrganizationKey,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " OrganizationName = @OrganizationName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " Paramater = @Paramater,"
                        + " Value = @Value,"
                        + " Money = @Money,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE SupportKey = @SupportKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SupportKey", SqlDbType.Int).Value = Payroll_Support.SupportKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Payroll_Support.ParentKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Support.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Support.ItemID;
                if (Payroll_Support.SupportDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SupportDate", SqlDbType.DateTime).Value = Payroll_Support.SupportDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Support.EmployeeName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Support.Description;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Payroll_Support.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationPath;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Payroll_Support.OrganizationName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Support.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Support.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Support.BranchName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Support.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Support.PositionName;
                zCommand.Parameters.Add("@Paramater", SqlDbType.Float).Value = Payroll_Support.Paramater;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Payroll_Support.Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Payroll_Support.Money;
                if (Payroll_Support.PartnerNumber != "" && Payroll_Support.PartnerNumber.Length == 36)
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Support.PartnerNumber);
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Support.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Support.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Support SET RecordStatus = 99 WHERE SupportKey = @SupportKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SupportKey", SqlDbType.Int).Value = Payroll_Support.SupportKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Support WHERE SupportKey = @SupportKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SupportKey", SqlDbType.Int).Value = Payroll_Support.SupportKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
