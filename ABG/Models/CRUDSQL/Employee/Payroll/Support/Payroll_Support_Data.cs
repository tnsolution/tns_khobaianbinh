﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Payroll_Support_Data
    {
        public static List<Payroll_Support_Model> List(string part, string id)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Payroll_Support WHERE RecordStatus != 99 AND ParentKey = '" + id + "'";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {

            }
            var zlist = new List<Payroll_Support_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zlist.Add(new Payroll_Support_Model()
                {
                    ParentKey = r["ParentKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    SupportKey = r["SupportKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    Money = r["Money"].ToDouble(),
                    Description = r["Description"].ToString(),
                });
            }
            return zlist;
        }
    }
}
