﻿using System;
using System.Drawing;

namespace ABG
{
    public class Employee_Model
    {
        #region [ Field Name ]
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _LastName = "";
        private string _FirstName = "";
        private string _ReportToKey = "";
        private string _ReportToName = "";
        private int _OrganizationKey = 0;
        private string _OrganizationPath = "";
        private string _OrganizationName = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private int _JobKey = 0;
        private string _JobName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private int _WorkingStatusKey = 0;
        private string _WorkingStatusName = "";
        private DateTime _StartingDate;
        private DateTime _LeavingDate;
        private int _ContractTypeKey = 0;
        private string _ContractTypeName = "";
        private string _WorkingAddress = "";
        private string _WorkingLocation = "";
        private string _CompanyPhone = "";
        private string _CompanyEmail = "";
        private int _Gender = 0;
        private DateTime _Birthday;
        private string _BirthPlace = "";
        private int _MaritalStatusKey = 0;
        private string _Nationality = "";
        private string _Ethnicity = "";
        private string _PassportNumber = "";
        private DateTime _IssueDate;
        private DateTime _ExpireDate;
        private string _IssuePlace = "";
        private string _AddressRegister = "";
        private string _CityRegister = "";
        private string _AddressContact = "";
        private string _CityContact = "";
        private string _EmergencyContact = "";
        private string _MobiPhone = "";
        private string _Email = "";
        private Image _Photo;
        private string _PhotoPath = "";
        private string _TaxNumber = "";
        private string _BankAccount = "";
        private string _BankName = "";
        private string _Note = "";
        private int _Slug = 0;
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private string _NickName = "";
        private string _Password = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string ReportToKey
        {
            get { return _ReportToKey; }
            set { _ReportToKey = value; }
        }
        public string ReportToName
        {
            get { return _ReportToName; }
            set { _ReportToName = value; }
        }
        public int OrganizationKey
        {
            get { return _OrganizationKey; }
            set { _OrganizationKey = value; }
        }
        public string OrganizationPath
        {
            get { return _OrganizationPath; }
            set { _OrganizationPath = value; }
        }
        public string OrganizationName
        {
            get { return _OrganizationName; }
            set { _OrganizationName = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public int JobKey
        {
            get { return _JobKey; }
            set { _JobKey = value; }
        }
        public string JobName
        {
            get { return _JobName; }
            set { _JobName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public int WorkingStatusKey
        {
            get { return _WorkingStatusKey; }
            set { _WorkingStatusKey = value; }
        }
        public string WorkingStatusName
        {
            get { return _WorkingStatusName; }
            set { _WorkingStatusName = value; }
        }
        public DateTime StartingDate
        {
            get { return _StartingDate; }
            set { _StartingDate = value; }
        }
        public DateTime LeavingDate
        {
            get { return _LeavingDate; }
            set { _LeavingDate = value; }
        }
        public int ContractTypeKey
        {
            get { return _ContractTypeKey; }
            set { _ContractTypeKey = value; }
        }
        public string ContractTypeName
        {
            get { return _ContractTypeName; }
            set { _ContractTypeName = value; }
        }
        public string WorkingAddress
        {
            get { return _WorkingAddress; }
            set { _WorkingAddress = value; }
        }
        public string WorkingLocation
        {
            get { return _WorkingLocation; }
            set { _WorkingLocation = value; }
        }
        public string CompanyPhone
        {
            get { return _CompanyPhone; }
            set { _CompanyPhone = value; }
        }
        public string CompanyEmail
        {
            get { return _CompanyEmail; }
            set { _CompanyEmail = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string BirthPlace
        {
            get { return _BirthPlace; }
            set { _BirthPlace = value; }
        }
        public int MaritalStatusKey
        {
            get { return _MaritalStatusKey; }
            set { _MaritalStatusKey = value; }
        }
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }
        public string Ethnicity
        {
            get { return _Ethnicity; }
            set { _Ethnicity = value; }
        }
        public string PassportNumber
        {
            get { return _PassportNumber; }
            set { _PassportNumber = value; }
        }
        public DateTime IssueDate
        {
            get { return _IssueDate; }
            set { _IssueDate = value; }
        }
        public DateTime ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public string IssuePlace
        {
            get { return _IssuePlace; }
            set { _IssuePlace = value; }
        }
        public string AddressRegister
        {
            get { return _AddressRegister; }
            set { _AddressRegister = value; }
        }
        public string CityRegister
        {
            get { return _CityRegister; }
            set { _CityRegister = value; }
        }
        public string AddressContact
        {
            get { return _AddressContact; }
            set { _AddressContact = value; }
        }
        public string CityContact
        {
            get { return _CityContact; }
            set { _CityContact = value; }
        }
        public string EmergencyContact
        {
            get { return _EmergencyContact; }
            set { _EmergencyContact = value; }
        }
        public string MobiPhone
        {
            get { return _MobiPhone; }
            set { _MobiPhone = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public Image Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }
        public string PhotoPath
        {
            get { return _PhotoPath; }
            set { _PhotoPath = value; }
        }
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }
        public string BankAccount
        {
            get { return _BankAccount; }
            set { _BankAccount = value; }
        }
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public string NickName
        {
            get { return _NickName; }
            set { _NickName = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        public string HDLD { get; set; } = "";
        public string LuongTT { get; set; } = "";
        public string TN { get; set; } = "";
    }
}
