﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Company_Info
    {

        public Company_Model Company = new Company_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Company_Info()
        {
        }
        public Company_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM INF_Company WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Company.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Company.Time = zReader["Time"].ToString();
                    Company.Title1 = zReader["Title1"].ToString();
                    Company.Title2 = zReader["Title2"].ToString();
                    Company.Title3 = zReader["Title3"].ToString();
                    Company.Description = zReader["Description"].ToString();
                    Company.FileAttack = zReader["FileAttack"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        Company.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        Company.Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Company.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Company.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Company.CreatedBy = zReader["CreatedBy"].ToString();
                    Company.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Company.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Company.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Company.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200";
                }
                else
                {
                    _Message = "404";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO INF_Company ("
         + " Time , Title1 , Title2 , Title3 , Description , FileAttack , Parent , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @Time , @Title1 , @Title2 , @Title3 , @Description , @FileAttack , @Parent , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            zSQL += "SELECT SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Time", SqlDbType.NVarChar).Value = Company.Time;
                zCommand.Parameters.Add("@Title1", SqlDbType.NVarChar).Value = Company.Title1;
                zCommand.Parameters.Add("@Title2", SqlDbType.NVarChar).Value = Company.Title2;
                zCommand.Parameters.Add("@Title3", SqlDbType.NVarChar).Value = Company.Title3;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Company.FileAttack;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Company.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Company.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Company.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Company.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                Company.AutoKey = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                _Message = "201";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO INF_Company("
         + " AutoKey , Time , Title1 , Title2 , Title3 , Description , FileAttack , Parent , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @Time , @Title1 , @Title2 , @Title3 , @Description , @FileAttack , @Parent , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Company.AutoKey;

                zCommand.Parameters.Add("@Time", SqlDbType.NVarChar).Value = Company.Time;
                zCommand.Parameters.Add("@Title1", SqlDbType.NVarChar).Value = Company.Title1;
                zCommand.Parameters.Add("@Title2", SqlDbType.NVarChar).Value = Company.Title2;
                zCommand.Parameters.Add("@Title3", SqlDbType.NVarChar).Value = Company.Title3;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Company.FileAttack;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Company.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Company.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Company.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Company.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE INF_Company SET "
                        + " Time = @Time,"
                        + " Title1 = @Title1,"
                        + " Title2 = @Title2,"
                        + " Title3 = @Title3,"
                        + " Description = @Description,"
                        + " FileAttack = @FileAttack,"
                        + " Parent = @Parent,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Company.AutoKey;

                zCommand.Parameters.Add("@Time", SqlDbType.NVarChar).Value = Company.Time;
                zCommand.Parameters.Add("@Title1", SqlDbType.NVarChar).Value = Company.Title1;
                zCommand.Parameters.Add("@Title2", SqlDbType.NVarChar).Value = Company.Title2;
                zCommand.Parameters.Add("@Title3", SqlDbType.NVarChar).Value = Company.Title3;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Company.FileAttack;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Company.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Company.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE INF_Company SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Company.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM INF_Company WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Company.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
