﻿namespace ABG
{
    public class Payroll_Item
    {
        public string PhotoPath { get; set; }
        public string EmployeeKey { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeID { get; set; }
        public string PositionName { get; set; }
        public string DepartmentName { get; set; }
        public string DateStart { get; set; }
        public string ItemKey { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Param { get; set; }
        public string Formula { get; set; }
        public string CategoryName { get; set; }
        public string CategoryKey { get; set; }
        public string Amount { get; set; }
        public string Branch { get; set; }
        public string Style { get; set; }
        public string BranchKey { get; set; }
        public string DepartmentKey { get; set; }
    }
}