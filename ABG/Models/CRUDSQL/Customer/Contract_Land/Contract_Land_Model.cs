﻿using System;
namespace ABG
{
    public class Contract_Land_Model
    {
        #region [ Field Name ]
        private string _LandItemKey = "";
        private string _ItemKey = "";
        private string _ItemID = "";
        private string _ItemName = "";
        private int _UnitKey = 0;
        private string _UnitName = "";
        private double _StandardCost = 0;
        private float _Quantity = 0;
        private double _Area = 0;
        private double _Price = 0;
        private double _VAT = 0;
        private float _VAT_Percent = 0;
        private double _SubTotal = 0;
        private string _ContractKey = "";
        private string _CurrencyID = "";
        private double _CurrencyRate = 0;
        private double _TotalCurrencyMain = 0;
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private string _PartnerNumber = "";
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string LandItemKey
        {
            get { return _LandItemKey; }
            set { _LandItemKey = value; }
        }
        public string ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public double StandardCost
        {
            get { return _StandardCost; }
            set { _StandardCost = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public double Area
        {
            get { return _Area; }
            set { _Area = value; }
        }
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public double VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public float VAT_Percent
        {
            get { return _VAT_Percent; }
            set { _VAT_Percent = value; }
        }
        public double SubTotal
        {
            get { return _SubTotal; }
            set { _SubTotal = value; }
        }
        public string ContractKey
        {
            get { return _ContractKey; }
            set { _ContractKey = value; }
        }
        public string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }
        public double CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public double TotalCurrencyMain
        {
            get { return _TotalCurrencyMain; }
            set { _TotalCurrencyMain = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
