﻿using System;
using System.Collections.Generic;

namespace ABG.Customer
{
    public class Contract_Model
    {
        #region [ Field Name ]
        private string _ContractKey = "";
        private string _ContractID = "";
        private string _SubContract = "";
        private string _ContractName = "";
        private DateTime _ContractDate = DateTime.MinValue;
        private string _BuyerKey = "";
        private string _BuyerID = "";
        private string _BuyerName = "";
        private string _BuyerAddress = "";
        private string _BuyerPhone = "";
        private string _BuyerTaxCode = "";
        private string _BuyerBankCode = "";
        private string _BuyerAddressBank = "";
        private string _BuyerRepresent = "";
        private string _BuyerPosition = "";
        private string _SellerKey = "";
        private string _SellerID = "";
        private string _SellerName = "";
        private string _SellerAddress = "";
        private string _SellerPhone = "";
        private string _SellerTaxCode = "";
        private string _SellerBankCode = "";
        private string _SellerAddressBank = "";
        private string _SellerRepresent = "";
        private string _SellerPosition = "";
        private string _Parent = "";
        private string _Purpose = "";
        private DateTime _FromDate = DateTime.MinValue;
        private DateTime _ToDate = DateTime.MinValue;
        private int _PercentIncrease = 0;
        private string _DepositExt = "";
        private double _DepositMoney = 0;
        private string _PayExt = "";
        private string _PayNo = "";
        private int _WithinNo = 0;
        private string _WithinExt = "";
        private string _Currency = "";
        private string _SpecialRequirements = "";
        private double _PowerFee = 0;
        private double _WaterFee = 0;
        private double _WashFee = 0;
        private int _DateRenewal = 0;
        private int _DateExpired = 0;
        private DateTime _DateTransfer = DateTime.MinValue;
        private DateTime _DateSign = DateTime.MinValue;
        private int _IsDraft = 0;
        private int _Status = 0;
        private string _Organization = "";
        private int _Rank = 0;
        private string _RegionKey = "";
        private string _RegionName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _ContractTypeKey = 0;
        private string _ContractTypeName = "";
        private int _PeriodPayKey = 0;
        private string _PeriodPayName = "";
        private int _ContractStatusKey = 0;
        private string _ContractStatusName = "";
        private double _SubTotal = 0;
        private double _VAT = 0;
        private double _Total = 0;
        private string _CurrencyID = "";
        private double _CurrencyRate = 0;
        private double _TotalCurrencyMain = 0;
        private int _StatusContract = 0;
        private string _Description = "";
        private string _FileAttack = "";
        private string _FilePath = "";
        private DateTime _FileUploadDate = DateTime.MinValue;
        private int _Liquidated = 0;
        private DateTime _DateLiquidation = DateTime.MinValue;
        private string _FileLiquidationName = "";
        private string _FileLiquidationPath = "";
        private DateTime _FileLiquidationUploadDate = DateTime.MinValue;
        private string _EmployeeBusinessKey = "";
        private string _EmployeeBusinessName = "";
        private string _Presenter = "";
        private double _RoseMoney = 0;
        private string _ReasonLiquidation = "";
        private string _Drafts = "";
        private string _Note = "";
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string ContractKey
        {
            get { return _ContractKey; }
            set { _ContractKey = value; }
        }
        public string ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }
        public string SubContract
        {
            get { return _SubContract; }
            set { _SubContract = value; }
        }
        public string ContractName
        {
            get { return _ContractName; }
            set { _ContractName = value; }
        }
        public DateTime ContractDate
        {
            get { return _ContractDate; }
            set { _ContractDate = value; }
        }
        public string BuyerKey
        {
            get { return _BuyerKey; }
            set { _BuyerKey = value; }
        }
        public string BuyerID
        {
            get { return _BuyerID; }
            set { _BuyerID = value; }
        }
        public string BuyerName
        {
            get { return _BuyerName; }
            set { _BuyerName = value; }
        }
        public string BuyerAddress
        {
            get { return _BuyerAddress; }
            set { _BuyerAddress = value; }
        }
        public string BuyerPhone
        {
            get { return _BuyerPhone; }
            set { _BuyerPhone = value; }
        }
        public string BuyerTaxCode
        {
            get { return _BuyerTaxCode; }
            set { _BuyerTaxCode = value; }
        }
        public string BuyerBankCode
        {
            get { return _BuyerBankCode; }
            set { _BuyerBankCode = value; }
        }
        public string BuyerAddressBank
        {
            get { return _BuyerAddressBank; }
            set { _BuyerAddressBank = value; }
        }
        public string BuyerRepresent
        {
            get { return _BuyerRepresent; }
            set { _BuyerRepresent = value; }
        }
        public string BuyerPosition
        {
            get { return _BuyerPosition; }
            set { _BuyerPosition = value; }
        }
        public string SellerKey
        {
            get { return _SellerKey; }
            set { _SellerKey = value; }
        }
        public string SellerID
        {
            get { return _SellerID; }
            set { _SellerID = value; }
        }
        public string SellerName
        {
            get { return _SellerName; }
            set { _SellerName = value; }
        }
        public string SellerAddress
        {
            get { return _SellerAddress; }
            set { _SellerAddress = value; }
        }
        public string SellerPhone
        {
            get { return _SellerPhone; }
            set { _SellerPhone = value; }
        }
        public string SellerTaxCode
        {
            get { return _SellerTaxCode; }
            set { _SellerTaxCode = value; }
        }
        public string SellerBankCode
        {
            get { return _SellerBankCode; }
            set { _SellerBankCode = value; }
        }
        public string SellerAddressBank
        {
            get { return _SellerAddressBank; }
            set { _SellerAddressBank = value; }
        }
        public string SellerRepresent
        {
            get { return _SellerRepresent; }
            set { _SellerRepresent = value; }
        }
        public string SellerPosition
        {
            get { return _SellerPosition; }
            set { _SellerPosition = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public string Purpose
        {
            get { return _Purpose; }
            set { _Purpose = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public int PercentIncrease
        {
            get { return _PercentIncrease; }
            set { _PercentIncrease = value; }
        }
        public string DepositExt
        {
            get { return _DepositExt; }
            set { _DepositExt = value; }
        }
        public double DepositMoney
        {
            get { return _DepositMoney; }
            set { _DepositMoney = value; }
        }
        public string PayExt
        {
            get { return _PayExt; }
            set { _PayExt = value; }
        }
        public string PayNo
        {
            get { return _PayNo; }
            set { _PayNo = value; }
        }
        public int WithinNo
        {
            get { return _WithinNo; }
            set { _WithinNo = value; }
        }
        public string WithinExt
        {
            get { return _WithinExt; }
            set { _WithinExt = value; }
        }
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }
        public string SpecialRequirements
        {
            get { return _SpecialRequirements; }
            set { _SpecialRequirements = value; }
        }
        public double PowerFee
        {
            get { return _PowerFee; }
            set { _PowerFee = value; }
        }
        public double WaterFee
        {
            get { return _WaterFee; }
            set { _WaterFee = value; }
        }
        public double WashFee
        {
            get { return _WashFee; }
            set { _WashFee = value; }
        }
        public int DateRenewal
        {
            get { return _DateRenewal; }
            set { _DateRenewal = value; }
        }
        public int DateExpired
        {
            get { return _DateExpired; }
            set { _DateExpired = value; }
        }
        public DateTime DateTransfer
        {
            get { return _DateTransfer; }
            set { _DateTransfer = value; }
        }
        public DateTime DateSign
        {
            get { return _DateSign; }
            set { _DateSign = value; }
        }
        public int IsDraft
        {
            get { return _IsDraft; }
            set { _IsDraft = value; }
        }
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string RegionKey
        {
            get { return _RegionKey; }
            set { _RegionKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int ContractTypeKey
        {
            get { return _ContractTypeKey; }
            set { _ContractTypeKey = value; }
        }
        public string ContractTypeName
        {
            get { return _ContractTypeName; }
            set { _ContractTypeName = value; }
        }
        public int PeriodPayKey
        {
            get { return _PeriodPayKey; }
            set { _PeriodPayKey = value; }
        }
        public string PeriodPayName
        {
            get { return _PeriodPayName; }
            set { _PeriodPayName = value; }
        }
        public int ContractStatusKey
        {
            get { return _ContractStatusKey; }
            set { _ContractStatusKey = value; }
        }
        public string ContractStatusName
        {
            get { return _ContractStatusName; }
            set { _ContractStatusName = value; }
        }
        public double SubTotal
        {
            get { return _SubTotal; }
            set { _SubTotal = value; }
        }
        public double VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
        public string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }
        public double CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public double TotalCurrencyMain
        {
            get { return _TotalCurrencyMain; }
            set { _TotalCurrencyMain = value; }
        }
        public int StatusContract
        {
            get { return _StatusContract; }
            set { _StatusContract = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string FileAttack
        {
            get { return _FileAttack; }
            set { _FileAttack = value; }
        }
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        public DateTime FileUploadDate
        {
            get { return _FileUploadDate; }
            set { _FileUploadDate = value; }
        }
        public int Liquidated
        {
            get { return _Liquidated; }
            set { _Liquidated = value; }
        }
        public DateTime DateLiquidation
        {
            get { return _DateLiquidation; }
            set { _DateLiquidation = value; }
        }
        public string FileLiquidationName
        {
            get { return _FileLiquidationName; }
            set { _FileLiquidationName = value; }
        }
        public string FileLiquidationPath
        {
            get { return _FileLiquidationPath; }
            set { _FileLiquidationPath = value; }
        }
        public DateTime FileLiquidationUploadDate
        {
            get { return _FileLiquidationUploadDate; }
            set { _FileLiquidationUploadDate = value; }
        }
        public string EmployeeBusinessKey
        {
            get { return _EmployeeBusinessKey; }
            set { _EmployeeBusinessKey = value; }
        }
        public string EmployeeBusinessName
        {
            get { return _EmployeeBusinessName; }
            set { _EmployeeBusinessName = value; }
        }
        public string Presenter
        {
            get { return _Presenter; }
            set { _Presenter = value; }
        }
        public double RoseMoney
        {
            get { return _RoseMoney; }
            set { _RoseMoney = value; }
        }
        public string ReasonLiquidation
        {
            get { return _ReasonLiquidation; }
            set { _ReasonLiquidation = value; }
        }
        public string Drafts
        {
            get { return _Drafts; }
            set { _Drafts = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string RegionName
        {
            get
            {
                return _RegionName;
            }

            set
            {
                _RegionName = value;
            }
        }
        #endregion
    }
}
