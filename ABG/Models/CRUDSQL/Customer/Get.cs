﻿using ABG.Customer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ABG.Models.Customer
{
    public class Get
    {
        //tìm khách hàng trong hợp đồng
        public static List<Contract_Model> Search(string PartnerNumber, string Area, string Buyer, string ContractID, string DataAccess = "")
        {
            string zSQL = @"SELECT A.BuyerKey, A.BuyerID, A.BuyerName, A.ContractID, A.ContractKey FROM CRM_Contract A WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber";
            if (DataAccess != string.Empty)
            {
                zSQL += " AND A.RegionKey IN (" + DataAccess + ")";
            }
            if (Area != string.Empty)
            {
                zSQL += " AND A.RegionKey = @Area";
            }
            if (ContractID != string.Empty)
            {
                zSQL += " AND A.ContractID = @ContractID";
            }
            else
            {
                zSQL += " AND LEN(A.SubContract) = 0";
            }
            if (Buyer != string.Empty)
            {
                zSQL += " AND A.BuyerKey = @Buyer";
            }
            zSQL += " ORDER BY ContractID, SubContract, DateSign";
            DataTable zTable = new DataTable();

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Buyer", SqlDbType.NVarChar).Value = Buyer;
                zCommand.Parameters.Add("@Area", SqlDbType.NVarChar).Value = Area;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = ContractID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Model> zList = new List<Contract_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Contract_Model()
                    {
                        BuyerKey = r["BuyerKey"].ToString(),
                        BuyerID = r["BuyerID"].ToString(),
                        BuyerName = r["BuyerName"].ToString(),
                        ContractID = r["ContractID"].ToString(),
                        ContractKey = r["ContractKey"].ToString(),
                    });
                }
            }
            return zList;
        }

        public static List<Contract_Object> Contract(string PartnerNumber, string ContractID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*
FROM CRM_Contract A
WHERE A.PartnerNumber = @PartnerNumber
AND A.RecordStatus <> 99
AND A.ContractID = @ContractID
ORDER BY A.ContractID, A.SubContract, A.FromDate
";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractID", SqlDbType.NVarChar).Value = ContractID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Object> zList = new List<Contract_Object>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zFromDate = DateTime.MinValue;
                    DateTime.TryParse(r["FromDate"].ToString(), out zFromDate);

                    DateTime zToDate = DateTime.MinValue;
                    DateTime.TryParse(r["ToDate"].ToString(), out zToDate);

                    DateTime zDate = DateTime.MinValue;
                    DateTime.TryParse(r["DateLiquidation"].ToString(), out zDate);

                    DateTime zDateSign = DateTime.MinValue;
                    DateTime.TryParse(r["DateSign"].ToString(), out zDateSign);

                    Contract_Model zModel = new Contract_Model();
                    zModel.ContractKey = r["ContractKey"].ToString();
                    zModel.ContractID = r["ContractID"].ToString();
                    zModel.SubContract = r["SubContract"].ToString();
                    zModel.Purpose = r["Purpose"].ToString();
                    zModel.Description = r["Description"].ToString();
                    zModel.DateSign = zDateSign;
                    zModel.FromDate = zFromDate;
                    zModel.ToDate = zToDate;
                    zModel.DateLiquidation = zDate;
                    zModel.ReasonLiquidation = r["ReasonLiquidation"].ToString();
                    zModel.Liquidated = r["Liquidated"].ToInt();
                    Contract_Object zObj = new Contract_Object();
                    zObj.Contract = zModel;
                    zObj.ListItem = ContractDetail(PartnerNumber, r["ContractKey"].ToString());

                    zList.Add(zObj);
                }
            }
            return zList;
        }
        private static List<Contract_Land_Model> ContractDetail(string PartnerNumber, string ContractKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Contract_Land WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ContractKey = @ContractKey ORDER BY ItemID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ContractKey", SqlDbType.NVarChar).Value = ContractKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Contract_Land_Model> zList = new List<Contract_Land_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Contract_Land_Model()
                    {
                        LandItemKey = r["LandItemKey"].ToString(),
                        ItemKey = r["ItemKey"].ToString(),
                        ItemName = r["ItemName"].ToString(),
                        Area = r["Area"].ToFloat(),
                        Price = r["Price"].ToFloat(),
                        VAT_Percent = r["VAT_Percent"].ToFloat(),
                        TotalCurrencyMain = r["TotalCurrencyMain"].ToFloat(),
                        UnitName = r["UnitName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}