﻿using System;
namespace ABG
{
    public class Comment_Model
    {
        #region [ Field Name ]
        private string _CommentKey = "";
        private string _CommentID = "";
        private string _ProductKey = "";
        private string _ProductName = "";
        private DateTime _CommentDate = DateTime.MinValue;
        private string _Description = "";
        private int _Rate = 0;
        private string _CustomerKey = "";
        private string _CustomerName = "";
        private string _CustomerEmail = "";
        private string _Referrer = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string CommentKey
        {
            get { return _CommentKey; }
            set { _CommentKey = value; }
        }
        public string CommentID
        {
            get { return _CommentID; }
            set { _CommentID = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public DateTime CommentDate
        {
            get { return _CommentDate; }
            set { _CommentDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public string CustomerEmail
        {
            get { return _CustomerEmail; }
            set { _CustomerEmail = value; }
        }
        public string Referrer
        {
            get { return _Referrer; }
            set { _Referrer = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
