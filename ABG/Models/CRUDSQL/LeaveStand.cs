﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABG
{
    public class LeaveStand
    {
        public string CloseYear { get; set; }
        public int STD { get; set; }
        public int TN { get; set; }
        public float END { get; set; }
        public bool isFinish { get; set; }
    }
}