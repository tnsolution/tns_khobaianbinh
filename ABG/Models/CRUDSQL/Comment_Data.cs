﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Comment_Data
{
public static List<Comment_Model> List(string PartnerNumber,  out string Message)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT * FROM PDT_Comment WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
string zConnectionString =ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close(); Message = string.Empty;
}
catch (Exception ex)
{
Message = ex.ToString();
}
List<Comment_Model> zList = new List<Comment_Model>();
foreach (DataRow r in zTable.Rows){zList.Add(new Comment_Model() {CommentKey= r["CommentKey"].ToString(),
CommentID= r["CommentID"].ToString(),
ProductKey= r["ProductKey"].ToString(),
ProductName= r["ProductName"].ToString(),
CommentDate=(r["CommentDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CommentDate"]),
Description= r["Description"].ToString(),
Rate= r["Rate"].ToInt(),
CustomerKey= r["CustomerKey"].ToString(),
CustomerName= r["CustomerName"].ToString(),
CustomerEmail= r["CustomerEmail"].ToString(),
Referrer= r["Referrer"].ToString(),
PartnerNumber= r["PartnerNumber"].ToString(),
RecordStatus= r["RecordStatus"].ToInt(),
CreatedOn=(r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
CreatedBy= r["CreatedBy"].ToString(),
CreatedName= r["CreatedName"].ToString(),
ModifiedOn=(r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
ModifiedBy= r["ModifiedBy"].ToString(),
ModifiedName= r["ModifiedName"].ToString(),
});}
return zList;
}
}
}
