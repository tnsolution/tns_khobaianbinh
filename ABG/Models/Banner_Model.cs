﻿using System;
namespace ABG
{
    public class Banner_Model
    {
        #region [ Field Name ]
        private int _BannerKey = 0;
        private string _Url = "";
        private string _Description = "";
        private string _Title = "";
        private string _Partnernumber = "";
        private int _Rank = 0;
        private bool _Publish;
        private bool _ContentPublish;
        private string _ContentTemplate = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int BannerKey
        {
            get { return _BannerKey; }
            set { _BannerKey = value; }
        }
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public string Partnernumber
        {
            get { return _Partnernumber; }
            set { _Partnernumber = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public bool ContentPublish
        {
            get { return _ContentPublish; }
            set { _ContentPublish = value; }
        }
        public string ContentTemplate
        {
            get { return _ContentTemplate; }
            set { _ContentTemplate = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
