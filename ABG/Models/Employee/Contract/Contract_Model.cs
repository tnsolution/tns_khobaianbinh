﻿using System;
namespace ABG.Employee
{
    public class Contract_Model
    {
        #region [ Field Name ]
        private string _ContractKey = "";
        private string _ContractID = "";
        private string _SubContract = "";
        private string _ContractName = "";
        private int _ContractType = 0;
        private string _ContractTypeName = "";
        private string _Parent = "";
        private DateTime _SignDate = DateTime.MinValue;
        private string _SignBy = "";
        private string _SignName = "";
        private int _SignPositionKey = 0;
        private string _SignPositionName = "";
        private string _SignAddress = "";
        private string _SignCompany = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private DateTime _EmployeeBirthDay = DateTime.MinValue;
        private string _EmployeeBirthPlace = "";
        private string _EmployeeGender = "";
        private string _EmployeePassport = "";
        private DateTime _EmployeeIssueDate = DateTime.MinValue;
        private string _EmployeeIssuePlace = "";
        private string _EmployeeAdress = "";
        private string _EmployeeInsurceNumber = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private string _BranchAdress = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private string _OrganizationPath = "";
        private string _ReportToKey = "";
        private string _ReportToName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private string _JobName = "";
        private DateTime _ProbationFromDate = DateTime.MinValue;
        private DateTime _ProbationToDate = DateTime.MinValue;
        private double _ProbationSalary = 0;
        private DateTime _FromDate = DateTime.MinValue;
        private DateTime _ToDate = DateTime.MinValue;
        private DateTime _DateBeginSalary = DateTime.MinValue;
        private double _SalaryBasic = 0;
        private double _SalaryNet = 0;
        private double _SalaryGross = 0;
        private double _SalaryInsurce = 0;
        private DateTime _DateJoinInsurce = DateTime.MinValue;
        private DateTime _DateSeniority = DateTime.MinValue;
        private DateTime _DateBeginPaidLeave = DateTime.MinValue;
        private bool _Activate = false;
        private string _Note = "";
        private int _Slug = 0;
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string ContractKey
        {
            get { return _ContractKey; }
            set { _ContractKey = value; }
        }
        public string ContractID
        {
            get { return _ContractID; }
            set { _ContractID = value; }
        }
        public string SubContract
        {
            get { return _SubContract; }
            set { _SubContract = value; }
        }
        public string ContractName
        {
            get { return _ContractName; }
            set { _ContractName = value; }
        }
        public int ContractType
        {
            get { return _ContractType; }
            set { _ContractType = value; }
        }
        public string ContractTypeName
        {
            get { return _ContractTypeName; }
            set { _ContractTypeName = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public DateTime SignDate
        {
            get { return _SignDate; }
            set { _SignDate = value; }
        }
        public string SignBy
        {
            get { return _SignBy; }
            set { _SignBy = value; }
        }
        public string SignName
        {
            get { return _SignName; }
            set { _SignName = value; }
        }
        public int SignPositionKey
        {
            get { return _SignPositionKey; }
            set { _SignPositionKey = value; }
        }
        public string SignPositionName
        {
            get { return _SignPositionName; }
            set { _SignPositionName = value; }
        }
        public string SignAddress
        {
            get { return _SignAddress; }
            set { _SignAddress = value; }
        }
        public string SignCompany
        {
            get { return _SignCompany; }
            set { _SignCompany = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public DateTime EmployeeBirthDay
        {
            get { return _EmployeeBirthDay; }
            set { _EmployeeBirthDay = value; }
        }
        public string EmployeeBirthPlace
        {
            get { return _EmployeeBirthPlace; }
            set { _EmployeeBirthPlace = value; }
        }
        public string EmployeeGender
        {
            get { return _EmployeeGender; }
            set { _EmployeeGender = value; }
        }
        public string EmployeePassport
        {
            get { return _EmployeePassport; }
            set { _EmployeePassport = value; }
        }
        public DateTime EmployeeIssueDate
        {
            get { return _EmployeeIssueDate; }
            set { _EmployeeIssueDate = value; }
        }
        public string EmployeeIssuePlace
        {
            get { return _EmployeeIssuePlace; }
            set { _EmployeeIssuePlace = value; }
        }
        public string EmployeeAdress
        {
            get { return _EmployeeAdress; }
            set { _EmployeeAdress = value; }
        }
        public string EmployeeInsurceNumber
        {
            get { return _EmployeeInsurceNumber; }
            set { _EmployeeInsurceNumber = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public string BranchAdress
        {
            get { return _BranchAdress; }
            set { _BranchAdress = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string OrganizationPath
        {
            get { return _OrganizationPath; }
            set { _OrganizationPath = value; }
        }
        public string ReportToKey
        {
            get { return _ReportToKey; }
            set { _ReportToKey = value; }
        }
        public string ReportToName
        {
            get { return _ReportToName; }
            set { _ReportToName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public string JobName
        {
            get { return _JobName; }
            set { _JobName = value; }
        }
        public DateTime ProbationFromDate
        {
            get { return _ProbationFromDate; }
            set { _ProbationFromDate = value; }
        }
        public DateTime ProbationToDate
        {
            get { return _ProbationToDate; }
            set { _ProbationToDate = value; }
        }
        public double ProbationSalary
        {
            get { return _ProbationSalary; }
            set { _ProbationSalary = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public DateTime DateBeginSalary
        {
            get { return _DateBeginSalary; }
            set { _DateBeginSalary = value; }
        }
        public double SalaryBasic
        {
            get { return _SalaryBasic; }
            set { _SalaryBasic = value; }
        }
        public double SalaryNet
        {
            get { return _SalaryNet; }
            set { _SalaryNet = value; }
        }
        public double SalaryGross
        {
            get { return _SalaryGross; }
            set { _SalaryGross = value; }
        }
        public double SalaryInsurce
        {
            get { return _SalaryInsurce; }
            set { _SalaryInsurce = value; }
        }
        public DateTime DateJoinInsurce
        {
            get { return _DateJoinInsurce; }
            set { _DateJoinInsurce = value; }
        }
        public DateTime DateSeniority
        {
            get { return _DateSeniority; }
            set { _DateSeniority = value; }
        }
        public DateTime DateBeginPaidLeave
        {
            get { return _DateBeginPaidLeave; }
            set { _DateBeginPaidLeave = value; }
        }
        public bool Activate
        {
            get { return _Activate; }
            set { _Activate = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string FilePath { get; set; }
        #endregion
    }
}
