﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Contract_Detail_Info
    {

        public Contract_Detail_Model Contract_Detail = new Contract_Detail_Model();

        #region [ Constructor Get Information ]
        public Contract_Detail_Info()
        {
        }
        public Contract_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Contract_Detail WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Contract_Detail.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Contract_Detail.ContractKey = zReader["ContractKey"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                    {
                        Contract_Detail.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    }

                    Contract_Detail.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Quantity"] != DBNull.Value)
                    {
                        Contract_Detail.Quantity = float.Parse(zReader["Quantity"].ToString());
                    }

                    Contract_Detail.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Total"] != DBNull.Value)
                    {
                        Contract_Detail.Total = double.Parse(zReader["Total"].ToString());
                    }

                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Contract_Detail.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Contract_Detail.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["ItemType"] != DBNull.Value)
                    {
                        Contract_Detail.ItemType = int.Parse(zReader["ItemType"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Contract_Detail.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    Contract_Detail.Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Contract_Detail.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Contract_Detail.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Contract_Detail.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Contract_Detail.CreatedBy = zReader["CreatedBy"].ToString();
                    Contract_Detail.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Contract_Detail.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Contract_Detail.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Contract_Detail.ModifiedName = zReader["ModifiedName"].ToString();
                    Contract_Detail.Message = "200 OK";
                }
                else
                {
                    Contract_Detail.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Contract_Detail ("
         + " ContractKey , ItemKey , ItemName , Quantity , UnitName , Total , FromDate , ToDate , ItemType , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractKey , @ItemKey , @ItemName , @Quantity , @UnitName , @Total , @FromDate , @ToDate , @ItemType , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Contract_Detail.ContractKey != "" && Contract_Detail.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Contract_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Contract_Detail.Total;
                if (Contract_Detail.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract_Detail.FromDate;
                }

                if (Contract_Detail.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract_Detail.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Contract_Detail.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Contract_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Detail.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Detail.RecordStatus;
                if (Contract_Detail.PartnerNumber != "" && Contract_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Contract_Detail("
         + " AutoKey , ContractKey , ItemKey , ItemName , Quantity , UnitName , Total , FromDate , ToDate , ItemType , Rank , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ContractKey , @ItemKey , @ItemName , @Quantity , @UnitName , @Total , @FromDate , @ToDate , @ItemType , @Rank , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Contract_Detail.AutoKey;
                if (Contract_Detail.ContractKey != "" && Contract_Detail.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Contract_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Contract_Detail.Total;
                if (Contract_Detail.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract_Detail.FromDate;
                }

                if (Contract_Detail.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract_Detail.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Contract_Detail.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Contract_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Detail.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Detail.RecordStatus;
                if (Contract_Detail.PartnerNumber != "" && Contract_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Contract_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Contract_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Contract_Detail SET "
                        + " ContractKey = @ContractKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemName = @ItemName,"
                        + " Quantity = @Quantity,"
                        + " UnitName = @UnitName,"
                        + " Total = @Total,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " ItemType = @ItemType,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Contract_Detail.AutoKey;
                if (Contract_Detail.ContractKey != "" && Contract_Detail.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Contract_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Contract_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Contract_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Contract_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Contract_Detail.Total;
                if (Contract_Detail.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Contract_Detail.FromDate;
                }

                if (Contract_Detail.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Contract_Detail.ToDate;
                }

                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Contract_Detail.ItemType;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Contract_Detail.Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Contract_Detail.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Contract_Detail.RecordStatus;
                if (Contract_Detail.PartnerNumber != "" && Contract_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Contract_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Contract_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Contract_Detail SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Contract_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Contract_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Contract_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Contract_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Contract_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
