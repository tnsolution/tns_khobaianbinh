﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG.Employee
{
    public class Contract_Data
    {
        public static List<Contract_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Contract WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
        public static List<Contract_Model> List(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ContractKey, A.ContractID, A.SubContract, A.ContractTypeName, A.SignDate, 
A.PositionName, A.EmployeeName, A.BranchAdress, A.DepartmentName, A.ReportToName, 
A.FromDate, A.ToDate, A.Note, X.FilePath, B.Total
FROM HRM_Contract A 
LEFT JOIN HRM_Contract_Detail B ON A.ContractKey = B.ContractKey
CROSS APPLY (SELECT TOP 1 B.FilePath FROM GOB_Document B WHERE B.TableKey = A.ContractKey) 
X
WHERE 
A.RecordStatus != 99 
AND B.ItemKey = 81
AND B.RecordStatus != 99 
AND A.employeekey = @EmployeeKey
ORDER BY A.ContractID, SubContract";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                DateTime FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                DateTime ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                    DepartmentName= r["DepartmentName"].ToString(),
                    Note = r["Note"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    SubContract=r["SubContract"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),                   
                    ReportToName = r["ReportToName"].ToString(),                    
                    BranchAdress = r["BranchAdress"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    SalaryGross = r["Total"].ToDouble(),
                    FilePath = r["FilePath"].ToString(),
                });
            }

            return zList;
        }
        public static List<Contract_Model> Search(string PartnerNumber, string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.* 
FROM HRM_Contract A
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_ListPosition C ON C.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department D ON D.DepartmentKey = B.DepartmentKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND B.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND B.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND B.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND EmployeeName LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, B.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND B.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND B.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND B.PositionKey = @Position";
            }
            zSQL += " ORDER BY C.[Rank] ASC, D.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Contract_Model> zList = new List<Contract_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime SignDate = DateTime.MinValue;
                if (r["SignDate"] != DBNull.Value)
                {
                    SignDate = Convert.ToDateTime(r["SignDate"]);
                }

                FromDate = DateTime.MinValue;
                if (r["FromDate"] != DBNull.Value)
                {
                    FromDate = Convert.ToDateTime(r["FromDate"]);
                }

                ToDate = DateTime.MinValue;
                if (r["ToDate"] != DBNull.Value)
                {
                    ToDate = Convert.ToDateTime(r["ToDate"]);
                }

                zList.Add(new Contract_Model()
                {
                    ContractKey = r["ContractKey"].ToString(),
                    ContractID = r["ContractID"].ToString(),
                    ContractTypeName = r["ContractTypeName"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    SignDate = SignDate,
                    FromDate = FromDate,
                    ToDate = ToDate,
                });
            }

            return zList;
        }
    }
}
