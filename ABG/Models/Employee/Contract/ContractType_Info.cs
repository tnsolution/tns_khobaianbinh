﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class ContractType_Info
    {

        public ContractType_Model ListContractType = new ContractType_Model();

        #region [ Constructor Get Information ]
        public ContractType_Info()
        {
        }
        public ContractType_Info(int ContractTypeKey)
        {
            string zSQL = "SELECT * FROM HRM_ListContractType WHERE ContractTypeKey = @ContractTypeKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = ContractTypeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ContractTypeKey"] != DBNull.Value)
                    {
                        ListContractType.ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                    }

                    ListContractType.ContractTypeNameEN = zReader["ContractTypeNameEN"].ToString();
                    ListContractType.ContractTypeNameVN = zReader["ContractTypeNameVN"].ToString();
                    ListContractType.ContractTypeNameCN = zReader["ContractTypeNameCN"].ToString();
                    ListContractType.ContractTypeID = zReader["ContractTypeID"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                    {
                        ListContractType.Parent = int.Parse(zReader["Parent"].ToString());
                    }

                    ListContractType.Description = zReader["Description"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                    {
                        ListContractType.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    ListContractType.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        ListContractType.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        ListContractType.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    ListContractType.CreatedBy = zReader["CreatedBy"].ToString();
                    ListContractType.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        ListContractType.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    ListContractType.ModifiedBy = zReader["ModifiedBy"].ToString();
                    ListContractType.ModifiedName = zReader["ModifiedName"].ToString();
                    ListContractType.Message = "200 OK";
                }
                else
                {
                    ListContractType.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ListContractType ("
         + " ContractTypeNameEN , ContractTypeNameVN , ContractTypeNameCN , ContractTypeID , Parent , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractTypeNameEN , @ContractTypeNameVN , @ContractTypeNameCN , @ContractTypeID , @Parent , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractTypeNameEN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameEN;
                zCommand.Parameters.Add("@ContractTypeNameVN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameVN;
                zCommand.Parameters.Add("@ContractTypeNameCN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameCN;
                zCommand.Parameters.Add("@ContractTypeID", SqlDbType.NVarChar).Value = ListContractType.ContractTypeID;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = ListContractType.Parent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ListContractType.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = ListContractType.Rank;
                if (ListContractType.PartnerNumber != "" && ListContractType.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ListContractType.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = ListContractType.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = ListContractType.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = ListContractType.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ListContractType.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ListContractType.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListContractType.Message = "201 Created";
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ListContractType("
         + " ContractTypeKey , ContractTypeNameEN , ContractTypeNameVN , ContractTypeNameCN , ContractTypeID , Parent , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ContractTypeKey , @ContractTypeNameEN , @ContractTypeNameVN , @ContractTypeNameCN , @ContractTypeID , @Parent , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = ListContractType.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeNameEN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameEN;
                zCommand.Parameters.Add("@ContractTypeNameVN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameVN;
                zCommand.Parameters.Add("@ContractTypeNameCN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameCN;
                zCommand.Parameters.Add("@ContractTypeID", SqlDbType.NVarChar).Value = ListContractType.ContractTypeID;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = ListContractType.Parent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ListContractType.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = ListContractType.Rank;
                if (ListContractType.PartnerNumber != "" && ListContractType.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ListContractType.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = ListContractType.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = ListContractType.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = ListContractType.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ListContractType.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ListContractType.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListContractType.Message = "201 Created";
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_ListContractType SET "
                        + " ContractTypeNameEN = @ContractTypeNameEN,"
                        + " ContractTypeNameVN = @ContractTypeNameVN,"
                        + " ContractTypeNameCN = @ContractTypeNameCN,"
                        + " ContractTypeID = @ContractTypeID,"
                        + " Parent = @Parent,"
                        + " Description = @Description,"
                        + " Rank = @Rank,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ContractTypeKey = @ContractTypeKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = ListContractType.ContractTypeKey;
                zCommand.Parameters.Add("@ContractTypeNameEN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameEN;
                zCommand.Parameters.Add("@ContractTypeNameVN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameVN;
                zCommand.Parameters.Add("@ContractTypeNameCN", SqlDbType.NVarChar).Value = ListContractType.ContractTypeNameCN;
                zCommand.Parameters.Add("@ContractTypeID", SqlDbType.NVarChar).Value = ListContractType.ContractTypeID;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = ListContractType.Parent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = ListContractType.Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = ListContractType.Rank;
                if (ListContractType.PartnerNumber != "" && ListContractType.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ListContractType.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = ListContractType.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ListContractType.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ListContractType.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListContractType.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_ListContractType SET RecordStatus = 99 WHERE ContractTypeKey = @ContractTypeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = ListContractType.ContractTypeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListContractType.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_ListContractType WHERE ContractTypeKey = @ContractTypeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContractTypeKey", SqlDbType.Int).Value = ListContractType.ContractTypeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                ListContractType.Message = "200 OK";
            }
            catch (Exception Err)
            {
                ListContractType.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
