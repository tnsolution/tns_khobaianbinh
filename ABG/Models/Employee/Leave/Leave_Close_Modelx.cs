﻿using System;
namespace ABG
{
    public class Leave_Close_Model
    {
        #region [ Field Name ]
        private int _CloseKey = 0;
        private string _EmpoyeeKey = "";
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _BranchKey = "";
        private string _BranchName = "";
        private string _DepartmentKey = "";
        private string _DepartmentName = "";
        private int _PositionKey = 0;
        private string _PositionName = "";
        private string _OrganizationPath = "";
        private string _CloseYear = "";
        private float _StandardBegin = 0;
        private float _StandardPlus = 0;
        private float _StandardMiddle = 0;
        private float _StandardEnd = 0;
        private string _Description = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int CloseKey
        {
            get { return _CloseKey; }
            set { _CloseKey = value; }
        }
        public string EmpoyeeKey
        {
            get { return _EmpoyeeKey; }
            set { _EmpoyeeKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }
        public string OrganizationPath
        {
            get { return _OrganizationPath; }
            set { _OrganizationPath = value; }
        }
        public string CloseYear
        {
            get { return _CloseYear; }
            set { _CloseYear = value; }
        }
        public float StandardBegin
        {
            get { return _StandardBegin; }
            set { _StandardBegin = value; }
        }
        public float StandardPlus
        {
            get { return _StandardPlus; }
            set { _StandardPlus = value; }
        }
        public float StandardMiddle
        {
            get { return _StandardMiddle; }
            set { _StandardMiddle = value; }
        }
        public float StandardEnd
        {
            get { return _StandardEnd; }
            set { _StandardEnd = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
