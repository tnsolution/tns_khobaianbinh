﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Close_Data
    {
        public static List<Leave_Close_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Leave_Close WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY EmployeeName, CloseYear";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Leave_Close_Model> zList = new List<Leave_Close_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Leave_Close_Model()
                {
                    CloseKey = r["CloseKey"].ToInt(),
                    CloseYear = r["CloseYear"].ToString(),
                    Description = r["Description"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),                   
                    StandardBegin = r["StandardBegin"].ToFloat(),
                    StandardPlus = r["StandardPlus"].ToFloat(),
                    StandardMiddle = r["StandardMiddle"].ToFloat(),
                    StandardEnd = r["StandardEnd"].ToFloat(),                    
                });
            }
            return zList;
        }
    }
}
