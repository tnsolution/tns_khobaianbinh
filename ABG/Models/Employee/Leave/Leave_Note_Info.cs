﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Leave_Note_Info
    {

        public Leave_Note_Model Leave_Note = new Leave_Note_Model();

        #region [ Constructor Get Information ]
        public Leave_Note_Info()
        {
            Leave_Note.NoteKey = Guid.NewGuid().ToString();
        }
        public Leave_Note_Info(string NoteKey)
        {
            string zSQL = "SELECT * FROM HRM_Leave_Note WHERE NoteKey = @NoteKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(NoteKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Leave_Note.NoteKey = zReader["NoteKey"].ToString();
                    Leave_Note.NoteTitle = zReader["NoteTitle"].ToString();
                    if (zReader["NoteDate"] != DBNull.Value)
                    {
                        Leave_Note.NoteDate = (DateTime)zReader["NoteDate"];
                    }

                    Leave_Note.Description = zReader["Description"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Leave_Note.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Leave_Note.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["TotalDate"] != DBNull.Value)
                    {
                        Leave_Note.TotalDate = float.Parse(zReader["TotalDate"].ToString());
                    }

                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Leave_Note.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Leave_Note.CategoryName = zReader["CategoryName"].ToString();
                    Leave_Note.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Leave_Note.EmployeeID = zReader["EmployeeID"].ToString();
                    Leave_Note.EmployeeName = zReader["EmployeeName"].ToString();
                    Leave_Note.BranchKey = zReader["BranchKey"].ToString();
                    Leave_Note.BranchName = zReader["BranchName"].ToString();
                    Leave_Note.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Leave_Note.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Leave_Note.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Leave_Note.PositionName = zReader["PositionName"].ToString();
                    Leave_Note.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Leave_Note.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Leave_Note.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Leave_Note.Style = zReader["Style"].ToString();
                    Leave_Note.Class = zReader["Class"].ToString();
                    Leave_Note.CodeLine = zReader["CodeLine"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Leave_Note.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Leave_Note.CreatedBy = zReader["CreatedBy"].ToString();
                    Leave_Note.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Leave_Note.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Leave_Note.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Leave_Note.ModifiedName = zReader["ModifiedName"].ToString();
                    Leave_Note.Message = "200 OK";
                }
                else
                {
                    Leave_Note.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Note ("
         + " NoteTitle , NoteDate , Description , FromDate , ToDate , TotalDate , CategoryKey , CategoryName , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , Style , Class , CodeLine , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @NoteTitle , @NoteDate , @Description , @FromDate , @ToDate , @TotalDate , @CategoryKey , @CategoryName , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @Style , @Class , @CodeLine , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@NoteTitle", SqlDbType.NVarChar).Value = Leave_Note.NoteTitle;
                if (Leave_Note.NoteDate == null)
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = Leave_Note.NoteDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Note.Description;
                if (Leave_Note.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Leave_Note.FromDate;
                }

                if (Leave_Note.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Leave_Note.ToDate;
                }

                zCommand.Parameters.Add("@TotalDate", SqlDbType.Float).Value = Leave_Note.TotalDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Leave_Note.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Leave_Note.CategoryName;
                if (Leave_Note.EmployeeKey != "" && Leave_Note.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Note.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Note.EmployeeName;
                if (Leave_Note.BranchKey != "" && Leave_Note.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Note.BranchName;
                if (Leave_Note.DepartmentKey != "" && Leave_Note.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Note.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Note.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Note.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Note.OrganizationPath;
                if (Leave_Note.PartnerNumber != "" && Leave_Note.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Note.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Leave_Note.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Leave_Note.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Leave_Note.CodeLine;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Note.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Note.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Note.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Note.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Note.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Leave_Note("
         + " NoteKey , NoteTitle , NoteDate , Description , FromDate , ToDate , TotalDate , CategoryKey , CategoryName , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , OrganizationPath , PartnerNumber , RecordStatus , Style , Class , CodeLine , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @NoteKey , @NoteTitle , @NoteDate , @Description , @FromDate , @ToDate , @TotalDate , @CategoryKey , @CategoryName , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @OrganizationPath , @PartnerNumber , @RecordStatus , @Style , @Class , @CodeLine , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Leave_Note.NoteKey != "" && Leave_Note.NoteKey.Length == 36)
                {
                    zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.NoteKey);
                }
                else
                {
                    zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@NoteTitle", SqlDbType.NVarChar).Value = Leave_Note.NoteTitle;
                if (Leave_Note.NoteDate == null)
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = Leave_Note.NoteDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Note.Description;
                if (Leave_Note.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Leave_Note.FromDate;
                }

                if (Leave_Note.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Leave_Note.ToDate;
                }

                zCommand.Parameters.Add("@TotalDate", SqlDbType.Float).Value = Leave_Note.TotalDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Leave_Note.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Leave_Note.CategoryName;
                if (Leave_Note.EmployeeKey != "" && Leave_Note.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Note.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Note.EmployeeName;
                if (Leave_Note.BranchKey != "" && Leave_Note.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Note.BranchName;
                if (Leave_Note.DepartmentKey != "" && Leave_Note.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Note.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Note.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Note.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Note.OrganizationPath;
                if (Leave_Note.PartnerNumber != "" && Leave_Note.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Note.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Leave_Note.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Leave_Note.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Leave_Note.CodeLine;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Leave_Note.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Leave_Note.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Note.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Note.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Note.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Leave_Note SET "
                        + " NoteTitle = @NoteTitle,"
                        + " NoteDate = @NoteDate,"
                        + " Description = @Description,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " TotalDate = @TotalDate,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " OrganizationPath = @OrganizationPath,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE NoteKey = @NoteKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Leave_Note.NoteKey != "" && Leave_Note.NoteKey.Length == 36)
                {
                    zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.NoteKey);
                }
                else
                {
                    zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@NoteTitle", SqlDbType.NVarChar).Value = Leave_Note.NoteTitle;
                if (Leave_Note.NoteDate == null)
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = Leave_Note.NoteDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Leave_Note.Description;
                if (Leave_Note.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Leave_Note.FromDate;
                }

                if (Leave_Note.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Leave_Note.ToDate;
                }

                zCommand.Parameters.Add("@TotalDate", SqlDbType.Float).Value = Leave_Note.TotalDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Leave_Note.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Leave_Note.CategoryName;
                if (Leave_Note.EmployeeKey != "" && Leave_Note.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Leave_Note.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Leave_Note.EmployeeName;
                if (Leave_Note.BranchKey != "" && Leave_Note.BranchKey.Length == 36)
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.BranchKey);
                }
                else
                {
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Leave_Note.BranchName;
                if (Leave_Note.DepartmentKey != "" && Leave_Note.DepartmentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.DepartmentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Leave_Note.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Leave_Note.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Leave_Note.PositionName;
                zCommand.Parameters.Add("@OrganizationPath", SqlDbType.NVarChar).Value = Leave_Note.OrganizationPath;
                if (Leave_Note.PartnerNumber != "" && Leave_Note.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Leave_Note.RecordStatus;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Leave_Note.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Leave_Note.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Leave_Note.CodeLine;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Leave_Note.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Leave_Note.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Note.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE HRM_Leave_Note SET RecordStatus = 99 WHERE CONVERT(NVARCHAR(50), NoteKey) = @NoteKey
UPDATE HRM_Leave_Close SET RecordStatus = 99 WHERE Reference = @NoteKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NoteKey", SqlDbType.NVarChar).Value = Leave_Note.NoteKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Note.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Leave_Note WHERE NoteKey = @NoteKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NoteKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Leave_Note.NoteKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Leave_Note.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Leave_Note.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
