﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Payroll_Person_Detail_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _PayrollKey = "";
private int _ItemKey = 0;
private string _ItemID = "";
private string _ItemName = "";
private float _Quantity= 0;
private string _UnitName = "";
private double _Total = 0;
private int _ItemType = 0;
private double _MoneyPay = 0;
private string _Description = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string PayrollKey
{
get { return _PayrollKey; }
set { _PayrollKey = value; }
}
public int ItemKey
{
get { return _ItemKey; }
set { _ItemKey = value; }
}
public string ItemID
{
get { return _ItemID; }
set { _ItemID = value; }
}
public string ItemName
{
get { return _ItemName; }
set { _ItemName = value; }
}
public float Quantity
{
get { return _Quantity; }
set { _Quantity = value; }
}
public string UnitName
{
get { return _UnitName; }
set { _UnitName = value; }
}
public double Total
{
get { return _Total; }
set { _Total = value; }
}
public int ItemType
{
get { return _ItemType; }
set { _ItemType = value; }
}
public double MoneyPay
{
get { return _MoneyPay; }
set { _MoneyPay = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
