﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Person_Info
    {

        public Payroll_Person_Model Payroll_Person = new Payroll_Person_Model();

        #region [ Constructor Get Information ]
        public Payroll_Person_Info()
        {
            Payroll_Person.PayrollKey = Guid.NewGuid().ToString();
        }
        public Payroll_Person_Info(string PayrollKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Person WHERE PayrollKey = @PayrollKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PayrollKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Payroll_Person.PayrollKey = zReader["PayrollKey"].ToString();
                    Payroll_Person.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Person.EmployeeID = zReader["EmployeeID"].ToString();
                    Payroll_Person.EmployeeName = zReader["EmployeeName"].ToString();
                    Payroll_Person.BranchKey = zReader["BranchKey"].ToString();
                    Payroll_Person.BranchName = zReader["BranchName"].ToString();
                    Payroll_Person.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Payroll_Person.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Payroll_Person.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Payroll_Person.PositionName = zReader["PositionName"].ToString();
                    if (zReader["Month"] != DBNull.Value)
                    {
                        Payroll_Person.Month = int.Parse(zReader["Month"].ToString());
                    }

                    if (zReader["Year"] != DBNull.Value)
                    {
                        Payroll_Person.Year = int.Parse(zReader["Year"].ToString());
                    }

                    if (zReader["TotalMoney"] != DBNull.Value)
                    {
                        Payroll_Person.TotalMoney = double.Parse(zReader["TotalMoney"].ToString());
                    }

                    if (zReader["TotalWorkTime"] != DBNull.Value)
                    {
                        Payroll_Person.TotalWorkTime = int.Parse(zReader["TotalWorkTime"].ToString());
                    }

                    if (zReader["TotalDateWorking"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDateWorking = float.Parse(zReader["TotalDateWorking"].ToString());
                    }

                    if (zReader["TotalDateLeaving"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDateLeaving = float.Parse(zReader["TotalDateLeaving"].ToString());
                    }

                    if (zReader["TotalDatePaidLeave"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDatePaidLeave = float.Parse(zReader["TotalDatePaidLeave"].ToString());
                    }

                    if (zReader["TotalDateUnpaidLeave"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDateUnpaidLeave = float.Parse(zReader["TotalDateUnpaidLeave"].ToString());
                    }

                    if (zReader["TotalDateAnnualLeave"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDateAnnualLeave = float.Parse(zReader["TotalDateAnnualLeave"].ToString());
                    }

                    if (zReader["TotalDateDayOff"] != DBNull.Value)
                    {
                        Payroll_Person.TotalDateDayOff = float.Parse(zReader["TotalDateDayOff"].ToString());
                    }

                    Payroll_Person.Style = zReader["Style"].ToString();
                    Payroll_Person.Class = zReader["Class"].ToString();
                    Payroll_Person.CodeLine = zReader["CodeLine"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                    {
                        Payroll_Person.FromDate = (DateTime)zReader["FromDate"];
                    }

                    if (zReader["ToDate"] != DBNull.Value)
                    {
                        Payroll_Person.ToDate = (DateTime)zReader["ToDate"];
                    }

                    if (zReader["Activated"] != DBNull.Value)
                    {
                        Payroll_Person.Activated = (bool)zReader["Activated"];
                    }

                    if (zReader["ActivatedDate"] != DBNull.Value)
                    {
                        Payroll_Person.ActivatedDate = (DateTime)zReader["ActivatedDate"];
                    }

                    Payroll_Person.Reference = zReader["Reference"].ToString();
                    Payroll_Person.Description = zReader["Description"].ToString();
                    Payroll_Person.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Person.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Person.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Person.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Person.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Person.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Person.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Person.ModifiedName = zReader["ModifiedName"].ToString();
                    Payroll_Person.Message = "200 OK";
                }
                else
                {
                    Payroll_Person.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Person ("
         + " EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , Month , Year , TotalMoney , TotalWorkTime , TotalDateWorking , TotalDateLeaving , TotalDatePaidLeave , TotalDateUnpaidLeave , TotalDateAnnualLeave , TotalDateDayOff , Style , Class , CodeLine , FromDate , ToDate , Activated , ActivatedDate , Reference , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @Month , @Year , @TotalMoney , @TotalWorkTime , @TotalDateWorking , @TotalDateLeaving , @TotalDatePaidLeave , @TotalDateUnpaidLeave , @TotalDateAnnualLeave , @TotalDateDayOff , @Style , @Class , @CodeLine , @FromDate , @ToDate , @Activated , @ActivatedDate , @Reference , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Person.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Person.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Person.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Person.PositionName;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Payroll_Person.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Payroll_Person.Year;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Payroll_Person.TotalMoney;
                zCommand.Parameters.Add("@TotalWorkTime", SqlDbType.Int).Value = Payroll_Person.TotalWorkTime;
                zCommand.Parameters.Add("@TotalDateWorking", SqlDbType.Float).Value = Payroll_Person.TotalDateWorking;
                zCommand.Parameters.Add("@TotalDateLeaving", SqlDbType.Float).Value = Payroll_Person.TotalDateLeaving;
                zCommand.Parameters.Add("@TotalDatePaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDatePaidLeave;
                zCommand.Parameters.Add("@TotalDateUnpaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateUnpaidLeave;
                zCommand.Parameters.Add("@TotalDateAnnualLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateAnnualLeave;
                zCommand.Parameters.Add("@TotalDateDayOff", SqlDbType.Float).Value = Payroll_Person.TotalDateDayOff;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Payroll_Person.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Payroll_Person.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Payroll_Person.CodeLine;
                if (Payroll_Person.FromDate == null)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Payroll_Person.FromDate;
                }

                if (Payroll_Person.ToDate == null)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Payroll_Person.ToDate;
                }

                if (Payroll_Person.Activated == null)
                {
                    zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Payroll_Person.Activated;
                }

                if (Payroll_Person.ActivatedDate == null)
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = Payroll_Person.ActivatedDate;
                }

                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Payroll_Person.Reference;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person.Description;
                if (Payroll_Person.PartnerNumber != "" && Payroll_Person.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Person.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Person.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Person("
         + " PayrollKey , EmployeeKey , EmployeeID , EmployeeName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , Month , Year , TotalMoney , TotalWorkTime , TotalDateWorking , TotalDateLeaving , TotalDatePaidLeave , TotalDateUnpaidLeave , TotalDateAnnualLeave , TotalDateDayOff , Style , Class , CodeLine , FromDate , ToDate , Activated , ActivatedDate , Reference , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PayrollKey , @EmployeeKey , @EmployeeID , @EmployeeName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @Month , @Year , @TotalMoney , @TotalWorkTime , @TotalDateWorking , @TotalDateLeaving , @TotalDatePaidLeave , @TotalDateUnpaidLeave , @TotalDateAnnualLeave , @TotalDateDayOff , @Style , @Class , @CodeLine , @FromDate , @ToDate , @Activated , @ActivatedDate , @Reference , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Payroll_Person.PayrollKey != "" && Payroll_Person.PayrollKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PayrollKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Person.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Person.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Person.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Person.PositionName;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Payroll_Person.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Payroll_Person.Year;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Payroll_Person.TotalMoney;
                zCommand.Parameters.Add("@TotalWorkTime", SqlDbType.Int).Value = Payroll_Person.TotalWorkTime;
                zCommand.Parameters.Add("@TotalDateWorking", SqlDbType.Float).Value = Payroll_Person.TotalDateWorking;
                zCommand.Parameters.Add("@TotalDateLeaving", SqlDbType.Float).Value = Payroll_Person.TotalDateLeaving;
                zCommand.Parameters.Add("@TotalDatePaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDatePaidLeave;
                zCommand.Parameters.Add("@TotalDateUnpaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateUnpaidLeave;
                zCommand.Parameters.Add("@TotalDateAnnualLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateAnnualLeave;
                zCommand.Parameters.Add("@TotalDateDayOff", SqlDbType.Float).Value = Payroll_Person.TotalDateDayOff;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Payroll_Person.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Payroll_Person.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Payroll_Person.CodeLine;
                if (Payroll_Person.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Payroll_Person.FromDate;
                }

                if (Payroll_Person.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Payroll_Person.ToDate;
                }

                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Payroll_Person.Activated;

                if (Payroll_Person.ActivatedDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = Payroll_Person.ActivatedDate;
                }

                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Payroll_Person.Reference;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person.Description;
                if (Payroll_Person.PartnerNumber != "" && Payroll_Person.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Person.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Person.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Person SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " Month = @Month,"
                        + " Year = @Year,"
                        + " TotalMoney = @TotalMoney,"
                        + " TotalWorkTime = @TotalWorkTime,"
                        + " TotalDateWorking = @TotalDateWorking,"
                        + " TotalDateLeaving = @TotalDateLeaving,"
                        + " TotalDatePaidLeave = @TotalDatePaidLeave,"
                        + " TotalDateUnpaidLeave = @TotalDateUnpaidLeave,"
                        + " TotalDateAnnualLeave = @TotalDateAnnualLeave,"
                        + " TotalDateDayOff = @TotalDateDayOff,"
                        + " Style = @Style,"
                        + " Class = @Class,"
                        + " CodeLine = @CodeLine,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " Activated = @Activated,"
                        + " ActivatedDate = @ActivatedDate,"
                        + " Reference = @Reference,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PayrollKey = @PayrollKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Payroll_Person.PayrollKey != "" && Payroll_Person.PayrollKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PayrollKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Person.EmployeeName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Payroll_Person.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Payroll_Person.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Payroll_Person.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Payroll_Person.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Payroll_Person.PositionName;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Payroll_Person.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Payroll_Person.Year;
                zCommand.Parameters.Add("@TotalMoney", SqlDbType.Money).Value = Payroll_Person.TotalMoney;
                zCommand.Parameters.Add("@TotalWorkTime", SqlDbType.Int).Value = Payroll_Person.TotalWorkTime;
                zCommand.Parameters.Add("@TotalDateWorking", SqlDbType.Float).Value = Payroll_Person.TotalDateWorking;
                zCommand.Parameters.Add("@TotalDateLeaving", SqlDbType.Float).Value = Payroll_Person.TotalDateLeaving;
                zCommand.Parameters.Add("@TotalDatePaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDatePaidLeave;
                zCommand.Parameters.Add("@TotalDateUnpaidLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateUnpaidLeave;
                zCommand.Parameters.Add("@TotalDateAnnualLeave", SqlDbType.Float).Value = Payroll_Person.TotalDateAnnualLeave;
                zCommand.Parameters.Add("@TotalDateDayOff", SqlDbType.Float).Value = Payroll_Person.TotalDateDayOff;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Payroll_Person.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Payroll_Person.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Payroll_Person.CodeLine;
                if (Payroll_Person.FromDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Payroll_Person.FromDate;
                }

                if (Payroll_Person.ToDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = Payroll_Person.ToDate;
                }
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Payroll_Person.Activated;
                if (Payroll_Person.ActivatedDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivatedDate", SqlDbType.DateTime).Value = Payroll_Person.ActivatedDate;
                }

                zCommand.Parameters.Add("@Reference", SqlDbType.NVarChar).Value = Payroll_Person.Reference;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person.Description;
                if (Payroll_Person.PartnerNumber != "" && Payroll_Person.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE HRM_Payroll_Person SET RecordStatus = 99 WHERE PayrollKey = @PayrollKey
UPDATE HRM_Payroll_Person_Detail SET RecordStatus = 99 WHERE PayrollKey = @PayrollKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PayrollKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Person WHERE PayrollKey = @PayrollKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PayrollKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Person_Detail SET RecordStatus = 99 WHERE PayrollKey = @PayrollKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person.PayrollKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
