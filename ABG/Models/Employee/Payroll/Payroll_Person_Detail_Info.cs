﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Person_Detail_Info
    {

        public Payroll_Person_Detail_Model Payroll_Person_Detail = new Payroll_Person_Detail_Model();

        #region [ Constructor Get Information ]
        public Payroll_Person_Detail_Info()
        {
        }
        public Payroll_Person_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Person_Detail WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Payroll_Person_Detail.PayrollKey = zReader["PayrollKey"].ToString();
                    if (zReader["ItemKey"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.ItemKey = int.Parse(zReader["ItemKey"].ToString());
                    }

                    Payroll_Person_Detail.ItemID = zReader["ItemID"].ToString();
                    Payroll_Person_Detail.ItemName = zReader["ItemName"].ToString();
                    if (zReader["Quantity"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.Quantity = float.Parse(zReader["Quantity"].ToString());
                    }

                    Payroll_Person_Detail.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Total"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.Total = double.Parse(zReader["Total"].ToString());
                    }

                    if (zReader["ItemType"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.ItemType = int.Parse(zReader["ItemType"].ToString());
                    }

                    if (zReader["MoneyPay"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.MoneyPay = double.Parse(zReader["MoneyPay"].ToString());
                    }

                    Payroll_Person_Detail.Description = zReader["Description"].ToString();
                    Payroll_Person_Detail.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Person_Detail.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Person_Detail.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Person_Detail.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Person_Detail.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Person_Detail.ModifiedName = zReader["ModifiedName"].ToString();
                    Payroll_Person_Detail.Message = "200 OK";
                }
                else
                {
                    Payroll_Person_Detail.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Person_Detail ("
         + " PayrollKey , ItemKey , ItemID , ItemName , Quantity , UnitName , Total , ItemType , MoneyPay , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PayrollKey , @ItemKey , @ItemID , @ItemName , @Quantity , @UnitName , @Total , @ItemType , @MoneyPay , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Payroll_Person_Detail.PayrollKey != "" && Payroll_Person_Detail.PayrollKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PayrollKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Person_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Person_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Person_Detail.Total;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Person_Detail.ItemType;
                zCommand.Parameters.Add("@MoneyPay", SqlDbType.Money).Value = Payroll_Person_Detail.MoneyPay;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person_Detail.Description;
                if (Payroll_Person_Detail.PartnerNumber != "" && Payroll_Person_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Person_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Person_Detail("
         + " AutoKey , PayrollKey , ItemKey , ItemID , ItemName , Quantity , UnitName , Total , ItemType , MoneyPay , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @PayrollKey , @ItemKey , @ItemID , @ItemName , @Quantity , @UnitName , @Total , @ItemType , @MoneyPay , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Person_Detail.AutoKey;
                if (Payroll_Person_Detail.PayrollKey != "" && Payroll_Person_Detail.PayrollKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PayrollKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Person_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Person_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Person_Detail.Total;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Person_Detail.ItemType;
                zCommand.Parameters.Add("@MoneyPay", SqlDbType.Money).Value = Payroll_Person_Detail.MoneyPay;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person_Detail.Description;
                if (Payroll_Person_Detail.PartnerNumber != "" && Payroll_Person_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person_Detail.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Person_Detail.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person_Detail.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Person_Detail SET "
                        + " PayrollKey = @PayrollKey,"
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " Quantity = @Quantity,"
                        + " UnitName = @UnitName,"
                        + " Total = @Total,"
                        + " ItemType = @ItemType,"
                        + " MoneyPay = @MoneyPay,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Person_Detail.AutoKey;
                if (Payroll_Person_Detail.PayrollKey != "" && Payroll_Person_Detail.PayrollKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PayrollKey);
                }
                else
                {
                    zCommand.Parameters.Add("@PayrollKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = Payroll_Person_Detail.ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemID;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ItemName;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Payroll_Person_Detail.Quantity;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.UnitName;
                zCommand.Parameters.Add("@Total", SqlDbType.Money).Value = Payroll_Person_Detail.Total;
                zCommand.Parameters.Add("@ItemType", SqlDbType.Int).Value = Payroll_Person_Detail.ItemType;
                zCommand.Parameters.Add("@MoneyPay", SqlDbType.Money).Value = Payroll_Person_Detail.MoneyPay;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Person_Detail.Description;
                if (Payroll_Person_Detail.PartnerNumber != "" && Payroll_Person_Detail.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Person_Detail.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Person_Detail.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Person_Detail.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Payroll_Person_Detail SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Person_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Person_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Payroll_Person_Detail.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Payroll_Person_Detail.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Payroll_Person_Detail.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
