﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Close_Info
    {

        public Payroll_Close_Model Payroll_Close = new Payroll_Close_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Payroll_Close_Info()
        {
        }
        public Payroll_Close_Info(string CloseDate, bool ViewDate)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Close WHERE CloseDate = @CloseDate AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = CloseDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Payroll_Close.CloseKey = zReader["CloseKey"].ToString();
                    Payroll_Close.CloseDate = zReader["CloseDate"].ToString();
                    Payroll_Close.Description = zReader["Description"].ToString();
                    Payroll_Close.EmployeeID = zReader["EmployeeID"].ToString();
                    Payroll_Close.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Close.EmployeeName = zReader["EmployeeName"].ToString();
                    Payroll_Close.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Payroll_Close.DepartmentName = zReader["DepartmentName"].ToString();
                    Payroll_Close.BranchKey = zReader["BranchKey"].ToString();
                    Payroll_Close.BranchName = zReader["BranchName"].ToString();
                    Payroll_Close.OrganizationID = zReader["OrganizationID"].ToString();
                    Payroll_Close.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Close.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Close.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Close.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Close.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Close.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Close.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Close.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Payroll_Close_Info(string CloseKey)
        {
            string zSQL = "SELECT * FROM HRM_Payroll_Close WHERE CloseKey = @CloseKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = CloseKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Payroll_Close.CloseKey = zReader["CloseKey"].ToString();
                    Payroll_Close.CloseDate = zReader["CloseDate"].ToString();
                    Payroll_Close.Description = zReader["Description"].ToString();
                    Payroll_Close.Title = zReader["Title"].ToString();
                    Payroll_Close.EmployeeID = zReader["EmployeeID"].ToString();
                    Payroll_Close.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Payroll_Close.EmployeeName = zReader["EmployeeName"].ToString();
                    Payroll_Close.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Payroll_Close.DepartmentName = zReader["DepartmentName"].ToString();
                    Payroll_Close.BranchKey = zReader["BranchKey"].ToString();
                    Payroll_Close.BranchName = zReader["BranchName"].ToString();
                    Payroll_Close.OrganizationID = zReader["OrganizationID"].ToString();
                    Payroll_Close.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Payroll_Close.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Payroll_Close.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Payroll_Close.CreatedBy = zReader["CreatedBy"].ToString();
                    Payroll_Close.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Payroll_Close.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Payroll_Close.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Payroll_Close.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Close ("
         + " CloseDate , Title, Description , EmployeeID , EmployeeKey , EmployeeName , DepartmentKey , DepartmentName , BranchKey , BranchName , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CloseDate , @Title, @Description , @EmployeeID , @EmployeeKey , @EmployeeName , @DepartmentKey , @DepartmentName , @BranchKey , @BranchName , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Payroll_Close.CloseDate;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Payroll_Close.Title;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Close.Description;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NChar).Value = Payroll_Close.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NChar).Value = Payroll_Close.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NChar).Value = Payroll_Close.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NChar).Value = Payroll_Close.BranchName;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payroll_Close.OrganizationID;
                if (Payroll_Close.PartnerNumber != "" && Payroll_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Close.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Payroll_Close("
         + " CloseKey , CloseDate , Title, Description , EmployeeID , EmployeeKey , EmployeeName , DepartmentKey , DepartmentName , BranchKey , BranchName , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CloseKey , @CloseDate , @Title, @Description , @EmployeeID , @EmployeeKey , @EmployeeName , @DepartmentKey , @DepartmentName , @BranchKey , @BranchName , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Close.CloseKey);
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Payroll_Close.CloseDate;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Payroll_Close.Title;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Close.Description;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NChar).Value = Payroll_Close.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NChar).Value = Payroll_Close.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NChar).Value = Payroll_Close.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NChar).Value = Payroll_Close.BranchName;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payroll_Close.OrganizationID;
                if (Payroll_Close.PartnerNumber != "" && Payroll_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Close.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Payroll_Close.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Payroll_Close.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Payroll_Close SET "
                        + " CloseDate = @CloseDate, Title = @Title,"
                        + " Description = @Description,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " OrganizationID = @OrganizationID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CloseKey = @CloseKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = Payroll_Close.CloseKey;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = Payroll_Close.CloseDate;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Payroll_Close.Title;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Close.Description;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Payroll_Close.EmployeeName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NChar).Value = Payroll_Close.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NChar).Value = Payroll_Close.DepartmentName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NChar).Value = Payroll_Close.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NChar).Value = Payroll_Close.BranchName;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Payroll_Close.OrganizationID;
                if (Payroll_Close.PartnerNumber != "" && Payroll_Close.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Payroll_Close.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Close.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Close()
        {
            string zSQL = "UPDATE HRM_Payroll_Close SET "
                            + " Title = @Title,"
                            + " Description = @Description,"
                            + " RecordStatus = @RecordStatus,"
                            + " ModifiedOn = GetDate(),"
                            + " ModifiedBy = @ModifiedBy,"
                            + " ModifiedName = @ModifiedName"
                            + " WHERE CloseKey = @CloseKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = Payroll_Close.CloseKey;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Payroll_Close.Title;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Payroll_Close.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Payroll_Close.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Payroll_Close.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE HRM_Payroll_Close SET RecordStatus = 99 WHERE CloseKey = @CloseKey
UPDATE HRM_Payroll_Close_Detail SET RecordStatus = 99 WHERE ParentKey = @CloseKey
UPDATE HRM_Work_Day SET RecordStatus = 99 WHERE ParentKey = @CloseKey
UPDATE HRM_Payroll_Index SET RecordStatus = 99 WHERE ParentKey = @CloseKey
UPDATE HRM_Payroll_Note SET RecordStatus = 99 WHERE ParentKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = Payroll_Close.CloseKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Payroll_Close WHERE CloseKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = Payroll_Close.CloseKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Detail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE HRM_Payroll_Close_Detail SET RecordStatus = 99 WHERE ParentKey = @CloseKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CloseKey", SqlDbType.NVarChar).Value = Payroll_Close.CloseKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion


        public string InsertDetail(string ParentKey, string CloseDate, string EmployeeKey, string JsonData, string PartnerNumber, string CreatedBy, string CreatedName, string ModifiedBy, string ModifiedName)
        {
            //---------- String SQL Access Database ---------------
            string SQL = "";
            SQL += " INSERT INTO HRM_Payroll_Close_Detail ";
            SQL += " (ParentKey , CloseDate , EmployeeKey , JsonData , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName )";
            SQL += " VALUES ";
            SQL += " (@ParentKey , @CloseDate , @EmployeeKey , @JsonData , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = CloseDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@JsonData", SqlDbType.NVarChar).Value = JsonData;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string InsertIndex(List<Payroll_Item> ListData, string ParentKey, string UserKey, string UserName)
        {
            //---------- String SQL Access Database ---------------
           string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                foreach (var rec in ListData)
                {
                    string zSQL = "INSERT INTO HRM_Payroll_Index ("
        + " DateStart , ParentKey , BranchKey , BranchName , DepartmentKey , DepartmentName , EmployeeKey , EmployeeID , EmployeeName , PositionName , CategoryKey , CategoryName , ItemKey , ItemID , ItemName , Amount , Param , Formula , Style , PartnerNumber  , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
        + " VALUES ( "
        + " @DateStart , @ParentKey , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @EmployeeKey , @EmployeeID , @EmployeeName , @PositionName , @CategoryKey , @CategoryName , @ItemKey , @ItemID , @ItemName , @Amount , @Param , @Formula , @Style , @PartnerNumber  , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";


                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;

                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DateTime.Parse(rec.DateStart);
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = rec.BranchKey;
                    zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = rec.Branch;
                    zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = rec.DepartmentKey;
                    zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = rec.DepartmentName;
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = rec.EmployeeKey;
                    zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = rec.EmployeeID;
                    zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = rec.EmployeeName;
                    zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = rec.PositionName;
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = rec.CategoryKey;
                    zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = rec.CategoryName;
                    zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = rec.ItemKey;
                    zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = rec.ItemID;
                    zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = rec.ItemName;
                    zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = rec.Amount;
                    zCommand.Parameters.Add("@Param", SqlDbType.Float).Value = rec.Param;
                    zCommand.Parameters.Add("@Formula", SqlDbType.NVarChar).Value = rec.Formula;
                    zCommand.Parameters.Add("@Style", SqlDbType.NVarChar).Value = rec.Style;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value =Helper.PartnerNumber;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = UserName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = UserKey;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = UserName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                }

                  
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
