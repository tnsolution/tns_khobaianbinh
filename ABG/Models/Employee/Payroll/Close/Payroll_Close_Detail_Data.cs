﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Payroll_Close_Detail_Data
    {
        public static List<Payroll_Close_Detail_Model> List(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Payroll_Close_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ParentKey = @ParentKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Close_Detail_Model> zList = new List<Payroll_Close_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Close_Detail_Model() {
                    AutoKey = r["AutoKey"].ToInt(),
                    ParentKey = r["ParentKey"].ToString(),
                    JsonData = r["JsonData"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),                    
                });
            }
            return zList;
        }
    }
}
