﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Over_Time_Detail_Data
    {
        public static List<Over_Time_Detail_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Over_Time WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ORDER BY OverTimeDate";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Over_Time_Detail_Model> zList = new List<Over_Time_Detail_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime zDate = DateTime.MinValue;
                DateTime.TryParse(r["OverTimeDate"].ToString(), out zDate);
                zList.Add(new Over_Time_Detail_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    ItemKey = r["ItemKey"].ToInt(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Paramater = r["Paramater"].ToFloat(),
                    Hours = r["Hours"].ToFloat(),
                    OverTimeDate = zDate
                });
            }
            return zList;
        }
    }
}
