﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public partial class SalaryTemp_Data
{
public static List<SalaryTemp_Model> List(string PartnerNumber,  out string Message)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT * FROM HRM_SalaryTemp WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
string zConnectionString =ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close(); Message = string.Empty;
}
catch (Exception ex)
{
Message = ex.ToString();
}
List<SalaryTemp_Model> zList = new List<SalaryTemp_Model>();
foreach (DataRow r in zTable.Rows){zList.Add(new SalaryTemp_Model() {AutoKey=(r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
DateStart=(r["DateStart"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DateStart"]),
Parent=(r["Parent"] == DBNull.Value ? "" : r["Parent"].ToString()),
BranchKey=(r["BranchKey"] == DBNull.Value ? "" : r["BranchKey"].ToString()),
BranchName=(r["BranchName"] == DBNull.Value ? "" : r["BranchName"].ToString()),
DepartmentKey=(r["DepartmentKey"] == DBNull.Value ? "" : r["DepartmentKey"].ToString()),
DepartmentName=(r["DepartmentName"] == DBNull.Value ? "" : r["DepartmentName"].ToString()),
EmployeeKey=(r["EmployeeKey"] == DBNull.Value ? "" : r["EmployeeKey"].ToString()),
EmployeeID=(r["EmployeeID"] == DBNull.Value ? "" : r["EmployeeID"].ToString()),
EmployeeName=(r["EmployeeName"] == DBNull.Value ? "" : r["EmployeeName"].ToString()),
PositionKey=(r["PositionKey"] == DBNull.Value ? 0 : int.Parse(r["PositionKey"].ToString())),
PositionName=(r["PositionName"] == DBNull.Value ? "" : r["PositionName"].ToString()),
CategoryKey=(r["CategoryKey"] == DBNull.Value ? 0 : int.Parse(r["CategoryKey"].ToString())),
CategoryName=(r["CategoryName"] == DBNull.Value ? "" : r["CategoryName"].ToString()),
ItemKey=(r["ItemKey"] == DBNull.Value ? 0 : int.Parse(r["ItemKey"].ToString())),
ItemID=(r["ItemID"] == DBNull.Value ? "" : r["ItemID"].ToString()),
ItemName=(r["ItemName"] == DBNull.Value ? "" : r["ItemName"].ToString()),
Amount=(r["Amount"] == DBNull.Value ? 0 : float.Parse(r["Amount"].ToString())),
Param=(r["Param"] == DBNull.Value ? 0 : float.Parse(r["Param"].ToString())),
Formula=(r["Formula"] == DBNull.Value ? "" : r["Formula"].ToString()),
Style=(r["Style"] == DBNull.Value ? "" : r["Style"].ToString()),
PartnerNumber=(r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
RecordStatus=(r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
CreatedOn=(r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
CreatedBy=(r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
CreatedName=(r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
ModifiedOn=(r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
ModifiedBy=(r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
ModifiedName=(r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
});}
return zList;
}
}
}
