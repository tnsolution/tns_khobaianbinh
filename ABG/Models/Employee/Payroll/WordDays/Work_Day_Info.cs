﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public partial class Work_Day_Info
    {

        public Work_Day_Model Work_Day = new Work_Day_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Work_Day_Info()
        {
        }
        public Work_Day_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Work_Day WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Work_Day.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Work_Day.ParentKey = zReader["ParentKey"].ToString();
                    Work_Day.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Work_Day.EmployeeID = zReader["EmployeeID"].ToString();
                    Work_Day.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                        Work_Day.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    Work_Day.OrganizationID = zReader["OrganizationID"].ToString();
                    Work_Day.OrganizationName = zReader["OrganizationName"].ToString();
                    Work_Day.BranchKey = zReader["BranchKey"].ToString();
                    Work_Day.BranchName = zReader["BranchName"].ToString();
                    Work_Day.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Work_Day.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        Work_Day.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    Work_Day.PositionName = zReader["PositionName"].ToString();
                    Work_Day.DateWrite = zReader["DateWrite"].ToString();
                    if (zReader["Days"] != DBNull.Value)
                        Work_Day.Days = float.Parse(zReader["Days"].ToString());
                    Work_Day.Description = zReader["Description"].ToString();
                    Work_Day.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Work_Day.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Work_Day.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Work_Day.CreatedBy = zReader["CreatedBy"].ToString();
                    Work_Day.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Work_Day.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Work_Day.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Work_Day.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Work_Day_Info(string ParentKey, string EmployeeKey )
        {
            string zSQL = "SELECT * FROM HRM_Work_Day WHERE ParentKey = @ParentKey AND EmployeeKey = @EmployeeKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Work_Day.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Work_Day.ParentKey = zReader["ParentKey"].ToString();
                    Work_Day.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Work_Day.EmployeeID = zReader["EmployeeID"].ToString();
                    Work_Day.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                        Work_Day.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    Work_Day.OrganizationID = zReader["OrganizationID"].ToString();
                    Work_Day.OrganizationName = zReader["OrganizationName"].ToString();
                    Work_Day.BranchKey = zReader["BranchKey"].ToString();
                    Work_Day.BranchName = zReader["BranchName"].ToString();
                    Work_Day.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Work_Day.DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        Work_Day.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    Work_Day.PositionName = zReader["PositionName"].ToString();
                    Work_Day.DateWrite = zReader["DateWrite"].ToString();
                    if (zReader["Days"] != DBNull.Value)
                        Work_Day.Days = float.Parse(zReader["Days"].ToString());
                    Work_Day.Description = zReader["Description"].ToString();
                    Work_Day.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Work_Day.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Work_Day.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Work_Day.CreatedBy = zReader["CreatedBy"].ToString();
                    Work_Day.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Work_Day.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Work_Day.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Work_Day.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Work_Day ("
         + " ParentKey , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , DateWrite , Days , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ParentKey , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @DateWrite , @Days , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Work_Day.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Work_Day.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Work_Day.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Work_Day.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Work_Day.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Work_Day.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Work_Day.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Work_Day.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Work_Day.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Work_Day.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Work_Day.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Work_Day.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Work_Day.PositionName;
                zCommand.Parameters.Add("@DateWrite", SqlDbType.NVarChar).Value = Work_Day.DateWrite;
                zCommand.Parameters.Add("@Days", SqlDbType.Float).Value = Work_Day.Days;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Work_Day.Description;
                if (Work_Day.PartnerNumber != "" && Work_Day.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Work_Day.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Work_Day.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Work_Day.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Work_Day.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Work_Day.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Work_Day.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Work_Day("
         + " AutoKey , ParentKey , EmployeeKey , EmployeeID , EmployeeName , OrganizationKey , OrganizationID , OrganizationName , BranchKey , BranchName , DepartmentKey , DepartmentName , PositionKey , PositionName , DateWrite , Days , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @ParentKey , @EmployeeKey , @EmployeeID , @EmployeeName , @OrganizationKey , @OrganizationID , @OrganizationName , @BranchKey , @BranchName , @DepartmentKey , @DepartmentName , @PositionKey , @PositionName , @DateWrite , @Days , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Work_Day.AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Work_Day.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Work_Day.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Work_Day.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Work_Day.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Work_Day.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Work_Day.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Work_Day.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Work_Day.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Work_Day.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Work_Day.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Work_Day.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Work_Day.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Work_Day.PositionName;
                zCommand.Parameters.Add("@DateWrite", SqlDbType.NVarChar).Value = Work_Day.DateWrite;
                zCommand.Parameters.Add("@Days", SqlDbType.Float).Value = Work_Day.Days;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Work_Day.Description;
                if (Work_Day.PartnerNumber != "" && Work_Day.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Work_Day.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Work_Day.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Work_Day.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Work_Day.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Work_Day.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Work_Day.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Work_Day SET "
                        + " ParentKey = @ParentKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " OrganizationKey = @OrganizationKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " OrganizationName = @OrganizationName,"
                        + " BranchKey = @BranchKey,"
                        + " BranchName = @BranchName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " PositionKey = @PositionKey,"
                        + " PositionName = @PositionName,"
                        + " DateWrite = @DateWrite,"
                        + " Days = @Days,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Work_Day.AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Work_Day.ParentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Work_Day.EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Work_Day.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Work_Day.EmployeeName;
                zCommand.Parameters.Add("@OrganizationKey", SqlDbType.Int).Value = Work_Day.OrganizationKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Work_Day.OrganizationID;
                zCommand.Parameters.Add("@OrganizationName", SqlDbType.NVarChar).Value = Work_Day.OrganizationName;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Work_Day.BranchKey;
                zCommand.Parameters.Add("@BranchName", SqlDbType.NVarChar).Value = Work_Day.BranchName;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Work_Day.DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = Work_Day.DepartmentName;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Work_Day.PositionKey;
                zCommand.Parameters.Add("@PositionName", SqlDbType.NVarChar).Value = Work_Day.PositionName;
                zCommand.Parameters.Add("@DateWrite", SqlDbType.NVarChar).Value = Work_Day.DateWrite;
                zCommand.Parameters.Add("@Days", SqlDbType.Float).Value = Work_Day.Days;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Work_Day.Description;
                if (Work_Day.PartnerNumber != "" && Work_Day.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Work_Day.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Work_Day.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Work_Day.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Work_Day.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Work_Day SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Work_Day.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Work_Day WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Work_Day.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
