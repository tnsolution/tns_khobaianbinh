﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class HistoryInsure_Info
{
 
public  HistoryInsure_Model HistoryInsure = new HistoryInsure_Model();
 
#region [ Constructor Get Information ]
public HistoryInsure_Info()
{
}
public HistoryInsure_Info(int AutoKey)
{
string zSQL = "SELECT * FROM HRM_HistoryInsure WHERE AutoKey = @AutoKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["AutoKey"]!= DBNull.Value)
HistoryInsure.AutoKey = int.Parse(zReader["AutoKey"].ToString());
HistoryInsure.EmployeeKey = zReader["EmployeeKey"].ToString();
if (zReader["DateWrite"]!= DBNull.Value)
HistoryInsure.DateWrite = (DateTime)zReader["DateWrite"];
HistoryInsure.Description = zReader["Description"].ToString();
HistoryInsure.StatusName = zReader["StatusName"].ToString();
HistoryInsure.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
HistoryInsure.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
HistoryInsure.CreatedOn = (DateTime)zReader["CreatedOn"];
HistoryInsure.CreatedBy = zReader["CreatedBy"].ToString();
HistoryInsure.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
HistoryInsure.ModifiedOn = (DateTime)zReader["ModifiedOn"];
HistoryInsure.ModifiedBy = zReader["ModifiedBy"].ToString();
HistoryInsure.ModifiedName = zReader["ModifiedName"].ToString();
HistoryInsure.Message = "200 OK";
}
else
{
HistoryInsure.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
HistoryInsure.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_HistoryInsure (" 
 + " EmployeeKey , DateWrite , Description , StatusName , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @EmployeeKey , @DateWrite , @Description , @StatusName , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = HistoryInsure.EmployeeKey;
if (HistoryInsure.DateWrite == null) 
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = DBNull.Value;
else
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = HistoryInsure.DateWrite;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = HistoryInsure.Description;
zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = HistoryInsure.StatusName;
if(HistoryInsure.PartnerNumber != "" && HistoryInsure.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(HistoryInsure.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = HistoryInsure.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = HistoryInsure.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = HistoryInsure.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
HistoryInsure.Message = "201 Created";
}
catch (Exception Err)
{
HistoryInsure.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_HistoryInsure(" 
 + " AutoKey , EmployeeKey , DateWrite , Description , StatusName , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @AutoKey , @EmployeeKey , @DateWrite , @Description , @StatusName , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = HistoryInsure.AutoKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = HistoryInsure.EmployeeKey;
if (HistoryInsure.DateWrite == null) 
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = DBNull.Value;
else
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = HistoryInsure.DateWrite;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = HistoryInsure.Description;
zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = HistoryInsure.StatusName;
if(HistoryInsure.PartnerNumber != "" && HistoryInsure.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(HistoryInsure.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = HistoryInsure.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = HistoryInsure.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = HistoryInsure.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
HistoryInsure.Message = "201 Created";
}
catch (Exception Err)
{
HistoryInsure.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_HistoryInsure SET " 
            + " EmployeeKey = @EmployeeKey,"
            + " DateWrite = @DateWrite,"
            + " Description = @Description,"
            + " StatusName = @StatusName,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE AutoKey = @AutoKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = HistoryInsure.AutoKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = HistoryInsure.EmployeeKey;
if (HistoryInsure.DateWrite == null) 
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = DBNull.Value;
else
zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = HistoryInsure.DateWrite;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = HistoryInsure.Description;
zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = HistoryInsure.StatusName;
if(HistoryInsure.PartnerNumber != "" && HistoryInsure.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(HistoryInsure.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = HistoryInsure.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = HistoryInsure.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
HistoryInsure.Message = "200 OK";
}
catch (Exception Err)
{
HistoryInsure.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_HistoryInsure SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = HistoryInsure.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
HistoryInsure.Message = "200 OK";
}
catch (Exception Err)
{
HistoryInsure.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_HistoryInsure WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = HistoryInsure.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
HistoryInsure.Message = "200 OK";
}
catch (Exception Err)
{
HistoryInsure.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
