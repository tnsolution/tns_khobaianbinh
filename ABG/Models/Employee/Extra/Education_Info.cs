﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Education_Info
    {

        public Education_Model Education = new Education_Model();

        #region [ Constructor Get Information ]
        public Education_Info()
        {
        }
        public Education_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Education WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Education.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Education.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Education.DegreeName = zReader["DegreeName"].ToString();
                    Education.DegreeBy = zReader["DegreeBy"].ToString();
                    Education.DegreePlace = zReader["DegreePlace"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                        Education.FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        Education.ToDate = (DateTime)zReader["ToDate"];
                    if (zReader["StatusKey"] != DBNull.Value)
                        Education.StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    Education.StatusName = zReader["StatusName"].ToString();
                    if (zReader["ClassifiedKey"] != DBNull.Value)
                        Education.ClassifiedKey = int.Parse(zReader["ClassifiedKey"].ToString());
                    Education.ClassifiedName = zReader["ClassifiedName"].ToString();
                    Education.TypeName = zReader["TypeName"].ToString();
                    if (zReader["ExpireDate"] != DBNull.Value)
                        Education.ExpireDate = (DateTime)zReader["ExpireDate"];
                    Education.Description = zReader["Description"].ToString();
                    Education.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Education.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Education.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Education.CreatedBy = zReader["CreatedBy"].ToString();
                    Education.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Education.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Education.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Education.ModifiedName = zReader["ModifiedName"].ToString();
                    Education.Message = "200 OK";
                }
                else
                {
                    Education.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Education.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Education ("
         + " EmployeeKey , DegreeName , DegreeBy , DegreePlace , FromDate , ToDate , StatusKey , StatusName , ClassifiedKey , ClassifiedName , TypeName , ExpireDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @EmployeeKey , @DegreeName , @DegreeBy , @DegreePlace , @FromDate , @ToDate , @StatusKey , @StatusName , @ClassifiedKey , @ClassifiedName , @TypeName , @ExpireDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Education.EmployeeKey != "" && Education.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.EmployeeKey);
                }
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DegreeName", SqlDbType.NVarChar).Value = Education.DegreeName;
                zCommand.Parameters.Add("@DegreeBy", SqlDbType.NVarChar).Value = Education.DegreeBy;
                zCommand.Parameters.Add("@DegreePlace", SqlDbType.NVarChar).Value = Education.DegreePlace;
                if (Education.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Education.FromDate;
                if (Education.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Education.ToDate;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Education.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Education.StatusName;
                zCommand.Parameters.Add("@ClassifiedKey", SqlDbType.Int).Value = Education.ClassifiedKey;
                zCommand.Parameters.Add("@ClassifiedName", SqlDbType.NVarChar).Value = Education.ClassifiedName;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Education.TypeName;
                if (Education.ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Education.ExpireDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Education.Description;
                if (Education.PartnerNumber != "" && Education.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Education.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Education.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Education.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Education.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Education.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Education.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Education.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Education("
         + " AutoKey , EmployeeKey , DegreeName , DegreeBy , DegreePlace , FromDate , ToDate , StatusKey , StatusName , ClassifiedKey , ClassifiedName , TypeName , ExpireDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @EmployeeKey , @DegreeName , @DegreeBy , @DegreePlace , @FromDate , @ToDate , @StatusKey , @StatusName , @ClassifiedKey , @ClassifiedName , @TypeName , @ExpireDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Education.AutoKey;
                if (Education.EmployeeKey != "" && Education.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.EmployeeKey);
                }
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DegreeName", SqlDbType.NVarChar).Value = Education.DegreeName;
                zCommand.Parameters.Add("@DegreeBy", SqlDbType.NVarChar).Value = Education.DegreeBy;
                zCommand.Parameters.Add("@DegreePlace", SqlDbType.NVarChar).Value = Education.DegreePlace;
                if (Education.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Education.FromDate;
                if (Education.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Education.ToDate;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Education.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Education.StatusName;
                zCommand.Parameters.Add("@ClassifiedKey", SqlDbType.Int).Value = Education.ClassifiedKey;
                zCommand.Parameters.Add("@ClassifiedName", SqlDbType.NVarChar).Value = Education.ClassifiedName;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Education.TypeName;
                if (Education.ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Education.ExpireDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Education.Description;
                if (Education.PartnerNumber != "" && Education.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Education.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Education.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Education.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Education.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Education.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Education.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Education.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Education SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " DegreeName = @DegreeName,"
                        + " DegreeBy = @DegreeBy,"
                        + " DegreePlace = @DegreePlace,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " StatusKey = @StatusKey,"
                        + " StatusName = @StatusName,"
                        + " ClassifiedKey = @ClassifiedKey,"
                        + " ClassifiedName = @ClassifiedName,"
                        + " TypeName = @TypeName,"
                        + " ExpireDate = @ExpireDate,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Education.AutoKey;
                if (Education.EmployeeKey != "" && Education.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.EmployeeKey);
                }
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@DegreeName", SqlDbType.NVarChar).Value = Education.DegreeName;
                zCommand.Parameters.Add("@DegreeBy", SqlDbType.NVarChar).Value = Education.DegreeBy;
                zCommand.Parameters.Add("@DegreePlace", SqlDbType.NVarChar).Value = Education.DegreePlace;
                if (Education.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Education.FromDate;
                if (Education.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Education.ToDate;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Education.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Education.StatusName;
                zCommand.Parameters.Add("@ClassifiedKey", SqlDbType.Int).Value = Education.ClassifiedKey;
                zCommand.Parameters.Add("@ClassifiedName", SqlDbType.NVarChar).Value = Education.ClassifiedName;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Education.TypeName;
                if (Education.ExpireDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = Education.ExpireDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Education.Description;
                if (Education.PartnerNumber != "" && Education.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Education.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Education.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Education.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Education.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Education.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Education.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Education SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Education.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Education.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Education.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Education WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Education.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Education.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Education.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
