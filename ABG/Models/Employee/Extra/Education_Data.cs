﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Education_Data
    {
        public static DataTable List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Education WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Education_Model> ListEdu(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Education WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey ORDER BY ToDate";          
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Education_Model> zList = new List<Education_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zFromDate = DateTime.MinValue;
                    DateTime.TryParse(r["FromDate"].ToString(), out zFromDate);

                    DateTime zToDate = DateTime.MinValue;
                    DateTime.TryParse(r["ToDate"].ToString(), out zToDate);

                    zList.Add(new Education_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        DegreeName = r["DegreeName"].ToString(),
                        DegreeBy = r["DegreeBy"].ToString(),
                        DegreePlace = r["DegreePlace"].ToString(),
                        FromDate = zFromDate,
                        ToDate = zToDate,
                        ClassifiedName = r["ClassifiedName"].ToString(),
                        StatusName = r["StatusName"].ToString(),
                        TypeName = r["TypeName"].ToString(),
                        Description = r["Description"].ToString()
                    });
                }
            }
            return zList;
        }
    }
}
