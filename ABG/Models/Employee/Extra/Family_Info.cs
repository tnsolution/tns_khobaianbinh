﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Family_Info
    {

        public Family_Model Family = new Family_Model();

        #region [ Constructor Get Information ]
        public Family_Info()
        {
        }
        public Family_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Family WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Family.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Family.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Family.FatherName = zReader["FatherName"].ToString();
                    if (zReader["FatherBirthday"] != DBNull.Value)
                        Family.FatherBirthday = (DateTime)zReader["FatherBirthday"];
                    Family.FatherWork = zReader["FatherWork"].ToString();
                    Family.FatherAddress = zReader["FatherAddress"].ToString();
                    Family.MotherName = zReader["MotherName"].ToString();
                    if (zReader["MotherBirthday"] != DBNull.Value)
                        Family.MotherBirthday = (DateTime)zReader["MotherBirthday"];
                    Family.MotherWork = zReader["MotherWork"].ToString();
                    Family.MotherAddress = zReader["MotherAddress"].ToString();
                    Family.PartnersName = zReader["PartnersName"].ToString();
                    if (zReader["PartnersBirthday"] != DBNull.Value)
                        Family.PartnersBirthday = (DateTime)zReader["PartnersBirthday"];
                    Family.PartnersWork = zReader["PartnersWork"].ToString();
                    Family.PartnersAddress = zReader["PartnersAddress"].ToString();
                    Family.ChildExtend = zReader["ChildExtend"].ToString();
                    Family.OrtherExtend = zReader["OrtherExtend"].ToString();
                    Family.Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Family.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Family.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Family.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Family.CreatedBy = zReader["CreatedBy"].ToString();
                    Family.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Family.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Family.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Family.ModifiedName = zReader["ModifiedName"].ToString();
                    Family.Message = "200 OK";
                }
                else
                {
                    Family.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Family.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Family_Info(string EmployeeKey)
        {
            string zSQL = "SELECT * FROM HRM_Family WHERE EmployeeKey = @EmployeeKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Family.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Family.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Family.FatherName = zReader["FatherName"].ToString();
                    if (zReader["FatherBirthday"] != DBNull.Value)
                        Family.FatherBirthday = (DateTime)zReader["FatherBirthday"];
                    Family.FatherWork = zReader["FatherWork"].ToString();
                    Family.FatherAddress = zReader["FatherAddress"].ToString();
                    Family.MotherName = zReader["MotherName"].ToString();
                    if (zReader["MotherBirthday"] != DBNull.Value)
                        Family.MotherBirthday = (DateTime)zReader["MotherBirthday"];
                    Family.MotherWork = zReader["MotherWork"].ToString();
                    Family.MotherAddress = zReader["MotherAddress"].ToString();
                    Family.PartnersName = zReader["PartnersName"].ToString();
                    if (zReader["PartnersBirthday"] != DBNull.Value)
                        Family.PartnersBirthday = (DateTime)zReader["PartnersBirthday"];
                    Family.PartnersWork = zReader["PartnersWork"].ToString();
                    Family.PartnersAddress = zReader["PartnersAddress"].ToString();
                    Family.ChildExtend = zReader["ChildExtend"].ToString();
                    Family.OrtherExtend = zReader["OrtherExtend"].ToString();
                    Family.Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Family.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Family.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Family.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Family.CreatedBy = zReader["CreatedBy"].ToString();
                    Family.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Family.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Family.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Family.ModifiedName = zReader["ModifiedName"].ToString();
                    Family.Message = "200 OK";
                }
                else
                {
                    Family.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Family.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Family ("
         + " EmployeeKey , FatherName , FatherBirthday , FatherWork , FatherAddress , MotherName , MotherBirthday , MotherWork , MotherAddress , PartnersName , PartnersBirthday , PartnersWork , PartnersAddress , ChildExtend , OrtherExtend , Description , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @EmployeeKey , @FatherName , @FatherBirthday , @FatherWork , @FatherAddress , @MotherName , @MotherBirthday , @MotherWork , @MotherAddress , @PartnersName , @PartnersBirthday , @PartnersWork , @PartnersAddress , @ChildExtend , @OrtherExtend , @Description , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Family.EmployeeKey;
                zCommand.Parameters.Add("@FatherName", SqlDbType.NVarChar).Value = Family.FatherName;
                if (Family.FatherBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@FatherBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FatherBirthday", SqlDbType.DateTime).Value = Family.FatherBirthday;
                zCommand.Parameters.Add("@FatherWork", SqlDbType.NVarChar).Value = Family.FatherWork;
                zCommand.Parameters.Add("@FatherAddress", SqlDbType.NVarChar).Value = Family.FatherAddress;
                zCommand.Parameters.Add("@MotherName", SqlDbType.NVarChar).Value = Family.MotherName;
                if (Family.MotherBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@MotherBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@MotherBirthday", SqlDbType.DateTime).Value = Family.MotherBirthday;
                zCommand.Parameters.Add("@MotherWork", SqlDbType.NVarChar).Value = Family.MotherWork;
                zCommand.Parameters.Add("@MotherAddress", SqlDbType.NVarChar).Value = Family.MotherAddress;
                zCommand.Parameters.Add("@PartnersName", SqlDbType.NVarChar).Value = Family.PartnersName;
                if (Family.PartnersBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@PartnersBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PartnersBirthday", SqlDbType.DateTime).Value = Family.PartnersBirthday;
                zCommand.Parameters.Add("@PartnersWork", SqlDbType.NVarChar).Value = Family.PartnersWork;
                zCommand.Parameters.Add("@PartnersAddress", SqlDbType.NVarChar).Value = Family.PartnersAddress;
                zCommand.Parameters.Add("@ChildExtend", SqlDbType.NVarChar).Value = Family.ChildExtend;
                zCommand.Parameters.Add("@OrtherExtend", SqlDbType.NVarChar).Value = Family.OrtherExtend;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Family.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Family.RecordStatus;
                if (Family.PartnerNumber != "" && Family.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Family.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Family.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Family.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Family.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Family.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Family.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Family.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE HRM_Family SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " FatherName = @FatherName,"
                        + " FatherBirthday = @FatherBirthday,"
                        + " FatherWork = @FatherWork,"
                        + " FatherAddress = @FatherAddress,"
                        + " MotherName = @MotherName,"
                        + " MotherBirthday = @MotherBirthday,"
                        + " MotherWork = @MotherWork,"
                        + " MotherAddress = @MotherAddress,"
                        + " PartnersName = @PartnersName,"
                        + " PartnersBirthday = @PartnersBirthday,"
                        + " PartnersWork = @PartnersWork,"
                        + " PartnersAddress = @PartnersAddress,"
                        + " ChildExtend = @ChildExtend,"
                        + " OrtherExtend = @OrtherExtend,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Family.AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Family.EmployeeKey;
                zCommand.Parameters.Add("@FatherName", SqlDbType.NVarChar).Value = Family.FatherName;
                if (Family.FatherBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@FatherBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FatherBirthday", SqlDbType.DateTime).Value = Family.FatherBirthday;
                zCommand.Parameters.Add("@FatherWork", SqlDbType.NVarChar).Value = Family.FatherWork;
                zCommand.Parameters.Add("@FatherAddress", SqlDbType.NVarChar).Value = Family.FatherAddress;
                zCommand.Parameters.Add("@MotherName", SqlDbType.NVarChar).Value = Family.MotherName;
                if (Family.MotherBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@MotherBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@MotherBirthday", SqlDbType.DateTime).Value = Family.MotherBirthday;
                zCommand.Parameters.Add("@MotherWork", SqlDbType.NVarChar).Value = Family.MotherWork;
                zCommand.Parameters.Add("@MotherAddress", SqlDbType.NVarChar).Value = Family.MotherAddress;
                zCommand.Parameters.Add("@PartnersName", SqlDbType.NVarChar).Value = Family.PartnersName;
                if (Family.PartnersBirthday == DateTime.MinValue)
                    zCommand.Parameters.Add("@PartnersBirthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PartnersBirthday", SqlDbType.DateTime).Value = Family.PartnersBirthday;
                zCommand.Parameters.Add("@PartnersWork", SqlDbType.NVarChar).Value = Family.PartnersWork;
                zCommand.Parameters.Add("@PartnersAddress", SqlDbType.NVarChar).Value = Family.PartnersAddress;
                zCommand.Parameters.Add("@ChildExtend", SqlDbType.NVarChar).Value = Family.ChildExtend;
                zCommand.Parameters.Add("@OrtherExtend", SqlDbType.NVarChar).Value = Family.OrtherExtend;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Family.Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Family.RecordStatus;
                if (Family.PartnerNumber != "" && Family.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Family.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Family.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Family.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Family.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Family.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Family SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Family.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Family.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Family.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Family WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Family.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Family.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Family.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
