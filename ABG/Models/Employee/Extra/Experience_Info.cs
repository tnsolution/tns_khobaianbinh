﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Experience_Info
    {

        public Experience_Model Experience = new Experience_Model();

        #region [ Constructor Get Information ]
        public Experience_Info()
        {
        }
        public Experience_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Experience WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Experience.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Experience.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Experience.UnitWork = zReader["UnitWork"].ToString();
                    Experience.UnitPosition = zReader["UnitPosition"].ToString();
                    if (zReader["FromDate"] != DBNull.Value)
                        Experience.FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        Experience.ToDate = (DateTime)zReader["ToDate"];
                    Experience.Description = zReader["Description"].ToString();
                    Experience.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Experience.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Experience.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Experience.CreatedBy = zReader["CreatedBy"].ToString();
                    Experience.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Experience.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Experience.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Experience.ModifiedName = zReader["ModifiedName"].ToString();
                    Experience.Message = "200 OK";
                }
                else
                {
                    Experience.Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Experience.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Experience ("
         + " EmployeeKey , UnitWork , UnitPosition , FromDate , ToDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @EmployeeKey , @UnitWork , @UnitPosition , @FromDate , @ToDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Experience.EmployeeKey;
                zCommand.Parameters.Add("@UnitWork", SqlDbType.NVarChar).Value = Experience.UnitWork;
                zCommand.Parameters.Add("@UnitPosition", SqlDbType.NVarChar).Value = Experience.UnitPosition;
                if (Experience.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Experience.FromDate;
                if (Experience.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Experience.ToDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Experience.Description;
                if (Experience.PartnerNumber != "" && Experience.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Experience.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Experience.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Experience.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Experience.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Experience.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Experience.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Experience.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Experience.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Experience("
         + " AutoKey , EmployeeKey , UnitWork , UnitPosition , FromDate , ToDate , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @EmployeeKey , @UnitWork , @UnitPosition , @FromDate , @ToDate , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Experience.AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Experience.EmployeeKey;
                zCommand.Parameters.Add("@UnitWork", SqlDbType.NVarChar).Value = Experience.UnitWork;
                zCommand.Parameters.Add("@UnitPosition", SqlDbType.NVarChar).Value = Experience.UnitPosition;
                if (Experience.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Experience.FromDate;
                if (Experience.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Experience.ToDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Experience.Description;
                if (Experience.PartnerNumber != "" && Experience.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Experience.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Experience.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Experience.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Experience.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Experience.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Experience.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Experience.Message = "201 Created";
            }
            catch (Exception Err)
            {
                Experience.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Experience SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " UnitWork = @UnitWork,"
                        + " UnitPosition = @UnitPosition,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Experience.AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Experience.EmployeeKey;
                zCommand.Parameters.Add("@UnitWork", SqlDbType.NVarChar).Value = Experience.UnitWork;
                zCommand.Parameters.Add("@UnitPosition", SqlDbType.NVarChar).Value = Experience.UnitPosition;
                if (Experience.FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = Experience.FromDate;
                if (Experience.ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = Experience.ToDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Experience.Description;
                if (Experience.PartnerNumber != "" && Experience.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Experience.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Experience.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Experience.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Experience.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Experience.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Experience.Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Experience SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Experience.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Experience.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Experience.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Experience WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Experience.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Experience.Message = "200 OK";
            }
            catch (Exception Err)
            {
                Experience.Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
