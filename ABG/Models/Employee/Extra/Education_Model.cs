﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Education_Model
{
#region [ Field Name ]
private int _AutoKey = 0;
private string _EmployeeKey = "";
private string _DegreeName = "";
private string _DegreeBy = "";
private string _DegreePlace = "";
private DateTime _FromDate = DateTime.MinValue;
private DateTime _ToDate = DateTime.MinValue;
private int _StatusKey = 0;
private string _StatusName = "";
private int _ClassifiedKey = 0;
private string _ClassifiedName = "";
private string _TypeName = "";
private DateTime _ExpireDate = DateTime.MinValue;
private string _Description = "";
private string _PartnerNumber = "";
private int _RecordStatus = 0;
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public string EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public string DegreeName
{
get { return _DegreeName; }
set { _DegreeName = value; }
}
public string DegreeBy
{
get { return _DegreeBy; }
set { _DegreeBy = value; }
}
public string DegreePlace
{
get { return _DegreePlace; }
set { _DegreePlace = value; }
}
public DateTime FromDate
{
get { return _FromDate; }
set { _FromDate = value; }
}
public DateTime ToDate
{
get { return _ToDate; }
set { _ToDate = value; }
}
public int StatusKey
{
get { return _StatusKey; }
set { _StatusKey = value; }
}
public string StatusName
{
get { return _StatusName; }
set { _StatusName = value; }
}
public int ClassifiedKey
{
get { return _ClassifiedKey; }
set { _ClassifiedKey = value; }
}
public string ClassifiedName
{
get { return _ClassifiedName; }
set { _ClassifiedName = value; }
}
public string TypeName
{
get { return _TypeName; }
set { _TypeName = value; }
}
public DateTime ExpireDate
{
get { return _ExpireDate; }
set { _ExpireDate = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Code 
{
get { 
if (_Message.Length >= 3)
return _Message.Substring(0, 3);
else return "";
}
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
}
}
