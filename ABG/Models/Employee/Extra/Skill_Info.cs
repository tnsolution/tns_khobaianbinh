﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
public class Skill_Info
{
 
public  Skill_Model Skill = new Skill_Model();
 
#region [ Constructor Get Information ]
public Skill_Info()
{
}
public Skill_Info(int AutoKey)
{
string zSQL = "SELECT * FROM HRM_Skill WHERE AutoKey = @AutoKey AND RecordStatus != 99 "; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["AutoKey"]!= DBNull.Value)
Skill.AutoKey = int.Parse(zReader["AutoKey"].ToString());
Skill.EmployeeKey = zReader["EmployeeKey"].ToString();
Skill.SkillName = zReader["SkillName"].ToString();
if (zReader["Levels"]!= DBNull.Value)
Skill.Levels = int.Parse(zReader["Levels"].ToString());
if (zReader["Maxlevels"]!= DBNull.Value)
Skill.Maxlevels = int.Parse(zReader["Maxlevels"].ToString());
Skill.Description = zReader["Description"].ToString();
if (zReader["Rank"]!= DBNull.Value)
Skill.Rank = int.Parse(zReader["Rank"].ToString());
Skill.PartnerNumber = zReader["PartnerNumber"].ToString();
if (zReader["RecordStatus"]!= DBNull.Value)
Skill.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
if (zReader["CreatedOn"]!= DBNull.Value)
Skill.CreatedOn = (DateTime)zReader["CreatedOn"];
Skill.CreatedBy = zReader["CreatedBy"].ToString();
Skill.CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedOn"]!= DBNull.Value)
Skill.ModifiedOn = (DateTime)zReader["ModifiedOn"];
Skill.ModifiedBy = zReader["ModifiedBy"].ToString();
Skill.ModifiedName = zReader["ModifiedName"].ToString();
Skill.Message = "200 OK";
}
else
{
Skill.Message = "404 Not Found";
}
zReader.Close();
zCommand.Dispose();
}
catch (Exception Err)
{
Skill.Message = "500 " + Err.ToString();
}
finally
{
zConnect.Close();
}
}

#endregion
 
#region [ Constructor Update Information ]
 
public string Create_ServerKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Skill (" 
 + " EmployeeKey , SkillName , Levels , Maxlevels , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @EmployeeKey , @SkillName , @Levels , @Maxlevels , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Skill.EmployeeKey;
zCommand.Parameters.Add("@SkillName", SqlDbType.NVarChar).Value = Skill.SkillName;
zCommand.Parameters.Add("@Levels", SqlDbType.Int).Value = Skill.Levels;
zCommand.Parameters.Add("@Maxlevels", SqlDbType.Int).Value = Skill.Maxlevels;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Skill.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Skill.Rank;
if(Skill.PartnerNumber != "" && Skill.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Skill.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Skill.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Skill.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Skill.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Skill.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Skill.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Skill.Message = "201 Created";
}
catch (Exception Err)
{
Skill.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Create_ClientKey()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO HRM_Skill(" 
 + " AutoKey , EmployeeKey , SkillName , Levels , Maxlevels , Description , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
 + " VALUES ( "
 + " @AutoKey , @EmployeeKey , @SkillName , @Levels , @Maxlevels , @Description , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Skill.AutoKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Skill.EmployeeKey;
zCommand.Parameters.Add("@SkillName", SqlDbType.NVarChar).Value = Skill.SkillName;
zCommand.Parameters.Add("@Levels", SqlDbType.Int).Value = Skill.Levels;
zCommand.Parameters.Add("@Maxlevels", SqlDbType.Int).Value = Skill.Maxlevels;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Skill.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Skill.Rank;
if(Skill.PartnerNumber != "" && Skill.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Skill.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Skill.RecordStatus;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Skill.CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Skill.CreatedName;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Skill.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Skill.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Skill.Message = "201 Created";
}
catch (Exception Err)
{
Skill.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Update() 
{ 
string zSQL = "UPDATE HRM_Skill SET " 
            + " EmployeeKey = @EmployeeKey,"
            + " SkillName = @SkillName,"
            + " Levels = @Levels,"
            + " Maxlevels = @Maxlevels,"
            + " Description = @Description,"
            + " Rank = @Rank,"
            + " PartnerNumber = @PartnerNumber,"
            + " RecordStatus = @RecordStatus,"
            + " ModifiedOn = GetDate(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
            + " WHERE AutoKey = @AutoKey";
string zResult = ""; 
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Skill.AutoKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Skill.EmployeeKey;
zCommand.Parameters.Add("@SkillName", SqlDbType.NVarChar).Value = Skill.SkillName;
zCommand.Parameters.Add("@Levels", SqlDbType.Int).Value = Skill.Levels;
zCommand.Parameters.Add("@Maxlevels", SqlDbType.Int).Value = Skill.Maxlevels;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Skill.Description;
zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Skill.Rank;
if(Skill.PartnerNumber != "" && Skill.PartnerNumber.Length == 36)
{
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Skill.PartnerNumber);
}
else
zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Skill.RecordStatus;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Skill.ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Skill.ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Skill.Message = "200 OK";
}
catch (Exception Err)
{
Skill.Message = "500 " + Err.ToString();
}
 finally
{
zConnect.Close();
}
return zResult;
}

 
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "UPDATE HRM_Skill SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Skill.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Skill.Message = "200 OK";
}
catch (Exception Err)
{
Skill.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
public string Empty()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM HRM_Skill WHERE AutoKey = @AutoKey";
string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Skill.AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
Skill.Message = "200 OK";
}
catch (Exception Err)
{
Skill.Message = "500" + Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
