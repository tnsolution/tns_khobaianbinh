﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Review_Detail_Data
    {
        public static List<Review_Detail_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Review_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Review_Detail_Model> zList = new List<Review_Detail_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Review_Detail_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        ReviewKey = r["ReviewKey"].ToString(),
                        CriteriaName = r["CriteriaName"].ToString(),
                        Point = r["Point"].ToInt(),
                        MaxPoint = r["MaxPoint"].ToInt(),
                        Rank = r["Rank"].ToInt(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
        public static List<Review_Detail_Model> List(string PartnerNumber,string ReviewKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.AutoKey, A.ReviewKey, B.CriteriaName,
A.Point, B.MaxPoint, A.[Description], B.Rank
FROM HRM_Review_Detail A
LEFT JOIN HRM_Criteria B ON A.CriteriaKey = B.CriteriaKey 
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND A.ReviewKey =@ReviewKey ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@ReviewKey", SqlDbType.NVarChar).Value = ReviewKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Review_Detail_Model> zList = new List<Review_Detail_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new Review_Detail_Model()
                    {
                        AutoKey = r["AutoKey"].ToInt(),
                        ReviewKey = r["ReviewKey"].ToString(),
                        CriteriaName = r["CriteriaName"].ToString(),
                        Point = r["Point"].ToInt(),
                        MaxPoint = r["MaxPoint"].ToInt(),
                        Rank = r["Rank"].ToInt(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
    }
}
