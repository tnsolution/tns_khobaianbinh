﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Review_Data
    {

        public static List<Review_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Review WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Review_Model> zList = new List<Review_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime zDateWrite = DateTime.MinValue;
                    DateTime.TryParse(r["DateWrite"].ToString(), out zDateWrite);

                    zList.Add(new Review_Model()
                    {
                        ReviewKey = r["ReviewKey"].ToString(),
                        DateWrite = zDateWrite,
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        ValuerName = r["ValuerName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }
            }
            return zList;
        }
        public static List<Review_Model> List(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Review WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Review_Model> zList = new List<Review_Model>();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    List<Review_Detail_Model> zDetail = Review_Detail_Data.List(PartnerNumber, r["ReviewKey"].ToString());


                    DateTime zDateWrite = DateTime.MinValue;
                    DateTime.TryParse(r["DateWrite"].ToString(), out zDateWrite);

                    zList.Add(new Review_Model()
                    {
                        ReviewKey = r["ReviewKey"].ToString(),
                        DateWrite = zDateWrite,
                        EmployeeName = r["EmployeeName"].ToString(),
                        BranchName = r["BranchName"].ToString(),
                        DepartmentName = r["DepartmentName"].ToString(),
                        ValuerName = r["ValuerName"].ToString(),
                        Description = r["Description"].ToString(),
                        ListItem = zDetail
                    });
                }
            }
            return zList;
        }
    }
}
