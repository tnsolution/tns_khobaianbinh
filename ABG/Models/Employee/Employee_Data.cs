﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ABG
{
    public class Employee_Data
    {
        public static List<Employee_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> ListRECURSIVE(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
; WITH R AS (
	-- ANCHOR PART
      SELECT 
		  A.EmployeeKey, 
		  A.LastName,
		  A.FirstName,
		  A.ReportToKey,
          A.PhotoPath,
		  B.PositionKey,
		  DEPTH = 0 , 
		  SORT = CAST(A.EmployeeKey AS VARCHAR(MAX))
      FROM HRM_Employee A LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey
      WHERE A.RecordStatus <> 99
      AND ReportToKey IS NULL	  
	  AND A.PartnerNumber = @PartnerNumber
      UNION ALL

	-- RECURSIVE PART
      SELECT 		
		Sub.EmployeeKey, 
		Sub.LastName, 
		Sub.FirstName,
		Sub.ReportToKey,
        Sub.PhotoPath,
		Sub.PositionKey,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '>' + CAST(Sub.EmployeeKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN HRM_Employee Sub ON R.EmployeeKey=Sub.ReportToKey	  
	  WHERE Sub.RecordStatus <> 99	  
	  AND Sub.PartnerNumber = @PartnerNumber
)

SELECT R.LastName, R.FirstName, R.SORT, R.DEPTH, R.EmployeeKey, ReportToKey, PhotoPath
FROM R 
ORDER BY SORT";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    ReportToKey = r["ReportToKey"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Slug = r["DEPTH"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                });
            }

            return zList;
        }

        public static List<Employee_Model> Search(string PartnerNumber, string SearchName, string Department, DateTime FromDate, DateTime ToDate, int FromAge, int ToAge, int Gender, string Edu, string Position, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Department != string.Empty &&
                Department.Length > 36)
            {
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            }
            else if (Department != string.Empty)
            {
                zSQL += " AND A.DepartmentKey = @Department";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.StartingDate BETWEEN @FromDate AND @ToDate";
            }
            if (SearchName.Trim().Length > 0)
            {
                zSQL += " AND (LastName + ' ' + FirstName) LIKE @SearchName";
            }
            if (FromAge != 0 && ToAge != 0)
            {
                zSQL += " AND DATEDIFF(YEAR, A.Birthday, GETDATE()) BETWEEN @FromAge AND @ToAge";
            }
            if (Gender >= 0)
            {
                zSQL += " AND A.Gender = @Gender";
            }
            if (Edu != string.Empty)
            {
                //TÌM THEO TRÌNH ĐỘ FIX CODE NHƯ HIỆN TẠI CHO NHANH
                //DỘ DÀI CHỮ "Chưa qua đào tạo, dưới trung cấp"
                if (Edu.Length < 30)
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) = @Edu)";
                }
                else
                {
                    zSQL += " AND A.EmployeeKey IN (SELECT EmployeeKey FROM HRM_Education WHERE RecordStatus <> 99 AND UPPER(TypeName) NOT IN (N'ĐẠI HỌC', N'CAO ĐẲNG' ,N'TRUNG CẤP', N'PHỔ THÔNG'))";
                }
            }
            if (Position != string.Empty)
            {
                zSQL += " AND A.PositionKey = @Position";
            }
            if (Status != 0)
            {
                zSQL += " AND A.WorkingStatusKey = @Status";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = Department;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Edu;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;
                zCommand.Parameters.Add("@FromAge", SqlDbType.Int).Value = FromAge;
                zCommand.Parameters.Add("@ToAge", SqlDbType.Int).Value = ToAge;
                zCommand.Parameters.Add("@SearchName", SqlDbType.NVarChar).Value = "%" + SearchName.Trim() + "%";
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Employee_Model> zList = new List<Employee_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                });
            }
            return zList;
        }

        public static List<Employee_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> ListReportTo(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT EmployeeKey, LastName, FirstName 
FROM HRM_Employee A
LEFT JOIN HRM_ListPosition B ON B.PositionKey=A.PositionKey
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber 
AND B.[TypeKey] = 1  
ORDER BY B.Rank ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                });
            }

            return zList;
        }

        public static List<Payroll_Item> TINHLUONG(string PartnerNumber, string Employee, DateTime Date)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TINHLUONGANBINH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Item> zList = new List<Payroll_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Item()
                {
                    ItemKey = r["ItemKey"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Param = r["Param"].ToString(),
                    Formula = r["Formula"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Amount = r["Amount"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable ListPayrollClose(string PartnerNumber, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT JsonData FROM HRM_Payroll_Close_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<Payroll_Item> GetPayrollClose(string PartnerNumber, string CloseDate, string EmployeeKey)
        {
            string zJSON = "";
            //string zDate = Date.ToString("MM/yyyy");
            string zSQL = "SELECT JsonData FROM HRM_Payroll_Close_Detail WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND EmployeeKey = @EmployeeKey AND CloseDate = @CloseDate";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@CloseDate", SqlDbType.NVarChar).Value = CloseDate;
                zJSON = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Payroll_Item> zList = JsonConvert.DeserializeObject<List<Payroll_Item>>(zJSON);

            return zList;
        }

        public static List<Leave_Item> TINHPHEP(string PartnerNumber, string Employee, string YearView, string Name, string ID, string Posittion, string Department, DateTime YearWork)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TINHPHEPANBINH";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@YearView", SqlDbType.NVarChar).Value = YearView;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Leave_Item> zList = new List<Leave_Item>();
            if (zTable.Rows.Count > 0)
            {
                float Total = 0;
                float CloseIn = 0;
                int Index = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    Index++;

                    float CloseEnd = r["CloseEnd"].ToFloat();
                    float BeginNo = r["BeginNo"].ToFloat();
                    float BeginPlus = r["BeginPlus"].ToFloat();
                    float Incurred = r["TotalDate"].ToFloat();
                    CloseIn += BeginNo + BeginPlus - Incurred;
                    Total += CloseEnd + BeginNo + BeginPlus - Incurred;

                    zList.Add(new Leave_Item()
                    {
                        EmployeeID = ID,
                        EmployeeKey = Employee,
                        EmployeeName = Name,
                        PositionName = Posittion,
                        DepartmentName = Department,
                        Sort = r["Sort"].ToString(),
                        BeginYear = r["BeginYear"].ToString(),
                        BeginNo = BeginNo.ToString(),
                        BeginPlus = BeginPlus.ToString(),
                        FromDate = r["FromDate"].ToDateString(),
                        ToDate = r["ToDate"].ToDateString(),
                        TotalDate = Incurred.ToString(),
                        CloseEnd = CloseIn.ToString(),
                        CloseYear = r["CloseYear"].ToString(),
                        Incremental = Total.ToString(),
                        CategoryName = r["CategoryName"].ToString(),
                        Description = r["Description"].ToString(),
                    });
                }

                TN_Utils.CalculateYourTime(YearWork, DateTime.Now, out int Plus);
                if (Plus - 5 >= 0)
                {
                    Plus = Plus / 5;
                }
                else
                {
                    Plus = 0;
                }

                Total += Plus + 12;

                string messg = "Số phép đắt đầu năm " + YearView + " !. <br />";
                messg += " 12 tiêu chuẩn, " + Plus + " thâm niên !. <br />";
                messg += " Số phép tích lũy tính đến hết năm " + YearView + " là " + Total + " !. <br />";

                zList.Add(new Leave_Item()
                {
                    EmployeeID = ID,
                    EmployeeKey = Employee,
                    EmployeeName = Name,
                    PositionName = Posittion,
                    DepartmentName = Department,
                    Sort = YearView,
                    BeginYear = YearView,
                    BeginNo = "12",
                    BeginPlus = Plus.ToString(),
                    FromDate = "",
                    ToDate = "",
                    TotalDate = "",
                    CloseEnd = CloseIn.ToString(),
                    CloseYear = YearView,
                    Incremental = Total.ToString(),
                    CategoryName = "",
                    Description = messg,
                });

            }
            return zList;
        }

        public static List<Employee_Model> Ready_Payroll(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
dbo.AB_HDLD_ID(A.EmployeeKey) AS HDLD,
dbo.AB_LuongTT(A.EmployeeKey) AS LTT, 
D.*, B.[Rank] 
FROM HRM_Payroll_Close_Detail A
LEFT JOIN HRM_Employee D ON A.EmployeeKey = D.EmployeeKey
LEFT JOIN HRM_ListPosition B ON D.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON D.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus <> 99
AND D.PartnerNumber = @PartnerNumber
AND D.WorkingStatusKey <> 2
AND A.ParentKey = @Parent
ORDER BY D.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent.ToUpper();
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    HDLD = r["HDLD"].ToString(),
                    LuongTT = r["LTT"].ToDouble().ToString("n0"),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    Style = r["Style"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable Report_BHXH(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_BHXH_V2";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            if (zTable.Rows.Count > 0)
            {
                zTable.Columns[0].ColumnName = "NỘI DUNG";
                DataColumn Col = zTable.Columns.Add("TOÀN CÔNG TY", typeof(double));
                Col.SetOrdinal(1);

                foreach (DataRow r in zTable.Rows)
                {
                    double Total = 0;
                    for (int i = 1; i < zTable.Columns.Count - 1; i++)
                    {
                        Total += r[i].ToDouble();
                    }
                    r[1] = Total;
                }

            }

            return zTable;
        }


        public static List<Payroll_Item> PayrollData(string PartnerNumber, string Parent, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, dbo.Get_PhotoPath(EmployeeKey) AS PhotoPath FROM HRM_Payroll_Index WHERE PartnerNumber = @PartnerNumber AND ParentKey = @ParentKey";
            if (BranchKey != string.Empty)
                zSQL += " AND BranchKey = @BranchKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Payroll_Item> zList = new List<Payroll_Item>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Payroll_Item()
                {
                    PhotoPath = r["PhotoPath"].ToString(),
                    Style = r["Style"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeName = r["EmployeeName"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    Branch = r["BranchName"].ToString(),
                    ItemKey = r["ItemKey"].ToString(),
                    ItemID = r["ItemID"].ToString(),
                    ItemName = r["ItemName"].ToString(),
                    Param = r["Param"].ToString(),
                    Formula = r["Formula"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    Amount = r["Amount"].ToString(),
                });
            }

            return zList;
        }

        public static string BranchName(string Key)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.AB_GetBranchName(@BranchKey)";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Key;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static string GetPhoto(string Employee)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT PhotoPath FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Employee;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static DataTable Report_Index(string PartnerNumber, string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"TONGHOP_INDEX";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            if (zTable.Rows.Count > 0)
            {
                zTable.Columns[0].ColumnName = "NỘI DUNG";
                DataColumn Col = zTable.Columns.Add("0", typeof(double));
                Col.SetOrdinal(1);

                foreach (DataRow r in zTable.Rows)
                {
                    double Total = 0;
                    for (int i = 1; i < zTable.Columns.Count - 1; i++)
                    {
                        Total += r[i].ToDouble();
                    }
                    r[1] = Total;
                }
            }
            return zTable;
        }
        public static DataTable Report_Index_Branch(string PartnerNumber, string Parent, string BranchKey)
        {
            DataTable zTable = new DataTable();
            string zSQL;
            if (BranchKey != string.Empty)
                zSQL = "TONGHOP_INDEX_Branch";
            else
                zSQL = "TONGHOP_INDEX_AllBranch";

            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@KEY", SqlDbType.NVarChar).Value = Parent;
                if (BranchKey != string.Empty)
                    zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static List<Employee_Model> List_Seniority(string PartnerNumber,string Employee, int Year )
        {
            DateTime zDate = new DateTime(Year, 12, 31, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey,D.BranchName,A.EmployeeKey,A.EmployeeID,A.LastName,A.FirstName,B.PositionNameVN AS PositionName,A.Gender,A.Birthday,A.StartingDate,
[dbo].[TinhSoNgayThamNien] (A.StartingDate,@Year) AS Seniority
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
LEFT JOIN HRM_Branch D ON A.BranchKey = D.BranchKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber= @PartnerNumber ";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY D.BranchName DESC, A.EmployeeID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Year", SqlDbType.DateTime).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    Note = r["Seniority"].ToString(),
                });
            }

            return zList;
        }
        public static DataTable Table_Seniority(string PartnerNumber, string Employee, int Year)
        {
            DateTime zDate = new DateTime(Year, 12, 31, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.BranchKey,D.BranchName,A.EmployeeKey,A.EmployeeID,A.LastName,A.FirstName,B.PositionNameVN AS PositionName,A.Gender,A.Birthday,A.StartingDate,
[dbo].[TinhSoNgayThamNien] (A.StartingDate,@Year) AS Seniority
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
LEFT JOIN HRM_Branch D ON A.BranchKey = D.BranchKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber= @PartnerNumber ";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY D.BranchName DESC, A.EmployeeID";
            string zConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@Year", SqlDbType.DateTime).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zTable;
        }
    }
}