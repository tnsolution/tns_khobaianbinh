﻿using ABG.Customer;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using CRMData = ABG.Models.Customer.Get;

namespace ABG.Controllers
{
    public class CustomerController : BaseController
    {
        [DisplayName("Truy cập bản đồ kho")]
        //Action On MAP
        public ActionResult Map(string ProductKey = "")
        {
            ViewBag.SelectLand = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.SelectCustomer = Customer_Data.List(Helper.PartnerNumber);

            if (ProductKey == string.Empty)
            {
                ProductKey = UserLog.DataAccess.Split(',')[0].Replace("'", "");
            }
            DataTable zTable = Product_Land_Data.GetLand(Helper.PartnerNumber, ProductKey);

            string[] zData = GetScripMap(zTable);
            ViewBag.StringMap = zData[0];
            ViewBag.Type1 = zData[1];   //đang sử dụng      XANH
            ViewBag.Type2 = zData[2];   //sắp đến hạn       VÀNG
            ViewBag.Type3 = zData[3];   //trống                 ĐỎ
            ViewBag.Key = ProductKey;

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Truy cập bản đồ kho ", ProductKey);
            //-------------------------------------------------------------------------------------------------

            return View();
        }
        [DisplayName("Tìm kiếm bản đồ kho")]
        public ActionResult MapSearch(string ProductKey = "", string CustomerKey = "", string ContractID = "")
        {
            ViewBag.SelectLand = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.SelectCustomer = Customer_Data.List(Helper.PartnerNumber);

            DataTable zTable = Product_Land_Data.SearchAsset(Helper.PartnerNumber, CustomerKey, ContractID);
            if (zTable.Rows.Count <= 0)
            {
                ViewBag.Message = "Khách hàng hiện không có thông tin hợp đồng nào hiện hữu với khách hàng và khu vực đang chọn, vui lòng kiểm tra lại !.";
            }
            else
            {
                string[] zData = GetScripMap(zTable);
                ViewBag.StringMap = zData[0];
                ViewBag.Type1 = zData[1];   //đang sử dụng      XANH
                ViewBag.Type2 = zData[2];   //sắp đến hạn       VÀNG
                ViewBag.Type3 = zData[3];   //trống                 ĐỎ
                ViewBag.Key = ProductKey;
            }
            return View("~/Views/Customer/Map.cshtml");
        }

        #region [--ACTION-- LOGGED]
        [HttpGet]
        public JsonResult ContractCheck(string ContractID, string SubContract)
        {
            ServerResult zResult = new ServerResult();
            string Exists = "";
            string Message = "";
            if (ContractID.Trim().Length > 0 &&
                SubContract.Trim().Length > 0)
            {
                Exists = Customer_Data.CheckContractD(Helper.PartnerNumber, ContractID, SubContract, out Message);
            }
            else
            {
                if (ContractID.Length >= 0)
                {
                    Exists = Customer_Data.CheckContractD(Helper.PartnerNumber, ContractID, out Message);
                }
            }

            if (TN_Utils.ValidateGuid(Exists))
            {
                zResult.Data = Exists;
                zResult.Success = true;
            }
            else
            {
                if (Message != string.Empty)
                {
                    zResult.Message = Message.GetFirstLine();
                    zResult.Success = false;
                }
                else
                {
                    zResult.Data = "";
                    zResult.Success = true;
                }
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [DisplayName("Cập nhật hợp đồng")]
        public JsonResult ContractNew(string ContractKey, string ContractID, string SubContract, string DateSign, string BuyerKey, string BuyerName, string RegionKey)
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);

            zInfo.Contract.PartnerNumber = Helper.PartnerNumber;
            zInfo.Contract.ContractID = ContractID.Trim();
            zInfo.Contract.SubContract = SubContract.Trim();
            DateTime.TryParseExact(DateSign, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDateSign);

            zInfo.Contract.DateSign = zDateSign;
            zInfo.Contract.BuyerName = BuyerName.Trim();
            zInfo.Contract.RegionKey = RegionKey.Trim();

            //kiem tra trường hợp khách hàng mới hoặc cũ
            if (BuyerKey != "newid")
            {
                zInfo.Contract.BuyerKey = BuyerKey.Trim();

                //get customer info
                Customer_Info zCus = new Customer_Info(BuyerKey.Trim());
                zInfo.Contract.BuyerAddress = zCus.Customer.Address;
                zInfo.Contract.BuyerBankCode = zCus.Customer.BankAccount;
                zInfo.Contract.BuyerAddressBank = zCus.Customer.BankName;
                zInfo.Contract.BuyerPhone = zCus.Customer.Phone;
                zInfo.Contract.BuyerTaxCode = zCus.Customer.TaxNumber;
            }
            else
            {
                //create new Key
                string CustomerKey = Guid.NewGuid().ToString();
                Customer_Info zCus = new Customer_Info();
                zCus.Customer.FullName = BuyerName;
                zCus.Customer.CustomerKey = CustomerKey;
                zCus.Customer.CreatedBy = UserLog.UserKey;
                zCus.Customer.CreatedName = UserLog.EmployeeName;
                zCus.Customer.ModifiedBy = UserLog.UserKey;
                zCus.Customer.ModifiedName = UserLog.EmployeeName;
                zCus.Customer.PartnerNumber = Helper.PartnerNumber;
                zCus.Create_ClientKey();

                zInfo.Contract.BuyerKey = CustomerKey;
            }

            zInfo.Contract.CreatedBy = UserLog.UserKey;
            zInfo.Contract.CreatedName = UserLog.EmployeeName;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;

            if (ContractKey == "")
            {
                zInfo.Contract.ContractKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Cập nhật hợp đồng ", "", ContractID.Trim());
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                zResult.Message = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ContractEditQuick(string ContractKey)
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            Contract_Model zModel = zInfo.Contract;

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                zResult.Data = JsonConvert.SerializeObject(zModel);
                zResult.Success = true;
                zResult.Message = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [DisplayName("Mở điều chỉnh hợp đồng")]
        public ActionResult ContractEdit(string Key)
        {
            Contract_Info zInfo = new Contract_Info(Key);
            Contract_Model zModel = zInfo.Contract;
            ViewBag.SelectCustomer = Customer_Data.List(Helper.PartnerNumber);
            ViewBag.SelectLand = Product_Land_Data.ListAsset(Helper.PartnerNumber, zInfo.Contract.RegionKey);
            ViewBag.ListItem = Contract_Land_Data.List(Helper.PartnerNumber, Key);
            ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, Key);

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Mở điều chỉnh hợp đồng ", "", zModel.ContractID.Trim());
            //-------------------------------------------------------------------------------------------------

            return View("~/Views/Customer/Contract/ContractEdit.cshtml", zModel);
        }
        [HttpPost]
        [DisplayName("Xóa hợp đồng")]
        public JsonResult ContractDelete(string ContractKey)
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info();
            zInfo.Contract.ContractKey = ContractKey;
            zInfo.Delete();
            Contract_Model zModel = zInfo.Contract;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Xóa hợp đồng ", "", zModel.ContractID.Trim());
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Thanh lý hợp đồng")]
        public JsonResult ContractEnd(string ContractKey, string DateTerminate, string ReasonTerminate)
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info();
            zInfo.Contract.Liquidated = 1;
            zInfo.Contract.ContractKey = ContractKey;
            zInfo.Contract.ReasonLiquidation = ReasonTerminate.Trim();
            DateTime zDate = DateTime.MinValue;
            DateTime.TryParseExact(DateTerminate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDate);
            zInfo.Contract.DateLiquidation = zDate;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;
            zInfo.Terminate();

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Thanh lý hợp đồng ", "", zInfo.Contract.ContractID.Trim());
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                zResult.Message = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Sao chép hợp đồng")]
        public JsonResult ContractCopy(string ContractKey, string ContractID, string ContractSub, string ContractSign)
        {
            string NewKey = Guid.NewGuid().ToString();
            ServerResult zResult = new ServerResult();
            Contract_Info zInfoOld = new Contract_Info(ContractKey);
            Contract_Info zInfoNew = new Contract_Info();

            DateTime zContractSign = DateTime.MinValue;
            DateTime.TryParseExact(ContractSign, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zContractSign);

            zInfoNew.Contract = zInfoOld.Contract;
            zInfoNew.Contract.DateSign = zContractSign;
            zInfoNew.Contract.ContractKey = NewKey;
            zInfoNew.Contract.SubContract = ContractSub;
            zInfoNew.Contract.ContractID = ContractID;
            zInfoNew.Contract.PartnerNumber = Helper.PartnerNumber;
            zInfoNew.Contract.CreatedBy = UserLog.UserKey;
            zInfoNew.Contract.CreatedName = UserLog.EmployeeName;
            zInfoNew.Contract.ModifiedBy = UserLog.UserKey;
            zInfoNew.Contract.ModifiedName = UserLog.EmployeeName;

            zInfoNew.Create_ClientKey();
            if (zInfoNew.Contract.Code == "200" ||
               zInfoNew.Contract.Code == "201")
            {
                List<Contract_Land_Model> zList = Contract_Land_Data.List(Helper.PartnerNumber, ContractKey);
                foreach (Contract_Land_Model zItem in zList)
                {
                    zItem.ContractKey = NewKey;
                    zItem.PartnerNumber = Helper.PartnerNumber;

                    Contract_Land_Info zItemInfo = new Contract_Land_Info();
                    zItemInfo.Contract_Land = zItem;
                    zItemInfo.Create_ServerKey();
                }

                zResult.Success = true;
                zResult.Data = NewKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfoNew.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult CustomerGetInfo(string CustomerKey)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            Customer_Model zModel = zInfo.Customer;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Customer.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Cập nhật hợp đồng")]
        public JsonResult ContractSave(string ContractKey, string ContractID, string SubContract, string FromDate, string ToDate,
            string DateSign, string BuyerKey, string BuyerName, string BuyerAddress, string BuyerPhone,
            string BuyerTaxCode, string BuyerBankCode, string BuyerAddressBank, string BuyerRepresent,
            string BuyerPosition, string Purpose, string Description, string chkStyle = "0")
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            zInfo.Contract.Style = chkStyle;
            zInfo.Contract.PartnerNumber = Helper.PartnerNumber;
            zInfo.Contract.ContractKey = ContractKey;
            zInfo.Contract.ContractID = ContractID.Trim();
            zInfo.Contract.SubContract = SubContract.Trim();
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            zInfo.Contract.FromDate = zFromDate;
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            zInfo.Contract.ToDate = zToDate;
            DateTime zDateSign = DateTime.MinValue;
            DateTime.TryParseExact(DateSign, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateSign);

            //kiem tra trường hợp khách hàng mới hoặc cũ
            if (BuyerKey != "newid")
            {
                zInfo.Contract.BuyerKey = BuyerKey.Trim();

                //get customer info
                Customer_Info zCus = new Customer_Info(BuyerKey.Trim());
                zInfo.Contract.BuyerAddress = zCus.Customer.Address;
                zInfo.Contract.BuyerBankCode = zCus.Customer.BankAccount;
                zInfo.Contract.BuyerAddressBank = zCus.Customer.BankName;
                zInfo.Contract.BuyerPhone = zCus.Customer.Phone;
                zInfo.Contract.BuyerTaxCode = zCus.Customer.TaxNumber;
            }
            else
            {
                //create new Key
                string CustomerKey = Guid.NewGuid().ToString();
                Customer_Info zCus = new Customer_Info();
                zCus.Customer.CustomerID = BuyerTaxCode.Trim();
                zCus.Customer.Phone = BuyerPhone.Trim();
                zCus.Customer.Address = BuyerAddress.Trim();
                zCus.Customer.BankAccount = BuyerBankCode.Trim();
                zCus.Customer.BankName = BuyerAddressBank.Trim();
                zCus.Customer.FullName = BuyerName;
                zCus.Customer.CustomerKey = CustomerKey;
                zCus.Customer.CreatedBy = UserLog.UserKey;
                zCus.Customer.CreatedName = UserLog.EmployeeName;
                zCus.Customer.ModifiedBy = UserLog.UserKey;
                zCus.Customer.ModifiedName = UserLog.EmployeeName;
                zCus.Customer.PartnerNumber = Helper.PartnerNumber;
                zCus.Create_ClientKey();

                zInfo.Contract.BuyerKey = CustomerKey;
            }

            zInfo.Contract.DateSign = zDateSign;

            zInfo.Contract.BuyerName = BuyerName.Trim();
            zInfo.Contract.BuyerAddress = BuyerAddress.Trim();
            zInfo.Contract.BuyerPhone = BuyerPhone.Trim();
            zInfo.Contract.BuyerTaxCode = BuyerTaxCode.Trim();
            zInfo.Contract.BuyerBankCode = BuyerBankCode.Trim();
            zInfo.Contract.BuyerAddressBank = BuyerAddressBank.Trim();
            zInfo.Contract.BuyerRepresent = BuyerRepresent.Trim();
            zInfo.Contract.BuyerPosition = BuyerPosition.Trim();
            zInfo.Contract.Purpose = Purpose;
            zInfo.Contract.Description = Description;
            zInfo.Contract.CreatedBy = UserLog.UserKey;
            zInfo.Contract.CreatedName = UserLog.EmployeeName;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;
            if (ContractKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }
            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {

                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Cập nhật hợp đồng ", "", ContractID.Trim());
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                zResult.Message = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ContractItemSave(string ContractKey = "", string LandItemKey = "", string ItemKey = "", string ItemName = "", int UnitKey = 0, string UnitName = "", string Area = "0", string Price = "0", string VAT_Percent = "0", string TotalCurrencyMain = "0", string SubTotal = "0", string ItemNote = "")
        {
            ServerResult zResult = new ServerResult();
            Contract_Land_Info zInfo = new Contract_Land_Info(LandItemKey);
            zInfo.Contract_Land.PartnerNumber = Helper.PartnerNumber;
            zInfo.Contract_Land.ContractKey = ContractKey.Trim();
            zInfo.Contract_Land.LandItemKey = LandItemKey.Trim();
            zInfo.Contract_Land.ItemKey = ItemKey.Trim();
            zInfo.Contract_Land.ItemName = ItemName.Trim();
            zInfo.Contract_Land.UnitKey = UnitKey;
            zInfo.Contract_Land.UnitName = UnitName;
            zInfo.Contract_Land.Area = Helper.GetDouble(Area);
            zInfo.Contract_Land.VAT_Percent = VAT_Percent.ToFloat();
            zInfo.Contract_Land.Price = Helper.GetDouble(Price);
            zInfo.Contract_Land.SubTotal = Helper.GetDouble(SubTotal);
            zInfo.Contract_Land.TotalCurrencyMain = Helper.GetDouble(TotalCurrencyMain);
            zInfo.Contract_Land.Description = ItemNote.Trim();
            zInfo.Contract_Land.CreatedBy = UserLog.UserKey;
            zInfo.Contract_Land.CreatedName = UserLog.EmployeeName;
            zInfo.Contract_Land.ModifiedBy = UserLog.UserKey;
            zInfo.Contract_Land.ModifiedName = UserLog.EmployeeName;
            if (LandItemKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }
            if (zInfo.Contract_Land.Code == "200" ||
                zInfo.Contract_Land.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ContractItemDetail(string LandItemKey)
        {
            ServerResult zResult = new ServerResult();
            Contract_Land_Info zInfo = new Contract_Land_Info(LandItemKey);
            Contract_Land_Model zModel = zInfo.Contract_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ContractItemDelete(string LandItemKey)
        {
            ServerResult zResult = new ServerResult();
            Contract_Land_Info zInfo = new Contract_Land_Info();
            zInfo.Contract_Land.LandItemKey = LandItemKey;
            zInfo.Delete();
            Contract_Land_Model zModel = zInfo.Contract_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContractItemList(string ParentKey)
        {
            ViewBag.ListItem = Contract_Land_Data.List(Helper.PartnerNumber, ParentKey);
            return View("~/Views/Customer/Contract/ContractItem.cshtml");
        }

        public ActionResult ContractFileList(string ContractKey)
        {
            ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, ContractKey);
            return View("~/Views/Shared/_DocumentAttach.cshtml");
        }
        [HttpPost]
        public JsonResult ContractFileDelete(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info(AutoKey);
            zInfo.Delete();

            if (zInfo.Document.Code == "200" ||
                zInfo.Document.Code == "201")
            {
                string zFilePath = zInfo.Document.FilePath;
                string zFileName = zInfo.Document.FileName;
                string zServerPath = Server.MapPath(zFilePath);
                string zFile = Path.Combine(zServerPath, zFileName);

                if (System.IO.File.Exists(zFile))
                {
                    System.IO.File.Delete(zFile);
                }

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Document.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        //upload file
        [HttpPost]
        public JsonResult UploadFile(string id)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string fileName = "";
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        string strGui = Guid.NewGuid().ToString();
                        fileName = strGui + Path.GetExtension(fileContent.FileName);

                        #region[Upload]   
                        string zFilePath = "/FileUpload/" + Helper.PartnerNumber + "/CustomerContract/" + id + "/";
                        string ServerPath = Server.MapPath(zFilePath);
                        string zFileSave = Path.Combine(ServerPath, fileName);

                        // Check Foder
                        DirectoryInfo zDir = new DirectoryInfo(ServerPath);
                        if (!zDir.Exists)
                        {
                            zDir.Create();
                        }
                        else
                        {
                            if (System.IO.File.Exists(zFileSave))
                            {
                                System.IO.File.Delete(zFileSave);
                            }
                        }

                        fileContent.SaveAs(zFileSave);
                        #endregion

                        Document_Info zInfo = new Document_Info();
                        zInfo.Document.TableJoin = "CRM_Contract";
                        zInfo.Document.TableKey = id;
                        zInfo.Document.PartnerNumber = Helper.PartnerNumber;
                        zInfo.Document.FileExt = Path.GetExtension(fileContent.FileName);
                        zInfo.Document.FileName = fileContent.FileName;
                        zInfo.Document.FilePath = Path.Combine(zFilePath, fileName);
                        zInfo.Create_ServerKey();
                    }
                }

                zResult.Success = true;

            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region //  LIST CONTRACT -- ContractList.cshtml -- _ContractView.cshtml -- _ContractSub.cshtml -- ListFile.cshtml -- ListItem.cshtml   -- SubView    //
        /// <summary>
        /// DANH SÁCH
        /// </summary>
        /// <returns></returns>
        [DisplayName("Xem danh sách hơp đồng")]
        public ActionResult ContractList(string ProductKey = "")
        {
            if (ProductKey == string.Empty)
            {
                ProductKey = UserLog.DataAccess.Split(',')[0].Replace("'", "");
            }

            ViewBag.SelectLand = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.SelectCustomer = Customer_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = CRMData.Search(Helper.PartnerNumber, ProductKey, "", "");
            ViewBag.SelectDefault = ProductKey;

            return View("~/Views/Customer/Contract/ContractList.cshtml");
        }
        /// <summary>
        /// KẾT QUẢ TÌM KIẾM
        /// </summary>
        /// <param name="Buyer"></param>
        /// <param name="ContractID"></param>
        /// <param name="Area"></param>
        /// <param name="DateSign"></param>
        /// <returns></returns>
        public ActionResult ContractSearch(string Buyer = "", string ContractID = "", string Area = "", DateTime? DateSign = null)
        {
            ViewBag.SelectLand = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.SelectCustomer = Customer_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = CRMData.Search(Helper.PartnerNumber, Area, Buyer, ContractID);

            return View("~/Views/Customer/Contract/ContractList.cshtml");
        }
        [DisplayName("Xem chi tiết hơp đồng (bản đồ)")]
        public ActionResult ContractView(string ContractKey)
        {
            Contract_Info zInfo = new Contract_Info(ContractKey);
            Contract_Object zObj = new Contract_Object();
            zObj.Contract = zInfo.Contract;
            zObj.ListItem = Contract_Land_Data.List(Helper.PartnerNumber, ContractKey);
            zObj.ListFile = Document_Data.List(Helper.PartnerNumber, ContractKey);
            return View("~/Views/Customer/Contract/_ContractView.cshtml", zObj);
        }
        public ActionResult ContractSubView(string ContractID)
        {
            ViewBag.ListData = CRMData.Contract(Helper.PartnerNumber, ContractID);
            return PartialView("~/Views/Customer/Contract/_ContractSub.cshtml");
        }
        #endregion

        #region [Activity - LOGGED]
        [DisplayName("Ghi thêm nhật ký")]
        public JsonResult AddDiary(string Asset)
        {
            Product_Diary_Model zModel = JsonConvert.DeserializeObject<Product_Diary_Model>(Asset, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
            ServerResult zResult = new ServerResult();
            Product_Diary_Info zInfo = new Product_Diary_Info();
            zModel.PhotoPath = Helper.StoreFilePostArray(Request.Files, "Diary", zModel.AutoKey.ToString());
            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.DateWrite = new DateTime(
                zModel.DateWrite.Year,
                zModel.DateWrite.Month,
                zModel.DateWrite.Day,
                DateTime.Now.Hour,
                DateTime.Now.Minute,
                DateTime.Now.Second);

            //***********lưu cho nhật ký tổng
            if (zModel.AssetKey == string.Empty &&
                zModel.AreaKey != string.Empty)
            {
                zModel.AssetKey = zModel.AreaKey;
            }

            Employee_Info zEmp = new Employee_Info(UserLog.EmployeeKey);
            zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
            zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
            zModel.BranchKey = zEmp.Employee.BranchKey;
            zModel.BranchName = zEmp.Employee.BranchName;
            zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
            zModel.DepartmentName = zEmp.Employee.DepartmentName;

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            zInfo.Product_Diary = zModel;
            zInfo.Create_ServerKey();

            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Ghi thêm nhật ký ", zModel.AssetKey, zModel.Description);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zModel.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [DisplayName("Truy cập nhật ký kho")]
        public ActionResult LogDiary(string ProductKey)
        {
            ViewBag.ListData = Product_Diary_Data.List(Helper.PartnerNumber, ProductKey);
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Truy cập nhật ký kho ", ProductKey);
            //-------------------------------------------------------------------------------------------------
            return PartialView("~/Views/Customer/Activity/_LogDiary.cshtml");
        }
        [HttpPost]
        [DisplayName("Xóa nhật ký kho")]
        public JsonResult DelDiary(int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Product_Diary_Info zInfo = new Product_Diary_Info(AutoKey);
            zInfo.Delete();

            Product_Diary_Model zModel = zInfo.Product_Diary;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Xóa nhật ký kho ", zModel.AssetKey, zModel.Description);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zModel.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [DisplayName("Xem hoạt động kho (bản đồ)")]
        public ActionResult General(string ProductKey)
        {
            ViewBag.ListFeature = Product_Feature_Data.ListFeature(Helper.PartnerNumber, ProductKey);
            ViewBag.ListEquipment = Product_Equipment_Data.ListEquipment(Helper.PartnerNumber, ProductKey);

            #region [Info Contract]
            DataTable zTable = Product_Land_Data.GetAsset(Helper.PartnerNumber, ProductKey);
            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                DataRow r = zTable.Rows[0];
                string zHopDong = "";
                string zTinhTrang = "";
                int zNgayConLai = 0;
                int zLoai = 0;

                if (r["SubContract"] != DBNull.Value &&
                    r["SubContract"].ToString().Length > 0)
                {
                    zHopDong = r["SubContract"].ToString() + " của " + r["ContractID"].ToString() + " ngày ký " + r["DateSign"].ToDateString();
                }
                else
                {
                    zHopDong = r["ContractID"].ToString() + " ngày ký " + r["DateSign"].ToDateString();
                }

                if (r["FromDate"] != DBNull.Value &&
                    r["ToDate"] != DBNull.Value)
                {
                    TimeSpan Time = Convert.ToDateTime(r["ToDate"]) - DateTime.Now;
                    //có ngày mà đã hết hạn
                    if (Convert.ToDateTime(r["ToDate"]) < DateTime.Now)
                    {
                        zTinhTrang = "Kho trống";
                        zLoai = 0;
                    }
                    else if (Convert.ToDateTime(r["ToDate"]) >= DateTime.Now && Time.Days <= 30)
                    {
                        zTinhTrang = "Sắp hếp hạn";
                        zNgayConLai = Time.Days;
                        zLoai = 1;
                    }
                    else
                    {
                        zTinhTrang = "Đang sử dụng";
                        zNgayConLai = Time.Days;
                        zLoai = 2;
                    }
                }
                else
                {
                    zTinhTrang = "Kho còn trống";
                    zLoai = 0;
                }

                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Hợp đông số:</td><td>" + zHopDong + "</td>");
                zSb.AppendLine("</tr>");

                if (zLoai != 0)
                {
                    zSb.AppendLine("<td>Thời gian thuê:</td><td>" + Convert.ToDateTime(r["FromDate"]).ToString("dd/MM/yyyy") + "-" + Convert.ToDateTime(r["ToDate"]).ToString("dd/MM/yyyy") + "</td>");

                }
                else
                {
                    zSb.AppendLine("<td>Thời gian thuê:</td><td>Chưa cập nhật</td>");
                }

                if (r["Liquidated"].ToInt() > 0)
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("    <td style='width:125px;'>Tình trạng:</td><td>" + r["ReasonLiquidation"].ToString() + "-" + Convert.ToDateTime(r["DateLiquidation"]).ToString("dd/MM/yyyy") + "</td>");
                    zSb.AppendLine("</tr>");
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("    <td style='width:125px;'>Tình trạng:</td><td>" + zTinhTrang + " còn lại " + zNgayConLai + " Ngày </td>");
                    zSb.AppendLine("</tr>");
                }

                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Khách hàng:</td><td>" + r["BuyerName"].ToString() + " </td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Mục đích thuê:</td><td>" + r["Purpose"].ToString() + "</td>");
                zSb.AppendLine("</tr>");

                ViewBag.ProductKey = ProductKey;
                ViewBag.RegionKey = r["RegionKey"].ToString();
                ViewBag.ContractKey = r["ContractKey"].ToString();
                ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, r["ContractKey"].ToString());
            }
            ViewBag.ContractRaw = zSb.ToString();
            #endregion

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem hoạt động kho (bản đồ) ", ProductKey);
            //-------------------------------------------------------------------------------------------------

            return PartialView("~/Views/Customer/Activity/General.cshtml");
        }
        [DisplayName("Xem lịch sử thuê (bản đồ)")]
        public ActionResult History(string ProductKey)
        {
            ViewBag.Table = Customer_Data.HistoryContract(Helper.PartnerNumber, ProductKey);
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem lịch sử thuê (bản đồ) ", ProductKey);
            //-------------------------------------------------------------------------------------------------
            return PartialView("~/Views/Customer/Activity/History.cshtml");
        }
        public ActionResult Maintain(string ProductKey)
        {
            ViewBag.ProductKey = ProductKey;
            return PartialView("~/Views/Customer/Activity/Maintain.cshtml");
        }
        [DisplayName("Xem thông tin kỹ thuật xe nâng")]
        public ActionResult FileTech(string ProductKey)
        {
            ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, ProductKey);
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem thông tin kỹ thuật xe nâng ");
            //-------------------------------------------------------------------------------------------------
            return PartialView("~/Views/Customer/Activity/Technical.cshtml");
        }
        #endregion

        //~/Views/Customer/Contract
        #region[Contract]
        #region [Trang hợp đồng cập nhật]
        /// <summary>
        ///  kết quả tìm kiếm
        /// </summary>
        /// <param name="SearchArea"></param>
        /// <param name="SearchBuyer"></param>
        /// <param name="SearchName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ContractListSearch(string SearchArea = "", string SearchBuyer = "", string SearchName = "")
        {
            //ViewBag.ListCustomerSearch = Customer_Data.List_Search(Helper.PartnerNumber, SearchArea, SearchBuyer, SearchName);
            ViewBag.ListSelectLand = Product_Land_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectCustomer = Customer_Data.List(Helper.PartnerNumber);
            return View("~/Views/Customer/Contract/ContractList.cshtml");
        }
        public ActionResult ContractInfo(string CustomerKey)
        {
            ViewBag.ListData = Customer_Data.ListContract(Helper.PartnerNumber, CustomerKey);
            ViewBag.CustomerKey = CustomerKey;
            return PartialView("~/Views/Shared/_ContractCustomer.cshtml");
        }

        #endregion
        #endregion

        #region [Info Contract on Map]
        [HttpPost]
        public ActionResult Load_Fea(string ProductKey)
        {
            DataTable zTable = Product_Feature_Data.ListFea(Helper.PartnerNumber, ProductKey);
            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<tr FeatureKey = " + r["FeatureKey"].ToString() + ">");
                    zSb.AppendLine("    <td>" + r["Name"].ToString() + "</td>");
                    zSb.AppendLine("</tr>");
                }
            }
            return Json(new { message = zSb.ToString() });
        }
        [HttpPost]
        public ActionResult Load_Equ(string ProductKey)
        {
            DataTable zTable = Product_Equipment_Data.ListEqu(Helper.PartnerNumber, ProductKey);
            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<tr AutoKey = " + r["AutoKey"].ToString() + ">");
                    zSb.AppendLine("<td>" + r["ItemName"].ToString() + "</td>");
                    zSb.AppendLine("</tr>");
                }
            }
            return Json(new { message = zSb.ToString() });
        }
        [HttpPost]
        public ActionResult Load_Info(string ProductKey)
        {
            DataTable zTable = Product_Land_Data.GetAsset(Helper.PartnerNumber, ProductKey);
            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                DataRow r = zTable.Rows[0];

                string zTinhTrang = "";
                int zNgayConLai = 0;
                int zLoai = 0;
                if (r["FromDate"] != DBNull.Value &&
                        r["ToDate"] != DBNull.Value)
                {
                    TimeSpan Time = Convert.ToDateTime(r["DateLiquidation"]) - DateTime.Now;
                    //có ngày mà đã hết hạn
                    if (Convert.ToDateTime(r["DateLiquidation"]) < DateTime.Now)
                    {
                        zTinhTrang = "Kho trống";
                        zLoai = 0;
                    }
                    else if (Convert.ToDateTime(r["DateLiquidation"]) >= DateTime.Now && Time.Days <= 30)
                    {
                        zTinhTrang = "Sắp hếp hạn";
                        zNgayConLai = Time.Days;
                        zLoai = 1;
                    }
                    else
                    {
                        zTinhTrang = "Đang sử dụng";
                        zNgayConLai = Time.Days;
                        zLoai = 2;
                    }
                }
                else
                {
                    zTinhTrang = "Kho còn trống";
                    zLoai = 0;
                }

                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td style='width:125px;'>Tình trạng:</td><td>" + zTinhTrang + "</td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                if (zLoai != 0)
                {
                    zSb.AppendLine("<td>Thời gian thuê:</td><td>Từ " + Convert.ToDateTime(r["FromDate"]).ToString("dd/MM/yyyy") + " đến " + Convert.ToDateTime(r["DateLiquidation"]).ToString("dd/MM/yyyy") + "</td>");
                }
                else
                {
                    zSb.AppendLine("<td>Thời gian thuê:</td><td></td>");
                }

                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Còn lại:</td><td>" + zNgayConLai + " Ngày </td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Hợp đông số:</td><td>" + r["ContractID"] + "</td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Khách hàng:</td><td>" + r["BuyerName"] + " </td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr>");
                zSb.AppendLine("    <td>Mục đích thuê:</td><td>" + r["Purpose"] + "</td>");
                zSb.AppendLine("</tr>");
            }

            return Json(new { message = zSb.ToString() });
        }
        [HttpPost]
        public ActionResult Load_Map(string ProductKey)
        {
            DataTable zTable = Product_Land_Data.GetLand(Helper.PartnerNumber, ProductKey);
            return Json(new { message = GetScripMap(zTable) });
        }
        #endregion

        #region[Customer]
        [DisplayName("Xem danh sách khách hàng")]
        public ActionResult CustomerList()
        {
            ViewBag.ListData = Customer_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectCategory = Customer_Data.ListCategory(Helper.PartnerNumber);

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem danh sách khách hàng ");
            //-------------------------------------------------------------------------------------------------

            return View("~/Views/Customer/List.cshtml");
        }
        public ActionResult CustomerSearch(string SearchName)
        {
            ViewBag.ListData = Customer_Data.Search(Helper.PartnerNumber, SearchName);
            ViewBag.ListSelectCategory = Customer_Data.ListCategory(Helper.PartnerNumber);
            return View("~/Views/Customer/List.cshtml");
        }
        [HttpPost]
        [DisplayName("Cập nhật khách hàng")]
        public JsonResult SaveCustomer(
            string CustomerKey, string CustomerID, string FullName, string Aliases, string JobTitle,
            int CustomerType, string CustomerTypeName, string TaxNumber,
            string Address, string Email, string Phone, string Note, string IDCard, string Contact)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.Customer.PartnerNumber = Helper.PartnerNumber;
            zInfo.Customer.CustomerTypeName = CustomerTypeName.Trim();
            zInfo.Customer.CustomerType = CustomerType;
            zInfo.Customer.CustomerKey = CustomerKey;
            zInfo.Customer.CustomerID = CustomerID.Trim();
            zInfo.Customer.FullName = FullName.Trim();
            zInfo.Customer.Aliases = Aliases.Trim();
            zInfo.Customer.JobTitle = JobTitle.Trim();
            zInfo.Customer.TaxNumber = TaxNumber.Trim();
            zInfo.Customer.Address = Address.Trim();
            zInfo.Customer.Email = Email.Trim();
            zInfo.Customer.Phone = Phone.Trim();
            zInfo.Customer.Note = Note.Trim();
            zInfo.Customer.IDCard = IDCard.Trim();
            zInfo.Customer.Contact = Contact;
            zInfo.Customer.CreatedBy = UserLog.UserKey;
            zInfo.Customer.CreatedName = UserLog.EmployeeName;
            zInfo.Customer.ModifiedBy = UserLog.UserKey;
            zInfo.Customer.ModifiedName = UserLog.EmployeeName;

            if (CustomerKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Customer.Code == "200" ||
                zInfo.Customer.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Cập nhật khách hàng ", "", FullName);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                zResult.Message = zInfo.Customer.CustomerKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Customer.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailCustomer(string CustomerKey)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);

            Customer_Model zModel = zInfo.Customer;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Customer.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa khách hàng")]
        public JsonResult DeleteCustomer(string CustomerKey)
        {
            ServerResult zResult = new ServerResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            //zInfo.Customer.CustomerKey = CustomerKey;
            zInfo.Delete();
            Customer_Model zModel = zInfo.Customer;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {

                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Xoá khách hàng ", "", zInfo.Customer.FullName);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Customer.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        private string[] GetScripMap(DataTable zTable)
        {
            int Sudung = 0,    //sử dụng
                Denhan = 0,    //đến hạn
                Trong = 0;    //trống
            string[] zResult = new string[4];
            StringBuilder zSb = new StringBuilder();
            int n = 0;
            zSb.AppendLine("<script>");
            zSb.AppendLine("var _ObjLocation = { ");
            zSb.AppendLine("\"quantity\":@quantity,");
            zSb.AppendLine("\"Plot\": [");
            foreach (DataRow zRow in zTable.Rows)
            {
                string zLatLng = zRow["LngLat"].ToString();
                if (zLatLng.Trim().Length > 0)
                {
                    if (n > 0)
                    {
                        zSb.Append(",");
                    }

                    //Info
                    zSb.AppendLine("{");
                    zSb.AppendLine("\"asset\":\"" + zRow["ProductKey"].ToString() + "\",");
                    zSb.AppendLine("\"contractKey\":\"" + zRow["ContractKey"].ToString() + "\",");
                    zSb.AppendLine("\"customerKey\":\"" + zRow["BuyerKey"].ToString() + "\",");

                    zSb.AppendLine("\"area\":\"" + zRow["Area"].ToDouble() + "\",");
                    zSb.AppendLine("\"customer\":\"" + zRow["BuyerName"].ToString() + "\", ");
                    zSb.AppendLine("\"name\":\"" + zRow["ProductName"].ToString() + "\", ");
                    zSb.AppendLine("\"class\":\"" + zRow["Class"].ToString() + "\",");

                    if (zRow["Liquidated"].ToInt() == 1)
                    {
                        zSb.AppendLine("\"time\":\"" +
                            " Từ ngày " + Convert.ToDateTime(zRow["FromDate"]).ToString("dd/MM/yyyy") +
                            " đến ngày " + Convert.ToDateTime(zRow["ToDate"]).ToString("dd/MM/yyyy") +
                            " đã thanh lý " + Convert.ToDateTime(zRow["DateLiquidation"]).ToString("dd/MM/yyyy") +
                            "\",");
                        zSb.AppendLine("\"fillColor\":\"" + "#FF0000" + "\",");
                        Trong++;
                    }
                    else
                    {
                        //xem ngày
                        if (zRow["FromDate"] != DBNull.Value &&
                            zRow["ToDate"] != DBNull.Value)
                        {
                            TimeSpan Time = Convert.ToDateTime(zRow["ToDate"]) - DateTime.Now;

                            //có ngày mà đã hết hạn
                            if (Convert.ToDateTime(zRow["ToDate"]) < DateTime.Now)
                            {
                                zSb.AppendLine("\"time\":\"" + Convert.ToDateTime(zRow["ToDate"]).ToString("dd/MM/yyyy") + " Đã hết hạn." + "\",");
                                zSb.AppendLine("\"fillColor\":\"" + "#FF0000" + "\",");
                            }
                            else if (Convert.ToDateTime(zRow["ToDate"]) >= DateTime.Now && Time.Days <= 30)
                            {
                                zSb.AppendLine("\"time\":\""
                                    + " Từ ngày " + Convert.ToDateTime(zRow["FromDate"]).ToString("dd/MM/yyyy")
                                    + " đến ngày " + Convert.ToDateTime(zRow["ToDate"]).ToString("dd/MM/yyyy")
                                    + " Sắp hết hạn." + "\",");
                                zSb.AppendLine("\"fillColor\":\"" + "#FFFF00" + "\",");

                                Denhan++;
                            }
                            else
                            {
                                zSb.AppendLine("\"time\":\"" + " Từ ngày " + Convert.ToDateTime(zRow["FromDate"]).ToString("dd/MM/yyyy") + " đến ngày " + Convert.ToDateTime(zRow["ToDate"]).ToString("dd/MM/yyyy") + "\",");
                                zSb.AppendLine("\"fillColor\":\"" + "#009933" + "\",");

                                Sudung++;
                            }

                        }
                        else
                        {
                            if (zRow["CodeLine"].ToString() == "SH")
                            {
                                zSb.AppendLine("\"fillColor\":\"" + "#5565A5" + "\",");
                            }
                            else
                            {
                                zSb.AppendLine("\"time\":\"" + "Trống" + "\",");
                                zSb.AppendLine("\"fillColor\":\"" + "#FF0000" + "\",");
                                Trong++;
                            }
                        }
                    }

                    //LngLat
                    string strLatLng = zRow["LngLat"].ToString()
                       .Replace("\r\n", string.Empty)
                       .Replace("\n", string.Empty);
                    zSb.AppendLine("\"LatLng\" : \"" + strLatLng.Trim() + "\",");

                    //style
                    zSb.AppendLine("\"strokeColor\":\"" + "FF008000" + "\",");
                    zSb.AppendLine("\"strokeWeight\":" + "2" + ",");
                    zSb.AppendLine("\"strokeOpacity\":" + "0.5" + ",");
                    zSb.AppendLine("\"fillOpacity\":" + "0.5" + ",");
                    zSb.AppendLine("}");
                    n++;
                }
            }

            zSb.AppendLine("]};");
            zSb.Replace("@quantity", n.ToString());
            zSb.AppendLine("</script>");

            zResult[0] = zSb.ToString();
            zResult[1] = Sudung.ToString();
            zResult[2] = Denhan.ToString();
            zResult[3] = Trong.ToString();

            return zResult;
        }
        //Debit note
        public ActionResult DebitNote(string ContractKey = "", string ItemKey = "")
        {
            ViewBag.ContractKey = ContractKey;
            ViewBag.ItemKey = ItemKey;
            return PartialView("~/Views/Customer/Activity/DebitNote.cshtml");
        }
        public ActionResult DataDebit(string ContractKey = "", string ItemKey = "")
        {
            Helper.LogAction(UserLog, " Truy cập công nợ kho ", ItemKey);
            var List = DebitNote_Data.List(UserLog.PartnerNumber, ContractKey, out _);
            return PartialView("~/Views/Customer/Activity/_LogDebit.cshtml", List);
        }
        [DisplayName("Ghi thêm công nợ")]
        public JsonResult AddNote(string DebitNote)
        {
            DebitNote_Model zModel = JsonConvert.DeserializeObject<DebitNote_Model>(DebitNote, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
            ServerResult zResult = new ServerResult();
            DebitNote_Info zInfo = new DebitNote_Info();
            zModel.PhotoPath = Helper.StoreFilePostArray(Request.Files, "DebitNote", zModel.AutoKey.ToString());
            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.DebitDate = new DateTime(
                zModel.DebitDate.Year,
                zModel.DebitDate.Month,
                zModel.DebitDate.Day,
                DateTime.Now.Hour,
                DateTime.Now.Minute,
                DateTime.Now.Second);

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            zInfo.DebitNote = zModel;
            zInfo.Create_KeyAuto();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Ghi thêm công nợ ", zModel.ItemKey, zModel.DebitNote);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa công nợ")]
        public JsonResult DelNote(int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            DebitNote_Info zInfo = new DebitNote_Info(AutoKey);
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                //-------------------------------------------------------------------------------------------------
                Helper.LogAction(UserLog, " Xóa nhật ký kho ", zInfo.DebitNote.ItemKey, zInfo.DebitNote.DebitNote);
                //-------------------------------------------------------------------------------------------------

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
    }
}