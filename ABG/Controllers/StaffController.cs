﻿
using ABG.Employee;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABG.Controllers
{
    public class StaffController : BaseController
    {
        // GET: Staff
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ListStaff(string Branch)
        {
            var zList = new List<Employee_Model>();
            if (Branch == null || Branch == "0")
            {
                zList = Employee_Data.List(Helper.PartnerNumber);
            }
            else
            {
                zList = Employee_Data.SearchBranch(Branch);
            }
            ViewBag.List = zList;
            //Summary HTML
            ViewBag.BranchTable = Employee_Data.BranchSummary(Branch);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.BranchKey = Branch;
            Session["DSCBN"] = zList;
            return View();
        }
        public ActionResult ListStaffFilter(string Branch, string type)
        {
            var zList = new List<Employee_Model>();
            zList = Employee_Data.SearchFilter(Branch, type);
            ViewBag.List = zList;
            return PartialView("~/Views/Staff/_DataStaff.cshtml");
        }
        public ActionResult SearchName(string Name)
        {
            var zList = new List<Employee_Model>();
            zList = Employee_Data.SearchName(Name);
            ViewBag.List = zList;
            return PartialView("~/Views/Staff/_DataStaff.cshtml");
        }
        public ActionResult ListContract(string Branch, string Employee, string EmployeeIN, string title)
        {
            ViewBag.ListSelectType = ContractType_Data.ListType(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.BranchKey = Branch;
            Branch = Branch ?? "";
            var SHD = KiemTraHopDongNhanVien(Branch, out MaNV cacmanv);

            ViewBag.VoTH = SHD[0];
            ViewBag.CoTH = SHD[1];
            ViewBag.DenH = SHD[2];

            ViewBag.MaNVHetHan30 = cacmanv.MaNVHetHan30.AddDoubleQuoted();
            ViewBag.MaNVVoHan = cacmanv.MaNVVoHan.AddDoubleQuoted();
            ViewBag.MaNVCoHan = cacmanv.MaNVCoHan.AddDoubleQuoted();

            if (EmployeeIN != null)
            {
                if (title != null)
                {
                    if (title == "CH")
                    { ViewBag.Header = "Các hợp đồng có xác định thời hạn"; }
                    if (title == "VH")
                    { ViewBag.Header = "Các hợp đồng không xác định thời hạn"; }
                    if (title == "HH")
                    { ViewBag.Header = "Các hợp đồng hết, quá hạn"; }
                }
                ViewBag.ListData = Contract_Data.ListContractEmployeeIN(EmployeeIN);
                return View();
            }

            if (Employee != null && Employee.Length >= 36)
            {
                ViewBag.ListData = Contract_Data.ListContractEmployee(Employee);
                return View();
            }

            if (Branch == null || Branch == "0")
                ViewBag.ListData = Contract_Data.ListContract();
            else
                ViewBag.ListData = Contract_Data.ListContractBranch(Branch);

            return View();
        }
        public ActionResult ViewContract(string ContractKey)
        {
            ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, ContractKey);
            Contract_Info zInfo = new Contract_Info(ContractKey);
            string empkey = zInfo.Contract.EmployeeKey;
            Employee_Info zEmp = new Employee_Info(empkey);
            ViewBag.Employee = zEmp.Employee;
            return View("~/Views/Staff/_DataContract.cshtml", zInfo.Contract);
        }
        public ActionResult ViewContractSub(string EmployeeKey)
        {
            var zList = Contract_Data.ListContract(EmployeeKey);
            ViewBag.ListData = zList;
            return View("~/Views/Staff/_DataContractQuick.cshtml");
        }
        public ActionResult Branch_DataIndex(string Branch, string Close)
        {
            var zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Close, Branch);

            ViewBag.Employee = PopulateGrid(zList, out _);
            var zTable = Employee_Data.Report_Index_Branch(UserLog.PartnerNumber, Close, Branch);
            ViewBag.Table = zTable;
            ViewBag.Id = Close;
            ViewBag.Message = "OK";

            var zListExtra = Employee_Data.PayrollData(UserLog.PartnerNumber, Close, string.Empty);
            zListExtra = zListExtra.Where(s => s.BranchKey == Branch).ToList();
            ViewBag.InsuranceTable = InsuranceTable(zListExtra);
            ViewBag.OverTimeTable = OverTimeTable(zListExtra);
            ViewBag.SupportTable = SupportTable(zListExtra);
            ViewBag.PaymentTable = PaymentTable(zListExtra);


            return View("~/Views/Employee/DashBoard/_Detail.cshtml");
        }
        public ActionResult ListPayment()
        {
            var zList = Payroll_Close_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = zList;

            var dt = new DataTable();
            DataColumn dc = new DataColumn("Id", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Time", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("1", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("2", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("3", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("4", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("5", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("6", typeof(string));   //0
            dt.Columns.Add(dc);

            foreach (var item in zList)
            {
                var Table = Employee_Data.Report_Index_SUM_V2(Helper.PartnerNumber, item.CloseKey);
                if (Table.Rows.Count > 0)
                {
                    DataRow r = dt.NewRow();

                    r[0] = item.CloseKey;
                    r[1] = item.CloseDate;

                    r[2] = Table.Rows[0]["Amount"];
                    r[3] = Table.Rows[1]["Amount"];
                    r[4] = Table.Rows[2]["Amount"];
                    r[5] = Table.Rows[3]["Amount"];
                    r[6] = Table.Rows[4]["Amount"];
                    r[7] = Table.Rows[5]["Amount"];

                    dt.Rows.Add(r);
                }

            }
            ViewBag.Table = dt;
            return View();
        }
        public ActionResult PaymentQuick(string Id)
        {
            var zTable = Employee_Data.Report_Index_V2(Helper.PartnerNumber, Id);
            ViewBag.Key = Id;
            return View("~/Views/Staff/_DataPaymentQuick.cshtml", zTable);
        }
        public ActionResult PaymentDetail(string Id)
        {
            var zInfo = new Payroll_Close_Info(Id);
            if (zInfo.Code == "200")
            {
                var zList = new List<Payroll_Item>();
                zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, string.Empty);
                if (zList.Count == 0)
                {
                    ViewBag.Message = "Bạn chưa lưu bảng tính chi tiết ở bước 4 trong phần tính lương bạn phải thực hiện lại !.";
                }
                else
                {
                    ViewBag.InsuranceTable = InsuranceTable(zList);
                    ViewBag.OverTimeTable = OverTimeTable(zList);
                    ViewBag.SupportTable = SupportTable(zList);
                    ViewBag.PaymentTable = PaymentTable(zList);
                    ViewBag.OveralTable = OveralTable(Id);
                }
            }
            else
            {
                ViewBag.Message = zInfo.Message;
            }

            return View("~/Views/Staff/_DataPayment.cshtml", zInfo.Payroll_Close);
        }
        public ActionResult ReportPayment()
        {
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            return View();
        }
        public ActionResult ReportData(string type, string branch, string employee, string year)
        {
            if (type == "branch")
            {
                string name = "";
                if (branch != null && branch != "0")
                    name = new Branch_Info(branch).Branch.BranchName;
                else
                    name = " AB GROUP";
                string TieuDe = "Các khoản thanh toán chi phí nhân sự <br />" + name;
                ViewBag.TieuDe = TieuDe;

                var zTable = new DataTable();
                if (branch == "0")
                {
                    zTable = Employee_Data.PayrollIndex_Branch_V2(Helper.PartnerNumber, year.ToInt());
                }
                else
                {
                    zTable = Employee_Data.PayrollIndex_Branch(Helper.PartnerNumber, year.ToInt(), 0, branch);
                }

                var pvt = new PivotTable(zTable);
                var tblresult = pvt.Generate("Month", "NOIDUNG", "Amount", "Nội dung", "Tổng", "Tổng");
                DataRow r = tblresult.NewRow();

                double totalrow = 0;
                double totalcol = 0;

                r[0] = "7. Tổng chi phí chi trả";
                for (int c = 1; c < tblresult.Columns.Count - 1; c++)
                {
                    totalrow = tblresult.Rows[3][c].ToDouble() + tblresult.Rows[4][c].ToDouble();
                    r[c] = totalrow;
                    totalcol += totalrow;
                }
                r[tblresult.Columns.Count - 1] = totalcol;
                tblresult.Rows.InsertAt(r, tblresult.Rows.Count - 1);
                ViewBag.Table = tblresult;
                return View("~/Views/Staff/_DataBranch.cshtml");
            }
            if (type == "pesonal")
            {
                var zEmp = new Employee_Info(employee).Employee;
                var name = "";
                if (zEmp.Gender == 1)
                    name = " ông " + zEmp.LastName + " " + zEmp.FirstName;
                else
                    name = " bà " + zEmp.LastName + " " + zEmp.FirstName;

                string TieuDe = "Các khoản thanh toán lương của <br />" + name;
                ViewBag.TieuDe = TieuDe;

                var zTable = Employee_Data.PayrollIndex_Employee(Helper.PartnerNumber, year.ToInt(), 0, employee);
                var pvt = new PivotTable(zTable);
                var tblresult = pvt.Generate("Month", "ItemName", "Amount", "Nội dung", "Tổng", "Tổng");
                ViewBag.Table = tblresult;
                return View("~/Views/Staff/_DataPersonal.cshtml");
            }

            return View();
        }
        public ActionResult EmployeeHtml(string branchkey)
        {

            var zList = new List<Employee_Model>();
            if (branchkey == null || branchkey == "0")
            {
                zList = Employee_Data.List(Helper.PartnerNumber);
            }
            else
            {
                zList = Employee_Data.SearchBranch(branchkey);
            }
            return View("~/Views/Staff/_EmployeeHtml.cshtml", zList);
        }
        public ActionResult Contract(string EmployeeKey)
        {
            ViewBag.ListFile = Employee_Data.ListContract(EmployeeKey);
            return View("~/Views/Employee/Profile/Contract.cshtml");
        }
        public ActionResult CacDonPhep(string branchkey, string year, string employee)
        {
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.BranchKey = branchkey;

            branchkey = branchkey ?? "";
            year = year ?? DateTime.Now.Year.ToString();
            if (branchkey != null)
            {
                var zList = Employee_Data.CacDonPhepPhatSinh(branchkey, string.Empty, year);
                ViewBag.ListData = zList;
                ViewBag.Year = year;
            }
            if (employee != null)
            {
                var zList = Employee_Data.CacDonPhepPhatSinh(branchkey, employee, year);
                ViewBag.ListData = zList;
                ViewBag.Year = year;
            }

            return View();
        }
        public ActionResult DuLieuPhep(string employeekey, string year)
        {
            //dữ liệu phát sinh
            var zList = Employee_Data.CacDonPhep(employeekey, year);
            ViewBag.ListData = zList;

            //xử lý thông tin phép
            double SoNgayNghi = zList.Sum(s => s.TotalDate);
            double conlai = 0;

            //xử lý đầu phép
            var db = new Models.TN_ERP_V01Entities();
            int y = year.ToInt();
            var data = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == employeekey && s.CloseYear == y).First();
            if (data != null)
            {
                ViewBag.TongPhepSuDung = data.ConLai;
                ViewBag.TieuChuan = data.TieuChuan + data.ThamNien;
                ViewBag.ThamNien = data.ThamNien;
                ViewBag.CongTac = Employee_Data.SoThamNien(employeekey, year);
                ViewBag.NamGanNhat = data.HaiNamGanNhat;
            }
            else
            {
                //tính lại phép nếu chưa có "trường hợp qua năm"
                y = year.ToInt() - 1;
                var kyphepvuaroi = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == employeekey && s.CloseYear == y).First();
                if (kyphepvuaroi != null)
                {
                    //đã có dữ liệu trước đó
                    //1 lay dữ liệu phát sinh trong năm đó
                    var zold = Employee_Data.CacDonPhep(employeekey, y.ToString());
                    double tongnghi = zold.Sum(s => s.TotalDate);
                    double tinhconlai = kyphepvuaroi.HaiNamGanNhat.Value - tongnghi;
                    //tính hiện tại
                    int thamnien = Employee_Data.SoThamNien(employeekey, year).ToInt();
                    int tieuchuan = 12;

                    var rec = db.HRM_Leave_Close_New.Find(kyphepvuaroi.CloseKey);
                    if (rec != null)
                    {
                        rec.TongCong = tinhconlai + thamnien + tieuchuan;
                        rec.ThamNien = thamnien;
                        rec.TieuChuan = tieuchuan;
                        rec.ConLai = tinhconlai;
                        rec.ModifiedBy = UserLog.UserKey;
                        rec.ModifiedName = UserLog.EmployeeName;
                        rec.ModifiedOn = DateTime.Now;

                        db.SaveChanges();
                    }
                }
                else
                {
                    //chưa có dữ liệu có thể nhân viên mới vào làm
                    db.HRM_Leave_Close_New.Add(new Models.HRM_Leave_Close_New()
                    {
                        CloseYear = DateTime.Now.Year,
                        HaiNamGanNhat = 0,
                        ConLai = 0,
                        ThamNien = 0,
                        TieuChuan = 12,
                        TongCong = 12,
                        //
                        RecordStatus = 0,
                        CreatedBy = UserLog.UserKey,
                        CreatedName = UserLog.EmployeeName,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = UserLog.UserKey,
                        ModifiedName = UserLog.EmployeeName,
                        ModifiedOn = DateTime.Now,
                    });
                    db.SaveChanges();

                    ViewBag.TongPhepSuDung = 12;
                    ViewBag.TieuChuan = 12;
                    ViewBag.ThamNien = 0;
                    ViewBag.CongTac = 0;
                    ViewBag.NamGanNhat = 0;
                }
            }

            if (data != null)
            {
                conlai = data.ConLai.Value - SoNgayNghi;
                ViewBag.SuDung = SoNgayNghi;
                ViewBag.ConLai = conlai == 0 ? data.ConLai : conlai;
            }
            else
            {
                ViewBag.SuDung = SoNgayNghi;
                ViewBag.ConLai = 12 - SoNgayNghi;
            }
            return View("~/Views/Staff/_DataNghiPhep.cshtml");
        }
        //
        private DataTable InsuranceTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));//2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //5  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //6   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //7 C

            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương đóng BHXH", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("BHYT", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("BHTN", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //12
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "C1").Amount;
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "Q").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "R").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "S").Amount;
                    dr[12] = (dr[9].ToDouble()
                        + dr[10].ToDouble()
                        + dr[11].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable OverTimeTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //3  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //4   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //5 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //6 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Gờ làm thêm ngày thường", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm ngày thường", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Giờ làm thêm ngày lễ", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm lễ", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng tiền trực", typeof(string));   //13
            dt.Columns.Add(dc);
            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = (dr[5].ToDouble() + dr[7].ToDouble()).ToString("n0");
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "K").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "L").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[12] = data.SingleOrDefault(d => d.ItemID == "N").Amount;
                    dr[13] = data.SingleOrDefault(d => d.ItemID == "O").Amount;
                    dr[14] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[15] = data.SingleOrDefault(d => d.ItemID == "P1").Amount;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable SupportTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));    //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp chức vụ", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp điện thoại", typeof(string));   //6
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp xăng xe", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp đi lại", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp phát sinh", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //10          
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //12
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "G").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "H").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "I").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "J").Amount;
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "P11").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[12] = (dr[5].ToDouble()
                        + dr[6].ToDouble()
                        + dr[7].ToDouble()
                        + dr[8].ToDouble()
                        + dr[9].ToDouble()
                        + dr[10].ToDouble()
                        + dr[11].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable PaymentTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));//2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thoả thuận", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));//6
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng thu nhập", typeof(string));//7
            dt.Columns.Add(dc);
            dc = new DataColumn("Thu nhập theo ngày công", typeof(string));//8
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp khác", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH, BHYT, BHTN", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tạm ứng", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Thực nhận", typeof(string));   //13
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    double F2 = data.SingleOrDefault(d => d.ItemID == "F2").Amount.ToDouble();
                    double TN = data.SingleOrDefault(d => d.ItemID == "C").Amount.ToDouble();
                    double J1 = data.SingleOrDefault(d => d.ItemID == "J1").Amount.ToDouble();
                    double P1 = data.SingleOrDefault(d => d.ItemID == "P1").Amount.ToDouble();
                    double P2 = data.SingleOrDefault(d => d.ItemID == "P2").Amount.ToDouble();
                    double T = data.SingleOrDefault(d => d.ItemID == "T").Amount.ToDouble();
                    double Q = data.SingleOrDefault(d => d.ItemID == "Q").Amount.ToDouble();
                    double R = data.SingleOrDefault(d => d.ItemID == "R").Amount.ToDouble();
                    double S = data.SingleOrDefault(d => d.ItemID == "S").Amount.ToDouble();
                    double P11 = data.SingleOrDefault(d => d.ItemID == "P11").Amount.ToDouble();
                    double C1 = data.SingleOrDefault(d => d.ItemID == "C1").Amount.ToDouble();
                    double Z = Q + R + S;
                    //if (F2 > 0)
                    //{
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = TN;
                    dr[7] = P11 + C1;
                    dr[8] = F2;
                    dr[9] = (P11 + J1 + P1).ToString("n0");
                    dr[10] = P2.ToString("n0");
                    dr[11] = Z.ToString("n0");
                    dr[12] = T.ToString("n0");
                    dr[13] = (P2 - T - Z).ToString("n0");
                    //}
                    //else
                    //{
                    //    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    //    dr[6] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    //    dr[7] = P11 + C1;
                    //    dr[8] = F2;
                    //    dr[9] = (P11 + J1 + P1).ToString("n0");
                    //    dr[10] = P2.ToString("n0");
                    //    dr[11] = Z.ToString("n0");
                    //    dr[12] = T.ToString("n0");
                    //    dr[13] = (P2 - T - Z - TN).ToString("n0");
                    //}
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable OveralTable(string ID)
        {
            var zInfo = new Payroll_Close_Info(ID);
            var zId = zInfo.Payroll_Close.CloseKey;
            DataTable zTable = Employee_Data.Report_BHXH(UserLog.PartnerNumber, zId);
            return zTable;
        }
        private DataTable PopulateGrid(List<Payroll_Item> ListData, out string Message)
        {
            Message = string.Empty;
            DataTable dt = new DataTable();

            DataColumn dc = new DataColumn("Department", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));
            dt.Columns.Add(dc);
            //get header
            var tblLabel = ListData.DistinctBy(o => o.ItemName).ToList();

            //create data column
            foreach (Payroll_Item s in tblLabel)
            {
                dc = new DataColumn(s.ItemName, typeof(string));
                dt.Columns.Add(dc);
            }

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();
            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    try
                    {
                        int no = 5;
                        foreach (Payroll_Item item in data)
                        {
                            dr[no] = item.Amount.ToString();
                            no++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Message = rec.EmployeeName + " : " + ex.ToString();
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private int[] KiemTraHopDongNhanVien(string branch, out MaNV MaNV)
        {
            MaNV = new MaNV();

            List<string> MaNVHetHan30 = new List<string>();
            List<string> MaNVVoHan = new List<string>();
            List<string> MaNVCoHan = new List<string>();

            int[] KetQua = new int[3];
            var zList = Employee_Data.CacKeyNhanVien(branch);

            int sohdvothoihan = 0;
            int sohdhethan30 = 0;
            int sohdcothoihan = 0;
            foreach (var emp in zList)
            {
                var cachopdong = Contract_Data.ChiLayHopDong(emp.EmployeeKey);
                var vothoihan = cachopdong.Where(s => s.ContractType == 2).ToList();
                if (vothoihan.Count > 0)
                {
                    sohdvothoihan++;
                    MaNVVoHan.Add(emp.EmployeeKey);
                }
                else
                {
                    var hopdongmoinhat = cachopdong.OrderByDescending(s => s.SignDate).FirstOrDefault();
                    if (hopdongmoinhat != null)
                    {
                        DateTime ToDate = hopdongmoinhat.ToDate;
                        TimeSpan Time = Convert.ToDateTime(ToDate) - DateTime.Now;
                        if (Time.Days <= 30)
                        {
                            sohdhethan30++;
                            MaNVHetHan30.Add(emp.EmployeeKey);
                        }
                        else
                        {
                            if (ToDate >= DateTime.Now)
                            {
                                sohdcothoihan++;
                                MaNVCoHan.Add(emp.EmployeeKey);
                            }
                        }
                    }
                }
            }

            KetQua[0] = sohdvothoihan;
            KetQua[1] = sohdcothoihan;
            KetQua[2] = sohdhethan30;

            MaNV.MaNVHetHan30 = MaNVHetHan30;
            MaNV.MaNVVoHan = MaNVVoHan;
            MaNV.MaNVCoHan = MaNVCoHan;

            return KetQua;
        }

        //chạy 1 lần duy nhất của từng người nếu chưa có dữ liệu trước đó
        private void ChayPhep3Nam()
        {
            var db = new Models.TN_ERP_V01Entities();
            int currentyear = DateTime.Now.Year;
            var danhsachnhanvien = Employee_Data.CacKeyNhanVien(String.Empty);
            foreach (var rec in danhsachnhanvien)
            {
                string key = rec.EmployeeKey;
                var kyphep2020 = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == key && s.CloseYear == 2020).FirstOrDefault();
                if (kyphep2020 != null)
                {
                    double SoPhepConlai = kyphep2020.ConLai.Value;
                    //phep 2021
                    int Phep21 = 12;
                    int TN21 = Employee_Data.SoThamNien(key, "2021").ToInt() / 5;

                    //phep 2022
                    int Phep22 = 12;
                    int TN22 = Employee_Data.SoThamNien(key, "2022").ToInt() / 5;

                    double TongSD = SoPhepConlai + Phep21 + Phep22 + TN21 + TN22;

                    db.HRM_Leave_Close_New.Add(new Models.HRM_Leave_Close_New()
                    {
                        EmployeeKey = key,
                        CloseYear = 2022,
                        HaiNamGanNhat = SoPhepConlai + Phep21 + TN21,
                        TieuChuan = Phep22,
                        ThamNien = TN22,
                        TongCong = TongSD
                    });
                }
                else
                {
                    var kyphep2021 = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == key && s.CloseYear == 2021).FirstOrDefault();
                    if (kyphep2021 != null)
                    {
                        double SoPhepConlai = kyphep2021.ConLai.Value;
                        //phep 2022
                        int Phep22 = 12;
                        int TN22 = Employee_Data.SoThamNien(key, "2022").ToInt() / 5;
                        double TongSD = SoPhepConlai + Phep22 + TN22;
                        db.HRM_Leave_Close_New.Add(new Models.HRM_Leave_Close_New()
                        {
                            EmployeeKey = key,
                            CloseYear = 2022,
                            HaiNamGanNhat = SoPhepConlai,
                            TieuChuan = Phep22,
                            ThamNien = TN22,
                            TongCong = TongSD,
                        });
                    }
                    else
                    {
                        //phep 2020
                        int Phep20 = 12;
                        int TN20 = Employee_Data.SoThamNien(key, "2020").ToInt() / 5;

                        //phep 2021
                        int Phep21 = 12;
                        int TN21 = Employee_Data.SoThamNien(key, "2021").ToInt() / 5;

                        //phep 2022
                        int Phep22 = 12;
                        int TN22 = Employee_Data.SoThamNien(key, "2022").ToInt() / 5;
                        double TongSD = Phep20 + Phep21 + Phep22 + TN20 + TN21 + TN22;
                        db.HRM_Leave_Close_New.Add(new Models.HRM_Leave_Close_New()
                        {
                            EmployeeKey = key,
                            CloseYear = 2022,
                            HaiNamGanNhat = Phep20 + Phep21 + TN20 + TN21,
                            TieuChuan = Phep22,
                            ThamNien = TN22,
                            TongCong = TongSD
                        });
                    }
                }

                db.SaveChanges();

            }
        }
    }
}