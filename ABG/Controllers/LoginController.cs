﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ABG.Controllers
{
    public class LoginController : Controller
    {
        [Description("truy cập đăng nhập")]
        public ActionResult Login()
        {
            if (Request.Cookies[".ASPXAUTH"] != null)
            {
                string cok = Request.Cookies[".ASPXAUTH"].Value;
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cok);
                if (ticket.UserData.Length > 0)
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    var data = JsonConvert.DeserializeObject<User_Model>(ticket.UserData, settings);
                    ViewBag.Info = data;
                }
            }

            return View("~/Views/Shared/Login.cshtml");
        }

        [Description("Thực hiện đăng nhập")]
        [HttpPost]
        public ActionResult Login(string UserName, string Password, bool Remember = false)
        {
            ServerResult zResult = new ServerResult();

            if (UserName != string.Empty &&
                Password != string.Empty)
            {
                User_Info zInfo = new User_Info(UserName, Password, Helper.PartnerNumber);
                if (zInfo.User.Code == "200")
                {
                    List<string> zList = zInfo.User.DataAccess.ToListString();
                    if (zList != null)
                    {
                        string zData = zList.ToStringComma();
                        zInfo.User.DataAccess = zData;
                    }

                    string Message = "";
                    zInfo.User.ListRole = User_Data.ReadUserRole(Helper.PartnerNumber, zInfo.User.UserKey, out Message);
                    if (zInfo.User.EmployeeName == string.Empty)
                    {
                        zInfo.User.EmployeeName = zInfo.User.UserName;
                    }
                    Session["User_Model"] = zInfo.User;

                    var UserData = JsonConvert.SerializeObject(new User_Model() { UserName = UserName, Password = Password, PartnerNumber = Helper.PartnerNumber });

                    int timeout = Remember ? 525600 : 30; // Timeout in minutes, 525600 = 365 days.
                    var ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddMinutes(timeout), false, UserData);
                    string encrypted = FormsAuthentication.Encrypt(ticket);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                    cookie.Expires = DateTime.Now.AddMinutes(timeout);
                    cookie.HttpOnly = true; // make sure cookie not available in javascript.
                    Response.Cookies.Add(cookie);
                    zInfo.UpdateLogged();

                    //-------------------------------------------------------------------------------------------------
                    Helper.LogAction(zInfo.User, " Thực hiện đăng nhập ");
                    //-------------------------------------------------------------------------------------------------

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "Tên hoặc mật khẩu không đúng !.";
                }
            }
            else
            {
                ViewBag.Message = "Tên hoặc mật khẩu không đúng !.";
            }

            return View("~/Views/Shared/Login.cshtml");
        }
    }
}