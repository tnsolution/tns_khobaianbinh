﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Web;
using System.Web.Mvc;
using TN_Tools;

namespace ABG.Controllers
{
    public class ConfigController : BaseController
    {
        #region [SYSTEM AUTH]
        public ActionResult AuthRole(string UserKey)
        {
            string Message = "";
            var zList = User_Data.ListUserRole(Helper.PartnerNumber, UserKey, out Message);
            ViewBag.ListData = zList;
            return PartialView("~/Views/Config/Auth/_Role.cshtml");
        }

        public ActionResult AuthData(string UserKey)
        {
            string Message = "";
            var zList = User_Data.ListUserAccess(Helper.PartnerNumber, UserKey, out Message);
            ViewBag.ListArea = Product_Land_Data.List(Helper.PartnerNumber);
            ViewBag.DataAccess = zList.ToStringComma();
            return PartialView("~/Views/Config/Auth/_Data.cshtml");
        }
        [HttpPost]
        [DisplayName("Cập nhật quyền/chức năng người dùng")]
        public JsonResult AuthSave(string UserKey, string Role, string Data)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info();

            zInfo.User.PartnerNumber = Helper.PartnerNumber;
            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (Role.Length > 0)
            {
                List<User_Role> zRole = JsonConvert.DeserializeObject<List<User_Role>>(Role);

                zInfo.SetRoleAccess(zRole, UserKey);
                if (zInfo.User.Code == "200" ||
                    zInfo.User.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.User.Message.GetFirstLine();
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            if (Data.Length > 0)
            {
                List<string> zData = JsonConvert.DeserializeObject<List<string>>(Data);

                zInfo.SetDataAccess(zData, UserKey);
                if (zInfo.User.Code == "200" ||
                    zInfo.User.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.User.Message.GetFirstLine();
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserTracking()
        {
            ViewBag.ListUser = User_Data.List(Helper.PartnerNumber);
            return View("~/Views/Config/Track/UserTracking.cshtml");
        }
        public ActionResult UserData(string UserKey)
        {
            ViewBag.ListData = User_Data.Track(UserKey);
            return View("~/Views/Config/Track/UserData.cshtml");
        }
        #endregion

        #region[User_Config]
        [DisplayName("Xem danh sách người dùng")]
        public ActionResult Users()
        {
            ViewBag.ListUser = User_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            return View("~/Views/Config/Auth/Users.cshtml");
        }
        [HttpGet]
        public ActionResult SearchUsers(string SearchName)
        {
            #region [Data]
            ViewBag.ListUser = User_Data.ListSearch(Helper.PartnerNumber, SearchName);
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            #endregion

            return View("~/Views/Config/Auth/Users.cshtml");
        }
        [HttpPost]
        [DisplayName("Cập nhật người dùng")]
        public JsonResult SaveUser(string UserKey, string UserName, string Password, string lbl_Password, string EmployeeKey, string EmployeeName, string Description, string Activate, string ExpireDate)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);
            zInfo.User.PartnerNumber = Helper.PartnerNumber;
            zInfo.User.UserName = UserName.Trim();
            zInfo.User.EmployeeKey = EmployeeKey.Trim();
            zInfo.User.EmployeeName = EmployeeName;
            zInfo.User.Description = Description;

            if (lbl_Password.Trim() != Password.Trim())
            {
                zInfo.User.Password = MyCryptography.HashPass(Password.Trim());
            }
            DateTime zExpireDate = DateTime.MinValue;
            if (DateTime.TryParse(ExpireDate, out zExpireDate))
            {

            }
            zInfo.User.ExpireDate = zExpireDate;
            if (Activate.ToInt() == 0)
            {
                zInfo.User.Activate = false;
            }
            else
            {
                zInfo.User.Activate = true;
            }

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (UserKey.Length < 36)
            {
                zInfo.User.UserKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [DisplayName("Xem chi tiết người dùng")]
        public JsonResult DetailUser(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info(UserKey);

            User_Model zModel = zInfo.User;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa người dùng")]
        public JsonResult DeleteUser(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Info zInfo = new User_Info();
            zInfo.Delete(UserKey);
            User_Model zModel = zInfo.User;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.User.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [DisplayName("Reset mật khẩu người dùng")]
        public JsonResult ResetPass(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];
            string Random = TN_Utils.RandomPassword();
            User_Info zInfo = new User_Info();
            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.ResetPass(UserKey, TN_Utils.HashPass(Random));

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = Random;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + Random;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Activate(string UserKey)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];

            User_Info zInfo = new User_Info();
            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.SetActivate(UserKey);

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [DisplayName("Đổi mật khẩu người dùng")]
        public JsonResult ChangePass(string OldPass, string NewPass)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];

            User_Info zInfo = new User_Info(UserLog.UserName, OldPass, Helper.PartnerNumber);
            if (zInfo.User.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = "Tên mật khẩu không đúng vui lòng thử lại !.";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            zInfo.User.ModifiedName = zUserLog.EmployeeName;
            zInfo.User.ModifiedBy = zUserLog.UserKey;
            zInfo.ResetPass(zUserLog.UserKey, TN_Utils.HashPass(NewPass));

            if (zInfo.User.Code == "200" ||
                zInfo.User.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = NewPass;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + NewPass;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.User.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Areas_Config]
        public ActionResult Areas()
        {
            ViewBag.ListArea = Product_Land_Data.List(Helper.PartnerNumber);
            return View();
        }

        [HttpPost]
        public JsonResult SaveArea(string Area, HttpPostedFileBase[] File)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info();
            Product_Land_Model zModel = JsonConvert.DeserializeObject<Product_Land_Model>(Area);
            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.ParentKey = "00000000-0000-0000-0000-000000000000";
            zModel.Class = "TK";

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zModel.ProductKey.Length < 36)
            {
                zModel.ProductKey = Guid.NewGuid().ToString();
                string zPath = Helper.StoreFilePost(File, "Land", zModel.ProductKey);
                if (zPath != "0")
                {
                    zModel.PhotoPath = zPath;
                }

                zInfo.Product_Land = zModel;
                zInfo.Create_ClientKey();
            }
            else
            {
                zModel.ProductKey = zModel.ProductKey;
                string zPath = Helper.StoreFilePost(File, "Land", zModel.ProductKey);
                if (zPath != "0")
                {
                    zModel.PhotoPath = zPath;
                }

                zInfo.Product_Land = zModel;
                zInfo.Update();
            }

            if (zInfo.Product_Land.Code == "200" ||
                zInfo.Product_Land.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailArea(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info(ProductKey);
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteArea(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info();
            zInfo.Product_Land.ProductKey = ProductKey;
            zInfo.Delete();
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Position_Config]
        public ActionResult Position()
        {
            ViewBag.ListPosition = Position_Data.List(Helper.PartnerNumber);
            return View();
        }
        [HttpGet]
        public ActionResult SearchPosition(string SearchName)
        {

            #region [Data]
            ViewBag.ListPosition = Position_Data.ListSearch(Helper.PartnerNumber, SearchName);
            #endregion

            return View("~/Views/Config/Position.cshtml");
        }
        [HttpPost]
        public JsonResult SavePosition(int PositionKey = 0, string PositionID = "", string PositionNameVN = "", string Description = "", int Rank = 0)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info();
            Position_Model zModel = new Position_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.PositionKey = PositionKey;
            zModel.PositionID = PositionID.Trim();
            zModel.PositionNameVN = PositionNameVN.Trim();
            zModel.Rank = Rank;
            zModel.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (PositionKey == 0)
            {
                zInfo.ListPosition = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {

                zInfo.ListPosition = zModel;
                zInfo.Update();
            }

            if (zInfo.ListPosition.Code == "200" ||
                zInfo.ListPosition.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.ListPosition.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailPosition(int PositionKey)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info(PositionKey);

            Position_Model zModel = zInfo.ListPosition;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.ListPosition.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeletePosition(int PositionKey)
        {
            ServerResult zResult = new ServerResult();
            Position_Info zInfo = new Position_Info();
            zInfo.ListPosition.PositionKey = PositionKey;
            zInfo.Delete();
            Position_Model zModel = zInfo.ListPosition;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.ListPosition.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Depart_Config]

        public ActionResult Department()
        {
            ViewBag.ListDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(Helper.PartnerNumber);
            return View();
        }
        [HttpGet]
        public ActionResult SearchDepartment(string SearchName)
        {

            #region [Data]
            ViewBag.ListDepartment = Department_Data.ListSearch(Helper.PartnerNumber, SearchName);
            #endregion

            return View("~/Views/Config/ListDepartment.cshtml");
        }
        [HttpPost]
        public JsonResult SaveDepartment(string DepartmentKey = "", string DepartmentID = "", string DepartmentName = "", string Description = "", int Rank = 0, string BranchKey = "", string BranchName = "")
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info();
            Department_Model zModel = new Department_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.DepartmentKey = DepartmentKey;
            zModel.DepartmentID = DepartmentID.Trim();
            zModel.DepartmentName = DepartmentName.Trim();
            zModel.Rank = Rank;
            zModel.Description = Description.Trim();
            zModel.BranchKey = BranchKey;
            zModel.BranchName = BranchName;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (DepartmentKey == "")
            {
                zInfo.Department = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Department = zModel;
                zInfo.Update();
            }

            if (zInfo.Department.Code == "200" ||
                zInfo.Department.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Department.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailDepartment(string DepartmentKey)
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info(DepartmentKey);

            Department_Model zModel = zInfo.Department;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Department.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteDepartment(string DepartmentKey)
        {
            ServerResult zResult = new ServerResult();
            Department_Info zInfo = new Department_Info();
            zInfo.Department.DepartmentKey = DepartmentKey;
            zInfo.Delete();
            Department_Model zModel = zInfo.Department;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Department.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Product_Config]
        public ActionResult ProductClass()
        {
            ViewBag.ListClass = Product_Land_Class_Data.List(Helper.PartnerNumber);
            return View();
        }

        public JsonResult SaveClass(int ClassKey = 0, string ClassID = "", string ClassName = "", string Description = "", int Rank = 0)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Class_Info zInfo = new Product_Land_Class_Info();

            zInfo.Product_Land_Class.PartnerNumber = Helper.PartnerNumber;
            zInfo.Product_Land_Class.ClassKey = ClassKey;
            zInfo.Product_Land_Class.ClassID = ClassID.Trim().ToUpper().Substring(0, 2);
            zInfo.Product_Land_Class.ClassName = ClassName.Trim();
            zInfo.Product_Land_Class.Rank = Rank;
            zInfo.Product_Land_Class.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Product_Land_Class.ClassKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {

                zInfo.Update();
            }

            if (zInfo.Product_Land_Class.Code == "200" ||
                zInfo.Product_Land_Class.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land_Class.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailClass(int ClassKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Class_Info zInfo = new Product_Land_Class_Info(ClassKey);

            Product_Land_Class_Model zModel = zInfo.Product_Land_Class;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land_Class.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteClass(int ClassKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Class_Info zInfo = new Product_Land_Class_Info();
            zInfo.Product_Land_Class.ClassKey = ClassKey;
            zInfo.Delete();
            Product_Land_Class_Model zModel = zInfo.Product_Land_Class;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land_Class.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //popup thêm mới
        #region[Asset_Config]
        public ActionResult Assets()
        {
            string Parent = UserLog.DataAccess.Split(',')[0].Replace("'", "");
            ViewBag.ListSelectArea = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.ListAsset = Product_Land_Data.ListAsset(Helper.PartnerNumber, Parent);
            ViewBag.ListSelectClass = Product_Land_Class_Data.List(Helper.PartnerNumber);
            return View();
        }
        [HttpPost]
        public JsonResult SaveAsset(string ProductKey, string ProductID = "", string ProductName = "", string Address = "", int Rank = 0,
            string Parent = "", string Class = "", string LngLat = "")
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info(ProductKey);

            zInfo.Product_Land.PartnerNumber = Helper.PartnerNumber;
            zInfo.Product_Land.ProductName = ProductName.Trim();
            zInfo.Product_Land.ProductID = ProductID.Trim();
            zInfo.Product_Land.Address = Address.Trim();
            zInfo.Product_Land.Class = Class.Trim();
            zInfo.Product_Land.ParentKey = Parent.Trim();
            zInfo.Product_Land.LngLat = LngLat.Trim();
            zInfo.Product_Land.Rank = Rank;

            zInfo.Product_Land.CreatedBy = UserLog.UserKey;
            zInfo.Product_Land.CreatedName = UserLog.EmployeeName;
            zInfo.Product_Land.ModifiedBy = UserLog.UserKey;
            zInfo.Product_Land.ModifiedName = UserLog.EmployeeName;

            if (ProductKey.Length < 36)
            {
                zInfo.Product_Land.ProductKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Product_Land.ProductKey = ProductKey;
                zInfo.Update();
            }

            if (zInfo.Product_Land.Code == "200" ||
                zInfo.Product_Land.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailAsset(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info(ProductKey);
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteAsset(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info();
            zInfo.Product_Land.ProductKey = ProductKey;
            zInfo.Delete();
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //trang chi tiết
        #region[Product_Config]
        public ActionResult SearchProduct(string Parent = "", string Name = "")
        {
            ViewBag.ListAsset = Product_Land_Data.Search(Helper.PartnerNumber, Parent, Name);
            ViewBag.ListSelectArea = Helper.SelectLand(UserLog.DataAccess);
            ViewBag.ListSelectClass = Product_Land_Class_Data.List(Helper.PartnerNumber);

            TempData["Parent"] = Parent;
            TempData["Name"] = Name;

            return View("~/Views/Config/Assets.cshtml");
        }

        public ActionResult ProductEdit(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info(ProductKey);
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                ViewBag.ListSelectArea = Product_Land_Data.List(Helper.PartnerNumber);
                ViewBag.ListSelectClass = Product_Land_Class_Data.List(Helper.PartnerNumber);
                ViewBag.ListFeature = Product_Feature_Data.ListFeature(Helper.PartnerNumber, ProductKey);
                ViewBag.ListEqu = Product_Equipment_Data.ListEquipment(Helper.PartnerNumber, ProductKey);
                ViewBag.ListXN = Document_Data.List(Helper.PartnerNumber, ProductKey);
            }

            return View(zModel);
        }

        #region Product Action
        [HttpPost]
        public JsonResult SaveProduct(string ProductKey, string ProductID, string ProductName, string Address, int Rank,
           string Parent, string Class, string LngLat, string Description)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info(ProductKey);

            zInfo.Product_Land.PartnerNumber = Helper.PartnerNumber;
            zInfo.Product_Land.ProductName = ProductName.Trim();
            zInfo.Product_Land.ProductID = ProductID.Trim();
            zInfo.Product_Land.Address = Address.Trim();
            zInfo.Product_Land.Class = Class.Trim();
            zInfo.Product_Land.ParentKey = Parent.Trim();
            zInfo.Product_Land.LngLat = LngLat.Trim();
            zInfo.Product_Land.Rank = Rank;
            zInfo.Product_Land.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (ProductKey.Length < 36)
            {
                zInfo.Product_Land.ProductKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Product_Land.ProductKey = ProductKey;
                zInfo.Update();
            }

            if (zInfo.Product_Land.Code == "200" ||
                zInfo.Product_Land.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteProduct(string ProductKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Land_Info zInfo = new Product_Land_Info();
            zInfo.Product_Land.ProductKey = ProductKey;
            zInfo.Delete();
            Product_Land_Model zModel = zInfo.Product_Land;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Land.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Feature]
        public JsonResult SaveFea(int FeatureKey = 0, string Name = "", string Description = "", int Rank = 0, string ProductKey = "")
        {
            ServerResult zResult = new ServerResult();
            Product_Feature_Info zInfo = new Product_Feature_Info(FeatureKey);

            zInfo.Product_Feature.PartnerNumber = Helper.PartnerNumber;
            zInfo.Product_Feature.FeatureKey = FeatureKey;
            zInfo.Product_Feature.Name = Name.Trim();
            zInfo.Product_Feature.Rank = Rank;
            zInfo.Product_Feature.Description = Description.Trim();
            zInfo.Product_Feature.ProductKey = ProductKey.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Product_Feature.FeatureKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {

                zInfo.Update();
            }

            if (zInfo.Product_Feature.Code == "200" ||
                zInfo.Product_Feature.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Feature.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailFea(int FeatureKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Feature_Info zInfo = new Product_Feature_Info(FeatureKey);

            Product_Feature_Model zModel = zInfo.Product_Feature;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Feature.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteFea(int FeatureKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Feature_Info zInfo = new Product_Feature_Info();
            zInfo.Product_Feature.FeatureKey = FeatureKey;
            zInfo.Delete();
            Product_Feature_Model zModel = zInfo.Product_Feature;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Feature.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Feature-XN]
        public JsonResult SaveFeaXN()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zObj = Request.Form["Fea"];
                Document_Model zModel = JsonConvert.DeserializeObject<Document_Model>(zObj);
                zModel.TableJoin = "PDT_Product_Land";
                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.UserName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.UserName;

                int Key = zModel.AutoKey;
                Document_Info zInfo = new Document_Info(Key);

                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    //đã có file thì xóa
                    if (zInfo.Document.FilePath.Length > 0)
                    {
                        string Message = "";
                        Helper.DeleteSingleFile(zInfo.Document.FilePath, out Message);
                    }
                    string zFilePath = Helper.UploadPath + "/XENANG/";
                    bool zUploadResult = Helper.UploadSingleFile(Request.Files[0], zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zModel.FilePath = zFilePath;
                        zModel.FileExt = Path.GetExtension(Request.Files[0].FileName);
                        zModel.FileName = Request.Files[0].FileName;
                    }
                }
                else
                {
                    //không có file mới thì gắn lại file cũ
                    zModel.FilePath = zInfo.Document.FilePath;
                }

                zInfo.Document = zModel;
                if (zInfo.Document.AutoKey == 0)
                {

                    zInfo.Create_ServerKey();
                }
                else
                {

                    zInfo.Update();
                }

                if (zInfo.Document.Code == "200" ||
                    zInfo.Document.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Document.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailFeaXN(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info(AutoKey);

            Document_Model zModel = zInfo.Document;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Document.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteFeaXN(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Document_Info zInfo = new Document_Info();
            zInfo.Document.AutoKey = AutoKey;
            zInfo.Delete();
            Document_Model zModel = zInfo.Document;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Document.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Equipment]
        public JsonResult SaveEqu(int AutoKey = 0, string ItemName = "", float Value = 0, string Description = "", string ProductKey = "")
        {
            ServerResult zResult = new ServerResult();
            Product_Equipment_Info zInfo = new Product_Equipment_Info(AutoKey);

            zInfo.Product_Equipment.PartnerNumber = Helper.PartnerNumber;
            zInfo.Product_Equipment.AutoKey = AutoKey;
            zInfo.Product_Equipment.ItemName = ItemName.Trim();
            zInfo.Product_Equipment.Value = Value;
            zInfo.Product_Equipment.Description = Description.Trim();
            zInfo.Product_Equipment.ProductKey = ProductKey.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Product_Equipment.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {

                zInfo.Update();
            }

            if (zInfo.Product_Equipment.Code == "200" ||
                zInfo.Product_Equipment.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Equipment.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailEqu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Equipment_Info zInfo = new Product_Equipment_Info(AutoKey);

            Product_Equipment_Model zModel = zInfo.Product_Equipment;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Equipment.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteEqu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Product_Equipment_Info zInfo = new Product_Equipment_Info();
            zInfo.Product_Equipment.AutoKey = AutoKey;
            zInfo.Delete();
            Product_Equipment_Model zModel = zInfo.Product_Equipment;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Product_Equipment.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        //cài đặt loại phí mẫu
        #region[Payroll-sample]
        public ActionResult Payroll()
        {
            ViewBag.ListPayroll = Payroll_Sample_Data.ListConfig(Helper.PartnerNumber);
            ViewBag.ListCategory = Payroll_Category_Data.List(Helper.PartnerNumber);
            return View();
        }
        [HttpPost]
        public JsonResult SavePayroll(int ItemKey, string ItemID, string ItemName, string Description, int Rank, string UnitName, float Parameter, string Formula, int CategoryKey, string CategoryName)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Sample_Info zInfo = new Payroll_Sample_Info(ItemKey);

            zInfo.Payroll_Sample.PartnerNumber = Helper.PartnerNumber;
            zInfo.Payroll_Sample.ItemKey = ItemKey;

            zInfo.Payroll_Sample.ItemID = ItemID.Trim();
            zInfo.Payroll_Sample.ItemName = ItemName.Trim();
            zInfo.Payroll_Sample.Rank = Rank.ToInt();
            zInfo.Payroll_Sample.Description = Description.Trim();
            zInfo.Payroll_Sample.UnitName = UnitName.Trim();
            zInfo.Payroll_Sample.Parameter = Parameter.ToFloat();
            zInfo.Payroll_Sample.Formula = Formula.Trim();
            zInfo.Payroll_Sample.CategoryKey = CategoryKey;
            zInfo.Payroll_Sample.CategoryName = CategoryName.Trim();

            zInfo.Payroll_Sample.ItemType = 1;
            zInfo.Payroll_Sample.Quantity = 1;
            //zInfo.Payroll_Sample.UnitName = "Tháng";
            zInfo.Payroll_Sample.IsContract = 1;

            if (zInfo.Payroll_Sample.ItemKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Payroll_Sample.Code == "200" ||
                zInfo.Payroll_Sample.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Sample.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailPayroll(int ItemKey)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Sample_Info zInfo = new Payroll_Sample_Info(ItemKey);
            Payroll_Sample_Model zModel = zInfo.Payroll_Sample;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Sample.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeletePayroll(int ItemKey)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Sample_Info zInfo = new Payroll_Sample_Info();
            zInfo.Payroll_Sample.ItemKey = ItemKey;
            zInfo.Delete();
            Payroll_Sample_Model zModel = zInfo.Payroll_Sample;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Sample.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //chi nhanh
        #region [Branch_Config]

        public ActionResult Branch()
        {
            ViewBag.ListBranch = Branch_Data.List(Helper.PartnerNumber);
            return View();
        }

        [HttpPost]
        public JsonResult SaveBranch(string BranchKey = "", string BranchID = "", string BranchName = "", string Address = "", string Description = "", int Rank = 0)
        {
            ServerResult zResult = new ServerResult();
            Branch_Info zInfo = new Branch_Info();
            Branch_Model zModel = new Branch_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.BranchKey = BranchKey;
            zModel.BranchID = BranchID.Trim();
            zModel.BranchName = BranchName.Trim();
            zModel.Address = Address.Trim();
            zModel.Rank = Rank;
            zModel.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (BranchKey == "")
            {
                zInfo.Branch = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {

                zInfo.Branch = zModel;
                zInfo.Update();
            }

            if (zInfo.Branch.Code == "200" ||
                zInfo.Branch.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Branch.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailBranch(string BranchKey)
        {
            ServerResult zResult = new ServerResult();
            Branch_Info zInfo = new Branch_Info(BranchKey);

            Branch_Model zModel = zInfo.Branch;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Branch.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteBranch(string BranchKey)
        {
            ServerResult zResult = new ServerResult();
            Branch_Info zInfo = new Branch_Info();
            zInfo.Branch.BranchKey = BranchKey;
            zInfo.Delete();
            Branch_Model zModel = zInfo.Branch;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Branch.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //cài đặt tồn phép
        #region [Leave_Close_Config]
        //public ActionResult Leave_Close()
        //{
        //    ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
        //    ViewBag.ListData = Leave_Close_Data.List(Helper.PartnerNumber);
        //    return View();
        //}
        //[HttpPost]
        //public JsonResult SaveLeave_Close(int AutoKey = 0, string EmployeeKey = "", string EmployeeName = "", string CloseYear = "", float StandardBegin = 0, float StandardPlus = 0, float StandardEnd = 0, string Description = "")
        //{
        //    ServerResult zResult = new ServerResult();
        //    Leave_Close_Info zInfo = new Leave_Close_Info();
        //    Leave_Close_Model zModel = new Leave_Close_Model();

        //    zModel.PartnerNumber = Helper.PartnerNumber;
        //    zModel.AutoKey = AutoKey;
        //    zModel.EmployeeKey = EmployeeKey.Trim();
        //    zModel.EmployeeName = EmployeeName.Trim();
        //    zModel.CloseYear = CloseYear.Trim();
        //    zModel.StandardBegin = StandardBegin;
        //    zModel.StandardPlus = StandardPlus;
        //    zModel.StandardEnd = StandardEnd;
        //    zModel.Description = Description.Trim();

        //    zModel.CreatedBy = UserLog.UserKey;
        //    zModel.CreatedName = UserLog.EmployeeName;
        //    zModel.ModifiedBy = UserLog.UserKey;
        //    zModel.ModifiedName = UserLog.EmployeeName;

        //    if (AutoKey == 0)
        //    {
        //        zInfo.Leave_Close = zModel;
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Leave_Close = zModel;
        //        zInfo.Update();
        //    }

        //    if (zInfo.Leave_Close.Code == "200" ||
        //        zInfo.Leave_Close.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Leave_Close.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailLeave_Close(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Leave_Close_Info zInfo = new Leave_Close_Info(AutoKey);

        //    Leave_Close_Model zModel = zInfo.Leave_Close;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Leave_Close.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteLeave_Close(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Leave_Close_Info zInfo = new Leave_Close_Info();
        //    zInfo.Leave_Close.AutoKey = AutoKey;
        //    zInfo.Delete();

        //    if (zInfo.Leave_Close.Code == "200" ||
        //        zInfo.Leave_Close.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Leave_Close.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        #region [UserRoles_Config]
        public ActionResult UserRoles()
        {
            return View();
        }

        public ActionResult UserRolesEdit()
        {
            return View();
        }

        public ActionResult UserLands()
        {
            return View();
        }


        #endregion

        //định nghĩa tiêu chí đánh giá
        //~/Views/Config/Criteria.cshtml
        #region[Criteria]
        public ActionResult Criteria()
        {
            ViewBag.ListData = Criteria_Data.List(Helper.PartnerNumber);
            return View("~/Views/Config/Criteria.cshtml");
        }
        [HttpPost]
        public JsonResult SaveCriteria(int CriteriaKey, string CriteriaID, string CriteriaName, float MaxPoint, string Description, int Rank)
        {
            ServerResult zResult = new ServerResult();
            Criteria_Info zInfo = new Criteria_Info(CriteriaKey);

            zInfo.Criteria.PartnerNumber = Helper.PartnerNumber;
            zInfo.Criteria.CriteriaKey = CriteriaKey;

            zInfo.Criteria.CriteriaID = CriteriaID.Trim();
            zInfo.Criteria.CriteriaName = CriteriaName.Trim();
            zInfo.Criteria.MaxPoint = MaxPoint.ToInt();
            zInfo.Criteria.Rank = Rank.ToInt();
            zInfo.Criteria.Description = Description.Trim();

            zInfo.Criteria.CreatedBy = UserLog.UserKey;
            zInfo.Criteria.CreatedName = UserLog.EmployeeName;
            zInfo.Criteria.ModifiedBy = UserLog.UserKey;
            zInfo.Criteria.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Criteria.CriteriaKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Criteria.Code == "200" ||
                zInfo.Criteria.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Criteria.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailCriteria(int CriteriaKey)
        {
            ServerResult zResult = new ServerResult();
            Criteria_Info zInfo = new Criteria_Info(CriteriaKey);
            Criteria_Model zModel = zInfo.Criteria;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Criteria.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteCriteria(int CriteriaKey)
        {
            ServerResult zResult = new ServerResult();
            Criteria_Info zInfo = new Criteria_Info();
            zInfo.Criteria.CriteriaKey = CriteriaKey;
            zInfo.Delete();
            Criteria_Model zModel = zInfo.Criteria;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Criteria.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion // tiêu chí đánh giá

        //cài đặt website anbinh group
        #region [WEBSITE]
        [DisplayName("Xem hình ảnh cổ đông")]
        public ActionResult GalleryImage(int ChildCate = 0)
        {
            //lấy các danh mục con của danh mục cha đhcđ 37
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out string Message, 37);
            //các bài viết của danh mục con
            ViewBag.ListArticle = Article_Data.ListImage(UserLog.PartnerNumber, out Message, ChildCate);
            return View("~/Views/Config/GalleryImage.cshtml");
        }
        [DisplayName("Tải lên hình ảnh cổ đông")]
        public ActionResult UploadImage(string ChildCate = "")
        {
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    string zFilePath = Helper.UploadPath + "/ArticleAttach/" + file.FileName;
                    bool zUploadResult = Helper.UploadSingleFile(file, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        Article_Info zInfo = new Article_Info();
                        zInfo.Article.FileAttack = zFilePath;
                        zInfo.Article.FileName = file.FileName;
                        zInfo.Article.CategoryKey = ChildCate.ToInt();
                        zInfo.Article.Slug = 9;

                        zInfo.Article.PartnerNumber = UserLog.PartnerNumber;
                        zInfo.Article.CreatedBy = UserLog.UserKey;
                        zInfo.Article.CreatedName = UserLog.EmployeeName;
                        zInfo.Article.ModifiedBy = UserLog.UserKey;
                        zInfo.Article.ModifiedName = UserLog.EmployeeName;

                        zInfo.Create_ServerKey();
                    }
                }
            }

            return RedirectToAction("GalleryImage", new { ChildCate });
        }


        [DisplayName("Xem tài liệu cổ đông")]
        public ActionResult GalleryDocument(int ChildCate = 0)
        {
            //lấy các danh mục con của danh mục cha đhcđ 37
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out string Message, 37);
            //các bài viết của danh mục con
            ViewBag.ListArticle = Article_Data.ListDocument(UserLog.PartnerNumber, out Message, ChildCate);
            return View("~/Views/Config/GalleryDocument.cshtml");
        }
        [DisplayName("Tải lên tài liệu cổ đông")]
        public ActionResult UploadDocument(string ChildCate = "")
        {
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    string zFilePath = Helper.UploadPath + "/ArticleAttach/" + file.FileName;
                    bool zUploadResult = Helper.UploadSingleFile(file, zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        Article_Info zInfo = new Article_Info();
                        zInfo.Article.FileAttack = zFilePath;
                        zInfo.Article.FileName = file.FileName;
                        zInfo.Article.CategoryKey = ChildCate.ToInt();
                        zInfo.Article.Slug = 8;

                        zInfo.Article.PartnerNumber = UserLog.PartnerNumber;
                        zInfo.Article.CreatedBy = UserLog.UserKey;
                        zInfo.Article.CreatedName = UserLog.EmployeeName;
                        zInfo.Article.ModifiedBy = UserLog.UserKey;
                        zInfo.Article.ModifiedName = UserLog.EmployeeName;

                        zInfo.Create_ServerKey();
                    }
                }
            }

            return RedirectToAction("GalleryDocument", new { ChildCate });
        }

        public ActionResult ArticleMisc(string ArticleKey = "")
        {
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out string Message, 63);

            string Error = "";
            Article_Info zInfo = new Article_Info(ArticleKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {

            }
            else
            {
                Error = zInfo.Message.GetFirstLine();
            }

            return View(zInfo.Article);
        }

        public ActionResult UpdateArticleMisc(string ArticleKey = "", string Summarize = "")
        {
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out string Message, 63);

            string Error = "";
            Article_Info zInfo = new Article_Info(ArticleKey);
            zInfo.Article.Summarize = Summarize.Trim();
            zInfo.Update();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Error = "Cập nhật thành công !.";
            }
            else
            {
                Error = zInfo.Message.GetFirstLine();
            }

            ViewBag.Error = Error;
            return View("~/Views/Config/ArticleMisc.cshtml", zInfo.Article);
        }

        #region[Banner]
        [DisplayName("Xem danh sách banner website")]
        public ActionResult Banner()
        {
            ViewBag.ListBanner = Banner_Data.List(UserLog.PartnerNumber, out string Message);
            return View();
        }
        [HttpPost]
        [DisplayName("Cập nhật banner website")]
        public JsonResult SaveBanner()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zBannerObj = Request.Form["BannerObj"];
                int zBannerKey = Request.Form["BannerKey"].ToInt();
                Banner_Model zModel = JsonConvert.DeserializeObject<Banner_Model>(zBannerObj);

                zModel.Partnernumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Banner_Info zInfo = new Banner_Info(zBannerKey);

                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    //đã có file thì xóa
                    if (zInfo.Banner.Url.Length > 0)
                    {
                        string Message = "";
                        Helper.DeleteSingleFile(zInfo.Banner.Url, out Message);
                    }
                    string zFilePath = Helper.UploadPath + "/Banner/";
                    bool zUploadResult = Helper.UploadSingleFile(Request.Files[0], zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zModel.Url = zFilePath;
                    }
                }
                else
                {
                    //không có file mới thì gắn lại file cũ
                    zModel.Url = zInfo.Banner.Url;
                }

                zInfo.Banner = zModel;
                if (zInfo.Banner.BannerKey == 0)
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [DisplayName("Chi tiết banner website")]
        public JsonResult DetailBanner(int BannerKey)
        {
            ServerResult zResult = new ServerResult();
            Banner_Info zInfo = new Banner_Info(BannerKey);
            Banner_Model zModel = zInfo.Banner;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa banner website")]
        public JsonResult DeleteBanner(int BannerKey)
        {
            ServerResult zResult = new ServerResult();
            Banner_Info zInfo = new Banner_Info();
            zInfo.Banner.BannerKey = BannerKey;
            zInfo.Banner.ModifiedName = UserLog.EmployeeName;
            zInfo.Banner.ModifiedBy = UserLog.UserKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActivateBanner(int BannerKey)
        {
            ServerResult zResult = new ServerResult();

            Banner_Info zInfo = new Banner_Info();
            zInfo.Banner.ModifiedName = UserLog.EmployeeName;
            zInfo.Banner.ModifiedBy = UserLog.UserKey;
            zInfo.SetActivate(BannerKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Danh mục]
        public ActionResult ArticleCategory()
        {
            ViewBag.ListCategory = Article_Category_Data.ListCategory(UserLog.PartnerNumber, out string Message);
            return View();
        }
        [HttpPost]
        public JsonResult SaveArticleCategory()
        {
            ServerResult zResult = new ServerResult();
            try
            {
                string zCategoryObj = Request.Form["CategoryObj"];
                int zCategoryKey = Request.Form["CategoryKey"].ToInt();
                Article_Category_Model zModel = JsonConvert.DeserializeObject<Article_Category_Model>(zCategoryObj);

                zModel.PartnerNumber = UserLog.PartnerNumber;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Article_Category_Info zInfo = new Article_Category_Info(zCategoryKey);

                //Check có file upload
                if (Request.Files.Count > 0)
                {
                    //đã có file thì xóa
                    if (zInfo.Article_Category.Image.Length > 0)
                    {
                        string Message = "";
                        Helper.DeleteSingleFile(zInfo.Article_Category.Image, out Message);
                    }
                    string zFilePath = Helper.UploadPath + "/Category/";
                    bool zUploadResult = Helper.UploadSingleFile(Request.Files[0], zFilePath, out zFilePath);
                    if (zUploadResult)
                    {
                        zModel.Image = zFilePath;
                    }
                }
                else
                {
                    //không có file mới thì gắn lại file cũ
                    zModel.Image = zInfo.Article_Category.Image;
                }
                zInfo.Article_Category = zModel;
                if (zInfo.Article_Category.CategoryKey == 0)
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DetailArticleCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            Article_Category_Info zInfo = new Article_Category_Info(CategoryKey);
            Article_Category_Model zModel = zInfo.Article_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteArticleCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            Article_Category_Info zInfo = new Article_Category_Info();
            zInfo.Article_Category.CategoryKey = CategoryKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult PublishCategory(int CategoryKey)
        {
            ServerResult zResult = new ServerResult();
            User_Model zUserLog = (User_Model)Session["User_Model"];

            Article_Category_Info zInfo = new Article_Category_Info();
            zInfo.Article_Category.ModifiedName = zUserLog.EmployeeName;
            zInfo.Article_Category.ModifiedBy = zUserLog.UserKey;
            zInfo.SetPublish(CategoryKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Tin tức]
        [DisplayName("Xem danh sách tin tức")]
        public ActionResult ArticleList(int Category = 0)
        {
            string Message = "";
            //xem ngược danh mục cha
            int Parent = Category;
            Article_Category_Info zCate = new Article_Category_Info(Category);
            if (zCate.Article_Category.Parent == 0)
            {
                Parent = Category;
            }
            else
            {
                Parent = zCate.Article_Category.Parent;
            }

            ViewBag.ParentCategory = Parent;
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out Message, Parent);
            ViewBag.ListArticle = Article_Data.List(UserLog.PartnerNumber, out Message, Category);
            return View("~/Views/Config/ArticleList.cshtml");
        }

        [HttpPost, ValidateInput(false)]
        [DisplayName("Cập nhật tin tức")]
        public ActionResult SaveArticle(string ArticleKey = "", string ArticleName = "", string Summarize = "", string ArticleContent = "", string ArticleTag = "", int Category = 0, string Publish = "")
        {
            string Error = "";
            try
            {
                Article_Info zInfo = new Article_Info(ArticleKey);
                zInfo.Article.ArticleKey = ArticleKey;
                zInfo.Article.ArticleName = ArticleName.Trim();
                zInfo.Article.Summarize = Summarize.Trim();
                zInfo.Article.CategoryKey = Category.ToInt();
                zInfo.Article.Publish = Publish.ToBool();
                zInfo.Article.ArticleContent = ArticleContent.Trim();
                zInfo.Article.ArticleTag = ArticleTag.Trim();

                zInfo.Article.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Article.CreatedBy = UserLog.UserKey;
                zInfo.Article.CreatedName = UserLog.EmployeeName;
                zInfo.Article.ModifiedBy = UserLog.UserKey;
                zInfo.Article.ModifiedName = UserLog.EmployeeName;

                if (Request.Files.Count > 0)
                {
                    if (Request.Files["ArticleAttach"].ContentLength > 0)
                    {
                        //đã có file thì xóa
                        if (zInfo.Article.FileAttack.Length > 0)
                        {
                            string Message = "";
                            Helper.DeleteSingleFile(zInfo.Article.FileAttack, out Message);
                        }
                        HttpPostedFileBase file = Request.Files["ArticleAttach"];
                        string zFilePath = Helper.UploadPath + "/ArticleAttach/";
                        bool zUploadResult = Helper.UploadSingleFile(file, zFilePath, out zFilePath);
                        if (zUploadResult)
                        {
                            zInfo.Article.FileAttack = zFilePath;
                        }
                    }
                    else
                    {
                        //không có file mới thì gắn lại file cũ
                        zInfo.Article.FileAttack = zInfo.Article.FileAttack;
                    }

                    if (Request.Files["ArticlePicture"].ContentLength > 0)
                    {
                        //đã có file thì xóa
                        if (zInfo.Article.ImageLarge.Length > 0)
                        {
                            string Message = "";
                            Helper.DeleteSingleFile(zInfo.Article.FileAttack, out Message);
                        }
                        HttpPostedFileBase file = Request.Files["ArticlePicture"];
                        string zFilePath = Helper.UploadPath + "/ArticlePicture/";
                        bool zUploadResult = Helper.UploadSingleFile(file, zFilePath, out zFilePath);
                        if (zUploadResult)
                        {
                            zInfo.Article.ImageLarge = zFilePath;
                        }
                    }
                    else
                    {
                        //không có file mới thì gắn lại file cũ
                        zInfo.Article.ImageLarge = zInfo.Article.ImageLarge;
                    }
                }

                if (zInfo.Article.ArticleKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {

                    return RedirectToAction("ArticleList", new { Category });
                }
                else
                {
                    Error = zInfo.Message.GetFirstLine();
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString().GetFirstLine();
            }
            ViewBag.Message = Error;

            return View("~/Views/Config/Article.cshtml");
        }
        [HttpGet]
        [DisplayName("Xem chi tiết tin tức")]
        public ActionResult DetailArticle(string ArticleKey = "", int Category = 0)
        {
            Article_Model zModel = new Article_Model();
            if (ArticleKey != string.Empty)
            {
                Article_Info zInfo = new Article_Info(ArticleKey);
                zModel = zInfo.Article;

                if (zInfo.Code != "200" &&
                    zInfo.Code != "201")
                {
                    ViewBag.Message = zInfo.Message;
                }
            }
            ViewBag.ListCategory = Article_Category_Data.List(UserLog.PartnerNumber, out string Message, Category);
            return View("~/Views/Config/Article.cshtml", zModel);
        }
        [HttpPost]
        [DisplayName("Xóa tin tức")]
        public JsonResult DeleteArticle(string ArticleKey)
        {
            ServerResult zResult = new ServerResult();
            Article_Info zInfo = new Article_Info();
            zInfo.Article.ArticleKey = ArticleKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Publish(string ArticleKey)
        {
            ServerResult zResult = new ServerResult();
            Article_Info zInfo = new Article_Info();
            zInfo.Article.ModifiedName = UserLog.EmployeeName;
            zInfo.Article.ModifiedBy = UserLog.UserKey;
            zInfo.SetPublish(ArticleKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult CommentList()
        {
            ViewBag.List = Comment_Data.List(UserLog.PartnerNumber, out string Message);
            return View();
        }

        [HttpGet]
        public JsonResult ViewComment(string Key)
        {
            ServerResult zResult = new ServerResult();
            Comment_Info zInfo = new Comment_Info(Key);
            zResult.Data = JsonConvert.SerializeObject(zInfo.Comment);
            zResult.Success = true;
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteComment(string Key)
        {
            ServerResult zResult = new ServerResult();
            Comment_Info zInfo = new Comment_Info(Key);
            zInfo.Delete();
            zResult.Success = true;
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //thông tin công ty
        #region MyRegion
        public ActionResult Company_History()
        {
            ViewBag.List = Company_Data.List(out _, 1);
            return View("~/Views/Config/Info/History.cshtml");
        }
        [HttpGet]
        public JsonResult Edit_History(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info(key);
            Company_Model zModel = zInfo.Company;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Delete_History(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info();
            zInfo.Company.AutoKey = key;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Save_History(string ObjectData, HttpPostedFileBase[] File)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info();
            Company_Model zModel = JsonConvert.DeserializeObject<Company_Model>(ObjectData, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            zModel.Slug = 1;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zModel.AutoKey == 0)
            {
                string zPath = Helper.StoreFilePost(File, "History", TN_Utils.RandomString(7));
                if (zPath != "0")
                {
                    zModel.FileAttack = zPath;
                }

                zInfo.Company = zModel;
                zInfo.Create_KeyAuto();
            }
            else
            {
                string zPath = Helper.StoreFilePost(File, "History", TN_Utils.RandomString(7));
                if (zPath != "0")
                {
                    zModel.FileAttack = zPath;
                }

                zInfo.Company = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                 zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        //
        public ActionResult Company_Expand()
        {
            ViewBag.List = Company_Data.List(out _, 2);
            return View("~/Views/Config/Info/Expand.cshtml");
        }
        [HttpGet]
        public JsonResult Edit_Expand(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info(key);
            Company_Model zModel = zInfo.Company;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Delete_Expand(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info();
            zInfo.Company.AutoKey = key;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Save_Expand(string ObjectData, HttpPostedFileBase[] File)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info();
            Company_Model zModel = JsonConvert.DeserializeObject<Company_Model>(ObjectData, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            zModel.Slug = 2;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zModel.AutoKey == 0)
            {
                string zPath = Helper.StoreFilePost(File, "Expand", TN_Utils.RandomString(7));
                if (zPath != "0")
                {
                    zModel.FileAttack = zPath;
                }

                zInfo.Company = zModel;
                zInfo.Create_KeyAuto();
            }
            else
            {
                string zPath = Helper.StoreFilePost(File, "Expand", TN_Utils.RandomString(7));
                if (zPath != "0")
                {
                    zModel.FileAttack = zPath;
                }

                zInfo.Company = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                 zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Company_Tree()
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.List = Company_Data.List(out _, 3);
            return View("~/Views/Config/Info/Tree.cshtml");
        }
        [HttpGet]
        public JsonResult Edit_Tree(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info(key);
            Company_Model zModel = zInfo.Company;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Delete_Tree(int key)
        {
            ServerResult zResult = new ServerResult();
            Company_Info zInfo = new Company_Info();
            zInfo.Company.AutoKey = key;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Save_Tree(int AutoKey = 0, string Title1 = "", int Parent = 0)
        {
            ServerResult zResult = new ServerResult();

            var zEmp = new Employee_Info(Title1).Employee;

            Company_Info zInfo = new Company_Info();
            Company_Model zModel = new Company_Model();
            zModel.AutoKey = AutoKey;
            zModel.Title1 = Title1;
            zModel.Title2 = zEmp.PositionName;
            zModel.Title3 = zEmp.DepartmentName;
            zModel.Description = zEmp.LastName + " " + zEmp.FirstName;
            zModel.FileAttack = zEmp.PhotoPath;
            zModel.Parent = Parent;
            zModel.Slug = 3;
            zInfo.Company = zModel;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zModel.AutoKey == 0)
            {
                zInfo.Create_KeyAuto();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                 zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        //

        #endregion
    }
}