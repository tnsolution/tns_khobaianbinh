﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace ABG.Controllers
{
    public class WebSiteController : Controller
    {
        public WebSiteController()
        {
            string Message = "";
            string CacBanTinThue = Article_Data.CateChildKey(Helper.PartnerNumber, out Message, 38);
            ViewBag.TinChoThueMoiNhat = Article_Data.ListInCate(Helper.PartnerNumber, out Message, CacBanTinThue).Take(5).ToList();
            string CacBanGioiThieu = Article_Data.CateChildKey(Helper.PartnerNumber, out Message, 39);
            var danhsach = Article_Data.ListInCate(Helper.PartnerNumber, out Message, CacBanGioiThieu);
            ViewBag.GioiThieuCty = danhsach.Where(s => s.CategoryKey == 48 && s.Publish == true).OrderByDescending(s => s.CreatedOn).FirstOrDefault();
            ViewBag.GioiThieuKho = danhsach.Where(s => s.CategoryKey == 60 && s.Publish == true).OrderByDescending(s => s.CreatedOn).FirstOrDefault();
            ViewBag.GioiThieuXeNang = danhsach.Where(s => s.CategoryKey == 61 && s.Publish == true).OrderByDescending(s => s.CreatedOn).FirstOrDefault();
            ViewBag.GioiThieuXangDau = danhsach.Where(s => s.CategoryKey == 62 && s.Publish == true).OrderByDescending(s => s.CreatedOn).FirstOrDefault();

            var allowedStatus = new[] { 49, 50, 52, 53, 54 };
            ViewBag.CacDiaDiem = danhsach.Where(s => allowedStatus.Contains(s.CategoryKey)).ToList();

            ViewBag.LinkYoutube = new Article_Info("8a428fdd-e7e2-49a6-b29b-b85e0edfbe7b").Article.Summarize;
            ViewBag.LienHe = new Article_Info("87818f9e-a306-47bc-87df-0a8e2c60e679").Article.Summarize;
            ViewBag.ThongtinCty = new Article_Info("2d1b1a44-2c32-401c-b357-db5535ce01d8").Article.Summarize;
        }

        // GET: WebSite
        public ActionResult Index()
        {
            return RedirectToAction("trangchu");
        }

        public ActionResult lienhe()
        {
            return View();
        }

        [HttpPost]
        public JsonResult guilienhe(string name, string email, string subject, string message)
        {
            ServerResult zResult = new ServerResult();
            Comment_Info zInfo = new Comment_Info();
            zInfo.Comment.CustomerName = name.Trim();
            zInfo.Comment.CustomerEmail = email.Trim();
            zInfo.Comment.CommentDate = DateTime.Now;
            zInfo.Comment.CommentID = subject;
            zInfo.Comment.Description = message;
            zInfo.Comment.PartnerNumber = Helper.PartnerNumber;
            zInfo.Create_ServerKey();

            if (zInfo.Code == "200" || zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = "Thông tin của quí khách đã được gửi đến phòng kinh doanh !.";
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult trangchu()
        {
            ViewBag.List = Article_Data.List(Helper.PartnerNumber, out string Message, 0);
            return View();
        }

        public ActionResult slideHinhAnh()
        {
            ViewBag.ListBanner = Banner_Data.List(Helper.PartnerNumber, out string Message);
            return PartialView("~/Views/WebSite/_SlideHinhAnh.cshtml");
        }

        public ActionResult danhsachTin(int Category = 0)
        {
            Article_Category_Info zCat = new Article_Category_Info(Category);
            ViewBag.Cate2 = zCat.Article_Category.CategoryName;
            ViewBag.ArticleList = Article_Data.List(Helper.PartnerNumber, out string Message, Category);
            Article_Category_Info zCat1 = new Article_Category_Info(zCat.Article_Category.Parent);
            ViewBag.Cate1 = zCat1.Article_Category.CategoryName;

            if (zCat.Article_Category.Parent != 0)
            {
                Category = zCat.Article_Category.Parent;
            }
                       
            ViewBag.Category = Category;
            return View();
        }

        public ActionResult dichvu(int Parent = 38)
        {
            //mặc định Parent = 38 là các dịch vụ bên kinh doanh
            ViewBag.CateChild = Article_Category_Data.List(Helper.PartnerNumber, out string Message, Parent);
            return PartialView("~/Views/Website/_DichVu.cshtml");
        }

        public ActionResult chitietTin(string ArticleKey = "")
        {
            Article_Info zInfo = new Article_Info(ArticleKey);

            Article_Category_Info zCat = new Article_Category_Info(zInfo.Article.CategoryKey);
            ViewBag.Cate2 = zCat.Article_Category.CategoryName;
            //zCat = new Article_Category_Info(zCat.Article_Category.Parent);
            //ViewBag.Cate1 = zCat.Article_Category.CategoryName;
            ViewBag.Category = zCat.Article_Category.Parent;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                return View(zInfo.Article);
            }
            else
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public ActionResult allTag()
        {
            ViewBag.List = Article_Data.ListTag(Helper.PartnerNumber, out string Message);
            return PartialView("~/Views/WebSite/_Tag.cshtml");
        }

        public ActionResult thongbaoMoi()
        {
            ViewBag.List = Article_Data.ListNew(Helper.PartnerNumber, out string Message);
            return PartialView("~/Views/WebSite/_Thongbao.cshtml");
        }

        public ActionResult listTag(string Tag)
        {
            ViewBag.ArticleList = Article_Data.ListByTag(Helper.PartnerNumber, out string Message, Tag);
            return View("~/Views/Website/danhsachtin.cshtml");
        }


        public ActionResult coDong(int Category = 0)
        {
            var List = Article_Category_Data.List(Helper.PartnerNumber, out string Message, 37);
            ViewBag.CateList = List = List.OrderByDescending(s => s.CategoryName).ToList();
            int Cate = 0;
            if (Category == 0)
            {
                Cate = List[0].CategoryKey;
                ViewBag.Name = List[0].CategoryName;
            }
            else
            {
                Cate = Category;
                ViewBag.Name = List.Single(s => s.CategoryKey == Category).CategoryName;
            }

            ViewBag.Document = Article_Data.ListDocument(Helper.PartnerNumber, out Message, Cate);
            ViewBag.Image = Article_Data.ListImage(Helper.PartnerNumber, out Message, Cate);
            ViewBag.Category = Category;

            return View("~/Views/Website/codong.cshtml");
        }
    }
}