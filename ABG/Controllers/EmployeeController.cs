﻿using ABG.Employee;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ABG.Controllers
{
    public class EmployeeController : BaseController
    {
        #region [--------------- XỬ LÝ SƠ ĐỒ-------------------]
        [DisplayName("Bản đồ nhân sự")]
        [Route("so-do-nhan-su")]
        public ActionResult Map()
        {
            List<Employee_Model> ListData = Employee_Data.ListRECURSIVE(Helper.PartnerNumber);

            var sb = new StringBuilder();
            List<Employee_Model> item = ListData.FindAll(x => x.ReportToKey == "");
            string html = GenerateUL(item, ListData, sb);
            ViewBag.Map = html;

            //Summary HTML
            ViewBag.BranchTable = Employee_Data.BranchSummary();



            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Bản đồ nhân sự ");
            //-------------------------------------------------------------------------------------------------

            return View();
        }
        #endregion

        #region [-----------------XỬ LÝ PHÉP--------------------]        
        public ActionResult PhepMoi()
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Leave/Index.cshtml");
        }

        public async Task<ActionResult> TimPhepTheoTen(string ChiNhanh, string Employee = "", string sbutton = "")
        {
            ViewBag.ChiNhanh = ChiNhanh;
            ViewBag.Employee = Employee;
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            if (Employee.Length >= 36)
            {
                switch (sbutton)
                {
                    case "btn_Cal":
                        #region Cách tính phép theo kiểu nhập trước xuất trước
                        var zInfo = new Leave_Close_Info();
                        zInfo.DeleteByEmployeeKey(Employee);

                        var employee = new Employee_Info(Employee).Employee;
                        var name = employee.LastName + " " + employee.FirstName;
                        var chucvu = employee.PositionName;
                        var Incurred = Employee_Data.NoteIncurred(Employee);

                        if (Incurred.Rows.Count > 0)
                        {
                            foreach (DataRow rec in Incurred.Rows)
                            {
                                DateTime fromdate = Convert.ToDateTime(rec["FromDate"]);
                                DateTime todate = Convert.ToDateTime(rec["ToDate"]);

                                var data = Employee_Data.TINHPHEP(Employee, fromdate, todate, rec["TotalDate"].ToFloat(), rec["NoteKey"].ToString(), out _);
                                foreach (var close in data)
                                {
                                    zInfo = new Leave_Close_Info();
                                    zInfo.Leave_Close = close;
                                    zInfo.Leave_Close.Slug = 1;
                                    zInfo.Create_ServerKey();

                                    await Task.Delay(500);
                                }
                            }
                        }
                        #endregion

                        ViewBag.HoTen = name;
                        ViewBag.ChucVu = chucvu;
                        ViewBag.data = Employee_Data.XemKyPhep(Employee, out _);
                        return View("~/Views/Employee/Leave/Index.cshtml");

                    case "btn_Export":
                        employee = new Employee_Info(Employee).Employee;
                        name = employee.LastName + " " + employee.FirstName;
                        chucvu = employee.PositionName;
                        var zList = Employee_Data.XemKyPhep(Employee, out _);
                        var Filename = "BANGPHEP " + name + ".xls";
                        var stream = Template_PhepTable(zList, name, chucvu);
                        return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);

                    default:
                        employee = new Employee_Info(Employee).Employee;
                        name = employee.LastName + " " + employee.FirstName;
                        chucvu = employee.PositionName;
                        ViewBag.HoTen = name;
                        ViewBag.ChucVu = chucvu;
                        ViewBag.data = Employee_Data.XemKyPhep(Employee, out _);
                        return View("~/Views/Employee/Leave/Index.cshtml");
                }
            }

            switch (sbutton)
            {
                case "btn_Export":
                    var name = new Branch_Info(ChiNhanh).Branch.BranchName;
                    var zList = Employee_Data.CacKeyNhanVien(ChiNhanh);
                    var Filename = "BANGPHEP " + name + ".xls";
                    var stream = Template_PhepTableChiNhanh(zList, name);
                    return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);

                default:
                    ViewBag.TenChiNhanh = new Branch_Info(ChiNhanh).Branch.BranchName;
                    ViewBag.CacNhanVien = Employee_Data.CacKeyNhanVien(ChiNhanh);
                    return View("~/Views/Employee/Leave/Index.cshtml");
            }
        }

        #region [TỒN PHÉP]
        public ActionResult LeaveBeginNote()
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = Leave_Close_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Leave/Begin.cshtml");
        }
        [HttpPost]
        public JsonResult LeaveBeginSave(string BeginKey, string BeginYear, string BeginNo, string BeginPlus, string Description, string EmployeeKey, string EmployeeName)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                Leave_Close_Info zInfo = new Leave_Close_Info();
                zInfo.Leave_Close.CloseYear = BeginYear;
                zInfo.Leave_Close.CloseEnd = BeginNo.ToFloat();
                zInfo.Leave_Close.Incremental = BeginPlus.ToFloat();
                zInfo.Leave_Close.Description = Description;
                zInfo.Leave_Close.EmployeeKey = EmployeeKey;
                zInfo.Leave_Close.EmployeeName = EmployeeName;
                zInfo.Leave_Close.PartnerNumber = Helper.PartnerNumber;
                zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
                zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
                zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
                zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;

                if (BeginKey.Length >= 36)
                {
                    zInfo.Leave_Close.CloseKey = BeginKey;
                    zInfo.Update();
                }
                else
                {
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Leave_Close.Code == "200" ||
                    zInfo.Leave_Close.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Leave_Close.Message;
                    zResult.Success = false;
                }

                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LeaveBeginDelete(string BeginKey)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                Leave_Close_Info zInfo = new Leave_Close_Info();
                zInfo.Leave_Close.CloseKey = BeginKey;
                zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
                zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
                zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
                zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();

                if (zInfo.Leave_Close.Code == "200" ||
                    zInfo.Leave_Close.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Leave_Close.Message;
                    zResult.Success = false;
                }
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LeaveBeginOpen(string BeginKey)
        {
            ServerResult zResult = new ServerResult();
            Leave_Close_Info zInfo = new Leave_Close_Info(BeginKey, "");
            if (zInfo.Leave_Close.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Close.Message;
            }
            else
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Leave_Close);
                zResult.Success = true;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [NHẬP, XOÁ, DANH SÁCH]
        public ActionResult LeaveListNote()
        {
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem theo dõi phép ");
            //-------------------------------------------------------------------------------------------------

            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = Leave_Note_Data.List();

            ViewBag.Status = 0;
            ViewBag.Name = "";
            ViewBag.FromDate = "";
            ViewBag.ToDate = "";

            return View("~/Views/Employee/Leave/List.cshtml");
        }
        [HttpGet]
        public JsonResult LeaveOpenNote(string NoteKey)
        {
            ServerResult zResult = new ServerResult();
            Leave_Note_Info zInfo = new Leave_Note_Info(NoteKey);
            Leave_Note_Model zModel = zInfo.Leave_Note;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Note.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LeaveDeleteNote(string NoteKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Leave_Note_Info(NoteKey);
            var zModel = zInfo.Leave_Note;
            if (zModel.Code == "200")
            {
                zInfo.Delete();
                if (zModel.Code == "200" ||
                    zModel.Code == "201")
                {
                    //-------------------------------------------------------------------------------------------------
                    Helper.LogAction(UserLog, " Xoá kì phép " + zModel.FromDate.ToString("dd/MM/yyyy") + "-" + zModel.ToDate.ToString("dd/MM/yyyy") + " nhân sự " + zModel.EmployeeName);
                    //-------------------------------------------------------------------------------------------------

                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Leave_Note.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Không tìm thấy thông tin này";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public async Task<JsonResult> LeaveSaveNote(
            string NoteKey, string NoteDate, string EmployeeKey, string EmployeeName, string BranchKey,
            string BranchName, string DepartmentKey, string DepartmentName, int PositionKey, string PositionName,
            string FromDate, string ToDate, float TotalDate, int CategoryKey, string CategoryName, string Description)
        {
            ServerResult zResult = new ServerResult();
            Leave_Note_Info zInfo = new Leave_Note_Info(NoteKey);

            var emp = new Employee_Info(EmployeeKey).Employee;
            var name = (emp.LastName.Trim() + " " + emp.FirstName.Trim());

            zInfo.Leave_Note.NoteKey = NoteKey;
            zInfo.Leave_Note.EmployeeKey = EmployeeKey;
            zInfo.Leave_Note.EmployeeName = name;
            zInfo.Leave_Note.BranchKey = BranchKey;
            zInfo.Leave_Note.BranchName = BranchName;
            zInfo.Leave_Note.DepartmentKey = DepartmentKey;
            zInfo.Leave_Note.DepartmentName = DepartmentName;
            zInfo.Leave_Note.PositionKey = PositionKey;
            zInfo.Leave_Note.PositionName = PositionName;

            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
            zInfo.Leave_Note.FromDate = zFromDate;

            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);
            zInfo.Leave_Note.ToDate = zToDate;

            DateTime.TryParseExact(NoteDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zDateWrite);
            zInfo.Leave_Note.NoteDate = zDateWrite;

            zInfo.Leave_Note.TotalDate = TotalDate;
            zInfo.Leave_Note.CategoryKey = CategoryKey;
            zInfo.Leave_Note.CategoryName = CategoryName.Trim();
            zInfo.Leave_Note.Description = Description.Trim();

            zInfo.Leave_Note.PartnerNumber = Helper.PartnerNumber;
            zInfo.Leave_Note.CreatedBy = UserLog.UserKey;
            zInfo.Leave_Note.CreatedName = UserLog.EmployeeName;
            zInfo.Leave_Note.ModifiedBy = UserLog.UserKey;
            zInfo.Leave_Note.ModifiedName = UserLog.EmployeeName;

            string NewKey = "";
            if (zInfo.Leave_Note.NoteKey == "")
            {
                NewKey = Guid.NewGuid().ToString();
                zInfo.Leave_Note.NoteKey = NewKey;
                zInfo.Create_ClientKey();
            }
            else
            {
                NewKey = NoteKey;
                zInfo.Update();
            }

            if (zInfo.Leave_Note.Code == "200" ||
                zInfo.Leave_Note.Code == "201")
            {
                //cũ
                //Leave_Close_Model zModel = Helper.LeaveCloseEnd(EmployeeKey, zFromDate, zToDate, TotalDate, out string Mess);
                //zModel.Reference = NewKey;
                //SaveCloseLeave(zModel);

                //var data = Employee_Data.TINHPHEP(EmployeeKey, zFromDate, zToDate, TotalDate, NoteKey, out _);
                //foreach (var close in data)
                //{
                //    var Close = new Leave_Close_Info();
                //    Close.Leave_Close = close;
                //    Close.Leave_Close.Slug = 1;
                //    Close.Create_ServerKey();
                //}

                #region Cách tính phép theo kiểu nhập trước xuất trước
                var zClose = new Leave_Close_Info();
                zClose.DeleteByEmployeeKey(EmployeeKey);
                var Incurred = Employee_Data.NoteIncurred(EmployeeKey);

                if (Incurred.Rows.Count > 0)
                {
                    foreach (DataRow rec in Incurred.Rows)
                    {
                        DateTime fromdate = Convert.ToDateTime(rec["FromDate"]);
                        DateTime todate = Convert.ToDateTime(rec["ToDate"]);

                        var data = Employee_Data.TINHPHEP(EmployeeKey, fromdate, todate, rec["TotalDate"].ToFloat(), rec["NoteKey"].ToString(), out _);
                        foreach (var close in data)
                        {
                            zClose = new Leave_Close_Info();
                            zClose.Leave_Close = close;
                            zClose.Leave_Close.Slug = 1;
                            zClose.Create_ServerKey();

                            await Task.Delay(500);
                        }
                    }
                }
                #endregion

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Note.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LeaveTracking(string EmployeeKey, string FromDate, string ToDate, float No)
        {
            DateTime zFromDate;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            DateTime zToDate;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            var zResult = new ServerResult();
            var zModel = Helper.LeaveCloseEnd(EmployeeKey, zFromDate, zToDate, No, out string Mess);

            if (Mess != string.Empty)
            {
                zResult.Success = false;
                zResult.Message = Mess;
            }
            else
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Search_Leave(int Status = 0, string SearchEmployee = "", string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = Leave_Note_Data.Search(SearchEmployee, string.Empty, zFromDate, zToDate, 0, 0, 0, string.Empty, string.Empty, Status);

            ViewBag.Status = Status;
            ViewBag.Name = SearchEmployee;
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;

            return View("~/Views/Employee/Leave/List.cshtml");
        }

        #endregion

        #region [----------------BASIC MAP LIST, CONTRACT, ... ---------------]
        [HttpPost]
        public ActionResult ContractCopy(string ContractKey, string FromDate, string ToDate, string NewID, string SignDate, int ContractType)
        {
            var newkey = Guid.NewGuid().ToString();

            Contract_Info zInfo = new Contract_Info(ContractKey);
            zInfo.Contract.SubContract = NewID;
            zInfo.Contract.CreatedBy = UserLog.UserKey;
            zInfo.Contract.CreatedName = UserLog.EmployeeName;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            zInfo.Contract.FromDate = zFromDate;
            if (ToDate != String.Empty)
            {
                DateTime zToDate = DateTime.MinValue;
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zInfo.Contract.ToDate = zToDate;
            }
            else
            {
                zInfo.Contract.ToDate = DateTime.MinValue;
            }

            DateTime zSignDate = DateTime.MinValue;
            DateTime.TryParseExact(SignDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zSignDate);
            zInfo.Contract.SignDate = zSignDate;
            zInfo.Contract.ContractType = ContractType;
            zInfo.Contract.ContractKey = newkey;
            zInfo.Create_ClientKey();

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                return RedirectToAction(nameof(ContractEdit), new { ContractKey = newkey });
            }

            return View("~/Views/Shared/Error.cshtml");
        }

        #region [Init Info]
        [HttpPost]
        [DisplayName("Cập nhật nhân sự")]
        public JsonResult SaveEmployee(
                 string EmployeeKey, string EmployeeID, string LastName, string FirstName,
                 int PositionKey, string PositionName, int Gender, string BirthDay,
                 string Passport, string Address, string Email, string MobiPhone,
                 string ReportToKey, string ReportToName,
                 string BranchKey, string BranchName,
                 string DepartmentKey, string DepartmentName,
                 string StartDate, string Style)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.EmployeeID = EmployeeID.Trim();
            zModel.LastName = LastName.Trim();
            zModel.FirstName = FirstName.Trim();
            zModel.PositionKey = PositionKey;
            zModel.PositionName = PositionName.Trim();
            zModel.Gender = Gender;
            zModel.BranchKey = BranchKey;
            zModel.BranchName = BranchName;
            zModel.DepartmentKey = DepartmentKey;
            zModel.DepartmentName = DepartmentName;
            zModel.Style = Style;

            DateTime zBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zBirthDay);
            zModel.Birthday = zBirthDay;

            DateTime zStartDate = DateTime.MinValue;
            DateTime.TryParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartDate);
            zModel.StartingDate = zStartDate;

            zModel.PassportNumber = Passport.Trim();
            zModel.AddressRegister = Address.Trim();
            zModel.Email = Email.Trim();
            zModel.MobiPhone = MobiPhone.Trim();
            zModel.ReportToKey = ReportToKey.Trim();
            zModel.ReportToName = ReportToName.Trim();
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                zInfo.Employee = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.EmployeeKey = EmployeeKey;
                zInfo.Employee = zModel;
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [DisplayName("Xem chi tiết nhân sự")]
        public JsonResult DetailEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa nhân sự")]
        public JsonResult DeleteEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info();
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Edit Info]
        [DisplayName("Mở điều chỉnh nhân sự")]
        public ActionResult Edit(string EmployeeKey)
        {
            ViewBag.ListSelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.ListSelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(Helper.PartnerNumber);

            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListPayroll = Payroll_Person_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }

        #region[Info]
        [HttpPost]
        public ActionResult SaveInfo(string EmployeeKey, string LastName, string FirstName, int rdoGender, string BirthDay,
           string Passport, string Address, string Email, string MobiPhone, HttpPostedFileBase[] files)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = Helper.PartnerNumber;
            zInfo.Employee.LastName = LastName.Trim();
            zInfo.Employee.FirstName = FirstName.Trim();
            zInfo.Employee.Gender = rdoGender;

            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zBirthDay);

            zInfo.Employee.Birthday = zBirthDay;
            zInfo.Employee.PassportNumber = Passport.Trim();
            zInfo.Employee.AddressRegister = Address.Trim();
            zInfo.Employee.Email = Email.Trim();
            zInfo.Employee.MobiPhone = MobiPhone.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                EmployeeKey = Guid.NewGuid().ToString();
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                //zResult.Success = true;
                //return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //zResult.Success = false;
                //zResult.Message = zInfo.Employee.Message;
                //return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            ViewBag.Message = zResult.Message;
            return RedirectToAction("Edit", new { EmployeeKey });
        }
        [HttpGet]
        public JsonResult DetailInfo(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Family]
        [HttpPost]
        public JsonResult SaveFamily(string EmployeeKey, string FatherName, string FatherBirthday, string FatherWork,
            string FatherAddress, string MotherName, string MotherBirthday, string MotherWork, string MotherAddress,
            string PartnersName, string PartnersBirthday, string PartnersWork, string PartnersAddress,
            string ChildExtend, string OrtherExtend)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = new Family_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.EmployeeKey = EmployeeKey.Trim();
            zModel.FatherName = FatherName.Trim();

            DateTime zFatherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(FatherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFatherBirthday);

            zModel.FatherBirthday = zFatherBirthday;
            zModel.FatherWork = FatherWork.Trim();
            zModel.FatherAddress = FatherAddress.Trim();
            zModel.MotherName = MotherName.Trim();

            DateTime zMotherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(MotherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zMotherBirthday);

            zModel.MotherBirthday = zMotherBirthday;
            zModel.MotherWork = MotherWork.Trim();
            zModel.MotherAddress = MotherAddress.Trim();
            zModel.PartnersName = PartnersName.Trim();

            DateTime zPartnersBirthday = DateTime.MinValue;
            DateTime.TryParseExact(PartnersBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zPartnersBirthday);

            zModel.PartnersBirthday = zPartnersBirthday;
            zModel.PartnersWork = PartnersWork.Trim();
            zModel.PartnersAddress = PartnersAddress.Trim();
            zModel.ChildExtend = ChildExtend.Trim();
            zModel.OrtherExtend = OrtherExtend.Trim();

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Family.AutoKey == 0)
            {
                zInfo.Family = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.AutoKey = zInfo.Family.AutoKey;
                zInfo.Family = zModel;
                zInfo.Update();
            }

            if (zInfo.Family.Code == "200" ||
                zInfo.Family.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Family.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailFamily(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = zInfo.Family;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Family.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Job]
        [HttpPost]
        public JsonResult SaveJob(string EmployeeKey, string StartingDate, string ReportToKey, string ReportToName,
           string DepartmentKey, string DepartmentName, string BranchKey, string BranchName, int PositionKey, string PositionName, string Note, int StatusKey, string StatusName)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = Helper.PartnerNumber;
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Employee.ReportToKey = ReportToKey.Trim();
            zInfo.Employee.ReportToName = ReportToName.Trim();

            DateTime zStartingDate = DateTime.MinValue;
            DateTime.TryParseExact(StartingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartingDate);

            zInfo.Employee.WorkingStatusKey = StatusKey;
            zInfo.Employee.WorkingStatusName = StatusName;
            zInfo.Employee.StartingDate = zStartingDate;
            zInfo.Employee.DepartmentKey = DepartmentKey.Trim();
            zInfo.Employee.DepartmentName = DepartmentName.Trim();

            zInfo.Employee.BranchKey = BranchKey.Trim();
            zInfo.Employee.BranchName = BranchName.Trim();

            zInfo.Employee.PositionKey = PositionKey;
            zInfo.Employee.PositionName = PositionName.Trim();
            zInfo.Employee.Note = Note.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Employee.EmployeeKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailJob(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Education]
        [HttpPost]
        public JsonResult SaveEdu(string EmployeeKey, string FromDate, string ToDate, string DegreeName,
           string DegreeBy, int StatusKey, string StatusName, string Description, int ClassifiedKey, string ClassifiedName, string TypeName, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);

            zInfo.Education.PartnerNumber = Helper.PartnerNumber;
            zInfo.Education.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Education.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Education.ToDate = zToDate;
            zInfo.Education.DegreeName = DegreeName.Trim();
            zInfo.Education.DegreeBy = DegreeBy.Trim();

            zInfo.Education.StatusKey = StatusKey;
            zInfo.Education.StatusName = StatusName.Trim();
            zInfo.Education.Description = Description.Trim();
            zInfo.Education.ClassifiedKey = ClassifiedKey.ToInt();
            zInfo.Education.ClassifiedName = ClassifiedName.Trim();
            zInfo.Education.TypeName = TypeName.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Education.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Education.Code == "200" ||
                zInfo.Education.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);
            Education_Model zModel = zInfo.Education;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info();
            zInfo.Education.AutoKey = AutoKey;
            zInfo.Delete();
            Education_Model zModel = zInfo.Education;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Exprience]
        [HttpPost]
        public JsonResult SaveExp(string EmployeeKey, string FromDate, string ToDate, string UnitWork,
           string UnitPosition, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);

            zInfo.Experience.PartnerNumber = Helper.PartnerNumber;
            zInfo.Experience.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Experience.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Experience.ToDate = zToDate;
            zInfo.Experience.UnitWork = UnitWork.Trim();
            zInfo.Experience.UnitPosition = UnitPosition.Trim();
            zInfo.Experience.Description = Description.Trim();
            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Experience.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Experience.Code == "200" ||
                zInfo.Experience.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);
            Experience_Model zModel = zInfo.Experience;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info();
            zInfo.Experience.AutoKey = AutoKey;
            zInfo.Delete();
            Experience_Model zModel = zInfo.Experience;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Skills]
        [HttpPost]
        public JsonResult SaveSkill(string EmployeeKey, string SkillName, int Levels, int Maxlevels = 10, string Description = "", int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);

            zInfo.Skill.PartnerNumber = Helper.PartnerNumber;
            zInfo.Skill.EmployeeKey = EmployeeKey;


            zInfo.Skill.SkillName = SkillName.Trim();
            zInfo.Skill.Levels = Levels.ToInt();
            zInfo.Skill.Maxlevels = Maxlevels.ToInt();
            zInfo.Skill.Description = Description.Trim();

            if (zInfo.Skill.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Skill.Code == "200" ||
                zInfo.Skill.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailSkill(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);
            Skill_Model zModel = zInfo.Skill;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteSkill(int Autokey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info();
            zInfo.Skill.AutoKey = Autokey;
            zInfo.Delete();
            Skill_Model zModel = zInfo.Skill;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[History Insure]
        [HttpPost]
        public JsonResult SaveIn(string EmployeeKey, string DateWrite, string StatusName, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);

            zInfo.HistoryInsure.PartnerNumber = Helper.PartnerNumber;
            zInfo.HistoryInsure.EmployeeKey = EmployeeKey;

            DateTime zDateWrite = DateTime.MinValue;
            DateTime.TryParseExact(DateWrite, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateWrite);

            zInfo.HistoryInsure.DateWrite = zDateWrite;

            zInfo.HistoryInsure.StatusName = StatusName.Trim();
            zInfo.HistoryInsure.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.HistoryInsure.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.HistoryInsure.Code == "200" ||
                zInfo.HistoryInsure.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info();
            zInfo.HistoryInsure.AutoKey = AutoKey;
            zInfo.Delete();
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Salary apply]
        public ActionResult SalaryStandardList(string EmployeeKey)
        {
            List<Payroll_Person_Model> zList = Payroll_Person_Data.List(Helper.PartnerNumber, EmployeeKey);
            return PartialView("~/Views/Employee/Salary/StandardList.cshtml", zList);
        }

        //Partial view file
        public ActionResult SalaryStandard(string PayrollKey = "")
        {
            Payroll_Person_Info zInfo = new Payroll_Person_Info(PayrollKey);
            Payroll_Person_Model zModel = zInfo.Payroll_Person;
            zModel.ListItem = Payroll_Person_Data.ListDetail(PayrollKey);
            return PartialView("~/Views/Employee/Salary/StandardPayroll.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult SalarySave(string PayrollObj)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                Payroll_Person_Model zModel = JsonConvert.DeserializeObject<Payroll_Person_Model>(PayrollObj, settings);
                Payroll_Person_Info zInfo = new Payroll_Person_Info();

                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.Year = zModel.FromDate.Year;
                zModel.Month = zModel.FromDate.Month;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.Activated)
                {
                    zModel.ActivatedDate = DateTime.Now;
                }

                if (zModel.PayrollKey.Length >= 36)
                {
                    zInfo.Payroll_Person = zModel;
                    zInfo.Update();
                }
                else
                {
                    zModel.PayrollKey = Guid.NewGuid().ToString();
                    zInfo.Payroll_Person = zModel;
                    zInfo.Create_ClientKey();
                }

                if (zInfo.Payroll_Person.Code == "200" ||
                    zInfo.Payroll_Person.Code == "201")
                {
                    zInfo.DeleteDetail();
                    foreach (Payroll_Person_Detail_Model zItem in zModel.ListItem)
                    {
                        Payroll_Person_Detail_Info zDetail = new Payroll_Person_Detail_Info();
                        zDetail.Payroll_Person_Detail = zItem;
                        zDetail.Payroll_Person_Detail.PartnerNumber = Helper.PartnerNumber;
                        zDetail.Payroll_Person_Detail.PayrollKey = zModel.PayrollKey;
                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Payroll_Person.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SalaryDelete(string PayrollKey = "")
        {
            ServerResult zResult = new ServerResult();
            if (PayrollKey != string.Empty)
            {
                try
                {
                    Payroll_Person_Info zInfo = new Payroll_Person_Info();
                    zInfo.Payroll_Person.PayrollKey = PayrollKey;
                    zInfo.Delete();

                    if (zInfo.Payroll_Person.Code == "200" ||
                        zInfo.Payroll_Person.Code == "201")
                    {
                        zResult.Success = true;
                        zResult.Message = "";
                    }
                    else
                    {
                        zResult.Success = false;
                        zResult.Message = zInfo.Payroll_Person.Message.GetFirstLine();
                    }
                }
                catch (Exception ex)
                {
                    zResult.Success = false;
                    zResult.Message = ex.ToString();
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn 1 thông tin để xóa";
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region[Review] bỏ
        //[HttpPost]
        //public JsonResult SaveRev(string EmployeeKey, string DateWrite, int Point, int MaxPoint = 10, string Description = "", int AutoKey = 0)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info(AutoKey);

        //    zInfo.Review.PartnerNumber = Helper.PartnerNumber;
        //    //người ghi nhận
        //    zInfo.Review.EmployeeKey = EmployeeKey;

        //    DateTime zDateWrite = DateTime.MinValue;
        //    if (DateTime.TryParse(DateWrite, out zDateWrite))
        //    {

        //    }
        //    zInfo.Review.DateWrite = zDateWrite;

        //    zInfo.Review.Point = Point;
        //    zInfo.Review.MaxPoint = MaxPoint;
        //    zInfo.Review.Description = Description.Trim();
        //    //người đánh giá
        //    zInfo.Review.ReviewerKey = UserLog.UserKey;
        //    zInfo.Review.ReviewerName = UserLog.EmployeeName;

        //    if (zInfo.Review.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Review.Code == "200" ||
        //        zInfo.Review.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailRev(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info(AutoKey);
        //    Review_Model zModel = zInfo.Review;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteRev(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info();
        //    zInfo.Review.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Review_Model zModel = zInfo.Review;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion
        #endregion

        #region[Contract]
        public ActionResult Export_Contract()
        {
            var zList = new List<Contract_Model>();
            var Filename = "Các hợp đồng lao động.xls";
            var stream = new MemoryStream();
            if (Session["ContractExcel"] != null)
            {
                zList = Session["ContractExcel"] as List<Contract_Model>;
                stream = Template_Contract(zList, "");
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
            }
            else
            {
                ViewBag.ListData = Contract_Data.List(Helper.PartnerNumber);
                return View("~/Views/Employee/Contract/ContractList.cshtml");
            }
        }


        [HttpPost]
        public JsonResult ContractDelete(string ContractKey = "")
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            Contract_Model zModel = zInfo.Contract;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zInfo.Delete();
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContractList()
        {
            ViewBag.ListSelectType = ContractType_Data.ListType(Helper.PartnerNumber);
            ViewBag.ListData = Contract_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Contract/ContractList.cshtml");
        }

        public ActionResult ContractEdit(string ContractKey = "")
        {
            ViewBag.ListSelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.ListSelectType = ContractType_Data.ListType(Helper.PartnerNumber);
            ViewBag.ListSelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelecPayroll = Payroll_Sample_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectMinus = Payroll_Sample_Data.ListMinus(Helper.PartnerNumber);

            Contract_Model zModel = new Contract_Model();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            zModel = zInfo.Contract;
            if (zModel.Code != "200" &&
                zModel.Code != "201")
            {
                ViewBag.Message = zModel.Message;
            }
            else
            {
                ViewBag.ListItem = Contract_Detail_Data.List(Helper.PartnerNumber, ContractKey);
                ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, ContractKey);
            }

            return View("~/Views/Employee/Contract/ContractEdit.cshtml", zModel);
        }

        //#region [Cộng phí]
        //public ActionResult ContractItem(string ContractKey = "")
        //{
        //    ViewBag.ListItem = Contract_Detail_Data.List(Helper.PartnerNumber, ContractKey);
        //    return PartialView("~/Views/Employee/Contract/ContractItem.cshtml");
        //}

        //[HttpPost]
        //public JsonResult SaveItem(int AutoKey, string ContractKey, int ItemKey, string ItemName, float Total, string Description)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);

        //    zInfo.Contract_Detail.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Contract_Detail.ContractKey = ContractKey;
        //    zInfo.Contract_Detail.ItemKey = ItemKey;
        //    zInfo.Contract_Detail.ItemName = ItemName.Trim();
        //    zInfo.Contract_Detail.Total = Total.ToFloat();
        //    zInfo.Contract_Detail.Description = Description.Trim();

        //    zInfo.Contract_Detail.ItemType = 1;
        //    zInfo.Contract_Detail.Quantity = 1;
        //    zInfo.Contract_Detail.UnitName = "Tháng";
        //    //zModel.CreatedBy = UserLog.UserKey;
        //    //zModel.CreatedName = UserLog.EmployeeName;
        //    //zModel.ModifiedBy = UserLog.UserKey;
        //    //zModel.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Contract_Detail.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Contract_Detail.Code == "200" ||
        //        zInfo.Contract_Detail.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info();
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //#endregion

        //#region [Trừ phí]
        //public ActionResult ContractMinus(string ContractKey = "")
        //{
        //    ViewBag.ListItem = Contract_Detail_Data.ListMinus(Helper.PartnerNumber, ContractKey);
        //    return PartialView("~/Views/Employee/Contract/ContractMinus.cshtml");
        //}
        //[HttpPost]
        //public JsonResult SaveMinus(int AutoKey, string ContractKey, int ItemKey, string ItemName, float Quantity = 0, float Total = 0, string Description = "")
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);

        //    zInfo.Contract_Detail.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Contract_Detail.ContractKey = ContractKey;
        //    zInfo.Contract_Detail.ItemKey = ItemKey;
        //    zInfo.Contract_Detail.ItemName = ItemName.Trim();
        //    zInfo.Contract_Detail.Total = Total.ToFloat();
        //    zInfo.Contract_Detail.Description = Description.Trim();

        //    zInfo.Contract_Detail.ItemType = 2;
        //    zInfo.Contract_Detail.Quantity = Quantity;
        //    zInfo.Contract_Detail.UnitName = "%";

        //    zInfo.Contract_Detail.CreatedBy = UserLog.UserKey;
        //    zInfo.Contract_Detail.CreatedName = UserLog.EmployeeName;
        //    zInfo.Contract_Detail.ModifiedBy = UserLog.UserKey;
        //    zInfo.Contract_Detail.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Contract_Detail.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Contract_Detail.Code == "200" ||
        //        zInfo.Contract_Detail.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailMinus(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteMinus(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info();
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult MinusInfo(int ItemKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Sample_Info zInfo = new Payroll_Sample_Info(ItemKey);
        //    Payroll_Sample_Model zModel = zInfo.Payroll_Sample;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Sample.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //#endregion

        #region [Contract Save]
        [HttpPost]
        public JsonResult SaveContract(
            string ContractKey,
            string ContractID,
            string SubContract,
            string SignDate,
            int ContractType,
            string ContractTypeName,
            string SignBy,
            string SignName,
            int SignPositionKey,
            string SignPositionName,
            string EmployeeKey,
            string EmployeeName,
            string EmployeeGender,
            string EmployeeBirthDay,
            string EmployeeBirthPlace,
            string EmployeePassport,
            string EmployeeIssueDate,
            string EmployeeIssuePlace,
            string EmployeeAdress,
            string ReportToKey,
            string ReportToName,
            string DepartmentKey,
            string DepartmentName,
            string BranchKey,
            string BranchName,
            int PositionKey,
            string PositionName,
            string FromDate,
            string ToDate,
            string DateBeginSalary,
            string Note,
            string BranchAdress
            )
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);

            zInfo.Contract.PartnerNumber = Helper.PartnerNumber;
            zInfo.Contract.ContractKey = ContractKey;
            zInfo.Contract.ContractID = ContractID.Trim();
            zInfo.Contract.SubContract = SubContract.Trim();
            DateTime zSignDate = DateTime.MinValue;
            DateTime.TryParseExact(SignDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zSignDate);

            zInfo.Contract.SignDate = zSignDate;
            zInfo.Contract.ContractType = ContractType;
            zInfo.Contract.ContractTypeName = ContractTypeName;

            zInfo.Contract.SignBy = SignBy;
            zInfo.Contract.SignName = SignName.Trim();
            zInfo.Contract.SignPositionKey = SignPositionKey;
            zInfo.Contract.SignPositionName = SignPositionName.Trim();

            zInfo.Contract.EmployeeKey = EmployeeKey.Trim();
            zInfo.Contract.EmployeeName = EmployeeName.Trim();
            zInfo.Contract.EmployeeGender = EmployeeGender.Trim();
            DateTime zEmployeeBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(EmployeeBirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zEmployeeBirthDay);

            zInfo.Contract.EmployeeBirthDay = zEmployeeBirthDay;
            zInfo.Contract.EmployeeBirthPlace = EmployeeBirthPlace.Trim();
            zInfo.Contract.EmployeePassport = EmployeePassport.Trim();
            DateTime zEmployeeIssueDate = DateTime.MinValue;
            DateTime.TryParseExact(EmployeeIssueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zEmployeeIssueDate);

            zInfo.Contract.EmployeeIssueDate = zEmployeeIssueDate;
            zInfo.Contract.EmployeeIssuePlace = EmployeeIssuePlace.Trim();
            zInfo.Contract.EmployeeAdress = EmployeeAdress.Trim();

            zInfo.Contract.ReportToKey = ReportToKey.Trim();
            zInfo.Contract.ReportToName = ReportToName.Trim();
            zInfo.Contract.DepartmentKey = DepartmentKey;
            zInfo.Contract.DepartmentName = DepartmentName.Trim();
            zInfo.Contract.PositionKey = PositionKey;
            zInfo.Contract.PositionName = PositionName.Trim();
            zInfo.Contract.BranchAdress = BranchAdress.Trim();
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Contract.FromDate = zFromDate;
            if (ToDate != String.Empty)
            {
                DateTime zToDate = DateTime.MinValue;
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zInfo.Contract.ToDate = zToDate;
            }
            else
            {
                zInfo.Contract.ToDate = DateTime.MinValue;
            }

            DateTime zDateBeginSalary = DateTime.MinValue;
            DateTime.TryParseExact(DateBeginSalary, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateBeginSalary);

            zInfo.Contract.DateBeginSalary = zDateBeginSalary;
            zInfo.Contract.Note = Note.Trim();

            zInfo.Contract.CreatedBy = UserLog.UserKey;
            zInfo.Contract.CreatedName = UserLog.EmployeeName;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Contract.ContractKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //Partial view file
        [HttpGet]
        public ActionResult ListFile(string ContractKey = "")
        {
            List<Document_Model> zList = Document_Data.List(Helper.PartnerNumber, ContractKey);
            return PartialView("~/Views/Shared/_DocumentAttach.cshtml", zList);
        }
        [HttpPost]
        public JsonResult DeleteFile(int id)
        {
            ServerResult zResult = new ServerResult();
            var zInfo = new Document_Info(id);
            zInfo.Delete();
            if (zInfo.Document.Code == "200")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Document.Message;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UploadFile()
        {
            ServerResult zResult = new ServerResult();
            if (Request.Files.Count > 0)
            {
                try
                {
                    string ContractKey = Request["ContractKey"];
                    HttpFileCollectionBase files = Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fileExt = Path.GetExtension(file.FileName);
                        string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileReName = TN_Utils.ToEnglish(fileName) + fileExt;

                        #region[Upload]
                        string zFilePath = Helper.UploadPath + "/Employee/" + ContractKey + "/";
                        string zFileSave = Path.Combine(Server.MapPath(zFilePath), fileReName);

                        // Check Foder
                        DirectoryInfo zDir = new DirectoryInfo(Server.MapPath(zFilePath));
                        if (!zDir.Exists)
                        {
                            zDir.Create();
                        }
                        else
                        {
                            if (System.IO.File.Exists(zFileSave))
                            {
                                System.IO.File.Delete(zFileSave);
                            }
                        }

                        file.SaveAs(zFileSave);
                        #endregion

                        Document_Model zModel = new Document_Model();
                        zModel.TableKey = ContractKey;
                        zModel.TableJoin = Helper.TableContractEmployee;

                        zModel.FileExt = fileExt;
                        zModel.FileName = fileName;
                        zModel.FilePath = (zFilePath + fileReName);
                        zModel.PartnerNumber = Helper.PartnerNumber;
                        zModel.CreatedBy = UserLog.CreatedBy;
                        zModel.CreatedName = UserLog.CreatedName;
                        zModel.ModifiedBy = UserLog.ModifiedBy;
                        zModel.ModifiedName = UserLog.ModifiedName;

                        Document_Info zInfo = new Document_Info();
                        zInfo.Document = zModel;
                        zInfo.Create_ServerKey();

                        if (zInfo.Document.Code != "200" &&
                            zInfo.Document.Code != "201")
                        {
                            zResult.Message = "Lỗi upload tập tin !." + fileName;
                            zResult.Success = false;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }

                    zResult.Success = true;
                }
                catch (Exception ex)
                {
                    zResult.Message = ex.Message;
                }
            }
            else
            {
                zResult.Message = "Không có tập tin";
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult Filter(string ViewPage)
        {
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectAge = Helper.SelectData("Tuoi");
            ViewBag.SelectEdu = Helper.SelectData("Trinhdo");
            ViewBag.ViewPage = ViewPage;
            return PartialView("~/Views/Employee/Filter.cshtml");
        }

        public ActionResult Search(
            string SearchName = "", string Department = "", string FromDate = "", string ToDate = "",
            string Age = "", string Edu = "", int Gender = 0, string Position = "", string ViewPage = "",
            int Status = 0, string Social = "KO")
        {
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zFromDate);
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zToDate);

            int FromAge = 0, ToAge = 0;
            if (Age.Contains('-'))
            {
                FromAge = Age.Split('-')[0].ToInt();
                ToAge = Age.Split('-')[1].ToInt();
            }
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);

            switch (ViewPage)
            {
                default:
                    ViewBag.ListData = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status, Social);
                    return View("~/Views/Employee/List.cshtml");

                case "CONTRACT":
                    var zList = Contract_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status, Social);
                    ViewBag.ListSelectType = ContractType_Data.ListType(Helper.PartnerNumber);
                    ViewBag.ListData = zList;
                    Session["ContractExcel"] = zList;
                    return View("~/Views/Employee/Contract/ContractList.cshtml");

                case "LIST":
                    var Danhsach = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status, Social);
                    ViewBag.ListData = Danhsach;
                    Session["DSCBN"] = Danhsach;
                    return View("~/Views/Employee/List.cshtml");

                case "MAP":
                    ViewBag.ListData = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status, Social);
                    return View("~/Views/Employee/Map.cshtml");

                    //case "LEAVE":
                    //    ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
                    //    ViewBag.ListData = Leave_Note_Data.Search(SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status);
                    //    return View("~/Views/Employee/Leave/List.cshtml");

                    //case "TRACK":
                    //    ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
                    //    ViewBag.ListData = Leave_Note_Data.Search(SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status);
                    //    //ViewBag.ListData = Leave_Close_Data.Track(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position, Status);
                    //    return View("~/Views/Employee/Leave/List.cshtml");
            }
        }

        public ActionResult ExportList()
        {
            if (Session["DSCBN"] != null)
            {
                var list = Session["DSCBN"] as List<Employee_Model>;
                var Filename = "CBNV.xls";
                var stream = Template_ListEmployee(list);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
            }
            return RedirectToAction("List");
        }

        [DisplayName("Xem danh sách nhân sự")]
        [Route("danh-sach-nhan-su")]
        public ActionResult List()
        {
            var zList = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = zList;
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);

            Session["DSCBN"] = zList;
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem danh sách nhân sự ");
            //-------------------------------------------------------------------------------------------------

            return View();
        }



        [HttpGet]
        public JsonResult GetID()
        {
            string ID = Helper.AutoEmployeeID("AB", Helper.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Personal(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }

        #region [Info View Page]
        //View Total
        [DisplayName("Chi tiết nhân sự")]
        public ActionResult Info(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }
        public ActionResult History(string EmployeeKey = "")
        {
            ViewBag.ListContract = Contract_Data.List(Helper.PartnerNumber, EmployeeKey);
            return View("~/Views/Employee/Profile/History.cshtml");
        }
        public ActionResult General(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View("~/Views/Employee/Profile/General.cshtml", zObj);
        }
        public ActionResult Gross(string EmployeeKey = "")
        {
            //ViewBag.ListData = Employee_Data.GetPayrollClose(Helper.PartnerNumber, DateTime.Now.ToString("MM/yyyy"), EmployeeKey);

            var employee = new Employee_Info(EmployeeKey).Employee;
            var name = "";
            if (employee.Gender == 1)
                name = " ông " + employee.LastName + " " + employee.FirstName;
            else
                name = " bà " + employee.LastName + " " + employee.FirstName;

            string TieuDe = "Các khoản thanh toán lương của" + name;
            ViewBag.TieuDe = TieuDe;

            var zTable = Employee_Data.PayrollIndex_Employee(Helper.PartnerNumber, DateTime.Now.Year, 0, EmployeeKey);

            var pvt = new PivotTable(zTable);
            var tblresult = pvt.Generate("Month", "ItemName", "Amount", "Nội dung", "Tổng", "Tổng");

            ViewBag.Table = tblresult;
            return View("~/Views/Employee/Profile/Gross.cshtml");
        }
        public ActionResult GrossDetail(string EmployeeKey = "", string CloseDate = "")
        {
            //ViewBag.ListData = Employee_Data.GetPayrollClose(Helper.PartnerNumber, CloseDate, EmployeeKey);
            return View("~/Views/Employee/Profile/GrossDetail.cshtml");
        }

        public ActionResult Seniority(string EmployeeKey = "")
        {
            var employee = new Employee_Info(EmployeeKey).Employee;
            var name = employee.LastName + " " + employee.FirstName;
            var chucvu = employee.PositionName;
            //var thamnien = Employee_Data.SoThamNien(EmployeeKey, DateTime.Now.Year.ToString());
            //TN_Utils.CalculateYourTime(employee.StartingDate, DateTime.Now, out int Year);
            //ViewBag.ThamNien = thamnien;
            ViewBag.HoTen = name;
            ViewBag.ChucVu = chucvu;

            #region nghĩ phép
            string year = DateTime.Now.Year.ToString();
            //dữ liệu phát sinh
            var zList = Employee_Data.CacDonPhep(EmployeeKey, year);
            ViewBag.ListData = zList;

            //xử lý thông tin phép
            double SoNgayNghi = zList.Sum(s => s.TotalDate);
            double conlai = 0;

            //xử lý đầu phép
            var db = new Models.TN_ERP_V01Entities();
            int y = year.ToInt();
            var data = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == EmployeeKey && s.CloseYear == y).First();
            if (data != null)
            {
                ViewBag.TongPhepSuDung = data.ConLai;
                ViewBag.TieuChuan = data.TieuChuan + data.ThamNien;
                ViewBag.ThamNien = data.ThamNien;
                ViewBag.CongTac = Employee_Data.SoThamNien(EmployeeKey, year);
                ViewBag.NamGanNhat = data.HaiNamGanNhat;
            }
            else
            {
                //tính lại phép nếu chưa có "trường hợp qua năm"
                y = year.ToInt() - 1;
                var kyphepvuaroi = db.HRM_Leave_Close_New.Where(s => s.EmployeeKey == EmployeeKey && s.CloseYear == y).First();
                if (kyphepvuaroi != null)
                {
                    //đã có dữ liệu trước đó
                    //1 lay dữ liệu phát sinh trong năm đó
                    var zold = Employee_Data.CacDonPhep(EmployeeKey, y.ToString());
                    double tongnghi = zold.Sum(s => s.TotalDate);
                    double tinhconlai = kyphepvuaroi.HaiNamGanNhat.Value - tongnghi;
                    //tính hiện tại
                    int thamnien = Employee_Data.SoThamNien(EmployeeKey, year).ToInt();
                    int tieuchuan = 12;

                    var rec = db.HRM_Leave_Close_New.Find(kyphepvuaroi.CloseKey);
                    if (rec != null)
                    {
                        rec.TongCong = tinhconlai + thamnien + tieuchuan;
                        rec.ThamNien = thamnien;
                        rec.TieuChuan = tieuchuan;
                        rec.ConLai = tinhconlai;
                        rec.ModifiedBy = UserLog.UserKey;
                        rec.ModifiedName = UserLog.EmployeeName;
                        rec.ModifiedOn = DateTime.Now;

                        db.SaveChanges();
                    }
                }
                else
                {
                    //chưa có dữ liệu có thể nhân viên mới vào làm
                    db.HRM_Leave_Close_New.Add(new Models.HRM_Leave_Close_New()
                    {
                        CloseYear = DateTime.Now.Year,
                        HaiNamGanNhat = 0,
                        ConLai = 0,
                        ThamNien = 0,
                        TieuChuan = 12,
                        TongCong = 12,
                        //
                        RecordStatus = 0,
                        CreatedBy = UserLog.UserKey,
                        CreatedName = UserLog.EmployeeName,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = UserLog.UserKey,
                        ModifiedName = UserLog.EmployeeName,
                        ModifiedOn = DateTime.Now,
                    });
                    db.SaveChanges();

                    ViewBag.TongPhepSuDung = 12;
                    ViewBag.TieuChuan = 12;
                    ViewBag.ThamNien = 0;
                    ViewBag.CongTac = 0;
                    ViewBag.NamGanNhat = 0;
                }
            }

            if (data != null)
            {
                conlai = data.ConLai.Value - SoNgayNghi;
                ViewBag.SuDung = SoNgayNghi;
                ViewBag.ConLai = conlai == 0 ? data.ConLai : conlai;
            }
            else
            {
                ViewBag.SuDung = SoNgayNghi;
                ViewBag.ConLai = 12 - SoNgayNghi;
            }
            #endregion

            return View("~/Views/Staff/_DataNghiPhep.cshtml");
            //phép cũ
            //ViewBag.ListData = Employee_Data.XemKyPhep(EmployeeKey, out _);
            //return View("~/Views/Employee/Profile/Seniority.cshtml");
        }
        public ActionResult Review(string EmployeeKey = "")
        {
            ViewBag.ListData = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            return View("~/Views/Employee/Profile/Review.cshtml");
        }
        #endregion

        public ActionResult ReviewQuick(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;

            Employee_Info zReport = new Employee_Info(zModel.ReportToKey);
            ViewBag.ReportKey = zReport.Employee.EmployeeKey;
            ViewBag.ReportName = zReport.Employee.LastName + " " + zReport.Employee.FirstName;
            ViewBag.ReportPosition = zReport.Employee.PositionName;

            ViewBag.ListItem = Criteria_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Profile/ReviewQuick.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult ReviewSave(string Obj)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                Review_Model zReview = JsonConvert.DeserializeObject<Review_Model>(Obj);
                zReview.ReviewKey = Guid.NewGuid().ToString();
                zReview.PartnerNumber = Helper.PartnerNumber;
                zReview.DateWrite = DateTime.Now;
                zReview.CreatedBy = UserLog.UserKey;
                zReview.CreatedName = UserLog.EmployeeName;
                zReview.ModifiedBy = UserLog.UserKey;
                zReview.ModifiedName = UserLog.EmployeeName;

                Review_Info zInfo = new Review_Info();
                zInfo.Review = zReview;
                zInfo.Create_ClientKey();

                if (zInfo.Review.Code == "200" ||
                    zInfo.Review.Code == "201")
                {
                    foreach (Review_Detail_Model zItem in zReview.ListItem)
                    {
                        Review_Detail_Info zDetail = new Review_Detail_Info();
                        zDetail.Review_Detail.ReviewKey = zReview.ReviewKey;
                        zDetail.Review_Detail.CriteriaKey = zItem.CriteriaKey;
                        zDetail.Review_Detail.CriteriaName = zItem.CriteriaName;
                        zDetail.Review_Detail.Point = zItem.Point;
                        zDetail.Review_Detail.MaxPoint = zItem.MaxPoint;
                        zDetail.Review_Detail.PartnerNumber = Helper.PartnerNumber;

                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Review.Message;
                }

                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region [----------------FUNCTION BASIC ---------------]
        [HttpGet]
        public JsonResult GetInfoEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        private string SaveCloseLeave(Leave_Close_Model Item)
        {
            Leave_Close_Info zInfo = new Leave_Close_Info(Item.Reference);
            zInfo.Leave_Close.Reference = Item.Reference;
            zInfo.Leave_Close.EmployeeKey = Item.EmployeeKey;
            zInfo.Leave_Close.EmployeeName = Item.EmployeeName;
            zInfo.Leave_Close.EmployeeID = Item.EmployeeID;
            zInfo.Leave_Close.CloseEnd = Item.CloseEnd;
            zInfo.Leave_Close.CloseYear = Item.CloseYear;
            zInfo.Leave_Close.Description = Item.Description;
            zInfo.Leave_Close.Slug = 1; //mặt định ẩn trong form tiêu chuẩn phép
            zInfo.Leave_Close.PartnerNumber = Helper.PartnerNumber;
            zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
            zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
            zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
            zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Leave_Close.CloseKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            if (zInfo.Leave_Close.Code == "200" ||
                zInfo.Leave_Close.Code == "201")
            {
                return string.Empty;
            }
            else
            {
                return zInfo.Leave_Close.Message;
            }
        }
        private string GenerateUL(List<Employee_Model> listItem, List<Employee_Model> table, StringBuilder sb)
        {
            if (table.Count > 0)
            {
                sb.AppendLine(" <ul class=''>");

                if (listItem.Count > 0)
                {
                    foreach (Employee_Model dr in listItem)
                    {
                        string line = "<li>" + ItemInfo(dr);
                        sb.Append(line);
                        List<Employee_Model> subMenu = table.FindAll(x => x.ReportToKey == dr.EmployeeKey);
                        if (subMenu.Count > 0)
                        {
                            var subMenuBuilder = new StringBuilder();
                            sb.Append(GenerateUL(subMenu, table, subMenuBuilder));
                        }
                        sb.Append("</li>");
                    }
                }
                sb.Append("</ul>");
                return sb.ToString();
            }
            else
            {
                return sb.ToString();
            }
        }
        private string ItemInfo(Employee_Model Employee)
        {
            string img = Helper.DefaultImage;
            if (Employee.PhotoPath.Length > 0)
            {
                img = Url.Content(Employee.PhotoPath);
            }

            string Html = @"
                        <a href='javascript: void(0); '>
                              <div class='member-view-box'>
                                <div class='member-image'>
                                    <img src = '" + img + @"' alt='Member'>
                                    <span class='btn btn-outline-primary border-0 exInfo' name='" + Employee.EmployeeKey + @"'
                                          data-toggle='tooltip' data-placement='right' title='' data-original-title='Hồ sơ'>
                                        <i class='fas fa-lg fa-info-circle'></i>
                                    </span>
                                    <span class='btn btn-outline-warning border-0 exRate d-none' name='" + Employee.EmployeeKey + @"'
                                          data-toggle='tooltip' data-placement='right' title='' data-original-title='Đánh giá'>
                                        <i class='far fa-star'></i>
                                    </span>                                    
                                    <div class='member-details'>
                                        <h3>" + Employee.LastName + " " + Employee.FirstName + @"</h3>
                                    </div>
                                </div>
                            </div>
                        </a>";

            return Html;
        }
        private string StoreFilePost(HttpPostedFileBase[] files, string Key)
        {
            string zFilePath = "";
            string fileName = "";
            try
            {
                if (files.Length > 0)
                {
                    //Upload hình
                    foreach (HttpPostedFileBase file in files)
                    {
                        //Checking file is available to save.  
                        if (file != null && file.ContentLength > 0)
                        {
                            string strGui = Guid.NewGuid().ToString();
                            fileName = strGui + System.IO.Path.GetExtension(file.FileName);

                            #region[Upload]   
                            zFilePath = Helper.UploadPath + "/Employee/" + Key + "/";
                            string zFileSave = Path.Combine(Server.MapPath(zFilePath), fileName);

                            // Check Foder
                            DirectoryInfo zDir = new DirectoryInfo(Server.MapPath(zFilePath));
                            if (!zDir.Exists)
                            {
                                zDir.Create();
                            }
                            else
                            {
                                if (System.IO.File.Exists(zFileSave))
                                {
                                    System.IO.File.Delete(zFileSave);
                                }
                            }

                            file.SaveAs(zFileSave);
                            #endregion
                        }
                    }
                }

                return zFilePath + fileName;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        #endregion

        #region [-------------TÍNH LƯƠNG MỚI----------------]
        public ActionResult ViewPersonal()
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.Year = DateTime.Now.Year;
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.Employee = 0;

            return View("~/Views/Employee/Dashboard/Personal.cshtml");
        }
        public ActionResult SearchPersonal(int Year = 0, int Month = 0, string Employee = "", string sbutton = "")
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            if (Year == 0)
            {
                ViewBag.TieuDe = "Năm tìm kiếm không đúng !.";
                return View("~/Views/Employee/Dashboard/Personal.cshtml");
            }
            if (Employee == "")
            {
                ViewBag.TieuDe = "Bạn phải chọn 1 nhân sự !.";
                return View("~/Views/Employee/Dashboard/Personal.cshtml");
            }

            var employee = new Employee_Info(Employee).Employee;
            var name = "";
            if (employee.Gender == 1)
                name = " ông " + employee.LastName + " " + employee.FirstName;
            else
                name = " bà " + employee.LastName + " " + employee.FirstName;

            string TieuDe = "Các khoản thanh toán lương của" + name;
            ViewBag.TieuDe = TieuDe;

            var zTable = Employee_Data.PayrollIndex_Employee(Helper.PartnerNumber, Year, Month, Employee);
            var pvt = new PivotTable(zTable);
            var tblresult = pvt.Generate("Month", "ItemName", "Amount", "Nội dung", "Tổng", "Tổng");

            if (sbutton == "btn_Export")
            {
                var Filename = "BANGLUONG " + name + ".xls";
                var stream = Template_PersonalTable(tblresult, string.Empty, TieuDe);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
            }
            else
            {
                ViewBag.Table = tblresult;
                ViewBag.Year = Year;
                ViewBag.Month = Month;
                ViewBag.Employee = Employee;
                return View("~/Views/Employee/Dashboard/Personal.cshtml");
            }
        }
        private MemoryStream Template_PersonalTable(DataTable zTable, string Description, string Title)
        {
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("LUONGNHANSU");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = Title;
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                //worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                //worksheet.Row(4).Style.Font.Bold = true;
                //worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "Nội dung";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count - 1; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = "Tháng " + Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }
                worksheet.Cells[row, MaxColumn].Value = "Tổng";
                worksheet.Cells[row, MaxColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                int No = 1;
                //foreach (DataRow r in zTable.Rows)
                for (int k = 0; k < zTable.Rows.Count - 1; k++)
                {
                    DataRow r = zTable.Rows[k];

                    row++;
                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 0; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 1)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Column(i).Width = 15;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }
                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }


                #endregion
                //Custom
                worksheet.Column(1).Width = 35;
                worksheet.Column(MaxColumn).Width = 15;
                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }

        public ActionResult ViewBranch()
        {
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.Year = DateTime.Now.Year;
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.Branch = 0;
            return View("~/Views/Employee/Dashboard/Branch.cshtml");
        }
        public ActionResult SearchBranch(int Year = 0, int Month = 0, string Branch = "", string sbutton = "")
        {
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            if (Year == 0)
            {
                ViewBag.TieuDe = "Năm tìm kiếm không đúng !.";
                return View("~/Views/Employee/Dashboard/Branch.cshtml");
            }

            string name = "";
            if (Branch != "0")
                name = new Branch_Info(Branch).Branch.BranchName;
            else
                name = " các chi nhánh";
            string TieuDe = "Các khoản thanh toán chi phí lương " + name;
            ViewBag.TieuDe = TieuDe;

            var zTable = Employee_Data.PayrollIndex_Branch(Helper.PartnerNumber, Year, Month, Branch);
            var pvt = new PivotTable(zTable);
            var tblresult = pvt.Generate("Month", "NOIDUNG", "Amount", "Nội dung", "Tổng", "Tổng");

            if (sbutton == "btn_Export")
            {
                var Filename = "LUONGBOPHAN.xls";
                var stream = Template_BranchTable(tblresult, string.Empty, TieuDe);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
            }
            else
            {

                ViewBag.TableData = tblresult;
                ViewBag.TieuDe = TieuDe;
                ViewBag.Year = Year;
                ViewBag.Month = Month;
                ViewBag.Branch = Branch;
            }
            return View("~/Views/Employee/Dashboard/Branch.cshtml");
        }
        private MemoryStream Template_BranchTable(DataTable zTable, string Description, string Title)
        {
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANGCHIPHICACBOPHAN");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = Title;
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                //worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                //worksheet.Row(4).Style.Font.Bold = true;
                //worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "Nội dung";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = "Tháng " + Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }
                worksheet.Cells[row, MaxColumn].Value = "Tổng";
                worksheet.Cells[row, MaxColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 0;
                //foreach (DataRow r in zTable.Rows)
                for (int k = 0; k < zTable.Rows.Count - 1; k++)
                {
                    DataRow r = zTable.Rows[k];

                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString().Split(':')[0];
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;
                        No = 0;

                        worksheet.Cells[row, 1, row, MaxColumn].Value = Branch;
                        worksheet.Cells[row, 1, row, MaxColumn].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = r[0].ToString().Split(':')[1].Replace("&emsp;", "");
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count - 1; i++)
                    {
                        string val = r[i].ToString();
                        worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                        worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Column(i + 1).Width = 13;
                    }
                    if (No < 3)
                    {
                        worksheet.Cells[row, MaxColumn].Value = "";
                        worksheet.Cells[row, MaxColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        worksheet.Cells[row, MaxColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Column(MaxColumn).Width = 13;
                    }
                    else
                    {

                        worksheet.Cells[row, MaxColumn].Value = r[MaxColumn - 1].ToDouble().ToString("n0");
                        worksheet.Cells[row, MaxColumn].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        worksheet.Cells[row, MaxColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Column(MaxColumn).Width = 13;
                    }

                    No++;
                    #endregion
                }


                #endregion
                //Custom
                worksheet.Column(1).Width = 35;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }

        public ActionResult ViewMonth()
        {
            ViewBag.Year = DateTime.Now.Year;
            ViewBag.Month = DateTime.Now.Month;
            return View("~/Views/Employee/Dashboard/MonthPay.cshtml");
        }
        public ActionResult SearchMonth(int Year = 0, int Month = 0, string sbutton = "")
        {
            if (Year == 0)
            {
                ViewBag.TieuDe = "Năm tìm kiếm không đúng !.";
                return View("~/Views/Employee/Dashboard/MonthPay.cshtml");
            }
            if (sbutton == "btn_Export")
            {
                ViewBag.TieuDe = "Xuất Excel";
                return View("~/Views/Employee/Dashboard/MonthPay.cshtml");
            }


            string name = "";
            if (Month != 0)
            {

                string MonthYear = Month.ToString("00") + "/" + Year;
                var zInfo = new Payroll_Close_Info(MonthYear, true);
                var Id = zInfo.Payroll_Close.CloseKey;
                var zList = new List<Payroll_Item>();
                zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, string.Empty);

                ViewBag.InsuranceTable = InsuranceTable(zList);
                ViewBag.OverTimeTable = OverTimeTable(zList);
                ViewBag.SupportTable = SupportTable(zList);
                ViewBag.PaymentTable = PaymentTable(zList);
                ViewBag.OveralTable = OveralTable(Id);

                Session["PayrollID"] = Id;
                name = "Các khoản thanh toán lương của tháng " + Month + "/" + Year;
            }
            else
            {
                string MonthParam = "";
                for (int i = 1; i <= 12; i++)
                {
                    MonthParam += i.ToString("00") + ",";
                }
                string Branch = "";
                var ListBranch = Branch_Data.List(UserLog.PartnerNumber);
                foreach (var item in ListBranch)
                {
                    Branch += item.BranchKey + ",";
                }
                Branch = Branch.Remove(Branch.LastIndexOf(","));

                var zList = new List<Payroll_Item>();
                zList = Employee_Data.PayrollData(UserLog.PartnerNumber, MonthParam, Year, Branch);

                ViewBag.InsuranceTable = InsuranceTable(zList);
                ViewBag.OverTimeTable = OverTimeTable(zList);
                ViewBag.SupportTable = SupportTable(zList);
                ViewBag.PaymentTable = PaymentTable(zList);
                //ViewBag.OveralTable = OveralTable(Id);

                name = "Tổng các khoản thanh toán lương của năm " + Year;
            }

            ViewBag.TieuDe = name;
            ViewBag.Year = Year;
            ViewBag.Month = Month;
            return View("~/Views/Employee/Dashboard/MonthPay.cshtml");
        }

        public ActionResult ViewPayroll(string Id)
        {
            var zInfo = new Payroll_Close_Info(Id);
            if (zInfo.Code == "200")
            {
                var zList = new List<Payroll_Item>();
                zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, string.Empty);
                if (zList.Count == 0)
                {
                    ViewBag.Message = "Bạn chưa lưu bảng tính chi tiết ở bước 5 trong phần tính lương bạn phải thực hiện lại !.";
                }
                else
                {
                    ViewBag.InsuranceTable = InsuranceTable(zList);
                    ViewBag.OverTimeTable = OverTimeTable(zList);
                    ViewBag.SupportTable = SupportTable(zList);
                    ViewBag.PaymentTable = PaymentTable(zList);
                    ViewBag.OveralTable = OveralTable(Id);

                    string Message = "";
                    ViewBag.NoteTruLuong = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TruLuong' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteNgoaiGio = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_NgoaiGio' AND RecordStatus <> 99", out Message);
                    ViewBag.NotePhuCap = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_PhuCap' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteThanhToan = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_ThanhToan' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteTongHop = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TongHop' AND RecordStatus <> 99", out Message);
                    Session["PayrollID"] = Id;
                }
            }
            else
            {
                ViewBag.Message = zInfo.Message;
            }
            return View("~/Views/Employee/Salary/ViewPayroll.cshtml", zInfo.Payroll_Close);
        }
        public JsonResult DeletePayroll(string CloseKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Payroll_Close_Info(CloseKey);
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PayrollTable()
        {
            var zList = Payroll_Close_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = zList;
            return View("~/Views/Employee/Salary/PayrollTable.cshtml");
        }
        /// <summary>
        /// khởi tạo bảng lương mới
        /// </summary>
        /// <param name="CloseDate"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
        public ActionResult InitNewPayroll(string CloseDate, string Title, string Description)
        {
            var zInfo = new Payroll_Close_Info(CloseDate, true);
            if (zInfo.Payroll_Close.CloseKey.Length >= 36)
            {
                return RedirectToAction("SetupPayroll", new { Id = zInfo.Payroll_Close.CloseKey });
            }

            string Id = Guid.NewGuid().ToString();
            Session["PayrollID"] = Id;

            #region [1 - init Parent Data]
            zInfo = new Payroll_Close_Info();
            zInfo.Payroll_Close.CloseKey = Id;
            zInfo.Payroll_Close.Title = Title;
            zInfo.Payroll_Close.Description = Description;
            zInfo.Payroll_Close.CloseDate = CloseDate;
            zInfo.Payroll_Close.PartnerNumber = Helper.PartnerNumber;
            zInfo.Payroll_Close.CreatedBy = UserLog.CreatedBy;
            zInfo.Payroll_Close.CreatedName = UserLog.UserName;
            zInfo.Payroll_Close.ModifiedBy = UserLog.CreatedBy;
            zInfo.Payroll_Close.ModifiedName = UserLog.UserName;
            zInfo.Create_ClientKey();
            #endregion

            #region [2 - init Employee]
            var zList = Employee_Data.List(Helper.PartnerNumber);

            StringBuilder Query = new StringBuilder();
            foreach (var rec in zList)
            {
                string SQL = "";
                SQL += " INSERT INTO HRM_Payroll_Close_Detail ";
                SQL += " (ParentKey , CloseDate , EmployeeKey)";
                SQL += " VALUES ";
                SQL += " (@ParentKey , @CloseDate , @EmployeeKey)";

                SQL = SQL.Replace("@ParentKey", "'" + zInfo.Payroll_Close.CloseKey + "'");
                SQL = SQL.Replace("@CloseDate", "'" + zInfo.Payroll_Close.CloseDate + "'");
                SQL = SQL.Replace("@EmployeeKey", "'" + rec.EmployeeKey + "'");

                Query.AppendLine(SQL);
            }

            Helper.RunSQL(Query.ToString(), out _);
            #endregion

            return RedirectToAction("SetupPayroll", new { Id });
        }
        public ActionResult SetupPayroll(string Id)
        {
            ViewBag.ID = Id;
            Session["PayrollID"] = Id;
            return View("~/Views/Employee/Salary/SetupPayroll.cshtml");
        }
        public ActionResult PayrollStep1(string Id)
        {
            var zList = Employee_Data.Ready_Payroll(Helper.PartnerNumber, Id);
            ViewBag.ListData = zList;
            return PartialView("~/Views/Employee/Salary/_Step1.cshtml");
        }
        public ActionResult PayrollStep2(string Id)
        {
            ViewBag.ListData = Work_Day_Data.List(Helper.PartnerNumber, Id);
            return PartialView("~/Views/Employee/Salary/_Step2.cshtml");
        }
        public ActionResult PayrollStep3(string Id)
        {
            ViewBag.ListData = Over_Time_Data.List(Helper.PartnerNumber, Id);
            return PartialView("~/Views/Employee/Salary/_Step3.cshtml");
        }
        public ActionResult PayrollStep31(string Id)
        {
            ViewBag.ListData = Payroll_Support_Data.List(Helper.PartnerNumber, Id);
            return PartialView("~/Views/Employee/Salary/_Step31.cshtml");
        }
        public ActionResult PayrollStep4(string Id)
        {
            var zInfo = new Payroll_Close_Info(Id);
            var zListEmployee = new List<Employee_Model>();
            var zListPayroll = new List<Payroll_Item>();
            try
            {
                string Date = zInfo.Payroll_Close.CloseDate;
                int Month = Date.Split('/')[0].ToInt();
                int Year = Date.Split('/')[1].ToInt();
                if (Month == 0)
                {
                    Month = 12;
                    Year--;
                }

                var zDate = new DateTime(Year, Month, 1, 0, 0, 0);
                zDate = zDate.AddMonths(1).AddDays(-1);
                zDate = new DateTime(zDate.Year, zDate.Month, zDate.Day, 0, 0, 0);
                zListEmployee = Employee_Data.Ready_Payroll(Helper.PartnerNumber, Id);//.Where(s => s.EmployeeKey == "df463a61-6474-42c1-8fcf-c847dbe0441d").ToList();
                foreach (Employee_Model zEmployee in zListEmployee)
                {
                    string EmployeeKey = zEmployee.EmployeeKey;
                    string EmployeeName = zEmployee.LastName + " " + zEmployee.FirstName;
                    string EmployeeID = zEmployee.EmployeeID;
                    string Position = zEmployee.PositionName;
                    string Branch = zEmployee.BranchName;
                    string Department = zEmployee.DepartmentName;
                    string Style = zEmployee.Style;
                    string DepartmentKey = zEmployee.DepartmentKey;
                    string BranchKey = zEmployee.BranchKey;

                    zListPayroll.AddRange(CalculatorPayroll(EmployeeKey, EmployeeName, EmployeeID, Position, Department, Branch, zDate, Style.Trim(), DepartmentKey, BranchKey, Id));
                }

                //sắp xếp dữ liệu
                zListPayroll = zListPayroll
                  .OrderByDescending(o => o.Branch)
                  .ThenBy(o => o.EmployeeID)
                  .ToList();

                ViewBag.PayrollId = Id;
                ViewBag.PivotTable = PopulateGrid(zListPayroll, out _);
                ViewBag.Description = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S4' AND RecordStatus <> 99", out string Message);

                Session["PayrollTable"] = zListPayroll;
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Lỗi -" + ex.ToString();
            }

            return PartialView("~/Views/Employee/Salary/_Step4.cshtml");
        }
        public ActionResult PayrollStep5(string Id)
        {
            var zInfo = new Payroll_Close_Info(Id);
            if (zInfo.Code == "200")
            {
                var zList = new List<Payroll_Item>();
                zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, string.Empty);
                if (zList.Count == 0)
                {
                    ViewBag.Message = "Bạn chưa lưu bảng tính chi tiết ở bước 4 vui lòng quay về để thực hiện lại !.";
                }
                else
                {
                    ViewBag.InsuranceTable = InsuranceTable(zList);
                    ViewBag.OverTimeTable = OverTimeTable(zList);
                    ViewBag.SupportTable = SupportTable(zList);
                    ViewBag.PaymentTable = PaymentTable(zList);
                    ViewBag.OveralTable = OveralTable(Id);
                    TempData.Keep();

                    string Message = "";
                    ViewBag.NoteTruLuong = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TruLuong' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteNgoaiGio = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_NgoaiGio' AND RecordStatus <> 99", out Message);
                    ViewBag.NotePhuCap = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_PhuCap' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteThanhToan = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_ThanhToan' AND RecordStatus <> 99", out Message);
                    ViewBag.NoteTongHop = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TongHop' AND RecordStatus <> 99", out Message);
                }
            }
            else
            {
                ViewBag.Message = zInfo.Message;
            }

            return PartialView("~/Views/Employee/Salary/_Step5.cshtml", zInfo.Payroll_Close);
        }

        #region [Action - Step - 1 Employee]
        [HttpPost]
        public JsonResult DeleteStep1(string EmployeeKey)
        {
            var zResult = new ServerResult();
            if (Session["PayrollID"] == null)
            {
                zResult.Message = "Đã hết phiên làm việc vui lòng đăng nhập lại !.";
                zResult.Success = false;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            string PayrollID = Session["PayrollID"].ToString();
            var zInfo = new Payroll_Close_Detail_Info(PayrollID, EmployeeKey);
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Action - Step - 2 Work-Days]
        [HttpPost]
        public JsonResult DeleteStep2(int AutoKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Work_Day_Info(AutoKey);
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditStep2(int AutoKey)
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            var zModel = new Work_Day_Model();
            var zInfo = new Work_Day_Info(AutoKey);
            if (zInfo.Code == "404")
            {
                zModel.DateWrite = DateTime.Now.ToString("MM/yyyy");
            }
            else
            {
                zModel = zInfo.Work_Day;
            }

            return PartialView("~/Views/Employee/Salary/WorkDayEdit.cshtml", zModel);
        }
        [HttpPost]
        public JsonResult SaveStep2(string EmployeeKey, int Days, string DateWrite, string Description, int AutoKey = 0)
        {
            var zResult = new ServerResult();
            if (Session["PayrollID"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Phiên làm việc đã quá thời gian vui lòng làm lại !.";
            }

            try
            {
                string PayrollID = Session["PayrollID"].ToString();

                var zModel = new Work_Day_Model();
                zModel.ParentKey = PayrollID;
                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.Days = Days;
                zModel.DateWrite = DateWrite;
                zModel.Description = Description;

                Employee_Info zEmp = new Employee_Info(EmployeeKey);
                zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
                zModel.EmployeeID = zEmp.Employee.EmployeeID;
                zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
                zModel.BranchKey = zEmp.Employee.BranchKey;
                zModel.BranchName = zEmp.Employee.BranchName;
                zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
                zModel.DepartmentName = zEmp.Employee.DepartmentName;
                zModel.PositionKey = zEmp.Employee.PositionKey;
                zModel.PositionName = zEmp.Employee.PositionName;

                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Work_Day_Info zInfo = new Work_Day_Info(AutoKey);
                zInfo.Work_Day = zModel;
                if (AutoKey != 0)
                {
                    zInfo.Update();
                }
                else
                {
                    zInfo.Create_KeyAuto();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Action - Step - 3 Over-Time]
        public ActionResult OverTime()
        {
            ViewBag.ListData = Over_Time_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Salary/OverTimeList.cshtml");
        }

        public ActionResult OverTimeEdit(string OvertimeKey = "")
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            Over_Time_Info zInfo = new Over_Time_Info(OvertimeKey);
            Over_Time_Model zModel = zInfo.Over_Time;
            zModel.ListItem = Over_Time_Data.ListDetail(Helper.PartnerNumber, OvertimeKey);
            return PartialView("~/Views/Employee/Salary/OverTimeEdit.cshtml", zModel);
        }

        public ActionResult OverTimeRow()
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            return PartialView("~/Views/Employee/Salary/OverTimeRow.cshtml");
        }

        [HttpPost]
        public JsonResult SaveOverTime(string OverTimeObj)
        {
            ServerResult zResult = new ServerResult();
            if (Session["PayrollID"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Phiên làm việc đã quá thời gian vui lòng làm lại !.";
            }

            try
            {
                string PayrollID = Session["PayrollID"].ToString();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                Over_Time_Model zModel = JsonConvert.DeserializeObject<Over_Time_Model>(OverTimeObj, settings);
                Over_Time_Info zInfo = new Over_Time_Info();
                zModel.ParentKey = PayrollID;
                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.OverTimeDate = zModel.OverTimeDate;
                zModel.Paramater = zModel.Paramater;
                zModel.Description = zModel.Description.Trim();
                Employee_Info zEmp = new Employee_Info(UserLog.EmployeeKey);

                zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
                zModel.EmployeeID = zEmp.Employee.EmployeeID;
                zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
                zModel.BranchKey = zEmp.Employee.BranchKey;
                zModel.BranchName = zEmp.Employee.BranchName;
                zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
                zModel.DepartmentName = zEmp.Employee.DepartmentName;
                zModel.PositionKey = zEmp.Employee.PositionKey;
                zModel.PositionName = zEmp.Employee.PositionName;

                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.OvertimeKey.Length >= 36)
                {
                    zInfo.Over_Time = zModel;
                    zInfo.Update();
                }
                else
                {
                    zModel.OvertimeKey = Guid.NewGuid().ToString();
                    zInfo.Over_Time = zModel;
                    zInfo.Create_ClientKey();
                }

                if (zInfo.Over_Time.Code == "200" ||
                    zInfo.Over_Time.Code == "201")
                {
                    var layhesomoi = new Payroll_Sample_Info();
                    layhesomoi.CapNhatHeSo(zModel.Paramater, zModel.ItemKey);

                    zInfo.DeleteDetail();
                    foreach (Over_Time_Detail_Model zItem in zModel.ListItem)
                    {
                        Over_Time_Detail_Info zDetail = new Over_Time_Detail_Info();
                        zDetail.Over_Time_Detail = zItem;

                        zDetail.Over_Time_Detail.PartnerNumber = Helper.PartnerNumber;
                        zDetail.Over_Time_Detail.ParentKey = zModel.OvertimeKey;

                        Employee_Info zEmployee = new Employee_Info(zItem.EmployeeKey);
                        zDetail.Over_Time_Detail.EmployeeKey = zEmployee.Employee.EmployeeKey;
                        zDetail.Over_Time_Detail.EmployeeID = zEmployee.Employee.EmployeeID;
                        zDetail.Over_Time_Detail.EmployeeName = zEmployee.Employee.LastName + " " + zEmp.Employee.FirstName;
                        zDetail.Over_Time_Detail.BranchKey = zEmployee.Employee.BranchKey;
                        zDetail.Over_Time_Detail.BranchName = zEmployee.Employee.BranchName;
                        zDetail.Over_Time_Detail.DepartmentKey = zEmployee.Employee.DepartmentKey;
                        zDetail.Over_Time_Detail.DepartmentName = zEmployee.Employee.DepartmentName;
                        zDetail.Over_Time_Detail.PositionKey = zEmployee.Employee.PositionKey;
                        zDetail.Over_Time_Detail.PositionName = zEmployee.Employee.PositionName;

                        zDetail.Over_Time_Detail.Paramater = zInfo.Over_Time.Paramater;
                        zDetail.Over_Time_Detail.ItemKey = zInfo.Over_Time.ItemKey;
                        zDetail.Over_Time_Detail.ItemID = zInfo.Over_Time.ItemID;
                        zDetail.Over_Time_Detail.ItemName = zInfo.Over_Time.ItemName;
                        zDetail.Over_Time_Detail.OverTimeDate = zInfo.Over_Time.OverTimeDate;

                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Over_Time.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteOverTime(string OvertimeKey)
        {
            ServerResult zResult = new ServerResult();
            Over_Time_Info zInfo = new Over_Time_Info();
            zInfo.Over_Time.OvertimeKey = OvertimeKey;
            zInfo.Delete();
            Over_Time_Model zModel = zInfo.Over_Time;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Over_Time.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Action - Step - 31 Support]
        [HttpPost]
        public JsonResult DeleteStep31(int SupportKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Payroll_Support_Info(SupportKey);
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditStep31(int SupportKey)
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            var zModel = new Payroll_Support_Model();
            var zInfo = new Payroll_Support_Info(SupportKey);
            if (zInfo.Code == "404")
            {
                zModel.SupportDate = DateTime.Now;
            }
            else
            {
                zModel = zInfo.Payroll_Support;
            }

            return PartialView("~/Views/Employee/Salary/Support.cshtml", zModel);
        }
        [HttpPost]
        public JsonResult SaveStep31(int SupportKey, string EmployeeKey, string EmployeeName, string Description, string Money)
        {
            var zResult = new ServerResult();
            if (Session["PayrollID"] == null)
            {
                zResult.Success = false;
                zResult.Message = "Phiên làm việc đã quá thời gian vui lòng làm lại !.";
            }

            try
            {
                string PayrollID = Session["PayrollID"].ToString();
                var zClose = new Payroll_Close_Detail_Info(PayrollID, EmployeeKey);
                var ThangNam = zClose.Payroll_Close_Detail.CloseDate;
                var date = new DateTime(ThangNam.Split('/')[1].ToInt(), ThangNam.Split('/')[0].ToInt(), DateTime.Now.Day);

                var zModel = new Payroll_Support_Model();
                zModel.ParentKey = PayrollID;
                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.EmployeeKey = EmployeeKey;
                zModel.EmployeeName = EmployeeName;
                zModel.Description = Description;
                zModel.Money = Money.ToDouble();
                zModel.SupportDate = date;

                //đang fix mặt định trong store,
                //phải lấy key và id của PHỤ CẤP PHÁT SINH từ HRM_Payroll_Sample
                zModel.ItemKey = 112;
                zModel.ItemID = "P11";

                Employee_Info zEmp = new Employee_Info(EmployeeKey);
                zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
                zModel.EmployeeID = zEmp.Employee.EmployeeID;
                zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
                zModel.BranchKey = zEmp.Employee.BranchKey;
                zModel.BranchName = zEmp.Employee.BranchName;
                zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
                zModel.DepartmentName = zEmp.Employee.DepartmentName;
                zModel.PositionKey = zEmp.Employee.PositionKey;
                zModel.PositionName = zEmp.Employee.PositionName;

                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Payroll_Support_Info zInfo = new Payroll_Support_Info(SupportKey);
                zInfo.Payroll_Support = zModel;
                if (SupportKey != 0)
                {
                    zInfo.Update();
                }
                else
                {
                    zInfo.Create_KeyAuto();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Action - Step - 4 Save-Payroll]
        [HttpPost]
        public JsonResult SaveStep4(string Description)
        {
            ServerResult zResult = new ServerResult();
            string Message = "";
            try
            {
                if (Session["PayrollTable"] == null &&
                    Session["PayrollID"] == null)
                {
                    zResult.Success = false;
                    zResult.Message = "Phiên làm việc đã quá thời gian vui lòng làm lại !.";
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }

                var zListPayroll = Session["PayrollTable"] as List<Payroll_Item>;
                string PayrollID = Session["PayrollID"].ToString();

                var zInfo = new Payroll_Close_Info(PayrollID);
                zInfo.Payroll_Close.RecordStatus = 4;
                zInfo.Update();
                zInfo.Delete_Detail();

                StringBuilder Query = new StringBuilder();

                #region [--LƯU DÒNG NGANG--]
                Query = new StringBuilder();
                var listEmployee = zListPayroll.DistinctBy(o => o.EmployeeKey).ToList();
                Helper.RunSQL("DELETE HRM_Payroll_Close_Detail WHERE ParentKey = '" + PayrollID + "' ", out Message);
                foreach (var rec in listEmployee)
                {
                    var data = zListPayroll.FindAll(x => x.EmployeeKey == rec.EmployeeKey);

                    zInfo.InsertDetail(zInfo.Payroll_Close.CloseKey, zInfo.Payroll_Close.CloseDate, rec.EmployeeKey, JsonConvert.SerializeObject(data), Helper.PartnerNumber, UserLog.UserKey, UserLog.EmployeeName, UserLog.UserKey, UserLog.EmployeeName);
                    if (zInfo.Code != "200")
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                        return Json(zResult, JsonRequestBehavior.AllowGet);
                    }
                }
                #endregion

                #region [--LƯU DÒNG DỌC--]
                Helper.RunSQL("DELETE HRM_Payroll_Index WHERE ParentKey = '" + PayrollID + "' ", out Message);
                zInfo.InsertIndex(zListPayroll, PayrollID, UserLog.UserKey, UserLog.EmployeeName);
                #endregion

                SaveNote(PayrollID, Description, 4);
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            zResult.Success = true;
            zResult.Message = Message;
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Action - Step - 5 LuuGhiChu]
        [HttpPost]
        public JsonResult LuuGhiChu(string Description, int Type)
        {
            string PayrollID = Session["PayrollID"].ToString();
            var zResult = new ServerResult();

            string msg = SaveNote(PayrollID, Description, Type);
            if (msg == string.Empty)
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = msg;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PayrollClose(string Key, string Description, string Title, int Type)
        {
            var zResult = new ServerResult();

            var zInfo = new Payroll_Close_Info(Key);
            zInfo.Payroll_Close.Title = Title;
            zInfo.Payroll_Close.Description = Description;
            zInfo.Payroll_Close.RecordStatus = Type;
            zInfo.Payroll_Close.ModifiedBy = UserLog.CreatedBy;
            zInfo.Payroll_Close.ModifiedName = UserLog.UserName;
            zInfo.Close();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region [Action - Export EXCEL]
        public ActionResult ExportExcel(int Type)
        {
            if (Session["PayrollID"] == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            var zList = new List<Payroll_Item>();
            string ID = Session["PayrollID"].ToString();
            zList = Employee_Data.PayrollData(UserLog.PartnerNumber, ID, string.Empty);

            //sắp xếp dữ liệu
            zList = zList
              .OrderByDescending(o => o.Branch)
              .ThenBy(o => o.EmployeeID)
              .ToList();

            var stream = new MemoryStream();
            var zTable = new DataTable();
            string Filename = "";
            string Description = "";
            string Message = "";
            string Title = "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            switch (Type)
            {
                case 1:
                    Filename = "BANG_PHAI_TRU.xls";
                    zTable = InsuranceTable(zList);
                    Description = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + ID + "' AND Step = 'S5_TruLuong' AND RecordStatus <> 99", out Message);
                    stream = Template_InsureTable(zTable, Description, Title);
                    break;

                case 2:
                    Filename = "BANG_LAM_THEM.xls";
                    zTable = OverTimeTable(zList);
                    Description = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + ID + "' AND Step = 'S5_NgoaiGio' AND RecordStatus <> 99", out Message);
                    stream = Template_OverTimeTable(zTable, Description, Title);
                    break;

                case 3:
                    Filename = "BANG_PHU_CAP.xls";
                    zTable = SupportTable(zList);
                    Description = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + ID + "' AND Step = 'S5_PhuCap' AND RecordStatus <> 99", out Message);
                    stream = Template_SupportTable(zTable, Description, Title);
                    break;

                case 4:
                    Filename = "BANG_THANH_TOAN_LUONG.xls";
                    zTable = PaymentTable(zList);
                    Description = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + ID + "' AND Step = 'S5_ThanhToan' AND RecordStatus <> 99", out Message);
                    stream = Template_PaymentTable(zTable, Description, Title);
                    break;
                case 5:
                    Filename = "BANG_CHI_BHXH.xls";
                    zTable = Employee_Data.Report_BHXH(UserLog.PartnerNumber, ID);
                    Description = "";
                    stream = Template_BHXHTable(zTable, Description, Title);
                    break;

                default:
                    Filename = "BANG_TONG_HOP_KHOAN_LUONG.xls";
                    zTable = PopulateGrid(zList, out _);
                    Description = "";
                    stream = Template_PopulateTable(zTable, Description, Title);
                    break;
            }


            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
        }
        private MemoryStream Template_PopulateTable(DataTable zTable, string Description, string Title)
        {
            zTable.Columns.Remove("img");
            zTable.Columns.Remove("employee");
            int MaxColumn = zTable.Columns.Count - 1;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_CHI_TIET_TINH_LUONG");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG TÍNH LƯƠNG";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 3; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i + 1].Value = total.ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 3)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i + 1).Width = 13;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }

                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Column(i + 1).Width = 13;
                    }
                    #endregion
                }


                #endregion
                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 18;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 4);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_InsureTable(DataTable zTable, string Description, string Title)
        {
            zTable.Columns.Remove("img");
            zTable.Columns.Remove("employee");
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_PHAI_TRU");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG THANH TOÁN CÁC KHOẢN PHẢI TRỪ";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 3; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i + 1].Value = total.ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 3)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i + 1).Width = 13;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }

                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }

                #endregion

                #region [Sum dòng cuối]
                int rowTotal = row + 1;

                worksheet.Cells[rowTotal, 1, rowTotal, 3].Merge = true;
                worksheet.Cells[rowTotal, 1, rowTotal, 3].Value = "TỔNG CỘNG";

                worksheet.Row(rowTotal).Style.Font.Bold = true;
                worksheet.Row(rowTotal).Height = 30;
                worksheet.Row(rowTotal).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                for (int i = 3; i < zTable.Columns.Count; i++)
                {
                    var total = SumColumn(zTable, i, string.Empty);
                    worksheet.Cells[rowTotal, i + 1].Value = total.ToString("n0");
                    worksheet.Cells[rowTotal, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowTotal, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                #endregion

                #region [Ghi chú]
                int rowNumber = row + 2;
                double NoVN = worksheet.Cells[rowTotal, MaxColumn].Value.ToString().Replace(",", "").ToDouble();
                worksheet.Cells[rowNumber, 1].Value = "Bằng chữ: " + TN_Utils.NumberToWordsVN(NoVN);
                worksheet.Row(rowNumber).Style.Font.Bold = true;

                int rowDescription = row + 3;
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Value = "Phụ ghi";
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Merge = true;
                worksheet.Row(rowDescription).Style.Font.Bold = true;

                worksheet.Cells[rowDescription + 1, 1].Value = Description;
                #endregion

                #region [Ký tên]

                int rowSign = row + 7;

                worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                worksheet.Row(rowSign).Style.Font.Bold = true;
                worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 17;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_OverTimeTable(DataTable zTable, string Description, string Title)
        {
            zTable.Columns.Remove("img");
            zTable.Columns.Remove("employee");
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_LAM_NGOAI_GIO");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG THANH TOÁN TIỀN LÀM NGOÀI GIỜ/LỄ";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 3; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i + 1].Value = total.ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 3)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i + 1).Width = 13;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }

                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }

                #endregion

                #region [Sum dòng cuối]
                int rowTotal = row + 1;

                worksheet.Cells[rowTotal, 1, rowTotal, 3].Merge = true;
                worksheet.Cells[rowTotal, 1, rowTotal, 3].Value = "TỔNG CỘNG";

                worksheet.Row(rowTotal).Style.Font.Bold = true;
                worksheet.Row(rowTotal).Height = 30;
                worksheet.Row(rowTotal).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                for (int i = 3; i < zTable.Columns.Count; i++)
                {
                    var total = SumColumn(zTable, i, string.Empty);
                    worksheet.Cells[rowTotal, i + 1].Value = total.ToString("n0");
                    worksheet.Cells[rowTotal, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowTotal, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                #endregion

                #region [Ghi chú]
                int rowNumber = row + 2;
                double NoVN = worksheet.Cells[rowTotal, MaxColumn].Value.ToString().Replace(",", "").ToDouble();
                worksheet.Cells[rowNumber, 1].Value = "Bằng chữ: " + TN_Utils.NumberToWordsVN(NoVN);
                worksheet.Row(rowNumber).Style.Font.Bold = true;

                int rowDescription = row + 3;
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Value = "Phụ ghi";
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Merge = true;
                worksheet.Row(rowDescription).Style.Font.Bold = true;

                worksheet.Cells[rowDescription + 1, 1].Value = Description;
                #endregion

                #region [Ký tên]

                int rowSign = row + 7;

                worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                worksheet.Row(rowSign).Style.Font.Bold = true;
                worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 17;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_SupportTable(DataTable zTable, string Description, string Title)
        {
            zTable.Columns.Remove("img");
            zTable.Columns.Remove("employee");
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_PHU_CAP");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG THANH TOÁN CÁC KHOẢN PHU CẤP";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 3; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i + 1].Value = total.ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 3)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i + 1).Width = 13;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }

                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }

                #endregion

                #region [Sum dòng cuối]
                int rowTotal = row + 1;

                worksheet.Cells[rowTotal, 1, rowTotal, 3].Merge = true;
                worksheet.Cells[rowTotal, 1, rowTotal, 3].Value = "TỔNG CỘNG";

                worksheet.Row(rowTotal).Style.Font.Bold = true;
                worksheet.Row(rowTotal).Height = 30;
                worksheet.Row(rowTotal).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                for (int i = 3; i < zTable.Columns.Count; i++)
                {
                    var total = SumColumn(zTable, i, string.Empty);
                    worksheet.Cells[rowTotal, i + 1].Value = total.ToString("n0");
                    worksheet.Cells[rowTotal, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowTotal, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                #endregion

                #region [Ghi chú]
                int rowNumber = row + 2;
                double NoVN = worksheet.Cells[rowTotal, MaxColumn].Value.ToString().Replace(",", "").ToDouble();
                worksheet.Cells[rowNumber, 1].Value = "Bằng chữ: " + TN_Utils.NumberToWordsVN(NoVN);
                worksheet.Row(rowNumber).Style.Font.Bold = true;

                int rowDescription = row + 3;
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Value = "Phụ ghi";
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Merge = true;
                worksheet.Row(rowDescription).Style.Font.Bold = true;

                worksheet.Cells[rowDescription + 1, 1].Value = Description;
                #endregion

                #region [Ký tên]

                int rowSign = row + 7;

                worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                worksheet.Row(rowSign).Style.Font.Bold = true;
                worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 17;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_PaymentTable(DataTable zTable, string Description, string Title)
        {
            zTable.Columns.Remove("img");
            zTable.Columns.Remove("employee");
            int MaxColumn = zTable.Columns.Count;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_THANH_TOAN_LUONG");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG THANH TOÁN LƯƠNG";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 1; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 1].Value = Name;
                    worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 3; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i + 1].Value = total.ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 1; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 3)
                        {
                            worksheet.Cells[row, i + 1].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i + 1).Width = 13;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 1].Value = val;
                        }

                        worksheet.Cells[row, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }

                #endregion

                #region [Sum dòng cuối]
                int rowTotal = row + 1;

                worksheet.Cells[rowTotal, 1, rowTotal, 3].Merge = true;
                worksheet.Cells[rowTotal, 1, rowTotal, 3].Value = "TỔNG CỘNG";
                worksheet.Cells[rowTotal, 1, rowTotal, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Row(rowTotal).Style.Font.Bold = true;
                worksheet.Row(rowTotal).Height = 30;
                worksheet.Row(rowTotal).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                for (int i = 3; i < zTable.Columns.Count; i++)
                {
                    var total = SumColumn(zTable, i, string.Empty);
                    worksheet.Cells[rowTotal, i + 1].Value = total.ToString("n0");
                    worksheet.Cells[rowTotal, i + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[rowTotal, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[rowTotal, 1, rowTotal, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                #endregion

                #region [Ghi chú]
                int rowNumber = row + 2;
                double NoVN = worksheet.Cells[rowTotal, 9].Value.ToString().Replace(",", "").ToDouble();
                worksheet.Cells[rowNumber, 1].Value = "Bằng chữ: " + TN_Utils.NumberToWordsVN(NoVN);
                worksheet.Row(rowNumber).Style.Font.Bold = true;

                int rowDescription = row + 3;
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Value = "Phụ ghi";
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Merge = true;
                worksheet.Row(rowDescription).Style.Font.Bold = true;

                worksheet.Cells[rowDescription + 1, 1].Value = Description;
                #endregion

                #region [Ký tên]

                int rowSign = row + 7;

                worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                worksheet.Row(rowSign).Style.Font.Bold = true;
                worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 17;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_BHXHTable(DataTable zTable, string Description, string Title)
        {
            int MaxColumn = zTable.Columns.Count + 1;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_TONG_HOP_BHXH");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "BẢNG TỔNG HỢP BHXH";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Title;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 0; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i + 2].Value = Name;
                    worksheet.Cells[row, i + 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 0; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i == 0)
                        {
                            worksheet.Cells[row, i + 2].Value = val;
                            worksheet.Column(i + 1).Width = 50;
                        }
                        else
                        {
                            worksheet.Cells[row, i + 2].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Column(i + 2).Width = 20;
                        }
                        worksheet.Cells[row, i + 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }

                #endregion

                #region [Ghi chú]
                int rowTotal = zTable.Rows.Count + 1;
                int rowNumber = row + 2;
                double NoVN = worksheet.Cells[rowTotal, 9].Value.ToString().Replace(",", "").ToDouble();
                worksheet.Cells[rowNumber, 1].Value = "Bằng chữ: " + TN_Utils.NumberToWordsVN(NoVN);
                worksheet.Row(rowNumber).Style.Font.Bold = true;

                int rowDescription = row + 3;
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Value = "Phụ ghi";
                worksheet.Cells[rowDescription, 1, rowDescription, MaxColumn].Merge = true;
                worksheet.Row(rowDescription).Style.Font.Bold = true;

                worksheet.Cells[rowDescription + 1, 1].Value = Description;
                #endregion

                #region [Ký tên]

                int rowSign = row + 7;

                worksheet.Cells[rowSign, 1, rowSign, 2].Value = "PHÒNG  HC - NS";
                worksheet.Cells[rowSign, 1, rowSign, 2].Merge = true;

                worksheet.Cells[rowSign, 4, rowSign, 5].Value = "PHÒNG KẾ TOÁN";
                worksheet.Cells[rowSign, 4, rowSign, 5].Merge = true;

                worksheet.Cells[rowSign, 7, rowSign, 8].Value = "TỔNG GIÁM ĐỐC";
                worksheet.Cells[rowSign, 7, rowSign, 8].Merge = true;

                worksheet.Row(rowSign).Style.Font.Bold = true;
                worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 35;
                //worksheet.Column(3).Width = 17;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(6, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private double SumColumn(DataTable zTable, int No, string Name)
        {
            double result = 0;
            if (Name != string.Empty)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    if (r[0].ToString().ToUpper() == Name.ToUpper())
                    {
                        result += r[No].ToDouble();
                    }
                }
            }
            else
            {
                foreach (DataRow r in zTable.Rows)
                {
                    result += r[No].ToDouble();
                }
            }

            return result;
        }
        #endregion

        private List<Payroll_Item> CalculatorPayroll(
            string Employee, string EmployeeName, string EmployeeID,
            string PositionName, string Department, string Branch,
            DateTime DateCalulator, string Style, string DepartmentKey, string BranchKey, string Id)
        {
            var zList = Employee_Data.TINHLUONG(Helper.PartnerNumber, Employee, DateCalulator, Id);
            if (zList.Count > 0)
            {

                foreach (Payroll_Item Item in zList)
                {
                    Item.DateStart = DateCalulator.ToString("yyyy-MM-dd");
                    Item.EmployeeKey = Employee;
                    Item.EmployeeName = EmployeeName;
                    Item.EmployeeID = EmployeeID;
                    Item.PositionName = PositionName;
                    Item.DepartmentName = Department;
                    Item.Branch = Branch;
                    Item.Style = Style;
                    Item.BranchKey = BranchKey;
                    Item.DepartmentKey = DepartmentKey;

                    // có tham gia bảo hiểm
                    if (Style == "BH" || Style == "")
                    {
                        if (Item.Formula != string.Empty)
                        {
                            switch (Item.Formula)
                            {
                                default:
                                    Item.Amount = Item.Param;
                                    break;

                                //PHỤ CẤP THÂM NIÊN
                                case "C=A*B%":
                                    double A = GetData(zList, "A");
                                    double B = GetData(zList, "B");
                                    double C = A * (B / 100);
                                    Item.Amount = C.ToString();
                                    break;

                                //TỔNG THU NHẬP
                                case "C1=A+C":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double C1 = A + C;
                                    Item.Amount = C1.ToString();
                                    break;

                                //LƯƠNG NGÀY CHUẨN
                                case "E=(A+C)/26":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double E = (A + C) / 26;
                                    Item.Amount = E.ToString();
                                    break;

                                //LƯƠNG GIỜ CHUẨN
                                case "F=E/8":
                                    E = GetData(zList, "E");
                                    double F = E / 8;
                                    Item.Amount = F.ToString();// Math.Round(F, 0, MidpointRounding.AwayFromZero).ToString();
                                    break;

                                //LƯƠNG THỰC TẾ (CÓ CHẤM CÔNG)
                                case "F2=E*F1":
                                    E = GetData(zList, "E");
                                    double F1 = GetData(zList, "F1");
                                    double F2 = F1 * E;
                                    Item.Amount = F2.ToString();
                                    break;

                                //TỔNG PHỤ CẤP
                                case "J1=G+H+I+J":
                                    double G = GetData(zList, "G");
                                    double H = GetData(zList, "H");
                                    double I = GetData(zList, "I");
                                    double J = GetData(zList, "J");
                                    double J1 = G + H + I + J;
                                    Item.Amount = J1.ToString();
                                    break;

                                //SỐ TIỀN LÀM THÊM NGÀY THƯỜNG (GIỜ CHUẨN)
                                case "M=F*K*L":
                                    F = GetData(zList, "F");
                                    //F = Math.Round(F, 0, MidpointRounding.AwayFromZero);
                                    double K = GetData(zList, "K");
                                    double L = GetData(zList, "L");
                                    double M = (F * K * L) / 100;
                                    //M = Math.Round(M, 0, MidpointRounding.AwayFromZero);
                                    Item.Amount = M.ToString();
                                    break;

                                //SỐ TIỀN LÀM THÊM LỄ (GIỜ CHUẨN)
                                case "P=F*N*O":
                                    F = GetData(zList, "F");
                                    //F = Math.Round(F, 0, MidpointRounding.AwayFromZero);
                                    double N = GetData(zList, "N");
                                    double O = GetData(zList, "O");
                                    double P = (F * N * O) / 100;
                                    Item.Amount = P.ToString();
                                    break;

                                //TỔNG CÁC KHOẢN PC KHÁC
                                case "P1=M+P":
                                    M = GetData(zList, "M");
                                    P = GetData(zList, "P");
                                    double P1 = M + P;
                                    Item.Amount = P1.ToString();
                                    break;

                                //CỘNG LƯƠNG
                                case "P2=F2+P1+J1+P11":
                                    F2 = GetData(zList, "F2");
                                    P1 = GetData(zList, "P1");
                                    J1 = GetData(zList, "J1");
                                    double P11 = GetData(zList, "P11");
                                    double P2 = F2 + P1 + J1 + P11;
                                    Item.Amount = P2.ToString();
                                    break;

                                //BHXH
                                case "Q=(A+C)*8%":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double Q = (A + C) * 8 / 100;
                                    Item.Amount = Q.ToString();
                                    break;

                                //BHYT
                                case "R=(A+C)*1.5%":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double R = (A + C) * 1.5 / 100;
                                    Item.Amount = R.ToString();
                                    break;

                                //BHTN
                                case "S=(A+C)*1%":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double S = (A + C) * 1 / 100;
                                    Item.Amount = S.ToString();
                                    break;

                                //TỔNG CÁC KHOẢN PHẢI TRỪ
                                case "T1=Q+R+S+T":
                                    Q = GetData(zList, "Q");
                                    R = GetData(zList, "R");
                                    S = GetData(zList, "S");
                                    double T = GetData(zList, "T");
                                    double T1 = Q + R + S + T;
                                    Item.Amount = T1.ToString();
                                    break;

                                //THỰC LÃNH
                                case "Z=P2-T1":
                                    T1 = GetData(zList, "T1");
                                    P2 = GetData(zList, "P2");
                                    double Z = P2 - T1;
                                    Item.Amount = Z.ToString();
                                    break;
                            }
                        }
                        else
                        {
                            Item.Amount = Item.Param;
                        }
                    }
                    // không đóng bảo hiểm
                    else
                    {
                        if (Item.Formula != string.Empty)
                        {
                            switch (Item.Formula)
                            {
                                default:
                                    Item.Amount = Item.Param;
                                    break;

                                //PHỤ CẤP THÂM NIÊN
                                case "C=A*B%":
                                    double A = GetData(zList, "A");
                                    double B = GetData(zList, "B");
                                    double C = A * (B / 100);
                                    Item.Amount = C.ToString();
                                    break;

                                //TỔNG THU NHẬP
                                case "C1=A+C":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double C1 = A + C;
                                    Item.Amount = C1.ToString();
                                    break;

                                //LƯƠNG NGÀY CHUẨN
                                case "E=(A+C)/26":
                                    A = GetData(zList, "A");
                                    C = GetData(zList, "C");
                                    double E = (A + C) / 26;
                                    Item.Amount = E.ToString();
                                    break;

                                //LƯƠNG GIỜ CHUẨN
                                case "F=E/8":
                                    E = GetData(zList, "E");
                                    double F = E / 8;
                                    Item.Amount = F.ToString();//  Math.Round(F, 0, MidpointRounding.AwayFromZero).ToString();
                                    break;

                                //LƯƠNG THỰC TẾ (CÓ CHẤM CÔNG)
                                case "F2=E*F1":
                                    E = GetData(zList, "E");
                                    double F1 = GetData(zList, "F1");
                                    double F2 = F1 * E;
                                    Item.Amount = F2.ToString();
                                    break;

                                //TỔNG PHỤ CẤP
                                case "J1=G+H+I+J":
                                    double G = GetData(zList, "G");
                                    double H = GetData(zList, "H");
                                    double I = GetData(zList, "I");
                                    double J = GetData(zList, "J");
                                    double J1 = G + H + I + J;
                                    Item.Amount = J1.ToString();
                                    break;

                                //SỐ TIỀN LÀM THÊM NGÀY THƯỜNG (GIỜ CHUẨN)
                                case "M=F*K*L":
                                    F = GetData(zList, "F");
                                    //F = Math.Round(F, 0, MidpointRounding.AwayFromZero);
                                    double K = GetData(zList, "K");
                                    double L = GetData(zList, "L");
                                    double M = (F * K * L) / 100;
                                    //M = Math.Round(M, 0, MidpointRounding.AwayFromZero);
                                    Item.Amount = M.ToString("n0");
                                    break;

                                //SỐ TIỀN LÀM THÊM LỄ (GIỜ CHUẨN)
                                case "P=F*N*O":
                                    F = GetData(zList, "F");
                                    //F = Math.Round(F, 0, MidpointRounding.AwayFromZero);
                                    double N = GetData(zList, "N");
                                    double O = GetData(zList, "O");
                                    double P = (F * N * O) / 100;
                                    Item.Amount = P.ToString();
                                    break;

                                //TỔNG CÁC KHOẢN PC KHÁC
                                case "P1=M+P":
                                    M = GetData(zList, "M");
                                    P = GetData(zList, "P");
                                    double P1 = M + P;
                                    Item.Amount = P1.ToString();
                                    break;

                                //CỘNG LƯƠNG
                                case "P2=F2+P1+J1+P11":
                                    F2 = GetData(zList, "F2");
                                    P1 = GetData(zList, "P1");
                                    J1 = GetData(zList, "J1");
                                    double P11 = GetData(zList, "P11");
                                    double P2 = F2 + P1 + J1 + P11;
                                    Item.Amount = P2.ToString();
                                    break;

                                //BHXH
                                case "Q=(A+C)*8%":
                                    Item.Amount = "0";
                                    Item.Param = "0";
                                    break;

                                //BHYT
                                case "R=(A+C)*1.5%":
                                    Item.Amount = "0";
                                    Item.Param = "0";
                                    break;

                                //BHTN
                                case "S=(A+C)*1%":
                                    Item.Amount = "0";
                                    Item.Param = "0";
                                    break;

                                //TỔNG CÁC KHOẢN PHẢI TRỪ
                                case "T1=Q+R+S+T":
                                    double T = GetData(zList, "T");
                                    double T1 = T;
                                    Item.Amount = T1.ToString();
                                    break;

                                //THỰC LÃNH
                                case "Z=P2-T1":
                                    T1 = GetData(zList, "T1");
                                    P2 = GetData(zList, "P2");
                                    double Z = P2 - T1;
                                    Item.Amount = Z.ToString();
                                    break;
                            }
                        }
                        else
                        {
                            Item.Amount = Item.Param;
                        }
                    }
                }
            }

            return zList;
        }
        //
        private float GetData(List<Payroll_Item> ListData, string ID)
        {
            var obj = ListData.SingleOrDefault(x => x.ItemID == ID);
            if (obj.Amount.ToFloat() > 0)
                return obj.Amount.ToFloat();
            else
                return obj.Param.ToFloat();
        }
        private DataTable PopulateGrid(List<Payroll_Item> ListData, out string Message)
        {
            Message = string.Empty;
            DataTable dt = new DataTable();

            DataColumn dc = new DataColumn("Department", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));
            dt.Columns.Add(dc);
            //get header
            var tblLabel = ListData.DistinctBy(o => o.ItemName).ToList();

            //create data column
            foreach (Payroll_Item s in tblLabel)
            {
                dc = new DataColumn(s.ItemName, typeof(string));
                dt.Columns.Add(dc);
            }

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();
            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    try
                    {
                        int no = 5;
                        foreach (Payroll_Item item in data)
                        {
                            dr[no] = item.Amount.ToString();
                            no++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Message = rec.EmployeeName + " : " + ex.ToString();
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable InsuranceTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));//2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //5  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //6   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //7 C

            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương đóng BHXH", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("BHYT", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("BHTN", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //12
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "C1").Amount;
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "Q").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "R").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "S").Amount;
                    dr[12] = (dr[9].ToDouble()
                        + dr[10].ToDouble()
                        + dr[11].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable OverTimeTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //3  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //4   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //5 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //6 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Gờ làm thêm ngày thường", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm ngày thường", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Giờ làm thêm ngày lễ", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm lễ", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng tiền trực", typeof(string));   //13
            dt.Columns.Add(dc);
            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = (dr[5].ToDouble() + dr[7].ToDouble()).ToString("n0");
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "K").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "L").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[12] = data.SingleOrDefault(d => d.ItemID == "N").Amount;
                    dr[13] = data.SingleOrDefault(d => d.ItemID == "O").Amount;
                    dr[14] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[15] = data.SingleOrDefault(d => d.ItemID == "P1").Amount;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable SupportTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));    //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp chức vụ", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp điện thoại", typeof(string));   //6
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp xăng xe", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp đi lại", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp phát sinh", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //10          
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //12
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "G").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "H").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "I").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "J").Amount;
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "P11").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[12] = (dr[5].ToDouble()
                        + dr[6].ToDouble()
                        + dr[7].ToDouble()
                        + dr[8].ToDouble()
                        + dr[9].ToDouble()
                        + dr[10].ToDouble()
                        + dr[11].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable PaymentTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Img", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Employee", typeof(string));//2
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thoả thuận", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));//6
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng thu nhập", typeof(string));//7
            dt.Columns.Add(dc);
            dc = new DataColumn("Thu nhập theo ngày công", typeof(string));//8
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp khác", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH, BHYT, BHTN", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Tạm ứng", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Thực nhận", typeof(string));   //13
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.PhotoPath;
                dr[2] = rec.EmployeeKey;
                dr[3] = rec.EmployeeName;
                dr[4] = rec.PositionName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    double F2 = data.SingleOrDefault(d => d.ItemID == "F2").Amount.ToDouble();
                    double TN = data.SingleOrDefault(d => d.ItemID == "C").Amount.ToDouble();
                    double J1 = data.SingleOrDefault(d => d.ItemID == "J1").Amount.ToDouble();
                    double P1 = data.SingleOrDefault(d => d.ItemID == "P1").Amount.ToDouble();
                    double P2 = data.SingleOrDefault(d => d.ItemID == "P2").Amount.ToDouble();
                    double T = data.SingleOrDefault(d => d.ItemID == "T").Amount.ToDouble();
                    double Q = data.SingleOrDefault(d => d.ItemID == "Q").Amount.ToDouble();
                    double R = data.SingleOrDefault(d => d.ItemID == "R").Amount.ToDouble();
                    double S = data.SingleOrDefault(d => d.ItemID == "S").Amount.ToDouble();
                    double P11 = data.SingleOrDefault(d => d.ItemID == "P11").Amount.ToDouble();
                    double C1 = data.SingleOrDefault(d => d.ItemID == "C1").Amount.ToDouble();
                    double Z = Q + R + S;
                    //if (F2 > 0)
                    //{
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = TN;
                    dr[7] = P11 + C1;
                    dr[8] = F2;
                    dr[9] = (P11 + J1 + P1).ToString("n0");
                    dr[10] = P2.ToString("n0");
                    dr[11] = Z.ToString("n0");
                    dr[12] = T.ToString("n0");
                    dr[13] = (P2 - T - Z).ToString("n0");
                    //}
                    //else
                    //{
                    //    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    //    dr[6] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    //    dr[7] = P11 + C1;
                    //    dr[8] = F2;
                    //    dr[9] = (P11 + J1 + P1).ToString("n0");
                    //    dr[10] = P2.ToString("n0");
                    //    dr[11] = Z.ToString("n0");
                    //    dr[12] = T.ToString("n0");
                    //    dr[13] = (P2 - T - Z - TN).ToString("n0");
                    //}
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable OveralTable(string ID)
        {
            var zInfo = new Payroll_Close_Info(ID);
            var zId = zInfo.Payroll_Close.CloseKey;
            DataTable zTable = Employee_Data.Report_BHXH(UserLog.PartnerNumber, zId);
            return zTable;
        }
        private string SaveNote(string Id, string Description, int Type)
        {
            #region [--LƯU GHI CHÚ--]

            string SQL = "DELETE HRM_Payroll_Note WHERE ParentKey = @ParentKey AND Step = @Step";
            SQL += "INSERT HRM_Payroll_Note (ParentKey, Step, Description, PartnerNumber, CreatedBy, CreatedName, ModifiedBy, ModifiedName) VALUES";
            SQL += "(@ParentKey, @Step, @Description, @PartnerNumber, @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName)";

            SQL = SQL.Replace("@ParentKey", "'" + Id + "'");

            SQL = SQL.Replace("@Description", "N'" + Description + "'");
            SQL = SQL.Replace("@PartnerNumber", "'" + Helper.PartnerNumber + "'");
            SQL = SQL.Replace("@CreatedBy", "'" + UserLog.UserKey + "'");
            SQL = SQL.Replace("@CreatedName", "N'" + UserLog.EmployeeName + "'");
            SQL = SQL.Replace("@ModifiedBy", "'" + UserLog.UserKey + "'");
            SQL = SQL.Replace("@ModifiedName", "N'" + UserLog.EmployeeName + "'");

            switch (Type)
            {
                case 4:
                    SQL = SQL.Replace("@Step", "'S4'");
                    break;

                case 51:
                    SQL = SQL.Replace("@Step", "'S5_TruLuong'");
                    break;

                case 52:
                    SQL = SQL.Replace("@Step", "'S5_NgoaiGio'");
                    break;

                case 53:
                    SQL = SQL.Replace("@Step", "'S5_PhuCap'");
                    break;

                case 54:
                    SQL = SQL.Replace("@Step", "'S5_ThanhToan'");
                    break;
                case 55:
                    SQL = SQL.Replace("@Step", "'S5_TongHop'");
                    break;
            }
            Helper.RunSQL(SQL, out string Message);

            #endregion

            return Message;
        }
        #endregion

        //BÁO CÁO CHỈ SỐ TÍNH TOÁN LƯƠNG
        public ActionResult Index()
        {
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            string MonthYear = DateTime.Now.ToString("MM/yyyy");
            var zInfo = new Payroll_Close_Info(MonthYear, true);
            if (zInfo.Code == "200")
            {
                string Id = zInfo.Payroll_Close.CloseKey;
                var zTable = Employee_Data.Report_Index(UserLog.PartnerNumber, Id);
                ViewBag.Table = zTable;
                if (zTable.Rows.Count == 0)
                {
                    ViewBag.Message = "NODATA";
                }
                else
                {
                    ViewBag.NoteTruLuong = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TruLuong' AND RecordStatus <> 99", out _);
                    ViewBag.NoteNgoaiGio = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_NgoaiGio' AND RecordStatus <> 99", out _);
                    ViewBag.NotePhuCap = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_PhuCap' AND RecordStatus <> 99", out _);
                    ViewBag.NoteThanhToan = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_ThanhToan' AND RecordStatus <> 99", out _);
                    ViewBag.NoteTongHop = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TongHop' AND RecordStatus <> 99", out _);

                    Session["PayrollID"] = Id;
                    ViewBag.Message = "OK";
                }
            }
            else
            {
                ViewBag.Message = "NODATA";
            }

            return View("~/Views/Employee/DashBoard/Index.cshtml");
        }
        public ActionResult General_DataIndex(string MonthYear = "")
        {
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            if (MonthYear == string.Empty)
            {
                MonthYear = DateTime.Now.ToString("MM/yyyy");
            }
            else
            {
                if (MonthYear.Split('/')[0] == "0")
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        MonthYear += i + ", ";
                    }
                }
            }
            var zInfo = new Payroll_Close_Info(MonthYear, true);
            if (zInfo.Code == "200")
            {
                string Id = zInfo.Payroll_Close.CloseKey;
                var zTable = Employee_Data.Report_Index(UserLog.PartnerNumber, Id);
                ViewBag.Table = zTable;
                if (zTable.Rows.Count == 0)
                {
                    ViewBag.Message = "NODATA";
                }
                else
                {
                    ViewBag.NoteTruLuong = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TruLuong' AND RecordStatus <> 99", out _);
                    ViewBag.NoteNgoaiGio = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_NgoaiGio' AND RecordStatus <> 99", out _);
                    ViewBag.NotePhuCap = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_PhuCap' AND RecordStatus <> 99", out _);
                    ViewBag.NoteThanhToan = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_ThanhToan' AND RecordStatus <> 99", out _);
                    ViewBag.NoteTongHop = Helper.GetSQLString("SELECT ISNULL([DESCRIPTION],'') FROM HRM_Payroll_Note WHERE ParentKey ='" + Id + "' AND Step = 'S5_TongHop' AND RecordStatus <> 99", out _);


                    Session["PayrollID"] = Id;
                    ViewBag.Message = "OK";
                }
            }
            else
            {
                ViewBag.Message = "NODATA";
            }

            return View("~/Views/Employee/DashBoard/_Panel.cshtml");
        }
        public ActionResult Branch_DataIndex(string Branch, string MonthYear)
        {
            if (MonthYear == string.Empty)
            {
                MonthYear = DateTime.Now.ToString("MM/yyyy");
            }
            var zInfo = new Payroll_Close_Info(MonthYear, true);
            if (zInfo.Code == "200")
            {
                string Id = zInfo.Payroll_Close.CloseKey;
                var zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, Branch);
                if (zList.Count == 0)
                {
                    ViewBag.Message = "NODATA";
                }
                else
                {
                    ViewBag.Employee = PopulateGrid(zList, out _);
                    var zTable = Employee_Data.Report_Index_Branch(UserLog.PartnerNumber, Id, Branch);
                    ViewBag.Table = zTable;
                    ViewBag.Id = Id;
                    ViewBag.Message = "OK";
                    Session["PayrollID"] = Id;
                }
            }
            else
            {
                ViewBag.Message = "NODATA";
            }
            return View("~/Views/Employee/DashBoard/_Detail.cshtml");
        }
        public ActionResult Payroll_DataIndex(int Type)
        {
            if (Session["PayrollID"] == null)
            {
                ViewBag.Message = "Đã hết phiên làm việc vui lòng đăng nhập lại !.";
            }
            else
            {
                string Id = Session["PayrollID"].ToString();
                var zList = Employee_Data.PayrollData(UserLog.PartnerNumber, Id, string.Empty);
                var zTable = new DataTable();
                switch (Type)
                {
                    case 1:
                        zTable = InsuranceTable(zList);
                        break;

                    case 2:
                        zTable = OverTimeTable(zList);
                        break;

                    case 3:
                        zTable = SupportTable(zList);
                        break;

                    case 4:
                        zTable = PaymentTable(zList);
                        break;
                    case 5:
                        zTable = Employee_Data.Report_BHXH(UserLog.PartnerNumber, Id);
                        ViewBag.BHXH = true;
                        break;

                    default:
                        zTable = PopulateGrid(zList, out _);
                        break;
                }

                ViewBag.Id = Id;
                ViewBag.Table = zTable;
            }

            return View("~/Views/Employee/DashBoard/_Table.cshtml");
        }

        public ActionResult List_Seniority(string sbutton = "", int Year = 0, int Status = 0)
        {
            if (Year < 1900)
                Year = DateTime.Now.Year;
            if (sbutton == "btn_Export")
            {
                var zTable = Employee_Data.Table_Seniority(UserLog.PartnerNumber, string.Empty, Year, Status);
                var Filename = "BẢNG THÂM NIÊN.xls";
                var Description = "THÂM NIÊN ĐẾN NĂM " + Year;
                var stream = Template_Seniority(zTable, Description);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
            }
            else
            {
                ViewBag.ListData = Employee_Data.List_Seniority(UserLog.PartnerNumber, string.Empty, Year, Status);
                ViewBag.Message = "OK";
                ViewBag.Year = Year;
                ViewBag.Status = Status;
                return View("~/Views/Employee/List_Seniority.cshtml");
            }
        }
        //public ActionResult ExportSeniority(string Year)
        //{
        //    if (Year == null && Year.ToInt() < 1900)
        //        Year = DateTime.Now.Year.ToString();
        //    var zTable = new DataTable();
        //    zTable = Employee_Data.Table_Seniority(UserLog.PartnerNumber, string.Empty, Year.ToInt());

        //    var stream = new MemoryStream();

        //    string Filename = "";
        //    string Description = "";
        //    string Message = "";

        //    Filename = "BẢNG THÂM NIÊN.xlsx";
        //    Description = "";
        //    stream = Template_Seniority(zTable, Description);


        //    return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
        //}
        private MemoryStream Template_Seniority(DataTable zTable, string Description)
        {
            int MaxColumn = 9;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("ThamNien");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = Description;
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = "";
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //#region[Fill data]
                ////Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 2].Value = "Mã nhân viên";
                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = "Họ và tên";
                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = "Chức vụ";
                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = "Tuyển dụng";
                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = "Giới tính";
                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = "Ngày sinh";
                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = "Thâm niên";
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 9].Value = "Tình trạng";
                worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                ////Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r["BranchName"].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 2].Value = r["EmployeeID"].ToString();
                    worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 3].Value = r["LastName"].ToString() + " " + r["FirstName"].ToString();
                    worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 4].Value = r["PositionName"].ToString();
                    worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    DateTime zStartingDate = DateTime.MinValue;
                    if (r["StartingDate"] != null)
                        DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);
                    worksheet.Cells[row, 5].Value = zStartingDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    string zGender = "";
                    if (r["Gender"].ToInt() == 0)
                        zGender = "Nữ";
                    else
                        zGender = "Nam";
                    worksheet.Cells[row, 6].Value = zGender.ToString();
                    worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    DateTime zBirthday = DateTime.MinValue;
                    if (r["Birthday"] != null)
                        DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);
                    worksheet.Cells[row, 7].Value = zBirthday.ToString("dd/MM/yyyy");
                    worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 8].Value = r["Seniority"].ToString();
                    worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 9].Value = r["WorkingStatusName"].ToString();
                    worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    #endregion
                }

                #region [Ký tên]

                //int rowSign = row + 7;

                //worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                //worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                //worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                //worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                //worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                //worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                //worksheet.Row(rowSign).Style.Font.Bold = true;
                //worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 25;
                worksheet.Column(4).Width = 25;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 15;
                worksheet.Column(7).Width = 15;
                worksheet.Column(8).Width = 20;
                worksheet.Column(9).Width = 15;
                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(2, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_Contract(List<Contract_Model> ListData, string Description)
        {
            int MaxColumn = 9;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("HĐLĐ");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "CÁC HỢP ĐỒNG LAO ĐỘNG";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = "";
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //#region[Fill data]
                ////Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 2].Value = "Mã nhân viên";
                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = "Họ và tên";
                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = "Chức vụ";
                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = "Số HĐLĐ";
                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = "Loại HĐLĐ";
                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = "Ngày ký";
                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = "Thời hạn";
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 9].Value = "Tình trạng";
                worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                ////Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (var item in ListData)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = item.BranchName;
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 2].Value = "";
                    worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 3].Value = item.EmployeeName;
                    worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 4].Value = item.PositionName;
                    worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 5].Value = item.ContractID;
                    worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 6].Value = item.ContractTypeName;
                    worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    string SignDate = (item.SignDate == DateTime.MinValue ? "Không xác định" : item.SignDate.ToString("dd/MM/yyyy"));
                    worksheet.Cells[row, 7].Value = item.SignDate.ToString("dd/MM/yyyy");
                    worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    string FromDate = (item.FromDate == DateTime.MinValue ? "Không xác định" : item.FromDate.ToString("dd/MM/yyyy"));
                    string ToDate = (item.ToDate == DateTime.MinValue ? "Không xác định" : item.ToDate.ToString("dd/MM/yyyy"));
                    worksheet.Cells[row, 8].Value = FromDate + " - " + ToDate;
                    worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    string Status = "";
                    if (item.ToDate != DateTime.MinValue)
                    {
                        if (item.ToDate < DateTime.Now)
                        {
                            Status = "Đã quá hạn !.";
                        }
                        else
                        {
                            TimeSpan Time = item.ToDate - DateTime.Now;
                            if (item.ToDate >= DateTime.Now && Time.Days <= 30)
                            {
                                Status = "Gần đến hạn !.";
                                Status += "Còn lại " + Time.Days;
                            }
                        }
                    }
                    worksheet.Cells[row, 9].Value = Status;
                    worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    #endregion
                }

                #region [Ký tên]

                //int rowSign = row + 7;

                //worksheet.Cells[rowSign, 1, rowSign, 4].Value = "PHÒNG  HC - NS";
                //worksheet.Cells[rowSign, 1, rowSign, 4].Merge = true;

                //worksheet.Cells[rowSign, 5, rowSign, 8].Value = "PHÒNG KẾ TOÁN";
                //worksheet.Cells[rowSign, 5, rowSign, 8].Merge = true;

                //worksheet.Cells[rowSign, 9, rowSign, 12].Value = "TỔNG GIÁM ĐỐC";
                //worksheet.Cells[rowSign, 9, rowSign, 12].Merge = true;

                //worksheet.Row(rowSign).Style.Font.Bold = true;
                //worksheet.Row(rowSign).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //worksheet.Row(rowSign).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 25;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 30;
                worksheet.Column(7).Width = 15;
                worksheet.Column(8).Width = 22;
                worksheet.Column(9).Width = 20;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(2, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_PhepTable(List<Leave_Close_Model> ListData, string Name, string ChucVu)
        {
            int MaxColumn = 8;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANGPHEP");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "Bảng theo dõi phép " + Name;
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = ChucVu;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region [COT]
                var row = 5;
                worksheet.Cells[row, 1, row, 3].Value = "Kì phép sử dụng";
                worksheet.Cells[row, 1, row, 3].Merge = true;
                worksheet.Cells[row, 1, row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 4, row, 6].Value = "Phát sinh nghỉ phép";
                worksheet.Cells[row, 4, row, 6].Merge = true;
                worksheet.Cells[row, 4, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 7, row + 1, 7].Value = "Ghi chú";
                worksheet.Cells[row, 7, row + 1, 7].Merge = true;
                worksheet.Cells[row, 7, row + 1, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 8, row + 1, 8].Value = "Còn lại";
                worksheet.Cells[row, 8, row + 1, 8].Merge = true;
                worksheet.Cells[row, 8, row + 1, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 35;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                row = 6;
                worksheet.Cells[row, 1].Value = "Kì";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 2].Value = "Tiêu chuẩn";
                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = "Tồn phép";
                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = "Thời gian";
                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = "Số ngày";
                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = "Lý do";
                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //Custom
                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 10;
                worksheet.Column(7).Width = 30;
                worksheet.Column(8).Width = 10;
                #endregion

                var ki = ListData.DistinctBy(s => s.Reference).ToList();
                for (int i = 0; i < ki.Count; i++)
                {
                    var item = ki[i];
                    var rff = item.Reference;
                    var group = ListData.Where(s => s.Reference == item.Reference).ToList();
                    var css = false;
                    if (i % 2 == 0)
                    {
                        css = true;
                    }
                    else
                    {
                        css = false;
                    }

                    if (group.Count > 1)
                    {
                        int rspan = group.Count - 1;
                        var data = group[0];

                        row++;
                        worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                        worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 2].Value = data.STD + data.TN;
                        worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 3].Value = data.Incremental;
                        worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 4, row + rspan, 4].Value = data.FromDate.ToString("dd/MM") + "-" + data.ToDate.ToString("dd/MM");
                        worksheet.Cells[row, 4, row + rspan, 4].Merge = true;
                        worksheet.Cells[row, 4, row + rspan, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, 4, row + rspan, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[row, 4, row + rspan, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 5, row + rspan, 5].Value = data.TotalDate;
                        worksheet.Cells[row, 5, row + rspan, 5].Merge = true;
                        worksheet.Cells[row, 5, row + rspan, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, 5, row + rspan, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[row, 5, row + rspan, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 6, row + rspan, 6].Value = data.LyDo;
                        worksheet.Cells[row, 6, row + rspan, 6].Merge = true;
                        worksheet.Cells[row, 6, row + rspan, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, 6, row + rspan, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[row, 6, row + rspan, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                        worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                        worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(row).Style.WrapText = true;

                        worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                        worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(row).Style.WrapText = true;

                        if (css == true)
                        {
                            worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                        }


                        for (int g = 1; g < group.Count; g++)
                        {
                            data = group[g];
                            row++;

                            worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                            worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 2].Value = data.STD + data.TN;
                            worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 3].Value = data.Incremental;
                            worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                            worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;

                            worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                            worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;

                            if (css == true)
                            {
                                worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                            }
                        }
                    }
                    else
                    {
                        var data = group[0];
                        row++;

                        worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                        worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 2].Value = data.STD + data.TN;
                        worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 3].Value = data.Incremental;
                        worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 4].Value = data.FromDate.ToString("dd/MM") + "-" + data.ToDate.ToString("dd/MM");
                        worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 5].Value = data.TotalDate;
                        worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 6].Value = data.LyDo;
                        worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                        worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(row).Style.WrapText = true;

                        worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                        worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Row(row).Style.WrapText = true;
                    }
                }

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(2, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }
        private MemoryStream Template_PhepTableChiNhanh(List<Employee_Model> ListNV, string Name)
        {
            int MaxColumn = 8;

            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANGPHEP");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Value = "Bảng theo dõi phép ";
                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = Name;
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region [COT]
                var row = 5;
                worksheet.Cells[row, 1, row, 3].Value = "Kì phép sử dụng";
                worksheet.Cells[row, 1, row, 3].Merge = true;
                worksheet.Cells[row, 1, row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 4, row, 6].Value = "Phát sinh nghỉ phép";
                worksheet.Cells[row, 4, row, 6].Merge = true;
                worksheet.Cells[row, 4, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 7, row + 1, 7].Value = "Ghi chú";
                worksheet.Cells[row, 7, row + 1, 7].Merge = true;
                worksheet.Cells[row, 7, row + 1, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 8, row + 1, 8].Value = "Còn lại";
                worksheet.Cells[row, 8, row + 1, 8].Merge = true;
                worksheet.Cells[row, 8, row + 1, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 35;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                row = 6;
                worksheet.Cells[row, 1].Value = "Kì";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 2].Value = "Tiêu chuẩn";
                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = "Tồn phép";
                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = "Thời gian";
                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = "Số ngày";
                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = "Lý do";
                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //Custom
                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 10;
                worksheet.Column(7).Width = 30;
                worksheet.Column(8).Width = 10;
                #endregion


                foreach (var nv in ListNV)
                {
                    row++;

                    var zemp = new Employee_Info(nv.EmployeeKey).Employee;
                    string name = zemp.LastName + " " + zemp.FirstName;
                    var ListData = Employee_Data.XemKyPhep(nv.EmployeeKey, out _);

                    worksheet.Cells[row, 1, row, MaxColumn].Value = name;
                    worksheet.Cells[row, 1, row, MaxColumn].Merge = true;
                    worksheet.Row(row).Style.Font.Bold = true;
                    worksheet.Row(row).Height = 30;
                    worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    var ki = ListData.DistinctBy(s => s.Reference).ToList();
                    for (int i = 0; i < ki.Count; i++)
                    {
                        var item = ki[i];
                        var rff = item.Reference;
                        var group = ListData.Where(s => s.Reference == item.Reference).ToList();
                        var css = false;
                        if (i % 2 == 0)
                        {
                            css = true;
                        }
                        else
                        {
                            css = false;
                        }

                        if (group.Count > 1)
                        {
                            int rspan = group.Count - 1;
                            var data = group[0];

                            row++;
                            worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                            worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 2].Value = data.STD + data.TN;
                            worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 3].Value = data.Incremental;
                            worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 4, row + rspan, 4].Value = data.FromDate.ToString("dd/MM") + "-" + data.ToDate.ToString("dd/MM");
                            worksheet.Cells[row, 4, row + rspan, 4].Merge = true;
                            worksheet.Cells[row, 4, row + rspan, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Cells[row, 4, row + rspan, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Cells[row, 4, row + rspan, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 5, row + rspan, 5].Value = data.TotalDate;
                            worksheet.Cells[row, 5, row + rspan, 5].Merge = true;
                            worksheet.Cells[row, 5, row + rspan, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Cells[row, 5, row + rspan, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Cells[row, 5, row + rspan, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 6, row + rspan, 6].Value = data.LyDo;
                            worksheet.Cells[row, 6, row + rspan, 6].Merge = true;
                            worksheet.Cells[row, 6, row + rspan, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Cells[row, 6, row + rspan, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Cells[row, 6, row + rspan, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                            worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                            worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;

                            worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                            worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;

                            if (css == true)
                            {
                                worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                            }


                            for (int g = 1; g < group.Count; g++)
                            {
                                data = group[g];
                                row++;

                                worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                worksheet.Cells[row, 2].Value = data.STD + data.TN;
                                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                worksheet.Cells[row, 3].Value = data.Incremental;
                                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Row(row).Style.WrapText = true;

                                worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Row(row).Style.WrapText = true;

                                if (css == true)
                                {
                                    worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);
                                }
                            }
                        }
                        else
                        {
                            var data = group[0];
                            row++;

                            worksheet.Cells[row, 1].Value = data.CloseYear.Split('/')[1];
                            worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 2].Value = data.STD + data.TN;
                            worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 3].Value = data.Incremental;
                            worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 4].Value = data.FromDate.ToString("dd/MM") + "-" + data.ToDate.ToString("dd/MM");
                            worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 5].Value = data.TotalDate;
                            worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 6].Value = data.LyDo;
                            worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells[row, 7].Value = ScrubHtml(data.Description);
                            worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;

                            worksheet.Cells[row, 8].Value = (data.CloseEnd < 0 ? 0 : data.CloseEnd);
                            worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                            worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Row(row).Style.WrapText = true;
                        }
                    }
                }
                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(2, 1);
                package.Save();
            }

            stream.Position = 0;
            return stream;
        }

        public ActionResult List_Leave(string Name, string Year)
        {
            if (Year == null && Year.ToInt() < 1900)
                Year = DateTime.Now.Year.ToString();
            if (Name == null || Name == "")
            {
                Name = "";
                ViewBag.ListData = null;
            }
            else
            {
                ViewBag.ListData = Leave_Close_Data.Track(UserLog.PartnerNumber, Name, Year.ToInt());
            }
            ViewBag.Message = "OK";
            ViewBag.Year = Year;
            ViewBag.Name = Name;

            return View("~/Views/Employee/List_Leave.cshtml");
        }
        private MemoryStream Template_ListEmployee(List<Employee_Model> ListData)
        {
            int MaxColumn = 12;
            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("ThamNien");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, MaxColumn].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, MaxColumn].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, MaxColumn].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, MaxColumn].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, MaxColumn].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, MaxColumn].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Height = 30;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, MaxColumn].Value = "";
                worksheet.Cells[4, 1, 4, MaxColumn].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Height = 30;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                //#region[Fill data]
                ////Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 2].Value = "Mã nhân viên";
                worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = "Họ và tên";
                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = "Chức vụ";
                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = "Tình trạng";
                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = "Tuyển dụng";
                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = "Giới tính";
                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = "Ngày sinh";
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 9].Value = "CMND";
                worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 10].Value = "Địa chỉ";
                worksheet.Cells[row, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 11].Value = "Email";
                worksheet.Cells[row, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 12].Value = "SĐT";
                worksheet.Cells[row, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                ////Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (var item in ListData)
                {
                    var r = item;
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = item.BranchName;
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, MaxColumn].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 2].Value = r.EmployeeID;
                    worksheet.Cells[row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 3].Value = r.LastName + " " + r.FirstName;
                    worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 4].Value = r.PositionName;
                    worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 5].Value = r.WorkingStatusName;
                    worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if (r.StartingDate != DateTime.MinValue)
                        worksheet.Cells[row, 6].Value = r.StartingDate.ToString("dd/MM/yyyy");
                    else
                        worksheet.Cells[row, 6].Value = "";
                    worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    string zGender = "";
                    if (r.Gender == 0)
                        zGender = "Nữ";
                    else
                        zGender = "Nam";
                    worksheet.Cells[row, 7].Value = zGender.ToString();
                    worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if (r.Birthday != DateTime.MinValue)
                        worksheet.Cells[row, 8].Value = r.Birthday.ToString("dd/MM/yyyy");
                    else
                        worksheet.Cells[row, 8].Value = "";
                    worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 9].Value = r.PassportNumber;
                    worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 10].Value = r.AddressRegister;
                    worksheet.Cells[row, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 11].Value = r.Email;
                    worksheet.Cells[row, 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, 12].Value = r.MobiPhone;
                    worksheet.Cells[row, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    #endregion
                }
                //Custom
                worksheet.Cells.AutoFitColumns();
                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";
                worksheet.View.FreezePanes(2, 1);
                package.Save();
            }
            stream.Position = 0;
            return stream;
        }
        //
        string ScrubHtml(string value)
        {
            Regex regex = new Regex(@"(<br />|<br/>|</ br>|</br>)");
            // Replace new line with <br/> tag    
            var step0 = regex.Replace(value, "\r\n");
            var step1 = Regex.Replace(step0, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }
    }
}