﻿$(document).ready(function () {
    $("#map").height($(document).height() - 35);
    $("#cbo_Map").change(function () {
        var ProductKey = $('#cbo_Map').val();
        $('.se-pre-con').fadeIn('slow');
        window.location.href = '/Customer/Map?ProductKey=' + ProductKey;
    });
    $("#cbo_SearchBuyer").change(function () {
        var ProductKey = $('#cbo_Map').val();
        var CustomerKey = $('#cbo_SearchBuyer').val();
        var ContractID = $('#txt_ContractID').val();
        $('.se-pre-con').fadeIn('slow');
        window.location.href = '/Customer/MapSearch?ProductKey=' + ProductKey + '&ContractID=' + ContractID + '&CustomerKey=' + CustomerKey;
    });
    $(":file").on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }
    });
    
});
$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

var _ListItem = [];
var _markers = [];
var _Center;
var _map;
var _mapOptions;
var _infoWindow = new google.maps.InfoWindow;

function Initialize() {
    $('.se-pre-con').fadeIn('slow');

    _mapOptions = {
        zoom: 19,
        gestureHandling: 'greedy',
        center: new google.maps.LatLng(10.796696026816964, 106.7122220993042),
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };
    _map = new google.maps.Map(document.getElementById('map'), _mapOptions);
    const centerControlDiv = document.createElement("div");
    CenterControl(centerControlDiv, _map);
    _map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

    LoadLayer();

    setTimeout(function () { $('.se-pre-con').fadeOut('slow'); }, 3000);
}

function LoadLayer() {
    try {
        if (_ObjLocation !== null) {
            var n = _ObjLocation.quantity;
            var zLatLng;
            var zListLatLng;
            var zBermudaTriangle = [];
            var zMaker = [];
            var addListenersOnPolygon = function (polygon) {
                google.maps.event.addListener(polygon, 'click', function (event) {
                    var button = '<div class="btn-group flex-wrap">';
                    if (polygon.contractKey.length > 0) {
                        button += '<button type="button" class="btn btn-primary" onclick="OpenModelContractView(\'' + polygon.contractKey + '\')">Xem hợp đồng</button>';
                    }
                    button += '<button type="button" class="btn btn-primary" onclick="OpenGeneral(\'' + polygon.asset + '\',\'' + polygon.title + '\',\'' + polygon.contractKey + '\')">Hoạt động</button>';
                    button += '<button type="button" class="btn btn-primary" onclick="OpenHistory(\'' + polygon.asset + '\',\'' + polygon.title + '\')">Lịch sử thuê</button>';

                    button += '</div>';

                    var contentString = "";

                    if (polygon.title.indexOf("Kho") !== -1) {
                        contentString += "<blockquote class='success text-center'>";
                        contentString += "<h4 class='text-success'><b>" + polygon.title + "-" + parseFloat(polygon.area) + " (m<sup>2</sup>)</b></h4>";
                        contentString += "<div class='mb-1'><b>" + polygon.customer + "</b></div>";
                        contentString += "<div class='mb-3'><b>" + polygon.time + "</b></div>";
                        //if (polygon.contractKey !== undefined &&
                        //    polygon.contractKey.length > 0)
                        contentString += button;
                        contentString += "</blockquote>";
                    }
                    else {
                        contentString += "<blockquote class='success text-center'>";
                        contentString += "<h4 class='text-success'><b>" + polygon.title + "</b></h4>";
                        contentString += "<div class='mb-1'><b>" + polygon.customer + "</b></div>";
                        contentString += "<div class='mb-3'><b>" + polygon.time + "</b></div>";
                        //if (polygon.contractKey !== undefined &&
                        //    polygon.contractKey.length > 0)
                        contentString += button;
                        contentString += "</blockquote>";
                    }
                    _infoWindow.setContent(contentString);
                    _infoWindow.setPosition(event.latLng);
                    _infoWindow.open(_map);
                });
                google.maps.event.addListener(polygon, "mouseover", function () {
                    var zthis = $(this);
                    zthis.data('fillColor', polygon.fillColor);
                    this.setOptions({ fillColor: "#00FF00" });
                });
                google.maps.event.addListener(polygon, "mouseout", function () {
                    var zthis = $(this);
                    var zOldcolor = zthis.data('fillColor');
                    this.setOptions({ fillColor: zOldcolor });
                });
                google.maps.event.addListener(polygon, "click", function () {
                    $('#title').text(polygon.title);
                });
            };

            for (i = 0; i < n; i++) {
                var zTriangleCoords = [];
                zListLatLng = _ObjLocation.Plot[i].LatLng.split(";");

                for (j = 0; j < zListLatLng.length; j++) {
                    if (zListLatLng[j].length > 5) {
                        zLatLng = zListLatLng[j].split(",");
                        zTriangleCoords.push(new google.maps.LatLng(zLatLng[1], zLatLng[0]));
                    }
                }

                //Begin Label
                var bounds = new google.maps.LatLngBounds();
                var o;
                var polygonCoords = zTriangleCoords;
                for (o = 0; o < polygonCoords.length; o++) {
                    bounds.extend(polygonCoords[o]);
                }
                var marker = new google.maps.Marker({
                    position: bounds.getCenter(),
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: 0
                    },
                    label: _ObjLocation.Plot[i].name.replace("Nhà", "")
                });
                zMaker.push(marker);
                //End Label

                _Center = bounds.getCenter();

                // Construct the polygon.

                var zPlot = new google.maps.Polygon({
                    paths: zTriangleCoords,
                    strokeColor: _ObjLocation.Plot[i].strokeColor,
                    strokeWeight: _ObjLocation.Plot[i].strokeWeight,
                    strokeOpacity: 0.4,
                    fillColor: _ObjLocation.Plot[i].fillColor,
                    fillOpacity: 0.4,
                    asset: _ObjLocation.Plot[i].asset,
                    title: _ObjLocation.Plot[i].name,
                    time: _ObjLocation.Plot[i].time,
                    contractKey: _ObjLocation.Plot[i].contractKey,
                    area: _ObjLocation.Plot[i].area,
                    customer: _ObjLocation.Plot[i].customer,

                    editable: false,
                    draggable: false,
                });

                addListenersOnPolygon(zPlot);
                zBermudaTriangle.push(zPlot);
            }
            for (i = 0; i < n; i++) {
                zBermudaTriangle[i].setMap(_map);
                zMaker[i].setMap(_map);
            }

            //set focus map on change
            if (_Center === undefined) {
                _Center = new google.maps.LatLng(10.994824652295973, 106.67925226725616);                
                _map.setCenter(_Center);
                _map.setZoom(16);
            }
            else {
                _map.setCenter(_Center);
                _map.setZoom(19);
            }
        }
    } catch (e) {
        console.log(e);
    }    
}

google.maps.event.addDomListener(window, 'load', Initialize);
function Load_Map(Key) {
    $.ajax({
        url: URL_Map,
        type: 'POST',
        data: {
            "ProductKey": Key
        },
        success: function (r) {
            alert(r.message);
            $('#lbMap').append(r.message);
        },
        error: function (error) {
            alert(error);
        }
    });
}

function OpenGeneral(ProductKey, title) {
    var url = URL_General + '?ProductKey=' + ProductKey;
    $.dialog({
        theme: 'supervan',
        closeIcon: true,
        columnClass: 'xlarge',
        title: 'Thông tin ' + title,
        content: 'url:' + url,
        animation: 'scale',
        closeAnimation: 'scale',
        containerFluid: true,
        backgroundDismiss: true,
        onContentReady: function () {
            (function ($) {

                'use strict';

                $(function () {
                    $('[data-plugin-toggle]').each(function () {
                        var $this = $(this),
                            opts = {};

                        var pluginOptions = $this.data('plugin-options');
                        if (pluginOptions)
                            opts = pluginOptions;

                        $this.themePluginToggle(opts);
                    });
                });

            }).apply(this, [jQuery]);
            (function ($) {

                'use strict';

                if ($.isFunction($.fn['magnificPopup'])) {

                    $(function () {
                        $('[data-plugin-lightbox], .lightbox:not(.manual)').each(function () {
                            var $this = $(this),
                                opts = {};

                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;

                            $this.themePluginLightbox(opts);
                        });
                    });

                }

            }).apply(this, [jQuery]);
        }
    });
}
function OpenMaintain(ProductKey, ContractKey) {
    var url = URL_Maintain + '?ProductKey=' + ProductKey + '&ContractKey=' + ContractKey;
    var title = 'Nhật ký sữa chữa';

    $.dialog({
        theme: 'supervan',
        closeIcon: true,
        columnClass: 'xlarge',
        title: title,
        content: 'url:' + url,
        animation: 'scale',
        closeAnimation: 'scale',
        backgroundDismiss: true,
        onContentReady: function () {
            (function ($) {

                'use strict';

                if ($.isFunction($.fn['magnificPopup'])) {

                    $(function () {
                        $('[data-plugin-lightbox], .lightbox:not(.manual)').each(function () {
                            var $this = $(this),
                                opts = {};

                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;

                            $this.themePluginLightbox(opts);
                        });
                    });

                }

            }).apply(this, [jQuery]);
        },
    });
}
function OpenHistory(ProductKey, title) {
    var url = URL_History + '?ProductKey=' + ProductKey;
    $.dialog({
        theme: 'supervan',
        closeIcon: true,
        columnClass: 'xlarge',
        title: 'Quá trình khách hàng sử dụng ' + title,
        content: 'url:' + url,
        animation: 'scale',
        closeAnimation: 'scale',
        backgroundDismiss: true,
        onContentReady: function () {

        }
    });
}

function AddDiary() {
    Utils.OpenMagnific('#modalDiary');
    $("#txt_DateWrite").val(Utils.GetDate());
}
function SaveDiary() {

    var RegionKey = $("#cbo_Map").val(); //$('#txt_RegionKey').val();
    var ProductKey = $('#txt_ProductKey').val();    
    var ContractKey = $('#txt_ContractKey').val();
    var Date = $('#txt_DateWrite').val();
    var Note = $('#txt_Description').val();

    var formData = new FormData();
    var totalFiles = $("#fileUpload").get(0).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var files = $("#fileUpload").get(0).files[i];
        formData.append(files.name, files);  
    }

    var obj = {
        "AssetKey": ProductKey,
        "AreaKey": RegionKey,
        "ContractKey": ContractKey,
        "DateWrite": Date,
        "Description": Note,
    };

    formData.append("Asset", JSON.stringify(obj));
    
    $.ajax({
        url: URL_AddDiary,
        type: 'POST',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Lưu thành công !.", 'Thông báo', 'success');
                LoadDiary();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function LoadDiary() {
    var ProductKey = $('#txt_ProductKey').val();
    if (ProductKey === undefined) {
        ProductKey = $("#cbo_Map").val();
    }
    $.ajax({
        url: URL_LogDiary,
        type: 'POST',
        data: {
            "ProductKey": ProductKey
        },
        success: function (r) {
            $("#log").empty().append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        }
    });
}

//1 Open Modal Copy
function OpenModalCopy() {
    Utils.OpenMagnific("#modalContractCopy");
    var id = $('#txt_ContractViewID').val();
    $("#txt_CopyContractID").val(id);
    $(".mfp-bg").css('z-index', 999999999);
    $(".mfp-wrap").css('z-index', 999999999);
}
//2 Save Copy
function SaveCopy() {
    var ContractKey = $('#txt_ContractViewKey').val();
    var ContractID = $('#txt_ContractViewID').val();
    var ContractSub = $('#txt_CopySubContract').val();

    if (ContractKey.length <= 0) {
        Utils.OpenNotify('BUG INIT CONTRACT', 'Thông báo', 'error');
        return;
    }

    $.ajax({
        url: URL_ContractCopy,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "ContractID": ContractID,
            "ContractSub": ContractSub,
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                var url = URL_ContractCopyOpen;
                window.location.href = url.replace('contractkey', r.Data);
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//3 Open Page Copy
function OpenEditCopy(contractkey) {
    var url = URL_ContractCopyOpen;
    window.location.href = url.replace('contractkey', contractkey);
}
//mở thanh lý hợp đồng
function OpenModalContractEnd() {
    Utils.OpenMagnific('#modalContractEnd');
    Utils.ClearUI('#modalContractEnd');
}
//lưu thanh lý hợp đồng
function SaveContractEnd() {
    var ContractKey = $('#txt_ContractViewKey').val();
    var DateTerminate = $('#txt_DateLiquidation').val();
    var ReasonTerminate = $('#txt_ReasonLiquidation').val();

    $.ajax({
        url: URL_ContractEnd,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "DateTerminate": DateTerminate,
            "ReasonTerminate": ReasonTerminate,
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Đã cập nhật thanh lý !.', 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//mở thông tin hợp đồng chỉ xem
function OpenModelContractView(contractkey) {
    $.dialog({
        theme: 'bootstrap',
        closeIcon: true,
        columnClass: 'xlarge',
        title: 'Chi tiết',
        content: 'url:' + URL_ContractView + '?ContractKey=' + contractkey,
        animation: 'scale',
        closeAnimation: 'scale',
        backgroundDismiss: true,
    });
    return;
}

//
function CenterControl(controlDiv, map) {
    // Set CSS for the control border.
    const controlUI = document.createElement("div");
    controlUI.style.backgroundColor = "#fff";
    controlUI.style.border = "2px solid #fff";
    controlUI.style.borderRadius = "3px";
    controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
    controlUI.style.cursor = "pointer";
    controlUI.style.margin = "9px";
    controlUI.style.textAlign = "center";
    controlUI.title = "Xem nhật ký";
    controlDiv.appendChild(controlUI);
    // Set CSS for the control interior.
    const controlText = document.createElement("div");
    controlText.style.color = "rgb(25,25,25)";
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "16px";
    controlText.style.lineHeight = "38px";
    controlText.style.paddingLeft = "5px";
    controlText.style.paddingRight = "5px";
    controlText.innerHTML = "Nhật ký tổng khu";
    controlUI.appendChild(controlText);
    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener("click", () => {
        var ProductKey = $("#cbo_Map").val();
        var title = $("#cbo_Map option:selected").text();
        var url = URL_Maintain + '?ProductKey=' + ProductKey;
        $.dialog({
            theme: 'supervan',
            closeIcon: true,
            columnClass: 'xlarge',
            title: 'Nhật ký ' + title,
            content: 'url:' + url,
            animation: 'scale',
            closeAnimation: 'scale',
            containerFluid: true,
            backgroundDismiss: true,
            onContentReady: function () {
                (function ($) {

                    'use strict';

                    $(function () {
                        $('[data-plugin-toggle]').each(function () {
                            var $this = $(this),
                                opts = {};

                            var pluginOptions = $this.data('plugin-options');
                            if (pluginOptions)
                                opts = pluginOptions;

                            $this.themePluginToggle(opts);
                        });
                    });

                }).apply(this, [jQuery]);
                (function ($) {

                    'use strict';

                    if ($.isFunction($.fn['magnificPopup'])) {

                        $(function () {
                            $('[data-plugin-lightbox], .lightbox:not(.manual)').each(function () {
                                var $this = $(this),
                                    opts = {};

                                var pluginOptions = $this.data('plugin-options');
                                if (pluginOptions)
                                    opts = pluginOptions;

                                $this.themePluginLightbox(opts);
                            });
                        });

                    }

                }).apply(this, [jQuery]);
            }
        });
    });
}