﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ABG
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "trangchu",
               url: "trang-chu",
               defaults: new { controller = "WebSite", action = "trangchu" });

            routes.MapRoute(
              name: "gioithieu",
              url: "gioi-thieu/{ArticleKey}",
              defaults: new { controller = "WebSite", action = "chitietTin", ArticleKey = "" });

            routes.MapRoute(
            name: "cacbaiviet",
            url: "cac-bai-viet/{Category}",
            defaults: new { controller = "WebSite", action = "danhsachTin", Category = "" });


            routes.MapRoute(
                name: "Login",
                url: "dang-nhap",
                defaults: new { controller = "Login", action = "Login" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "WebSite", action = "Index", id = UrlParameter.Optional });
        }
    }
}
