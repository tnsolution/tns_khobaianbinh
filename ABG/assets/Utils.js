﻿var Utils = {
    ConvertToDDMMYYYY: function (input) {
        var today = new Date(input);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        if (yyyy === 1 &&
            dd === '01' &&
            mm === '01') {
            return '';
        }
        return today = dd + '/' + mm + '/' + yyyy;
    },
    PreviewImg: function (input, output) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(output).attr('src', e.target.result);
            };
            // convert to base64 string
            reader.readAsDataURL(input.files[0]);
        }
    },
    OpenMagnific: function (id) {
        $.magnificPopup.open({
            callbacks: {
                beforeOpen: function () { this.wrap.removeAttr('tabindex') }
            },
            items: {
                src: id
            },
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            modal: true
        });
    },
    OpenNotify: function (title, text, css) {

        new PNotify({
            title: title, //'Đã xóa thành công !.'
            text: text,
            type: css //success
        });

    },
    ClearUI: function (element) {
        $(element).find('input,textarea,select').not(':input[type=button], :input[type=submit], :input[type=reset]').val('');
        $(element).find('select').each(function () {
            $(this).val($(this).find("option:first").val());
        });
    },
    GetDate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
};
jQuery.loadScript = function (url, callback) {
    jQuery.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
    });
};
