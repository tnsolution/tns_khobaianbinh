﻿function Calculator() {
    var Total = 0;
    $("#OrderDetail").find(":input[name='SubTotal']").each(function () {
        Total += parseFloat($(this).val());
    });

    var SaleOff = parseFloat($("#txt_DiscountAmount").val());
    var Order = Total - SaleOff;

    $("#txt_TotalAmount").val(Total);
    $("#txt_OrderAmount").val(Order);
}
function LoadProduct(Category, Level) {
    $.ajax({
        url: URL_LoadProduct,
        type: 'GET',
        dataType: "html",
        data: {
            "Category": Category,
            "Level": Level,
            "Page": 1,
            "Name": $("#txt_SearchName").val()
        },
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            $("#DivProduct").empty().append(r);
        },
        error: function (err) {

        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');

            if (Level === 1) {
                LoadSubCategory(Category);
            }
        }
    });
}
function LoadSubCategory(Parent) {
    $.ajax({
        url: URL_LoadProductCategory,
        type: 'GET',
        dataType: "html",
        data: {
            "Level": 2,
            "Parent": Parent
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#DivSubCategory").empty().append(r);
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function ChooseProduct() {
    $("#DivProduct").on("click", "div[product]", function () {
        var Status = parseInt($("#txt_OrderStatusKey").val());
        if (Status === 2 ||
            Status === 9) {
            alert("Order này đã gửi xử lý hoặc đã thanh toán không thể thao tác thêm !. Vui lòng lập Order mới");
            return;
        }

        var productkey = $(this).attr("product");
        var orderKey = $("#txt_OrderKey").val();
        $.ajax({
            url: URL_ChooseProduct,
            type: 'POST',
            dataType: "json",
            data: {
                "OrderKey": orderKey,
                "ProductKey": productkey,
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    LoadOrderDetail();
                } else {
                    alert(r.Message);
                }
            },
            error: function (err) {

            },
            complete: function () {

            }
        });
    });
}
function EventDeleteOrderItem() {
    $("#OrderDetail").on("click", "button[name='DeleteItem']", function () {
        var Status = parseInt($("#txt_OrderStatusKey").val());
        if (Status === 2 ||
            Status === 9) {
            alert("Order này đã gửi xử lý hoặc đã thanh toán không thể thao tác thêm !. Vui lòng lập Order mới");
            return;
        }
        var orderKey = $("#txt_OrderKey").val();
        var itemkey = $(this).attr("itemkey");
        $.ajax({
            url: URL_EventDeleteOrderItem,
            type: 'POST',
            dataType: "json",
            data: {
                "OrderKey": orderKey,
                "ItemKey": itemkey,
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    LoadOrderDetail();
                } else {
                    alert(r.Message);
                }
            },
            error: function (err) {

            },
            complete: function () {
                Calculator();
            }
        });
    });
}
function LoadOrderDetail() {
    $.ajax({
        url: URL_OrderDetail,
        type: 'GET',
        dataType: "html",
        data: {

        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#OrderDetail").empty().append(r);
            $("#OrderDetail").find(":input.number").each(function () {
                $(this).number(true, 0, ',', '.');
            });
        },
        error: function (err) {

        },
        complete: function () {
            Calculator();
        }
    });
}
function ReviewOrder() {
    var orderKey = $("#txt_OrderKey").val();

    $.ajax({
        url: URL_Review,
        type: 'GET',
        dataType: "html",
        data: {
            "OrderKey": orderKey,
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#review-dialog").empty().append(r);
            $("#review-dialog input.number").number(true, 0, ',', '.');
        },
        error: function (err) {

        },
        complete: function () {
            $.magnificPopup.open({
                items: {
                    src: '#review-dialog'
                },
                type: 'inline'
            });
        }
    });
}
function Payment() {
    var orderKey = $("#txt_OrderKey").val();
    var ordermoney = $("#txt_AmountOrder").val();
    var discount = $("#txt_AmountDiscount").val();
    var ordermain = $("#txt_AmountCurrencyMain").val();
    var received = $("#txt_AmountReceived").val();
    var returned = $("#txt_AmountReturned").val();

    $.ajax({
        url: URL_Payment,
        type: 'POST',
        dataType: "json",
        data: {
            "OrderKey": orderKey,
            "AmountOrder": ordermoney,
            "AmountDiscount": discount,
            "AmountCurrencyMain": ordermain,
            "AmountReceived": received,
            "AmountReturned": returned
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                ShowInvoice();
            }
            else {
                alert(r.Message);
            }
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function ShowInvoice() {
    var orderKey = $("#txt_OrderKey").val();
    $.ajax({
        url: URL_ShowInvoice,
        type: 'GET',
        dataType: "html",
        data: {
            "OrderKey": orderKey,
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#invoice-dialog").empty().append(r);
        },
        error: function (err) {

        },
        complete: function () {
            $.magnificPopup.open({
                items: {
                    src: '#invoice-dialog'
                },
                type: 'inline'
            });
        }
    });
}
function CancelOrder() {
    var Status = parseInt($("#txt_OrderStatusKey").val());
    if (Status === 2 ||
        Status === 9) {
        alert("Order này đã gửi xử lý hoặc đã thanh toán không thể thao tác thêm !. Vui lòng lập Order mới");
        return;
    }

    if (confirm("Bạn có chắc hủy đơn hàng này !.")) {
        var orderKey = $("#txt_OrderKey").val();
        $.ajax({
            url: URL_OrderCancel,
            type: 'POST',
            dataType: "json",
            data: {
                "OrderKey": orderKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    window.location = r.Data;
                }
                else {
                    alert(r.Message);
                }
            },
            error: function (err) {

            },
            complete: function () {

            }
        });
    }
}
function AddOrder() {
    var ObjectKey = $("#txt_ObjectKey").val();    
    $.ajax({
        url: URL_AddOrder,
        type: 'POST',
        dataType: "json",
        data: {
            "ObjectKey": ObjectKey
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                window.location = r.Data;
            }
            else {
                alert(r.Message);
            }
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function PrintTicket() {
    $("#printSection").print({
        addGlobalStyles: true,
        stylesheet: URL_PrintCSS,
        rejectWindow: true,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null
    });
}
function OpenCustomer(Key) {
    $.ajax({
        url: URL_OpenCustomer,
        type: 'GET',
        dataType: "html",
        data: {
            "CustomerKey": Key
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#customer-dialog").empty().append(r);
        },
        error: function (err) {

        },
        complete: function () {
            $.magnificPopup.open({
                items: {
                    src: '#customer-dialog'
                },
                type: 'inline'
            });
        }
    });
}
function SaveCustomer() {
    var orderKey = $("#txt_OrderKey").val();
    var customerkey = $("#txt_CustomerKey").val();
    var phone = $("#txt_CustomerPhone").val();
    var id = $("#txt_CustomerID").val();
    var email = $("#txt_CustomerEmail").val();
    var name = $("#txt_CustomerName").val();
    var address = $("#txt_CustomerAddress").val();
    var note = $("#txt_CustomerNote").val();

    $.ajax({
        url: URL_SaveCustomer,
        type: 'POST',
        dataType: "json",
        data: {
            "OrderKey": orderKey,
            "CustomerKey": customerkey,
            "CustomerID": id,
            "CustomerName": name,
            "Phone": phone,
            "Email": email,
            "Address": address,
            "Description": note
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                $.magnificPopup.close();
                LoadOrderCustomer();
            }
            else {
                alert(r.Message);
            }
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function DeleteCustomer() {
    var Key = $("#txt_CustomerKey").val();
    if (confirm('Bạn có chắc xóa thông tin khách !.')) {
        $.ajax({
            url: URL_DeleteCustomer,
            type: 'POST',
            dataType: "html",
            data: {
                "CustomerKey": Key
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    LoadOrderCustomer();
                }
                else {
                    alert(r.Message);
                }
            },
            error: function (err) {

            },
            complete: function () {
                $.magnificPopup.close();
            }
        });
    }
}
function LoadOrderCustomer() {
    var orderKey = $("#txt_OrderKey").val();
    $.ajax({
        url: URL_LoadCustomer,
        type: 'GET',
        dataType: "html",
        data: {
            "OrderKey": orderKey
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#DivOrderCustomer").empty().append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {

        }
    });
}
function printDiv() {

    var divToPrint = document.getElementById('printSection');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html><body onload="window.print()" style="margin: 10px; font-size: 0.6rem !important; font-family: arial !important"><style>table { font-size: 0.6rem !important } </style>' + divToPrint.innerHTML + '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);

}
function SearchPhone() {
    var phone = $("#txt_CustomerPhone").val();
    $.ajax({
        url: URL_SearchPhone,
        type: 'GET',
        dataType: "html",
        data: {
            "Phone": phone
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#customer-dialog").empty().append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {

        }
    });
}
function SearchID() {
    var id = $("#txt_CustomerID").val();
    $.ajax({
        url: URL_SearchID,
        type: 'GET',
        dataType: "html",
        data: {
            "ID": id
        },
        beforeSend: function () {

        },
        success: function (r) {
            $("#customer-dialog").empty().append(r);
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {

        }
    });
}
function ClearSession() {
    $.ajax({
        url: URL_ClearSession,
        type: 'POST',
        dataType: "json",
        data: {

        },
        beforeSend: function () {

        },
        success: function (r) {

        },
        error: function (err) {

        },
        complete: function () {

        }
    });
    return true;
}
function SetUpOrder() {
    var orderKey = $("#txt_OrderKey").val();
    $.ajax({
        url: URL_SetupOrder,
        type: 'POST',
        dataType: "json",
        data: {
            "OrderKey": orderKey
        },
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            if (r.Success) {
                location.reload(true);
            }
            else {
                alert(r.Message);
            }
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        }
    });
}
window.onbeforeunload = function (e) {
    var isVisible = true;
    if (isVisible === true) {
        ClearSession();
        $(window).unbind();
        return undefined;
        //is there any way to disable dialog alert and close the website??
    } else {
        return 'you havent close';
    }
};

$('.magnificPopup').magnificPopup({
    type: 'inline',
    mainClass: 'mfp-fade',
    midClick: true
});
$('#sector-dialog').on("click", "div[data-action]", function () {
    var url = $(this).attr('data-action');
    if (url !== undefined)
        window.location.href = url;
});

$('#DivOrderList').on('click', 'a.btn', function () {
    $('#DivOrderList a.btn-primary').removeClass('btn-primary').addClass('btn-default');
    $(this).removeClass('btn-default').addClass('btn-primary');
});
$('#DivCategory').on('click', 'a.btn', function () {
    $('#DivCategory a.btn-primary').removeClass('btn-primary').addClass('btn-default');
    $(this).removeClass('btn-default').addClass('btn-primary');
});
$('#DivSubCategory').on('click', 'a.btn', function () {
    $('#DivSubCategory a.btn-primary').removeClass('btn-primary').addClass('btn-default');;
    $(this).removeClass('btn-default').addClass('btn-primary');
});
$('#review-dialog').on('blur', 'input.number:not(not-cal)', function () {
    var AmountOrder = parseFloat($('#txt_AmountOrder').val());
    var AmountDiscount = parseFloat($('#txt_AmountDiscount').val());

    var AmountReceived = parseFloat($('#txt_AmountReceived').val());
    var AmountReturned = parseFloat($('#txt_AmountReturned').val());

    var AmountCurrencyMain = AmountOrder - AmountDiscount;
    $('#txt_AmountCurrencyMain').val(AmountCurrencyMain);

    AmountCurrencyMain = parseFloat($('#txt_AmountCurrencyMain').val());
    AmountReturned = -(AmountCurrencyMain - AmountReceived);
    $('#txt_AmountReturned').val(AmountReturned);
});

//
$(document).on('click', '.custom-close', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});
$(document).ready(function () {
    //active order đang hiện xử lý
    var ActiveOrder = $("#txt_OrderKey").val();
    $("#DivOrderList a.btn").each(function () {
        $(this).removeClass('btn-primary').addClass('btn-default');
    });
    $("#DivOrderList a[orderkey='" + ActiveOrder + "']").addClass(('btn-primary'));

    var height = $(window).height() - 350;

    $('#LeftPanel .slim-scroll').slimScroll({
        height: height - 190
    });
    $('#RightPanel .slim-scroll').slimScroll({
        height: height
    });

    ChooseProduct();
    LoadOrderDetail();
    LoadOrderCustomer();
    EventDeleteOrderItem();

    $("#txt_TotalAmount").number(true, 0, ',', '.');
    $("#txt_OrderAmount").number(true, 0, ',', '.');
    $("#txt_DiscountAmount").number(true, 0, ',', '.');


});