﻿// We can attach the `fileselect` event to all file inputs on the page
$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

    SaveFile();
});

// We can watch for our custom `fileselect` event like this
$(document).ready(function () {
    $(':file').on('fileselect', function (e, files, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = files > 1 ? files + ' files selected' : label;

        if (input.length) {
            input.val(log);
        }
        else {
            if (log) {
                alert(log);
            }
        }
    });
    $('#cbo_Employee').change(function () {
        var key = $(this).val();
        GetEmployeeInfo(key);
    });
    $("#cbo_Minus").change(function () {
        var Item = $('#cbo_Minus').val();
        MinusInfo(Item);
    });
    InitView();
});
function SaveContract() {
    var ContractKey = $('#txt_ContractKey').val();
    var ContractID = $('#txt_ContractID').val();
    var SubContract = $('#txt_SubContract').val();
    var SignDate = $('#txt_SignDate').val();
    var ContractType = $('#cbo_Type').val();
    var ContractTypeName = $("#cbo_Type option:selected").text();
    var SignBy = $('#cbo_SignBy').val();
    var SignName = $("#cbo_SignBy option:selected").text();
    var SignPositionKey = $('#cbo_SignPosition').val();
    var SignPositionName = $("#cbo_SignPosition option:selected").text();
    var EmployeeKey = $('#cbo_Employee').val();
    var EmployeeName = $("#cbo_Employee option:selected").text();
    var EmployeeGender = $('#txt_EmployeeGender').val();
    var EmployeeBirthDay = $('#txt_EmployeeBirthDay').val();
    var EmployeeBirthPlace = $('#txt_EmployeeBirthPlace').val();
    var EmployeePassport = $('#txt_EmployeePassport').val();
    var EmployeeIssueDate = $('#txt_EmployeeIssueDate').val();
    var EmployeeIssuePlace = $('#txt_EmployeeIssuePlace').val();
    var BranchAdress = $('#txt_BranchAdress').val();
    var EmployeeAdress = $('#txt_EmployeeAdress').val();
    var ReportToKey = $('#cbo_ReportTo').val();
    var ReportToName = $("#cbo_ReportTo option:selected").text();
    var DepartmentKey = $('#cbo_Department').val();
    var DepartmentName = $("#cbo_Department option:selected").text();

    var BranchKey = $('#cbo_Branch').val();
    var BranchName = $("#cbo_Branch option:selected").text();

    var PositionKey = $('#cbo_Position').val();
    var PositionName = $("#cbo_Position option:selected").text();
    var FromDate = $('#txt_FromDate').val();
    var ToDate = $('#txt_ToDate').val();
    var DateBeginSalary = $('#txt_DateBeginSalary').val();
    var Note = $('#txt_Note').val();

    $.ajax({
        url: URL_SaveContract,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "ContractID": ContractID,
            "SubContract": SubContract,
            "SignDate": SignDate,
            "ContractType": ContractType,
            "ContractTypeName": ContractTypeName,
            "SignBy": SignBy,
            "SignName": SignName,
            "SignPositionKey": SignPositionKey,
            "SignPositionName": SignPositionName,
            "EmployeeKey": EmployeeKey,
            "EmployeeName": EmployeeName,
            "EmployeeGender": EmployeeGender,
            "EmployeeBirthDay": EmployeeBirthDay,
            "EmployeeBirthPlace": EmployeeBirthPlace,
            "EmployeePassport": EmployeePassport,
            "EmployeeIssueDate": EmployeeIssueDate,
            "EmployeeIssuePlace": EmployeeIssuePlace,
            "EmployeeAdress": EmployeeAdress,
            "ReportToKey": ReportToKey,
            "ReportToName": ReportToName,
            "DepartmentKey": DepartmentKey,
            "DepartmentName": DepartmentName,
            "BranchKey": BranchKey,
            "BranchName": BranchName,
            "PositionKey": PositionKey,
            "PositionName": PositionName,
            "FromDate": FromDate,
            "ToDate": ToDate,
            "DateBeginSalary": DateBeginSalary,
            "Note": Note,
            "BranchAdress": BranchAdress
        },
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Cập nhật thành công', 'Thông báo', 'success');
                $('#txt_ContractKey').val(r.Data);
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
            InitView();
            $('.se-pre-con').fadeOut('slow');
        }
    });
}

function ListFile() {
    var ContractKey = $('#txt_ContractKey').val();
    $.ajax({
        url: URL_ListFile,
        type: 'GET',
        data: {
            "ContractKey": ContractKey,
        },
        beforeSend: function () {
            $('#PanelFile').empty();
        },
        success: function (r) {
            $('#PanelFile').append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function SaveFile() {
    var ContractKey = $('#txt_ContractKey').val();
    var formData = new FormData();
    var fileUpload = $("#fileUpload").get(0);
    var files = fileUpload.files;
    for (var i = 0; i < files.length; i++) {
        formData.append(files[i].name, files[i]);
    }
    formData.append("ContractKey", ContractKey);
    formData.append("File", files);

    $.ajax({
        url: URL_UploadFile,
        type: 'POST',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Tải tập tin lên thành công !.", 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
            ListFile();
        }
    });
}
function DeleteFile(AutoKey) {
    $.ajax({
        url: URL_DeleteFile,
        type: 'POST',
        data: {
            "id": AutoKey,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                $('#PanelFile').empty();
                ListFile();
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function ViewFile(AutoKey) {

}

function ListItem() {
    var ContractKey = $('#txt_ContractKey').val();
    $.ajax({
        url: URL_ListItem,
        type: 'GET',
        data: {
            "ContractKey": ContractKey,
        },
        beforeSend: function () {
            $('#PanelSalary').empty();
        },
        success: function (r) {
            $('#PanelSalary').append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function SaveItem() {
    var AutoKey = $('#txt_AutoKey').val();
    var ContractKey = $('#txt_ContractKey').val();
    var ItemKey = $('#cbo_Payroll').val();
    var ItemName = $("#cbo_Payroll option:selected").text();
    var Total = $('#txt_TotalItem').val();
    var Description = $('#txt_DescriptionItem').val();
    $.ajax({
        url: URL_SaveItem,
        type: 'POST',
        data: {
            "AutoKey": AutoKey,
            "ContractKey": ContractKey,
            "ItemKey": ItemKey,
            "ItemName": ItemName,
            "Total": Total,
            "Description": Description
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                ListItem();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailItem(AutoKey) {
    Utils.OpenMagnific('#modalEditItem');
    if (AutoKey > 0) {
        $.ajax({
            url: URL_DetailItem,
            type: 'GET',
            data: {
                "AutoKey": AutoKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_AutoKey').val(obj.AutoKey);
                    $('#txt_ItemKey').val(obj.ItemKey);
                    $('#txt_TotalItem').val(obj.Total);
                    $('#txt_DescriptionItem').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI('#modalEditItem');
        $('#txt_AutoKey').val(0);
        $('#txt_TotalItem').val(0);
    }
}
function DeleteItem(AutoKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteItem,
                        type: 'POST',
                        data: {
                            "AutoKey": AutoKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                ListItem();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveMinus() {
    var AutoKey = $('#txt_MinusKey').val();
    var ContractKey = $('#txt_ContractKey').val();
    var ItemKey = $('#cbo_Minus').val();
    var ItemName = $("#cbo_Minus option:selected").text();
    var Quantity = $('#txt_QuantityMinus').val();
    var Total = $('#txt_TotalMinus').val();
    var Description = $('#txt_DescriptionMinus').val();
    $.ajax({
        url: URL_SaveMinus,
        type: 'POST',
        data: {
            "AutoKey": AutoKey,
            "ContractKey": ContractKey,
            "ItemKey": ItemKey,
            "ItemName": ItemName,
            "Quantity": Quantity,
            "Total": Total,
            "Description": Description
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                ListItem();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
function DetailMinus(AutoKey) {
    Utils.OpenMagnific('#modalEditMinus');
    if (AutoKey > 0) {
        $.ajax({
            url: URL_DetailMinus,
            type: 'GET',
            data: {
                "AutoKey": AutoKey
            },
            beforeSend: function () {
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_MinusKey').val(obj.AutoKey);
                    $('#cbo_Minus').val(obj.ItemKey);
                    $('#txt_QuantityMinus').val(obj.Quantity);
                    $('#txt_TotalMinus').val(obj.Total);
                    $('#txt_DescriptionMinus').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
            }
        });
    }
    else {
        Utils.ClearUI('#modalEditMinus');
        $('#txt_MinusKey').val(0);
        $('#txt_QuantityMinus').val(0);
        $('#txt_TotalMinus').val(0);
    }
}
function DeleteMinus(AutoKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteMinus,
                        type: 'POST',
                        data: {
                            "AutoKey": AutoKey
                        },
                        beforeSend: function () {
                        },
                        success: function (r) {
                            if (r.Success) {
                                ListItem();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {
                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}
function MinusInfo(Key) {
    var ContractKey = $('#txt_ContractKey').val();
    $.ajax({
        url: URL_MinusInfo,
        type: 'GET',
        data: {
            "ItemKey": Key
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                var obj = JSON.parse(r.Data);
                $('#txt_QuantityMinus').val(obj.Quantity);
                $('#txt_TotalMinus').val(obj.Total);
                Caculator();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}

function Caculator() {
    var Quantity = $('#txt_QuantityMinus').val();
    var Total = $('#txt_TotalMinus').val();
}

function GetEmployeeInfo(Key) {
    if (Key.length >= 36) {
        $.ajax({
            url: URL_EmployeeInfo,
            type: 'GET',
            data: {
                "EmployeeKey": Key
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zBirthDay = Utils.ConvertToDDMMYYYY(obj.Birthday);
                    var zIssueDate = Utils.ConvertToDDMMYYYY(obj.IssueDate);

                    $('#cbo_Employee').val(obj.EmployeeKey);
                    if (obj.EmployeeGender === 0)
                        $('#txt_EmployeeGender').val('Nữ');
                    else
                        $('#txt_EmployeeGender').val('Nam');
                    $('#txt_EmployeeBirthDay').val(zBirthDay);
                    $('#txt_EmployeeBirthPlace').val(obj.BirthPlace);
                    $('#txt_EmployeePassport').val(obj.PassportNumber);
                    $('#txt_EmployeeIssueDate').val(zIssueDate);
                    $('#txt_EmployeeIssuePlace').val(obj.IssuePlace);
                    $('#txt_EmployeeInsurceNumber').val('');
                    $('#txt_EmployeeAdress').val(obj.AddressRegister);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
            }
        });
    }
}
function InitView() {
    var ContractKey = $('#txt_ContractKey').val();
    if (ContractKey.length < 36) {
        $('#btn_AddNew').hide();
        $('#panel_FileUpload').hide();
    }
    else {
        $('#btn_AddNew').show();
        $('#panel_FileUpload').show();
    }
}