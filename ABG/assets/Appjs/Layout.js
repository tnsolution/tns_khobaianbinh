﻿$(document).ready(function () {
    $('#sidebar-left').on('click', 'a', function () {
        var link = $(this).attr('href');
        if (link !== '#') {
            $('.se-pre-con').fadeIn('slow');
            localStorage.setItem('url', link);
        }
    });
    $(".select2").select2({
        width: '100%',
        placeholder: '--Chọn--',
        tags: true,
        selectOnBlur: true,
        createTag: function (tag) {
            return { id: 'newid', text: tag.term, tag: true };
        }
    });
    $(".datepicker").datepicker({
        todayHighlight: true,
        autoclose: true
    });

    if (localStorage.getItem('url') !== null) {
        var url = localStorage.getItem('url');
        $('a[href="' + url + '"]').addClass('active')
            .closest('li')
            .parents('li.nav-parent')
            .addClass('nav-expanded');
    }
    else {
        var pathname = document.location.pathname;
        $('#sidebar-left a').each(function () {
            var value = jQuery(this).attr('href');
            if (pathname.indexOf(value) > -1) {
                $(this).addClass('active')
                    .closest('li')
                    .parents('li.nav-parent')
                    .addClass('nav-expanded');
                return false;
            }
        });
    }

    $('input.money').inputmask("decimal", {
        radixPoint: ",",
        autoGroup: true,
        digits: 2,
        groupSeparator: ".",
        groupSize: 3
    });

    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    $("#btnChangePass").click(function () {
        Utils.OpenMagnific("#modalChangePass");
    });
});

function ChangePass() {
    var OldPass = $("#txt_OldPass").val();
    var NewPass = $("#txt_NewPass").val();
    
    $.ajax({
        url: '/Config/ChangePass',
        type: 'POST',
        data: {
            "OldPass": OldPass,
            "NewPass": NewPass,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Đổi mật khẩu thành công !.", 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}

$(document).on('click', 'a[viewdoc]', function (e) {
    var link = '';
    var url = '';
    if (location.port.length > 0) {
        url = document.location.protocol + "//" + document.location.hostname + ":" + location.port;
    }
    else {
        url = document.location.protocol + "//" + document.location.hostname;
    }

    var attr = $(this).attr('href');
    if (attr.indexOf('.doc') !== -1 ||
        attr.indexOf('.docx') !== -1) {
        link += 'http://docs.google.com/viewer?embedded=true&url=' + url + $(this).attr('href');        
    }
    if (attr.indexOf('.xls') !== -1 ||
        attr.indexOf('.xlsx') !== -1) {
        link += 'http://docs.google.com/viewer?embedded=true&url=' + url + $(this).attr('href');       
    }
    if (attr.indexOf('.pdf') !== -1) {
        link += 'http://docs.google.com/viewer?embedded=true&url=' + url + $(this).attr('href');
    }

    var height = $(window).height();
    var iframe = "<iframe name = 'iFrameBackOffice' frameborder = '0' marginwidth = '0' marginheight = '0' allowfullscreen scrolling = 'no' style = 'position: relative; border: none; padding: 0px; margin: 0px;' width = '100%' height = '" + height + "' src = '" + link + "'></iframe> ";

    $.dialog({
        theme: 'supervan',
        closeIcon: true,
        columnClass: 'xlarge',
        title: 'Xem tập tin',
        content: iframe,
        animation: 'scale',
        closeAnimation: 'scale',
        backgroundDismiss: true,
    });
});
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});