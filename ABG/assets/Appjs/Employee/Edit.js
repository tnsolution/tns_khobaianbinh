﻿$(document).ready(function () {

});

$("#imgpreview").click(function () {
    $("#fileListPhoto").trigger('click');
});
$("#fileListPhoto").change(function () {
    Utils.PreviewImg(this, "#imgpreview");
});

function SaveInfo() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var LastName = $('#txt_LastName').val();
    var FirstName = $('#txt_FirstName').val();
    var rdoGender = $('input[name="rdoGender"]:checked').val();
    var BirthDay = $('#txt_Birthday').val();
    var Passport = $('#txt_Passport').val();
    var Address = $('#txt_Address').val();
    var Email = $('#txt_Email').val();
    var MobiPhone = $('#txt_MobiPhone').val();

    var obj = {
        "EmployeeKey": EmployeeKey,
        "LastName": LastName,
        "FirstName": FirstName,
        "Gender": rdoGender,
        "BirthDay": BirthDay,
        "Passport": Passport,
        "Address": Address,
        "Email": Email,
        "MobiPhone": MobiPhone,
    };

    var formData = new FormData();
    var totalFiles = $("#fileListPhoto").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = $("#fileListPhoto").files[i];
    }

    formData.append("Employee", obj);
    formData.append("File", file);

    $.ajax({
        url: URL_SaveInfo,
        type: 'POST',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Cập nhật thông tin cơ bản thành công !.", 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}

function SaveFamily() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var FatherName = $('#txt_FatherName').val();
    var FatherBirthday = $('#txt_FatherBirthday').val();
    var FatherWork = $('#txt_FatherWork').val();
    var FatherAddress = $('#txt_FatherAddress').val();
    var MotherName = $('#txt_MotherName').val();
    var MotherBirthday = $('#txt_MotherBirthday').val();
    var MotherWork = $('#txt_MotherWork').val();
    var MotherAddress = $('#txt_MotherAddress').val();
    var PartnersName = $('#txt_PartnersName').val();
    var PartnersBirthday = $('#txt_PartnersBirthday').val();
    var PartnersWork = $('#txt_PartnersWork').val();
    var PartnersAddress = $('#txt_PartnersAddress').val();
    var ChildExtend = $('#txt_ChildExtend').val();
    var OrtherExtend = $('#txt_OrtherExtend').val();

    $.ajax({
        url: URL_SaveFamily,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "FatherName": FatherName,
            "FatherBirthday": FatherBirthday,
            "FatherWork": FatherWork,
            "FatherAddress": FatherAddress,
            "MotherName": MotherName,
            "MotherBirthday": MotherBirthday,
            "MotherWork": MotherWork,
            "MotherAddress": MotherAddress,
            "PartnersName": PartnersName,
            "PartnersBirthday": PartnersBirthday,
            "PartnersWork": PartnersWork,
            "PartnersAddress": PartnersAddress,
            "ChildExtend": ChildExtend,
            "OrtherExtend": OrtherExtend
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Cập nhật thông tin gia đình thành công !.", 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailFamily(EmployeeKey) {
    if (EmployeeKey.length >= 36) {
        $.ajax({
            url: URL_DetailFamily,
            type: 'GET',
            data: {
                "EmployeeKey": EmployeeKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zFatherBirthday = Utils.ConvertToDDMMYYYY(obj.FatherBirthday);
                    var zMotherBirthday = Utils.ConvertToDDMMYYYY(obj.MotherBirthday);
                    var zPartnersBirthday = Utils.ConvertToDDMMYYYY(obj.PartnersBirthday);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#txt_FatherName').val(obj.FatherName);
                    $('#txt_FatherBirthday').val(zFatherBirthday);
                    $('#txt_FatherWork').val(obj.FatherWork);
                    $('#txt_FatherAddress').val(obj.FatherAddress);
                    $('#txt_MotherName').val(obj.MotherName);
                    $('#txt_MotherBirthday').val(zMotherBirthday);
                    $('#txt_MotherWork').val(obj.MotherWork);
                    $('#txt_MotherAddress').val(obj.MotherAddress);
                    $('#txt_PartnersName').val(obj.PartnersName);
                    $('#txt_PartnersBirthday').val(zPartnersBirthday);
                    $('#txt_PartnersWork').val(obj.PartnersWork);
                    $('#txt_PartnersAddress').val(obj.PartnersAddress);
                    $('#txt_ChildExtend').val(obj.ChildExtend);
                    $('#txt_OrtherExtend').val(obj.OrtherExtend);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

                // $(".select2").select2({ width: "100%", placeholder: "--Chọn--" });
            }
        });
    }
    else {
        Utils.ClearUI('#modalEdit');
    }
}

function SaveJob() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var StartingDate = $('#txt_StartingDate').val();
    var ReportToKey = $('#cbo_ReportTo').val();
    var ReportToName = $("#cbo_ReportTo option:selected").text();
    var DepartmentKey = $('#cbo_Department').val();
    var DepartmentName = $("#cbo_Department option:selected").text();
    var BranchKey = $('#cbo_Branch').val();
    var BranchName = $("#cbo_Branch option:selected").text();
    var PositionKey = $('#cbo_Position').val();
    var PositionName = $("#cbo_Position option:selected").text();
    var Note = $('#txt_Note').val();
    var StatusKey = $('#cbo_Status').val();
    var StatusName = $("#cbo_Status option:selected").text();
    $.ajax({
        url: URL_SaveJob,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "StartingDate": StartingDate,
            "ReportToKey": ReportToKey,
            "ReportToName": ReportToName,
            "DepartmentKey": DepartmentKey,
            "DepartmentName": DepartmentName,
            "BranchKey": BranchKey,
            "BranchName": BranchName,
            "PositionKey": PositionKey,
            "PositionName": PositionName,
            "Note": Note,
            "StatusKey": StatusKey,
            "StatusName": StatusName
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Cập nhật thông tin công việc thành công !.", 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailJob(EmployeeKey) {
    if (EmployeeKey.length >= 36) {
        $.ajax({
            url: URL_DetailJob,
            type: 'GET',
            data: {
                "EmployeeKey": EmployeeKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zStartingDate = Utils.ConvertToDDMMYYYY(obj.StartingDate);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#cbo_ReportTo').val(obj.ReportToKey);
                    $('#cbo_Department').val(obj.DepartmentKey);
                    $('#cbo_Position').val(obj.PositionKey);
                    $('#txt_Note').val(obj.Note);
                    $('#txt_StartingDate').val(zStartingDate);
                    $('#cbo_Status').val(obj.WorkingStatusKey);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
                $(".select2").select2({ width: "100%", placeholder: "--Chọn--" });
            }
        });
    }
    else {
        //Utils.ClearUI('#modalEdit');
    }
}

function SaveEdu() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var AutoKey = $('#txt_EduKey').val();
    var FromDate = $('#txt_FromDateEdu').val();
    var ToDate = $('#txt_ToDateEdu').val();
    var DegreeName = $('#txt_DegreeNameEdu').val();
    var DegreeBy = $('#txt_DegreeByEdu').val();
    var StatusKey = $('#cbo_StatusEdu').val();
    var StatusName = $("#cbo_StatusEdu option:selected").text();
    var Description = $('#txt_DescriptionEdu').val();
    var ClassifiedKey = $('#cbo_ClassifiedEdu').val();
    var ClassifiedName = $("#cbo_ClassifiedEdu option:selected").text();
    var TypeName = $('#txt_TypeNameEdu').val();

    $.ajax({
        url: URL_SaveEdu,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "AutoKey": AutoKey,
            "FromDate": FromDate,
            "ToDate": ToDate,
            "DegreeName": DegreeName,
            "DegreeBy": DegreeBy,
            "StatusKey": StatusKey,
            "StatusName": StatusName,
            "Description": Description,
            "ClassifiedKey": ClassifiedKey,
            "ClassifiedName": ClassifiedName,
            "TypeName": TypeName
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailEdu(EduKey) {
    Utils.OpenMagnific('#modalEditEdu');
    if (EduKey > 0) {
        $.ajax({
            url: URL_DetailEdu,
            type: 'GET',
            data: {
                "AutoKey": EduKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zFromDate = Utils.ConvertToDDMMYYYY(obj.FromDate);
                    var zToDate = Utils.ConvertToDDMMYYYY(obj.ToDate);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#txt_EduKey').val(obj.AutoKey);
                    $('#txt_FromDateEdu').val(zFromDate);
                    $('#txt_ToDateEdu').val(zToDate);
                    $('#txt_DegreeNameEdu').val(obj.DegreeName);
                    $('#txt_DegreeByEdu').val(obj.DegreeBy);
                    $('#cbo_StatusEdu').val(obj.StatusKey);
                    $('#txt_DescriptionEdu').val(obj.Description);
                    $('#cbo_ClassifiedEdu').val(obj.ClassifiedKey);
                    $('#txt_TypeNameEdu').val(obj.TypeName);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
                $(".select2").select2({ width: "100%", placeholder: "--Chọn--" });
            }
        });
    }
    else {
        Utils.ClearUI('#modalEditEdu');
    }
}
function DeleteEdu(EduKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteEdu,
                        type: 'POST',
                        data: {
                            "AutoKey": EduKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveExp() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var AutoKey = $('#txt_ExpKey').val();
    var FromDate = $('#txt_FromDateExp').val();
    var ToDate = $('#txt_ToDateExp').val();
    var UnitWork = $('#txt_UnitWorkExp').val();
    var UnitPosition = $('#txt_UnitPositionExp').val();
    var Description = $('#txt_DescriptionExp').val();

    $.ajax({
        url: URL_SaveExp,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "AutoKey": AutoKey,
            "FromDate": FromDate,
            "ToDate": ToDate,
            "UnitWork": UnitWork,
            "UnitPosition": UnitPosition,
            "Description": Description
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailExp(ExpKey) {
    Utils.OpenMagnific('#modalEditExp');
    if (ExpKey > 0) {
        $.ajax({
            url: URL_DetailExp,
            type: 'GET',
            data: {
                "AutoKey": ExpKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zFromDate = Utils.ConvertToDDMMYYYY(obj.FromDate);
                    var zToDate = Utils.ConvertToDDMMYYYY(obj.ToDate);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#txt_ExpKey').val(obj.AutoKey);
                    $('#txt_FromDateExp').val(zFromDate);
                    $('#txt_ToDateExp').val(zToDate);
                    $('#txt_UnitWorkExp').val(obj.UnitWork);
                    $('#txt_UnitPositionExp').val(obj.UnitPosition);
                    $('#txt_DescriptionExp').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
                $(".select2").select2({ width: "100%", placeholder: "--Chọn--" });
            }
        });
    }
    else {
        Utils.ClearUI('#modalEditExp');
    }
}
function DeleteExp(ExpKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteExp,
                        type: 'POST',
                        data: {
                            "AutoKey": ExpKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveSkill() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var AutoKey = $('#txt_SkillKey').val();
    var SkillName = $('#txt_SkillNameSk').val();
    var Levels = $('#txt_LevelsSk').val();
    var Maxlevels = 10;
    var Description = $('#txt_DescriptionSk').val();
    $.ajax({

        url: URL_SaveSkill,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "AutoKey": AutoKey,
            "SkillName": SkillName,
            "Levels": Levels,
            "Maxlevels": Maxlevels,
            "Description": Description
        },

        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailSkill(SkillKey) {
    Utils.OpenMagnific('#modalEditSkill');

    if (SkillKey > 0) {
        $.ajax({
            url: URL_DetailSkill,
            type: 'GET',
            data: {
                "AutoKey": SkillKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#txt_SkillKey').val(obj.AutoKey);
                    $('#txt_SkillNameSk').val(obj.SkillName);
                    $('#txt_LevelsSk').val(obj.Levels);
                    $('#txt_MaxlevelsSK').val(obj.Maxlevels);
                    $('#txt_DescriptionSk').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {

        Utils.ClearUI('#modalEditSkill');
        $('#txt_SkillKey').val(0);
        $('#txt_MaxlevelsSK').val(10);
    }

}
function DeleteSkill(SkillKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteSkill,
                        type: 'POST',
                        data: {
                            "AutoKey": SkillKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveIn() {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    var AutoKey = $('#txt_InKey').val();
    var DateWrite = $('#txt_DateWriteIn').val();
    var StatusName = $('#txt_StatusNameIn').val();
    var Description = $('#txt_DescriptionIn').val();

    $.ajax({
        url: URL_SaveIn,
        type: 'POST',
        data: {
            "EmployeeKey": EmployeeKey,
            "AutoKey": AutoKey,
            "DateWrite": DateWrite,
            "StatusName": StatusName,
            "Description": Description
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailIn(InKey) {
    Utils.OpenMagnific('#modalEditIn');
    if (InKey > 0) {
        $.ajax({
            url: URL_DetailIn,
            type: 'GET',
            data: {
                "AutoKey": InKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var zDateWrite = Utils.ConvertToDDMMYYYY(obj.DateWrite);
                    $('#txt_EmployeeKey').val(obj.EmployeeKey);
                    $('#txt_InKey').val(obj.AutoKey);
                    $('#txt_DateWriteIn').val(zDateWrite);
                    $('#txt_StatusNameIn').val(obj.StatusName);
                    $('#txt_DescriptionIn').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI('#modalEditIn');
        $('#txt_InKey').val(0);
    }
}
function DeleteIn(InKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteIn,
                        type: 'POST',
                        data: {
                            "AutoKey": InKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function DeletePayroll(Key) {
    var EmployeeKey = $('#txt_EmployeeKey').val();
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeletePayroll,
                        type: 'POST',
                        data: {
                            "PayrollKey": Key
                        },
                        beforeSend: function () {
                            $("#tbodySalary").empty();
                        },
                        success: function (r) {
                            if (r.Success) {
                                LoadPayroll(EmployeeKey);
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}
function LoadPayroll(Key) {
    $.ajax({
        url: URL_PayrollList,
        type: 'GET',
        data: {
            "EmployeeKey": Key
        },
        beforeSend: function () {
            $("#tbodySalary").empty();
        },
        success: function (r) {
            $("#tbodySalary").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailPayroll(Key) {
    Utils.OpenMagnific('#modalEditPayroll');
    $.ajax({
        url: URL_Payroll,
        type: 'GET',
        data: {
            "PayrollKey": Key
        },
        beforeSend: function () {
            $("#modalEditPayroll").empty();
        },
        success: function (r) {
            $("#modalEditPayroll").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
            Utils.InputMoney();
        }
    });
}
function SavePayroll() {

    var EmployeeKey = $("#txt_EmployeeKey").val();
    var EmployeeID = $("#txt_EmployeeID").val();
    var EmployeeName = $("#txt_LastName").val() + " " + $("#txt_FirstName").val();
    var BranchKey = $('#cbo_Branch').val();
    var BranchName = $("#cbo_Branch option:selected").text();
    var DepartmentKey = $('#cbo_Department').val()
    var DepartmentName = $("#cbo_Department option:selected").text();
    var PositionKey = $('#cbo_Position').val();
    var PositionName = $("#cbo_Position option:selected").text();
    var Description = $("#txt_DescriptionItem").val();
    var ToDate = $("#txt_ToDate").val();
    var FromDate = $("#txt_FromDate").val();
    var PayrollKey = $("#txt_PayrollKey").val();
    var Activated = $("#cbo_Active").val();

    if (FromDate.length === 0) {
        Utils.OpenNotify('Bạn phải nhập thời gian áp dụng', 'Thông báo', 'warning');
        return;
    }
    if (ToDate.length === 0) {
        ToDate = '01/01/0001';
    }

    var ListItem = [];
    $('#tblBody tr').each(function () {
        var temp = $(this).find('select[name=cbo_Payroll]').val();

        var AutoKey = $(this).attr('AutoKey');
        var ItemKey = parseInt(temp.split(";")[0]);
        var ItemID = temp.split(";")[1];
        var ItemName = $(this).find('select[name=cbo_Payroll] option:selected').text();
        var Quantity = $(this).find('input[name=txt_Quantity]').val();
        var Total = $(this).find('input[name=txt_Total]').val();
        var UnitName = $(this).find('select[name=cbo_Unit]').val();
        var RecordStatus = $(this).attr('RecordStatus');
        var Description = $(this).find('input[name=txt_Description]').val();

        var item = new Object();
        item.AutoKey = AutoKey;
        item.Quantity = Utils.ParseDouble(Quantity);
        item.ItemKey = ItemKey;
        item.ItemID = ItemID;
        item.ItemName = ItemName;
        item.Total = Utils.ParseDouble(Total);
        item.UnitName = UnitName;
        item.RecordStatus = RecordStatus;
        item.Description = Description;
        ListItem.push(item);
    });
    var Obj = {
        Activated: Activated,
        PayrollKey: PayrollKey,
        EmployeeKey: EmployeeKey,
        EmployeeID: EmployeeID,
        EmployeeName: EmployeeName,
        BranchKey: BranchKey,
        BranchName: BranchName,
        DepartmentKey: DepartmentKey,
        DepartmentName: DepartmentName,
        PositionKey: PositionKey,
        PositionName: PositionName,
        Description: Description,
        FromDate: FromDate,
        ToDate: ToDate,
        ListItem: ListItem
    };

    $.ajax({
        url: URL_SavePay,
        type: 'POST',
        dataType: 'json',
        data: { "PayrollObj": JSON.stringify(Obj) },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Đã cập nhật thành công !.', 'Thông báo', 'success');
                LoadPayroll(EmployeeKey);
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        }
    });
}
function AddRow() {

    var row = $("#tblBody").find("tr:first");
    var rowNew = row.clone();
    $("#tblBody").append(rowNew);
    CountRow();
    Utils.InputMoney();
}
function RemoveRow(obj) {
    var count = $('#tblBody tr').length;
    if (count > 1) {
        $(obj).closest('tr').hide();
        $(obj).closest('tr').attr('recordstatus', 99);
        CountRow();
    }
    else {
        Utils.OpenNotify(r.Message, 'Bạn không được xóa dòng đầu tiên !.', 'error');
    }
}
function CountRow() {
    $('#tblBody tr[recordstatus!="99"]').each(function (idx) {
        $(this).children(":eq(0)").html(idx + 1);
    });
}