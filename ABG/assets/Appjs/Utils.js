﻿var Utils = {
    ConvertToDDMMYYYY: function (input) {
        var today = new Date(input);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        if (yyyy === 1 &&
            dd === '01' &&
            mm === '01') {
            return '';
        }
        return today = dd + '/' + mm + '/' + yyyy;
    },
    ConvertToCDate: function (input) {
        var dateOut = input.split("/");
        return new Date(dateOut[1] + "/" + dateOut[0] + "/" + dateOut[2]);
    },
    PreviewImg: function (input, output) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(output).attr('src', e.target.result);
            };
            // convert to base64 string
            reader.readAsDataURL(input.files[0]);
        }
    },
    OpenMagnific: function (id) {
        $.magnificPopup.open({
            callbacks: {
                beforeOpen: function () { this.wrap.removeAttr('tabindex') }
            },
            items: {
                src: id
            },
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            preloader: true,

            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            modal: false
        });
        try {
            $('#txt_EmloyeeName').focus();
        } catch (e) {
            return;
        }
    },
    OpenNotify: function (title, text, css) {

        new PNotify({
            title: title, //'Đã xóa thành công !.'
            text: text,
            type: css //success
        });

    },
    ClearUI: function (element) {
        $(element).find('input,textarea,select').not(':input[type=button], :input[type=submit], :input[type=reset]').val('');
        $(element).find('select').each(function () {
            $(this).val($(this).find("option:first").val()).trigger('change');
        });
    },
    GetDate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    InputMoney: function () {
        $("input.money").inputmask("decimal", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: ''
        });
    },
    ParseDouble: function (value) {
        while (value.indexOf(".") > -1) {
            value = value.replace(".", "");
        }
        value = value.replace(",", ".");
        return parseFloat(value);
    },
    ShowLoading: function (id) {
        var html = '<div class="se-pre-con" id="loadingDiv"></div>';
        $(id).append(html);
    },
    DoneLoad: function() {
        $("#loadingDiv").fadeOut(500, function () {
            // fadeOut complete. Remove the loading div
            $("#loadingDiv").remove(); //makes page more lightweight 
        });
    },
    LoadIn: function () {
        $(".se-pre-con").fadeIn("slow");
    },
    LoadOut: function () {
        $(".se-pre-con").fadeOut("slow");
    },
};
jQuery.loadScript = function (url, callback) {
    if (url.length > 0 && callback.length > 0) {
        jQuery.ajax({
            url: url,
            dataType: 'script',
            success: callback,
            async: true
        });
    }
};
