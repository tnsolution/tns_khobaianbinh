﻿// We can attach the `fileselect` event to all file inputs on the page
$(document).on('change', ':file', function (e) {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);

    var files = e.target.files;
    var ContractKey = $("#txt_ContractKey").val(); //uncomment this to make sure the ajax URL works
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append("file" + x, files[x]);
            }

            $.ajax({
                type: "POST",
                url: '/Customer/UploadFile?id=' + ContractKey,
                contentType: false,
                processData: false,
                data: data,
                success: function (r) {
                    if (r.Success) {
                        ListFile(ContractKey);
                    }
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] == "{")
                        err = JSON.parse(xhr.responseText).Message;
                    console.log(err);
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
});
// We can watch for our custom `fileselect` event like this
$(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }
    });

    $("#cbo_Buyer").change(function () {
        var CustomerKey = $('#cbo_Buyer').val();
        CustomerItemInfo(CustomerKey);
    });
});
//mở thanh lý hợp đồng
function OpenModalContractEnd() {
    Utils.OpenMagnific('#modalContractEnd');
}
//lưu thanh lý hợp đồng
function SaveContractEnd() {
    var ContractKey = $('#txt_ContractKey').val();
    var DateTerminate = $('#txt_DateLiquidation').val();
    var ReasonTerminate = $('#txt_ReasonLiquidation').val();

    $.ajax({
        url: URL_ContractEnd,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "DateTerminate": DateTerminate,
            "ReasonTerminate": ReasonTerminate,
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                return;
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//lưu thông tin hợp đồng
function SaveContract() {
    var ContractKey = $('#txt_ContractKey').val();
    var ContractID = $('#txt_ContractID').val();
    var SubContract = $('#txt_SubContract').val();
    var FromDate = $('#txt_FromDate').val();
    var ToDate = $('#txt_ToDate').val();
    var DateSign = $('#txt_DateSign').val();
    var BuyerKey = $('#cbo_Buyer').val();
    var BuyerName = $("#cbo_Buyer option:selected").text();
    var BuyerAddress = $('#txt_BuyerAddress').val();
    var BuyerPhone = $('#txt_BuyerPhone').val();
    var BuyerTaxCode = $('#txt_BuyerTaxCode').val();
    var BuyerBankCode = $('#txt_BuyerBankCode').val();
    var BuyerAddressBank = $('#txt_BuyerAddressBank').val();
    var BuyerRepresent = $("#txt_BuyerRepresent").val();
    var BuyerPosition = $("#txt_BuyerPosition").val();
    var Purpose = $("#txt_Purpose").val();
    var Description = $("#txt_Description").val();
    var chkStyle = $("#txtStyle").val();
    $.ajax({
        url: URL_ContractSave,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "ContractID": ContractID,
            "SubContract": SubContract,
            "FromDate": FromDate,
            "ToDate": ToDate,
            "DateSign": DateSign,
            "BuyerKey": BuyerKey,
            "BuyerName": BuyerName,
            "BuyerAddress": BuyerAddress,
            "BuyerPhone": BuyerPhone,
            "BuyerTaxCode": BuyerTaxCode,
            "BuyerBankCode": BuyerBankCode,
            "BuyerAddressBank": BuyerAddressBank,
            "BuyerRepresent": BuyerRepresent,
            "BuyerPosition": BuyerPosition,
            "Purpose": Purpose,
            "Description": Description,
            "chkStyle": chkStyle,
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//xóa hợp đồng
function DeleteContract() {
    var ContractKey = $('#txt_ContractKey').val();
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_ContractDelete,
                        type: 'POST',
                        data: {
                            "ContractKey": ContractKey
                        },
                        beforeSend: function () {
                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {
                            history.back(1);
                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}
//1 Open Modal Copy
function OpenModalCopy() {
    Utils.OpenMagnific("#modalContractCopy");
}
//2 Save Copy
function SaveCopy() {
    var ContractKey = $('#txt_ContractKey').val();
    var ContractID = $('#txt_CopyContractID').val();
    var ContractSub = $('#txt_CopySubContract').val();
    var ContractSign = $('#txt_CopyDateSign').val();
    $.ajax({
        url: URL_ContractCopy,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "ContractID": ContractID,
            "ContractSub": ContractSub,
            "ContractSign": ContractSign
        },
        beforeSend: function () {
        },
        success: function (r) {
            if (r.Success) {
                var url = URL_ContractCopyOpen;
                window.location.href = url.replace('contractkey', r.Data);
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//danh sách tâp tin file
function FileList(Key) {
    $.ajax({
        url: URL_ContractFileList,
        type: 'GET',
        data: {
            "ContractKey": Key
        },
        beforeSend: function () {
        },
        success: function (r) {
            $("#PanelFile").empty().append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//xóa file
function FileDelete(AutoKey) {
    var ContractKey = $("#txt_ContractKey").val();
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_ContractFileDelete,
                        type: 'POST',
                        data: {
                            "AutoKey": AutoKey
                        },
                        beforeSend: function () {
                        },
                        success: function (r) {
                            if (r.Success) {
                                FileList(ContractKey);
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {
                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

//chi tiết địa điểm hợp đông
function ListItem(Key) {
    $.ajax({
        url: URL_ContractItemList,
        type: 'GET',
        data: {
            "ParentKey": Key
        },
        beforeSend: function () {
        },
        success: function (r) {
            $("#PanelProduct").empty().append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//lưu đia điểm
function ItemSave() {
    var ContractKey = $("#txt_ContractKey").val();
    var LandItemKey = $('#txt_AutoKey').val();
    var ItemKey = $('#cbo_Item').val();
    var ItemName = $("#cbo_Item option:selected").text();
    var UnitKey = $('#cbo_Unit').val();
    var UnitName = $("#cbo_Unit option:selected").text();
    var Area = $('#txt_Area').val();
    var Price = $('#txt_Price').val();
    var VAT_Percent = $('#txt_VAT_Percent').val();
    var TotalCurrencyMain = $('#txt_TotalCurrencyMain').val();
    var SubTotal = $('#txt_SubTotal').val();
    var ItemNote = $('#txt_DescriptionItem').val();

    $.ajax({
        url: URL_ContractItemSave,
        type: 'POST',
        data: {
            "ContractKey": ContractKey,
            "LandItemKey": LandItemKey,
            "ItemKey": ItemKey,
            "ItemName": ItemName,
            "UnitKey": UnitKey,
            "UnitName": UnitName,
            "Area": Area,
            "Price": Price,
            "VAT_Percent": VAT_Percent,
            "TotalCurrencyMain": TotalCurrencyMain,
            "SubTotal": SubTotal,
            "ItemNote": ItemNote
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                ListItem(ContractKey);
                Utils.OpenNotify('Đã cập nhật địa điểm', 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
        }
    });
}
//mở xem chi tiết địa điểm
function ItemOpen(AutoKey) {
    Utils.OpenMagnific('#modalContractItem');
    if (AutoKey !== '') {
        $.ajax({
            url: URL_ContractItemDetail,
            type: 'GET',
            data: {
                "LandItemKey": AutoKey
            },
            beforeSend: function () {
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_AutoKey').val(obj.LandItemKey);
                    $('#cbo_Item').val(obj.ItemKey).trigger('change');
                    $('#cbo_Unit').val(obj.UnitKey).trigger('change');
                    $('#txt_Price').val(obj.Price);
                    $('#txt_Area').val(obj.Area);
                    $('#txt_VAT_Percent').val(obj.VAT_Percent);
                    $('#txt_SubTotal').val(obj.SubTotal);
                    $('#txt_TotalCurrencyMain').val(obj.TotalCurrencyMain);
                    $('#txt_DescriptionItem').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
            }
        });
    }
    else {
        Utils.ClearUI('#modalContractItem');
        $('#txt_AutoKey').val('');
        $('#txt_Price').val(0);
        $('#txt_VAT_Percent').val(0);
        $('#txt_Area').val(0);
        $('#txt_TotalCurrencyMain').val(0);
    }
}
//xóa địa điểm
function ItemDelete(AutoKey) {
    var ContractKey = $("#txt_ContractKey").val();
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_ContractItemDelete,
                        type: 'POST',
                        data: {
                            "LandItemKey": AutoKey
                        },
                        beforeSend: function () {
                        },
                        success: function (r) {
                            if (r.Success) {
                                ListItem(ContractKey);
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {
                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function CalculationTotal() {
    var SubTotal = 0;
    var TotalMain = 0;
    var VAT = 0;
    var Area = 0;
    var Price = 0;
    Price = Utils.ParseDouble($("#txt_Price").val());
    Area = Utils.ParseDouble($("#txt_Area").val());
    VAT = Utils.ParseDouble($("#txt_VAT_Percent").val());
    SubTotal = Price * Area;
    if (VAT === 0)
        TotalMain = SubTotal;
    else
        TotalMain = SubTotal + (SubTotal * VAT) / 100
    $("#txt_SubTotal").val(SubTotal);
    $("#txt_TotalCurrencyMain").val(TotalMain);
}
function CustomerItemInfo(Key) {
    if (Key.length >= 36) {
        $.ajax({
            url: URL_CustomerGetInfo,
            type: 'GET',
            data: {
                "CustomerKey": Key
            },
            beforeSend: function () {
            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_BuyerAddress').val(obj.Address);
                    $('#txt_BuyerPhone').val(obj.Phone);
                    $('#txt_BuyerTaxCode').val(obj.TaxNumber);
                    $('#txt_BuyerBankCode').val(obj.BankAccount);
                    $('#txt_BuyerAddressBank').val(obj.BankName);
                    $('#txt_BuyerRepresent').val('');
                    $('#txt_BuyerPosition').val('');
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
            }
        });
    }
}