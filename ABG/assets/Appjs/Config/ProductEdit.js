﻿$(document).ready(function () {
    $("#cbo_Class").change(function () {
        var val = $(this).val();
        if (val === "XN") {
            $("#divFeaXN").show();
            $("#divFea").hide();
        }
    });

    $("#cbo_Class").trigger('change');
});

function SaveProduct() {
    var ProductKey = $('#txt_ProductKey').val();
    var ProductID = $('#txt_ProductID').val();
    var ProductName = $('#txt_ProductName').val();
    var Address = $('#txt_Address').val();
    var Rank = $('#txt_Rank').val();
    var Parent = $('#cbo_Area').val();
    var Class = $('#cbo_Class').val();
    var LngLat = $('#txt_LngLat').val();
    var Description = $('#txt_Description').val();

    $.ajax({
        url: URL_Save,
        type: 'POST',
        data: {
            "ProductKey": ProductKey,
            "ProductID": ProductID,
            "ProductName": ProductName,
            "Address": Address,
            "Rank": Rank,
            "Parent": Parent,
            "Class": Class,
            "LngLat": LngLat,
            "Description": Description
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function Delete(ProductKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: 'POST',
                        data: {
                            "ProductKey": ProductKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveFea() {
    var FeatureKey = $('#txt_FeaKey').val();
    var Name = $('#txt_NameFea').val();
    var Description = $('#txt_DescriptionFea').val();
    var Rank = $("#txt_RankFea").val();
    var ProductKey = $("#txt_ProductKey").val();
    
    $.ajax({
        url: URL_SaveFea,
        type: 'POST',
        data: {
            "FeatureKey": FeatureKey,
            "Name": Name,
            "Description": Description,
            "Rank": Rank,
            "ProductKey": ProductKey
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailFea(FeaKey) {
    Utils.OpenMagnific('#modalEditFea');
    if (FeaKey > 0) {
        $.ajax({
            url: URL_DetailFea,
            type: 'GET',
            data: {
                "FeatureKey": FeaKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_FeaKey').val(obj.FeaKey);
                    $('#txt_NameFea').val(obj.Name);
                    $('#txt_DescriptionFea').val(obj.Description);
                    $('#txt_RankFea').val(obj.Rank);
                    $("#txt_ProductKey").val(obj.ProductKey);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI('#modalEditFea');
        $('#txt_FeaKey').val(0);
        $('#txt_RankFea').val(0);
    }
}
function DeleteFea(FeaKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteFea,
                        type: 'POST',
                        data: {
                            "FeatureKey": FeaKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

function SaveEqu() {
    var AutoKey = $('#txt_EquKey').val();
    var ItemName = $('#txt_ItemNameEqu').val();
    var Description = $('#txt_DescriptionEqu').val();
    var Value = $("#txt_ValueEqu").val();
    var ProductKey = $("#txt_ProductKey").val();
    $.ajax({
        url: URL_SaveEqu,
        type: 'POST',
        data: {
            "AutoKey": AutoKey,
            "ItemName": ItemName,
            "Value": Value,
            "Description": Description,
            "ProductKey": ProductKey
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailEqu(EquKey) {
    Utils.OpenMagnific('#modalEditEqu');
    if (EquKey > 0) {
        $.ajax({
            url: URL_DetailEqu,
            type: 'GET',
            data: {
                "AutoKey": EquKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_EquKey').val(obj.AutoKey);
                    $('#txt_ItemNameEqu').val(obj.ItemName);
                    $('#txt_DescriptionEqu').val(obj.Description);
                    $('#txt_ValueEqu').val(obj.Value);
                    $("#txt_ProductKey").val(obj.ProductKey);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI('#modalEditEqu');
        $('#txt_EquKey').val(0);
        $('#txt_EquKey').val(0);
    }
}
function DeleteEqu(EquKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteEqu,
                        type: 'POST',
                        data: {
                            "AutoKey": EquKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}

//-----------------------------------------bổ sung xe nâng
function SaveFeaXN() {
    var FeatureKey = $('#txt_FeaKeyXN').val();
    var Name = $('#txt_NameFeaXN').val();
    var Description = $('#txt_DescriptionFeaXN').val();
    var ProductKey = $("#txt_ProductKey").val();

    var obj = {
        "AutoKey": FeatureKey,
        "Title": Name,
        "Description": Description,
        "TableKey": ProductKey,
    };

    var fileUpload = $("#txt_UrlXN").get(0);
    var files = fileUpload.files;
    // Create  a FormData object
    var fileData = new FormData();

    // if there are multiple files , loop through each files
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }
    fileData.append("Fea", JSON.stringify(obj));

    $.ajax({
        url: URL_SaveFeaXN,
        type: 'POST',
        data: fileData,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function () {
            Utils.LoadIn();
        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function DetailFeaXN(FeaKey) {
    Utils.OpenMagnific('#modalEditFeaXN');
    if (FeaKey > 0) {
        $.ajax({
            url: URL_DetailFeaXN,
            type: 'GET',
            data: {
                "AutoKey": FeaKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    $('#txt_FeaKeyXN').val(obj.AutoKey);
                    $('#txt_NameFeaXN').val(obj.Title);
                    $('#txt_DescriptionFeaXN').val(obj.Description);
                    $("#txt_ProductKey").val(obj.TableKey);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {

            }
        });
    }
    else {
        Utils.ClearUI('#modalEditFea');
        $('#txt_FeaKeyXN').val(0);
    }
}
function DeleteFeaXN(FeaKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_DeleteFeaXN,
                        type: 'POST',
                        data: {
                            "AutoKey": FeaKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}