﻿$(document).ready(function () {

});

function editProduct() {
    $.ajax({
        type: 'GET',
        url: URL_EditProduct,
        data: { id: 0 },
        dataType: 'html',
        success: function (r) {
            $("#tblbody")
                .append(r)
                .find("input.number")
                .number(true, 0, ',', '.');
            countProduct();
        },
        error: function (err) {
            console.log(err);
        },
        complete: function () {
            //$("#tblbody .select2").select2('destroy');
            $("#tblbody .select2").select2();
        }
    });
}
function deleteProduct(obj) {
    $(obj).closest('tr').remove();
    countProduct();
}
function countProduct() {
    var list = $("#tblbody td.autocount");
    for (var i = 0; i < list.length; i++) {
        list[i].innerHTML = i + 1;
    }
}
function calculationRow(obj) {
    var row = $(obj).closest('tr');
    var txtQuantity = row.find('td:eq(4) input').val();
    var txtItemPrice = row.find('td:eq(5) input').val();
    var txtItemAmount = row.find('td:eq(6) input');

    if (txtQuantity.length > 0 &&
        txtItemPrice.length > 0) {
        txtItemAmount.val(parseFloat(txtQuantity) * parseFloat(txtItemPrice));
    }

    var total = 0;
    $('#tblbody > tr').each(function () {
        var txtAmount = parseFloat($(this).find('td:eq(6) input').val());
        if (!isNaN(txtAmount)) {
            total += txtAmount;
        }
    });
    $('#txt_AmountOrderMain').val(total);
}
function SaveOrder() {
    var ListProduct = new Array();
    var Order = {
        OrderKey: $("#txt_OrderKey").val(),
        OrderID: $("#txt_OrderID").val(),
        OrderDate: $("#txt_OrderDate").val(),
        DocumentOrigin: $("#txt_DocumentOrigin").val(),
        Description: $("#txt_Description").val(),
        CustomerKey: $("#cbo_Customer").val(),
        AmountOrderMain: $("#txt_AmountOrderMain").val(),
        ListProduct: ListProduct,
    };

    $('#tblbody tr').each(function () {

        var objProduct = new Object();
        var AutoKey = $(this).find('input[name=txt_AutoKey]').val();
        var ProductKey = $(this).find('select[name=cbo_Product]').val();
        var ProductName = $(this).find('select[name=cbo_Product] option:selected').text();
        var UnitKey = $(this).find('select[name=cbo_Unit]').val();
        var UnitName = $(this).find('select[name=cbo_Unit] option:selected').text();
        var QuantityExactly = $(this).find('input[name=txt_Quantity]').val();
        var UnitPrice = $(this).find('input[name=txt_UnitPrice]').val();
        var AmountTemporary = $(this).find('input[name=txt_AmountTemporary]').val();

        objProduct.AutoKey = AutoKey === null ? "0" : AutoKey;
        objProduct.ProductKey = ProductKey === null ? "" : ProductKey;
        objProduct.ProductName = ProductName === null ? "" : ProductName;
        objProduct.UnitKey = UnitKey === null ? "0" : UnitKey;
        objProduct.UnitName = UnitName === null ? "" : UnitName;
        objProduct.QuantityExactly = QuantityExactly === null ? "0" : QuantityExactly;
        objProduct.UnitPrice = UnitPrice === null ? "0" : UnitPrice;
        objProduct.AmountTemporary = AmountTemporary === null ? "0" : AmountTemporary;

        ListProduct.push(objProduct);
    });

    $.ajax({
        url: URL_SaveOrder,
        type: 'POST',
        dataType: 'json',
        data: { "Order": JSON.stringify(Order) },
        success: function (r) {
            if (r.Success) {
                //$("#frmOrder").modal("toggle");
                location.reload();
            }
            else {
                alert(r.Message);
            }
        },
        error: function (xhr, status, error) {

        }
    });
}
function deleteOrder(id) {
    if (confirm('Bạn có chắc xóa thông tin')) {
        $.ajax({
            type: 'POST',
            url: URL_DeleteOrder,
            data: { id: id },
            dataType: 'json',
            success: function (r) {
                if (r.Success) {
                    location.reload();
                }
                else {
                    alert(r.Message);
                }
            }
        });
    }
}
function editOrder(id) {
    $.ajax({
        type: 'GET',
        url: URL_EditOrder,
        data: { id: id },
        dataType: 'html',
        success: function (r) {
            $("#modalWrapper").html(r);
            $("#frmOrder").modal("toggle");
            $("#frmOrder .select2").select2({
                placeholder: ' Nhà cung cấp',
                allowClear: false,
                width: '100%'
            });
            $("#frmOrder .datepicker").datepicker({
                todayHighlight: true,
                autoclose: true
            }).val(getDate());
            $("#frmOrder").find("input.number").number(true, 0, ',', '.');
        }
    });
}