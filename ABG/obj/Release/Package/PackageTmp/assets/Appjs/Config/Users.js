﻿
var roles = [];
$(document).ready(function () {

});

function OpenAuth(UserKey, UserName) {
    Utils.OpenMagnific('#modelAuth');
    $("#modelAuth").find(".card-title").text('Phân quyền cho người dùng ' + UserName);
    $("#txt_UserKey").val(UserKey);

    GetRole();
    GetData();
    roles = [];
}
function GetRole() {
    var UserKey = $("#txt_UserKey").val();
    $.ajax({
        url: URL_Role,
        type: 'POST',
        data: {
            "UserKey": UserKey,
        },
        beforeSend: function () {
            $("#tab2").empty();
        },
        success: function (r) {
            $("#tab2").append(r);
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function GetData() {
    var UserKey = $("#txt_UserKey").val();
    $.ajax({
        url: URL_Data,
        type: 'POST',
        data: {
            "UserKey": UserKey,
        },
        beforeSend: function () {
            $("#tab1").empty();
        },
        success: function (r) {
            $("#tab1").append(r);
        },
        error: function (err) {

        },
        complete: function () {

        }
    });
}
function SaveAuth() {    
    var data = $("#tbldata input:checkbox:checked").map(function () {
        return this.value;
    }).toArray();
    $("#txt_Data").val(JSON.stringify(data));

    $('#tblrole > tbody > tr').each(function () {
        var rolekey = $(this).attr('rolekey');
        var userkey = $(this).attr('userkey');
        RoleEventCheck(this, rolekey, userkey);
    });

    var UserKey = $("#txt_UserKey").val();
    var Role = $("#txt_Auth").val();
    var Data = $("#txt_Data").val();

    $.ajax({
        url: URL_SaveAuth,
        type: 'POST',
        data: {
            "UserKey": UserKey,
            "Role": Role,
            "Data": Data
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify("Thông báo", 'Đã cập nhật thành công', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}

function Save() {
    var UserKey = $('#txt_UserKey').val();
    var UserName = $('#txt_UserName').val();
    var Password = $('#txt_Password').val();
    var lbl_Password = $('#lbl_Password').val();
    var EmployeeKey = $("#cbo_Employee").val();
    var EmployeeName = $("#cbo_Employee option:selected").text();
    var Description = $('#txt_Description').val();
    var Activate = $('#cbo_Activate').val();
    var ExpireDate = $('#txt_ExpireDate').val();
    $.ajax({
        url: URL_Save,
        type: 'POST',
        data: {
            "UserKey": UserKey,
            "UserName": UserName,
            "Password": Password,
            "lbl_Password": lbl_Password,
            "EmployeeKey": EmployeeKey,
            "EmployeeName": EmployeeName,
            "Description": Description,
            "Activate": Activate,
            "ExpireDate": ExpireDate
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                location.reload();
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function Detail(UserKey) {
    Utils.OpenMagnific('#modalEdit');
    if (UserKey.length >= 36) {
        $.ajax({
            url: URL_Detail,
            type: 'GET',
            data: {
                "UserKey": UserKey
            },
            beforeSend: function () {

            },
            success: function (r) {
                if (r.Success) {
                    var obj = JSON.parse(r.Data);
                    var Date = Utils.ConvertToDDMMYYYY(obj.ExpireDate);

                    $('#txt_UserKey').val(obj.UserKey);
                    $('#txt_UserName').val(obj.UserName);
                    $('#txt_Password').val(obj.Password);
                    $('#lbl_Password').val(obj.Password);
                    $('#cbo_Employee').val(obj.EmployeeKey);
                    if (obj.Activate == true)
                        $('#cbo_Activate').val(1).trigger('change')
                    else
                        $('#cbo_Activate').val(0).trigger('change')
                    $('#txt_ExpireDate').val(Date);
                    $('#txt_Description').val(obj.Description);
                }
                else {
                    Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                }
            },
            error: function (err) {
                Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
            },
            complete: function () {
                $(".select2").select2({ width: "100%", placeholder: "--Chọn--" });
            }
        });
    }
}
function Delete(UserKey) {
    $.confirm({
        type: 'red',
        typeAnimated: true,
        title: 'Cảnh báo !.',
        content: 'Bạn có chắc xóa thông tin này ?.',
        buttons: {
            confirm: {
                text: 'Đồng ý',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        url: URL_Delete,
                        type: 'POST',
                        data: {
                            "UserKey": UserKey
                        },
                        beforeSend: function () {

                        },
                        success: function (r) {
                            if (r.Success) {
                                $("tr[id='" + UserKey + "']").remove();
                                //location.reload();
                            }
                            else {
                                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
                            }
                        },
                        error: function (err) {
                            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
                        },
                        complete: function () {

                        }
                    });
                }
            },
            cancel: {
                text: 'Hủy',
            }
        }
    });
}
function Activate(id) {
    $.ajax({
        url: URL_Active,
        type: 'POST',
        data: {
            "UserKey": id,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Cập nhật tình trạng thành công', 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function Reset(id) {
    $.ajax({
        url: URL_Reset,
        type: 'POST',
        data: {
            "UserKey": id,
        },
        beforeSend: function () {

        },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify(r.Message, 'Tạo mật khẩu mới thành công', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}

function InitCheckStyle() {
    $('[data-plugin-ios-switch]').each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
}

function RoleEventCheck(obj, rolekey, userkey) {
    var chk = $(obj).find('input:checkbox');
    var read = chk[0].checked;
    var edit = chk[1].checked;
    var del = chk[2].checked;

    var newobj = {
        "RoleKey": rolekey,
        "UserKey": userkey,
        "RoleRead": read,
        "RoleEdit": edit,
        "RoleDel": del
    };

    roles = ObjCheck(roles, "RoleKey", newobj);
    $("#txt_Auth").val(JSON.stringify(roles));
}

function ObjCheck(array, name, newElement) {
    var found = false;
    for (var i = 0; i < array.length; i++) {
        var element = array[i];
        if (element === undefined) {
            array.push(newElement);
            return array;
        }
        if (element[name].toString() ===
            newElement[name].toString()) {
            found = true;
            array[i] = newElement;
        }
    }
    if (found === false) {
        array.push(newElement);
    }

    return array;

}