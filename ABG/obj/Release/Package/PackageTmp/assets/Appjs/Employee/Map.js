﻿$(function () {
    $('#btn_Search').click(function () {
        Utils.OpenMagnific('#modalSearch');
    });

    $('.genealogy-tree li').on('click', 'span.btn', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var employeekey = $(this).attr('name');
        var url = '';
        var title = '';

        if ($(this).hasClass("exInfo")) {
            url = URL_Info + '?EmployeeKey=' + employeekey;
            title = 'Hồ sơ nhân sự';

            $.dialog({
                theme: 'supervan',
                closeIcon: true,
                columnClass: 'xlarge',
                title: title,
                content: 'url:' + url,
                animation: 'scale',
                closeAnimation: 'scale',
                backgroundDismiss: true,
            });
        }
        else {
            url = URL_ReviewQuick + '?EmployeeKey=' + employeekey;
            title = 'Đánh giá nhân sự';

            var jc = $.confirm({
                theme: 'supervan',
                closeIcon: true,
                columnClass: 'xlarge',
                title: title,
                content: 'url:' + url,
                animation: 'scale',
                closeAnimation: 'scale',
                backgroundDismiss: true,
                onContentReady: function () {
                    $('input[type="range"]').change(function () {
                        var key = $(this).attr("name");
                        $('.output[name=' + key + '] b').text(this.value);
                    });
                },
                buttons: {
                    confirm: {
                        text: "Cập nhật",
                        action: function () {
                            SaveReview().done(function (r) {
                                if (r.Success) {
                                    jc.close();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: "Đóng",
                        action: function () {
                            jc.close();
                        }
                    },
                }
            });
        }
    });
    $('.genealogy-tree ul').hide();
    $('.genealogy-tree > ul').show();
    $('.genealogy-tree ul.active').show();
    $('.genealogy-tree li').on('click', function (e) {
        var children = $(this).find('> ul');
        if (children.is(":visible")) {
            children.hide('normal').removeClass('active');
        }
        else {
            children.show('normal').addClass('active');
        }
        e.stopPropagation();
    });
    $('.genealogy-tree li').children('ul:has(li)').first().show();
});

function ViewHistory(Key) {
    $.ajax({
        url: URL_History,
        type: 'GET',
        data: {
            "EmployeeKey": Key,
        },
        beforeSend: function () {
            $("#RightPanel").empty();
        },
        success: function (r) {
            $("#RightPanel").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function ViewGeneral(Key) {
    $.ajax({
        url: URL_General,
        type: 'GET',
        data: {
            "EmployeeKey": Key,
        },
        beforeSend: function () {
            $("#RightPanel").empty();
        },
        success: function (r) {
            $("#RightPanel").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function ViewSeniority(Key) {
    $.ajax({
        url: URL_Seniority,
        type: 'GET',
        data: {
            "EmployeeKey": Key,
        },
        beforeSend: function () {
            $("#RightPanel").empty();
            Utils.ShowLoading("#RightPanel");            
        },
        success: function (r) {
            Utils.DoneLoad();
            $("#RightPanel").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {
            (function ($) {
                'use strict';
                if ($.isFunction($.fn['popover'])) {
                    $('[data-toggle="popover"]').popover();
                }
            }).apply(this, [jQuery]);
        }
    });
}
function ViewGross(Key) {
    $.ajax({
        url: URL_Gross,
        type: 'GET',
        data: {
            "EmployeeKey": Key,
        },
        beforeSend: function () {
            $("#RightPanel").empty();
        },
        success: function (r) {
            $("#RightPanel").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}
function Review(Key) {
    $.ajax({
        url: URL_Review,
        type: 'GET',
        data: {
            "EmployeeKey": Key,
        },
        beforeSend: function () {
            $("#RightPanel").empty();
        },
        success: function (r) {
            $("#RightPanel").append(r);
        },
        error: function (err) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        },
        complete: function () {

        }
    });
}

function SaveReview() {
    var Description = $("#txt_Description").val();
    var EmployeeKey = $("#txt_EmployeeKey").val();
    var ValuerKey = $("#txt_ReportKey").val();
    var ValuerName = $("#txt_ReportPosition").val() + ' ' + $("#txt_ReportName").val();


    var ListItem = [];
    $('#tblBody tr').each(function () {
        var id = $(this).attr('id');
        var item = new Object();
        item.CriteriaKey = $(this).find('input[name=' + id + ']').attr('name');
        item.Point = $(this).find('input[name=' + id + ']').val();
        item.Description = $(this).find('input[textarea=' + id + ']').text();
        ListItem.push(item);
    });
    var Obj = {
        EmployeeKey: EmployeeKey,
        Description: Description,
        ValuerKey: ValuerKey,
        ValuerName: ValuerName,
        ListItem: ListItem
    }

    return $.ajax({
        async: false,
        url: URL_ReviewSave,
        type: 'POST',
        dataType: 'json',
        data: { "Obj": JSON.stringify(Obj) },
        success: function (r) {
            if (r.Success) {
                Utils.OpenNotify('Cập nhật thành công', 'Thông báo', 'success');
            }
            else {
                Utils.OpenNotify(r.Message, 'Thông báo', 'error');
            }
        },
        error: function (xhr, status, error) {
            Utils.OpenNotify(err.responseText, 'Thông báo', 'error');
        }
    });
}