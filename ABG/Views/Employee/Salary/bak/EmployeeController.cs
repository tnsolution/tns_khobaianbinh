﻿using ABG.Employee;
using OfficeOpenXml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using OfficeOpenXml.Style;

namespace ABG.Controllers
{
    public class EmployeeController : BaseController
    {


        #region [Leave]        



        #region [BEGIN]
        public ActionResult LeaveBeginNote()
        {
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = Leave_Close_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Leave/Begin.cshtml");
        }
        [HttpPost]
        public JsonResult LeaveBeginSave(string BeginKey, string BeginYear, string BeginNo, string BeginPlus, string Description, string EmployeeKey, string EmployeeName)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                Leave_Close_Info zInfo = new Leave_Close_Info();
                zInfo.Leave_Close.CloseYear = BeginYear;
                zInfo.Leave_Close.CloseEnd = BeginNo.ToFloat();
                zInfo.Leave_Close.Incremental = BeginPlus.ToFloat();
                zInfo.Leave_Close.Description = Description;
                zInfo.Leave_Close.EmployeeKey = EmployeeKey;
                zInfo.Leave_Close.EmployeeName = EmployeeName;
                zInfo.Leave_Close.PartnerNumber = Helper.PartnerNumber;
                zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
                zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
                zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
                zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;

                if (BeginKey.Length >= 36)
                {
                    zInfo.Leave_Close.CloseKey = BeginKey;
                    zInfo.Update();
                }
                else
                {
                    zInfo.Create_ServerKey();
                }

                if (zInfo.Leave_Close.Code == "200" ||
                    zInfo.Leave_Close.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Leave_Close.Message;
                    zResult.Success = false;
                }

                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LeaveBeginDelete(string BeginKey)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                Leave_Close_Info zInfo = new Leave_Close_Info();
                zInfo.Leave_Close.CloseKey = BeginKey;
                zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
                zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
                zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
                zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;
                zInfo.Delete();

                if (zInfo.Leave_Close.Code == "200" ||
                    zInfo.Leave_Close.Code == "201")
                {
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Leave_Close.Message;
                    zResult.Success = false;
                }
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult LeaveBeginOpen(string BeginKey)
        {
            ServerResult zResult = new ServerResult();
            Leave_Close_Info zInfo = new Leave_Close_Info(BeginKey, "");
            if (zInfo.Leave_Close.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Close.Message;
            }
            else
            {
                zResult.Data = JsonConvert.SerializeObject(zInfo.Leave_Close);
                zResult.Success = true;
            }
            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult LeaveListNote()
        {
            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem theo dõi phép ");
            //-------------------------------------------------------------------------------------------------

            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListData = Leave_Close_Data.List(Helper.PartnerNumber, string.Empty);
            return View("~/Views/Employee/Leave/List.cshtml");
        }
        [HttpGet]
        public JsonResult LeaveOpenNote(string NoteKey)
        {
            ServerResult zResult = new ServerResult();
            Leave_Note_Info zInfo = new Leave_Note_Info(NoteKey);
            Leave_Note_Model zModel = zInfo.Leave_Note;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Note.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LeaveDeleteNote(string NoteKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Leave_Note_Info(NoteKey);
            var zModel = zInfo.Leave_Note;
            if (zModel.Code == "200")
            {
                zInfo.Delete();
                if (zModel.Code == "200" ||
                    zModel.Code == "201")
                {
                    //-------------------------------------------------------------------------------------------------
                    Helper.LogAction(UserLog, " Xoá kỳ phép " + zModel.FromDate.ToString("dd/MM/yyyy") + "-" + zModel.ToDate.ToString("dd/MM/yyyy") + " nhân sự " + zModel.EmployeeName);
                    //-------------------------------------------------------------------------------------------------

                    zResult.Success = true;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Leave_Note.Message;
                    return Json(zResult, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Không tìm thấy thông tin này";
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LeaveSaveNote(
            string NoteKey, string NoteDate, string EmployeeKey, string EmployeeName, string BranchKey,
            string BranchName, string DepartmentKey, string DepartmentName, int PositionKey, string PositionName,
            string FromDate, string ToDate, float TotalDate, int CategoryKey, string CategoryName, string Description)
        {
            ServerResult zResult = new ServerResult();
            Leave_Note_Info zInfo = new Leave_Note_Info(NoteKey);

            var emp = new Employee_Info(EmployeeKey).Employee;
            var name = (emp.LastName.Trim() + " " + emp.FirstName.Trim());

            zInfo.Leave_Note.NoteKey = NoteKey;
            zInfo.Leave_Note.EmployeeKey = EmployeeKey;
            zInfo.Leave_Note.EmployeeName = name;
            zInfo.Leave_Note.BranchKey = BranchKey;
            zInfo.Leave_Note.BranchName = BranchName;
            zInfo.Leave_Note.DepartmentKey = DepartmentKey;
            zInfo.Leave_Note.DepartmentName = DepartmentName;
            zInfo.Leave_Note.PositionKey = PositionKey;
            zInfo.Leave_Note.PositionName = PositionName;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
            zInfo.Leave_Note.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
            zInfo.Leave_Note.ToDate = zToDate;

            DateTime zDateWrite = DateTime.MinValue;
            DateTime.TryParseExact(NoteDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateWrite);
            zInfo.Leave_Note.NoteDate = zDateWrite;

            zInfo.Leave_Note.TotalDate = TotalDate;
            zInfo.Leave_Note.CategoryKey = CategoryKey;
            zInfo.Leave_Note.CategoryName = CategoryName.Trim();
            zInfo.Leave_Note.Description = Description.Trim();

            zInfo.Leave_Note.PartnerNumber = Helper.PartnerNumber;
            zInfo.Leave_Note.CreatedBy = UserLog.UserKey;
            zInfo.Leave_Note.CreatedName = UserLog.EmployeeName;
            zInfo.Leave_Note.ModifiedBy = UserLog.UserKey;
            zInfo.Leave_Note.ModifiedName = UserLog.EmployeeName;

            string NewKey = "";
            if (zInfo.Leave_Note.NoteKey == "")
            {
                NewKey = Guid.NewGuid().ToString();
                zInfo.Leave_Note.NoteKey = NewKey;
                zInfo.Create_ClientKey();
            }
            else
            {
                NewKey = NoteKey;
                zInfo.Update();
            }

            if (zInfo.Leave_Note.Code == "200" ||
                zInfo.Leave_Note.Code == "201")
            {
                Leave_Close_Model zModel = Helper.LeaveCloseEnd(EmployeeKey, zFromDate, zToDate, TotalDate, out string Mess);
                zModel.Reference = NewKey;
                SaveCloseLeave(zModel);

                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Leave_Note.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LeaveTracking(string EmployeeKey, string FromDate, string ToDate, float No)
        {
            DateTime zFromDate;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            DateTime zToDate;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            var zResult = new ServerResult();
            var zModel = Helper.LeaveCloseEnd(EmployeeKey, zFromDate, zToDate, No, out string Mess);

            if (Mess != string.Empty)
            {
                zResult.Success = false;
                zResult.Message = Mess;
            }
            else
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Filter(string ViewPage)
        {
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectAge = Helper.SelectData("Tuoi");
            ViewBag.SelectEdu = Helper.SelectData("Trinhdo");
            ViewBag.ViewPage = ViewPage;
            return PartialView("~/Views/Employee/Filter.cshtml");
        }

        public ActionResult Search(
            string SearchName = "", string Department = "", string FromDate = "", string ToDate = "",
            string Age = "", string Edu = "", int Gender = 0, string Position = "", string ViewPage = "")
        {
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zFromDate);
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out zToDate);

            int FromAge = 0, ToAge = 0;
            if (Age.Contains('-'))
            {
                FromAge = Age.Split('-')[0].ToInt();
                ToAge = Age.Split('-')[1].ToInt();
            }

            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);

            switch (ViewPage)
            {
                default:
                    ViewBag.ListData = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/List.cshtml");

                case "CONTRACT":
                    ViewBag.ListData = Contract_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/Contract/ContractList.cshtml");

                case "LIST":
                    ViewBag.ListData = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/List.cshtml");

                case "MAP":
                    ViewBag.ListData = Employee_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/Map.cshtml");

                case "LEAVE":
                    ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
                    ViewBag.ListData = Leave_Close_Data.Search(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/Leave/Begin.cshtml");

                case "TRACK":
                    ViewBag.SelectEmployee = Employee_Data.List(Helper.PartnerNumber);
                    ViewBag.ListData = Leave_Close_Data.Track(Helper.PartnerNumber, SearchName, Department, zFromDate, zToDate, FromAge, ToAge, Gender, Edu, Position);
                    return View("~/Views/Employee/Leave/List.cshtml");
            }
        }

        [DisplayName("Xem danh sách nhân sự")]
        [Route("danh-sach-nhan-su")]
        public ActionResult List()
        {
            ViewBag.ListData = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.SelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.SelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.SelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.SelectBranch = Branch_Data.List(Helper.PartnerNumber);

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Xem danh sách nhân sự ");
            //-------------------------------------------------------------------------------------------------

            return View();
        }

        [DisplayName("Bản đồ nhân sự")]
        [Route("so-do-nhan-su")]
        public ActionResult Map()
        {
            List<Employee_Model> ListData = Employee_Data.ListRECURSIVE(Helper.PartnerNumber);

            var sb = new StringBuilder();
            List<Employee_Model> item = ListData.FindAll(x => x.ReportToKey == "");
            string html = GenerateUL(item, ListData, sb);
            ViewBag.Map = html;

            //-------------------------------------------------------------------------------------------------
            Helper.LogAction(UserLog, " Bản đồ nhân sự ");
            //-------------------------------------------------------------------------------------------------

            return View();
        }

        [HttpGet]
        public JsonResult GetID()
        {
            string ID = Helper.AutoEmployeeID("AB", Helper.PartnerNumber);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Personal(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }

        #region [Info View Page]
        //View Total
        [DisplayName("Chi tiết nhân sự")]
        public ActionResult Info(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }
        public ActionResult History(string EmployeeKey = "")
        {
            ViewBag.ListContract = Contract_Data.List(Helper.PartnerNumber, EmployeeKey);
            return View("~/Views/Employee/Profile/History.cshtml");
        }
        public ActionResult General(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View("~/Views/Employee/Profile/General.cshtml", zObj);
        }
        public ActionResult Gross(string EmployeeKey = "")
        {
            ViewBag.ListData = Employee_Data.GetPayrollClose(Helper.PartnerNumber, DateTime.Now, EmployeeKey);
            return View("~/Views/Employee/Profile/Gross.cshtml");
        }
        public ActionResult Seniority(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            ViewBag.ListData = Employee_Data.TINHPHEP(Helper.PartnerNumber, EmployeeKey,
                DateTime.Now.Year.ToString(),
                zInfo.Employee.LastName + " " + zInfo.Employee.FirstName,
                zInfo.Employee.EmployeeID,
                zInfo.Employee.PositionName,
                zInfo.Employee.DepartmentName,
                zInfo.Employee.StartingDate);
            return View("~/Views/Employee/Profile/Seniority.cshtml");
        }
        public ActionResult Review(string EmployeeKey = "")
        {
            ViewBag.ListData = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
            return View("~/Views/Employee/Profile/Review.cshtml");
        }
        #endregion

        public ActionResult ReviewQuick(string EmployeeKey = "")
        {
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;

            Employee_Info zReport = new Employee_Info(zModel.ReportToKey);
            ViewBag.ReportKey = zReport.Employee.EmployeeKey;
            ViewBag.ReportName = zReport.Employee.LastName + " " + zReport.Employee.FirstName;
            ViewBag.ReportPosition = zReport.Employee.PositionName;

            ViewBag.ListItem = Criteria_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Profile/ReviewQuick.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult ReviewSave(string Obj)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                Review_Model zReview = JsonConvert.DeserializeObject<Review_Model>(Obj);
                zReview.ReviewKey = Guid.NewGuid().ToString();
                zReview.PartnerNumber = Helper.PartnerNumber;
                zReview.DateWrite = DateTime.Now;
                zReview.CreatedBy = UserLog.UserKey;
                zReview.CreatedName = UserLog.EmployeeName;
                zReview.ModifiedBy = UserLog.UserKey;
                zReview.ModifiedName = UserLog.EmployeeName;

                Review_Info zInfo = new Review_Info();
                zInfo.Review = zReview;
                zInfo.Create_ClientKey();

                if (zInfo.Review.Code == "200" ||
                    zInfo.Review.Code == "201")
                {
                    foreach (Review_Detail_Model zItem in zReview.ListItem)
                    {
                        Review_Detail_Info zDetail = new Review_Detail_Info();
                        zDetail.Review_Detail.ReviewKey = zReview.ReviewKey;
                        zDetail.Review_Detail.CriteriaKey = zItem.CriteriaKey;
                        zDetail.Review_Detail.CriteriaName = zItem.CriteriaName;
                        zDetail.Review_Detail.Point = zItem.Point;
                        zDetail.Review_Detail.MaxPoint = zItem.MaxPoint;
                        zDetail.Review_Detail.PartnerNumber = Helper.PartnerNumber;

                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Review.Message;
                }

                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        #region [Init Info]
        [HttpPost]
        [DisplayName("Cập nhật nhân sự")]
        public JsonResult SaveEmployee(
                 string EmployeeKey, string EmployeeID, string LastName, string FirstName,
                 int PositionKey, string PositionName, int Gender, string BirthDay,
                 string Passport, string Address, string Email, string MobiPhone,
                 string ReportToKey, string ReportToName,
                 string BranchKey, string BranchName,
                 string DepartmentKey, string DepartmentName, string StartDate, string Style)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info();
            Employee_Model zModel = new Employee_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.EmployeeID = EmployeeID.Trim();
            zModel.LastName = LastName.Trim();
            zModel.FirstName = FirstName.Trim();
            zModel.PositionKey = PositionKey;
            zModel.PositionName = PositionName.Trim();
            zModel.Gender = Gender;
            zModel.BranchKey = BranchKey;
            zModel.BranchName = BranchName;
            zModel.DepartmentKey = DepartmentKey;
            zModel.DepartmentName = DepartmentName;
            zModel.Style = Style;

            DateTime zBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zBirthDay);
            zModel.Birthday = zBirthDay;

            DateTime zStartDate = DateTime.MinValue;
            DateTime.TryParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartDate);
            zModel.StartingDate = zStartDate;

            zModel.PassportNumber = Passport.Trim();
            zModel.AddressRegister = Address.Trim();
            zModel.Email = Email.Trim();
            zModel.MobiPhone = MobiPhone.Trim();
            zModel.ReportToKey = ReportToKey.Trim();
            zModel.ReportToName = ReportToName.Trim();
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                zInfo.Employee = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.EmployeeKey = EmployeeKey;
                zInfo.Employee = zModel;
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [DisplayName("Xem chi tiết nhân sự")]
        public JsonResult DetailEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisplayName("Xóa nhân sự")]
        public JsonResult DeleteEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info();
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Edit Info]
        [DisplayName("Mở điều chỉnh nhân sự")]
        public ActionResult Edit(string EmployeeKey)
        {
            ViewBag.ListSelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.ListSelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(Helper.PartnerNumber);

            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Object zObj = new Employee_Object();

            zObj.Employee = zInfo.Employee;
            zObj.Family = zInfo.Family;

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                ViewBag.ListEdu = Education_Data.ListEdu(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListExperience = Experience_Data.ListExperience(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListSkill = Skill_Data.ListSkill(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListInsure = HistoryInsure_Data.ListInsure(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber, EmployeeKey);
                ViewBag.ListPayroll = Payroll_Person_Data.List(Helper.PartnerNumber, EmployeeKey);
            }

            return View(zObj);
        }

        #region[Info]
        [HttpPost]
        public ActionResult SaveInfo(string EmployeeKey, string LastName, string FirstName, int rdoGender, string BirthDay,
           string Passport, string Address, string Email, string MobiPhone, HttpPostedFileBase[] files)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = Helper.PartnerNumber;
            zInfo.Employee.LastName = LastName.Trim();
            zInfo.Employee.FirstName = FirstName.Trim();
            zInfo.Employee.Gender = rdoGender;

            DateTime zBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(BirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zBirthDay);

            zInfo.Employee.Birthday = zBirthDay;
            zInfo.Employee.PassportNumber = Passport.Trim();
            zInfo.Employee.AddressRegister = Address.Trim();
            zInfo.Employee.Email = Email.Trim();
            zInfo.Employee.MobiPhone = MobiPhone.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (EmployeeKey == "")
            {
                EmployeeKey = Guid.NewGuid().ToString();
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Employee.PhotoPath = StoreFilePost(files, EmployeeKey);
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                //zResult.Success = true;
                //return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //zResult.Success = false;
                //zResult.Message = zInfo.Employee.Message;
                //return Json(zResult, JsonRequestBehavior.AllowGet);
            }

            ViewBag.Message = zResult.Message;
            return RedirectToAction("Edit", new { EmployeeKey });
        }
        [HttpGet]
        public JsonResult DetailInfo(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Family]
        [HttpPost]
        public JsonResult SaveFamily(string EmployeeKey, string FatherName, string FatherBirthday, string FatherWork,
            string FatherAddress, string MotherName, string MotherBirthday, string MotherWork, string MotherAddress,
            string PartnersName, string PartnersBirthday, string PartnersWork, string PartnersAddress,
            string ChildExtend, string OrtherExtend)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = new Family_Model();

            zModel.PartnerNumber = Helper.PartnerNumber;
            zModel.EmployeeKey = EmployeeKey.Trim();
            zModel.FatherName = FatherName.Trim();

            DateTime zFatherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(FatherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFatherBirthday);

            zModel.FatherBirthday = zFatherBirthday;
            zModel.FatherWork = FatherWork.Trim();
            zModel.FatherAddress = FatherAddress.Trim();
            zModel.MotherName = MotherName.Trim();

            DateTime zMotherBirthday = DateTime.MinValue;
            DateTime.TryParseExact(MotherBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zMotherBirthday);

            zModel.MotherBirthday = zMotherBirthday;
            zModel.MotherWork = MotherWork.Trim();
            zModel.MotherAddress = MotherAddress.Trim();
            zModel.PartnersName = PartnersName.Trim();

            DateTime zPartnersBirthday = DateTime.MinValue;
            DateTime.TryParseExact(PartnersBirthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zPartnersBirthday);

            zModel.PartnersBirthday = zPartnersBirthday;
            zModel.PartnersWork = PartnersWork.Trim();
            zModel.PartnersAddress = PartnersAddress.Trim();
            zModel.ChildExtend = ChildExtend.Trim();
            zModel.OrtherExtend = OrtherExtend.Trim();

            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.EmployeeName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Family.AutoKey == 0)
            {
                zInfo.Family = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.AutoKey = zInfo.Family.AutoKey;
                zInfo.Family = zModel;
                zInfo.Update();
            }

            if (zInfo.Family.Code == "200" ||
                zInfo.Family.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Family.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailFamily(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Family_Info zInfo = new Family_Info(EmployeeKey);
            Family_Model zModel = zInfo.Family;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Family.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Job]
        [HttpPost]
        public JsonResult SaveJob(string EmployeeKey, string StartingDate, string ReportToKey, string ReportToName,
           string DepartmentKey, string DepartmentName, string BranchKey, string BranchName, int PositionKey, string PositionName, string Note, int StatusKey, string StatusName)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);

            zInfo.Employee.PartnerNumber = Helper.PartnerNumber;
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Employee.ReportToKey = ReportToKey.Trim();
            zInfo.Employee.ReportToName = ReportToName.Trim();

            DateTime zStartingDate = DateTime.MinValue;
            DateTime.TryParseExact(StartingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zStartingDate);

            zInfo.Employee.WorkingStatusKey = StatusKey;
            zInfo.Employee.WorkingStatusName = StatusName;
            zInfo.Employee.StartingDate = zStartingDate;
            zInfo.Employee.DepartmentKey = DepartmentKey.Trim();
            zInfo.Employee.DepartmentName = DepartmentName.Trim();

            zInfo.Employee.BranchKey = BranchKey.Trim();
            zInfo.Employee.BranchName = BranchName.Trim();

            zInfo.Employee.PositionKey = PositionKey;
            zInfo.Employee.PositionName = PositionName.Trim();
            zInfo.Employee.Note = Note.Trim();

            zInfo.Employee.CreatedBy = UserLog.UserKey;
            zInfo.Employee.CreatedName = UserLog.EmployeeName;
            zInfo.Employee.ModifiedBy = UserLog.UserKey;
            zInfo.Employee.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Employee.EmployeeKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Employee.Code == "200" ||
                zInfo.Employee.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailJob(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Education]
        [HttpPost]
        public JsonResult SaveEdu(string EmployeeKey, string FromDate, string ToDate, string DegreeName,
           string DegreeBy, int StatusKey, string StatusName, string Description, int ClassifiedKey, string ClassifiedName, string TypeName, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);

            zInfo.Education.PartnerNumber = Helper.PartnerNumber;
            zInfo.Education.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Education.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Education.ToDate = zToDate;
            zInfo.Education.DegreeName = DegreeName.Trim();
            zInfo.Education.DegreeBy = DegreeBy.Trim();

            zInfo.Education.StatusKey = StatusKey;
            zInfo.Education.StatusName = StatusName.Trim();
            zInfo.Education.Description = Description.Trim();
            zInfo.Education.ClassifiedKey = ClassifiedKey.ToInt();
            zInfo.Education.ClassifiedName = ClassifiedName.Trim();
            zInfo.Education.TypeName = TypeName.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Education.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Education.Code == "200" ||
                zInfo.Education.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info(AutoKey);
            Education_Model zModel = zInfo.Education;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteEdu(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Education_Info zInfo = new Education_Info();
            zInfo.Education.AutoKey = AutoKey;
            zInfo.Delete();
            Education_Model zModel = zInfo.Education;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Education.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region[Exprience]
        [HttpPost]
        public JsonResult SaveExp(string EmployeeKey, string FromDate, string ToDate, string UnitWork,
           string UnitPosition, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);

            zInfo.Experience.PartnerNumber = Helper.PartnerNumber;
            zInfo.Experience.EmployeeKey = EmployeeKey;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Experience.FromDate = zFromDate;

            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Experience.ToDate = zToDate;
            zInfo.Experience.UnitWork = UnitWork.Trim();
            zInfo.Experience.UnitPosition = UnitPosition.Trim();
            zInfo.Experience.Description = Description.Trim();
            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Experience.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Experience.Code == "200" ||
                zInfo.Experience.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info(AutoKey);
            Experience_Model zModel = zInfo.Experience;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteExp(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Experience_Info zInfo = new Experience_Info();
            zInfo.Experience.AutoKey = AutoKey;
            zInfo.Delete();
            Experience_Model zModel = zInfo.Experience;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Experience.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[Skills]
        [HttpPost]
        public JsonResult SaveSkill(string EmployeeKey, string SkillName, int Levels, int Maxlevels = 10, string Description = "", int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);

            zInfo.Skill.PartnerNumber = Helper.PartnerNumber;
            zInfo.Skill.EmployeeKey = EmployeeKey;


            zInfo.Skill.SkillName = SkillName.Trim();
            zInfo.Skill.Levels = Levels.ToInt();
            zInfo.Skill.Maxlevels = Maxlevels.ToInt();
            zInfo.Skill.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Skill.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Skill.Code == "200" ||
                zInfo.Skill.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailSkill(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info(AutoKey);
            Skill_Model zModel = zInfo.Skill;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteSkill(int Autokey)
        {
            ServerResult zResult = new ServerResult();
            Skill_Info zInfo = new Skill_Info();
            zInfo.Skill.AutoKey = Autokey;
            zInfo.Delete();
            Skill_Model zModel = zInfo.Skill;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Skill.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region[History Insure]
        [HttpPost]
        public JsonResult SaveIn(string EmployeeKey, string DateWrite, string StatusName, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);

            zInfo.HistoryInsure.PartnerNumber = Helper.PartnerNumber;
            zInfo.HistoryInsure.EmployeeKey = EmployeeKey;

            DateTime zDateWrite = DateTime.MinValue;
            DateTime.TryParseExact(DateWrite, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateWrite);

            zInfo.HistoryInsure.DateWrite = zDateWrite;

            zInfo.HistoryInsure.StatusName = StatusName.Trim();
            zInfo.HistoryInsure.Description = Description.Trim();

            //zModel.CreatedBy = UserLog.UserKey;
            //zModel.CreatedName = UserLog.EmployeeName;
            //zModel.ModifiedBy = UserLog.UserKey;
            //zModel.ModifiedName = UserLog.EmployeeName;

            if (zInfo.HistoryInsure.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.HistoryInsure.Code == "200" ||
                zInfo.HistoryInsure.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info(AutoKey);
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteIn(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            HistoryInsure_Info zInfo = new HistoryInsure_Info();
            zInfo.HistoryInsure.AutoKey = AutoKey;
            zInfo.Delete();
            HistoryInsure_Model zModel = zInfo.HistoryInsure;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.HistoryInsure.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Salary apply]
        public ActionResult SalaryStandardList(string EmployeeKey)
        {
            List<Payroll_Person_Model> zList = Payroll_Person_Data.List(Helper.PartnerNumber, EmployeeKey);
            return PartialView("~/Views/Employee/Salary/StandardList.cshtml", zList);
        }

        //Partial view file
        public ActionResult SalaryStandard(string PayrollKey = "")
        {
            Payroll_Person_Info zInfo = new Payroll_Person_Info(PayrollKey);
            Payroll_Person_Model zModel = zInfo.Payroll_Person;
            zModel.ListItem = Payroll_Person_Data.ListDetail(PayrollKey);
            return PartialView("~/Views/Employee/Salary/StandardPayroll.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult SalarySave(string PayrollObj)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                Payroll_Person_Model zModel = JsonConvert.DeserializeObject<Payroll_Person_Model>(PayrollObj, settings);
                Payroll_Person_Info zInfo = new Payroll_Person_Info();

                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.Year = zModel.FromDate.Year;
                zModel.Month = zModel.FromDate.Month;
                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.Activated)
                {
                    zModel.ActivatedDate = DateTime.Now;
                }

                if (zModel.PayrollKey.Length >= 36)
                {
                    zInfo.Payroll_Person = zModel;
                    zInfo.Update();
                }
                else
                {
                    zModel.PayrollKey = Guid.NewGuid().ToString();
                    zInfo.Payroll_Person = zModel;
                    zInfo.Create_ClientKey();
                }

                if (zInfo.Payroll_Person.Code == "200" ||
                    zInfo.Payroll_Person.Code == "201")
                {
                    zInfo.DeleteDetail();
                    foreach (Payroll_Person_Detail_Model zItem in zModel.ListItem)
                    {
                        Payroll_Person_Detail_Info zDetail = new Payroll_Person_Detail_Info();
                        zDetail.Payroll_Person_Detail = zItem;
                        zDetail.Payroll_Person_Detail.PartnerNumber = Helper.PartnerNumber;
                        zDetail.Payroll_Person_Detail.PayrollKey = zModel.PayrollKey;
                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Payroll_Person.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SalaryDelete(string PayrollKey = "")
        {
            ServerResult zResult = new ServerResult();
            if (PayrollKey != string.Empty)
            {
                try
                {
                    Payroll_Person_Info zInfo = new Payroll_Person_Info();
                    zInfo.Payroll_Person.PayrollKey = PayrollKey;
                    zInfo.Delete();

                    if (zInfo.Payroll_Person.Code == "200" ||
                        zInfo.Payroll_Person.Code == "201")
                    {
                        zResult.Success = true;
                        zResult.Message = "";
                    }
                    else
                    {
                        zResult.Success = false;
                        zResult.Message = zInfo.Payroll_Person.Message.GetFirstLine();
                    }
                }
                catch (Exception ex)
                {
                    zResult.Success = false;
                    zResult.Message = ex.ToString();
                }
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn 1 thông tin để xóa";
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region[Review] bỏ
        //[HttpPost]
        //public JsonResult SaveRev(string EmployeeKey, string DateWrite, int Point, int MaxPoint = 10, string Description = "", int AutoKey = 0)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info(AutoKey);

        //    zInfo.Review.PartnerNumber = Helper.PartnerNumber;
        //    //người ghi nhận
        //    zInfo.Review.EmployeeKey = EmployeeKey;

        //    DateTime zDateWrite = DateTime.MinValue;
        //    if (DateTime.TryParse(DateWrite, out zDateWrite))
        //    {

        //    }
        //    zInfo.Review.DateWrite = zDateWrite;

        //    zInfo.Review.Point = Point;
        //    zInfo.Review.MaxPoint = MaxPoint;
        //    zInfo.Review.Description = Description.Trim();
        //    //người đánh giá
        //    zInfo.Review.ReviewerKey = UserLog.UserKey;
        //    zInfo.Review.ReviewerName = UserLog.EmployeeName;

        //    if (zInfo.Review.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Review.Code == "200" ||
        //        zInfo.Review.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailRev(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info(AutoKey);
        //    Review_Model zModel = zInfo.Review;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteRev(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Review_Info zInfo = new Review_Info();
        //    zInfo.Review.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Review_Model zModel = zInfo.Review;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Review.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion
        #endregion

        //~/Views/Employee/Contract
        #region[Contract]
        [HttpPost]
        public JsonResult ContractDelete(string ContractKey = "")
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            Contract_Model zModel = zInfo.Contract;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zInfo.Delete();
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContractList()
        {
            ViewBag.ListData = Contract_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Contract/ContractList.cshtml");
        }
        //public ActionResult SearchContract(string Search)
        //{
        //    ViewBag.ListData = Contract_Data.Search(Helper.PartnerNumber, Search);
        //    return View("~/Views/Employee/SearchResult/Contract.cshtml");
        //}
        public ActionResult ContractEdit(string ContractKey = "")
        {
            ViewBag.ListSelectReportTo = Employee_Data.ListReportTo(Helper.PartnerNumber);
            ViewBag.ListSelectType = ContractType_Data.ListType(Helper.PartnerNumber);
            ViewBag.ListSelectDepartment = Department_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectPosition = Position_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelecPayroll = Payroll_Sample_Data.List(Helper.PartnerNumber);
            ViewBag.ListSelectMinus = Payroll_Sample_Data.ListMinus(Helper.PartnerNumber);

            Contract_Model zModel = new Contract_Model();
            Contract_Info zInfo = new Contract_Info(ContractKey);
            zModel = zInfo.Contract;
            if (zModel.Code != "200" &&
                zModel.Code != "201")
            {
                ViewBag.Message = zModel.Message;
            }
            else
            {
                ViewBag.ListItem = Contract_Detail_Data.List(Helper.PartnerNumber, ContractKey);
                ViewBag.ListFile = Document_Data.List(Helper.PartnerNumber, ContractKey);
            }

            return View("~/Views/Employee/Contract/ContractEdit.cshtml", zModel);
        }

        //#region [Cộng phí]
        //public ActionResult ContractItem(string ContractKey = "")
        //{
        //    ViewBag.ListItem = Contract_Detail_Data.List(Helper.PartnerNumber, ContractKey);
        //    return PartialView("~/Views/Employee/Contract/ContractItem.cshtml");
        //}

        //[HttpPost]
        //public JsonResult SaveItem(int AutoKey, string ContractKey, int ItemKey, string ItemName, float Total, string Description)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);

        //    zInfo.Contract_Detail.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Contract_Detail.ContractKey = ContractKey;
        //    zInfo.Contract_Detail.ItemKey = ItemKey;
        //    zInfo.Contract_Detail.ItemName = ItemName.Trim();
        //    zInfo.Contract_Detail.Total = Total.ToFloat();
        //    zInfo.Contract_Detail.Description = Description.Trim();

        //    zInfo.Contract_Detail.ItemType = 1;
        //    zInfo.Contract_Detail.Quantity = 1;
        //    zInfo.Contract_Detail.UnitName = "Tháng";
        //    //zModel.CreatedBy = UserLog.UserKey;
        //    //zModel.CreatedName = UserLog.EmployeeName;
        //    //zModel.ModifiedBy = UserLog.UserKey;
        //    //zModel.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Contract_Detail.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Contract_Detail.Code == "200" ||
        //        zInfo.Contract_Detail.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info();
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //#endregion

        //#region [Trừ phí]
        //public ActionResult ContractMinus(string ContractKey = "")
        //{
        //    ViewBag.ListItem = Contract_Detail_Data.ListMinus(Helper.PartnerNumber, ContractKey);
        //    return PartialView("~/Views/Employee/Contract/ContractMinus.cshtml");
        //}
        //[HttpPost]
        //public JsonResult SaveMinus(int AutoKey, string ContractKey, int ItemKey, string ItemName, float Quantity = 0, float Total = 0, string Description = "")
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);

        //    zInfo.Contract_Detail.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Contract_Detail.ContractKey = ContractKey;
        //    zInfo.Contract_Detail.ItemKey = ItemKey;
        //    zInfo.Contract_Detail.ItemName = ItemName.Trim();
        //    zInfo.Contract_Detail.Total = Total.ToFloat();
        //    zInfo.Contract_Detail.Description = Description.Trim();

        //    zInfo.Contract_Detail.ItemType = 2;
        //    zInfo.Contract_Detail.Quantity = Quantity;
        //    zInfo.Contract_Detail.UnitName = "%";

        //    zInfo.Contract_Detail.CreatedBy = UserLog.UserKey;
        //    zInfo.Contract_Detail.CreatedName = UserLog.EmployeeName;
        //    zInfo.Contract_Detail.ModifiedBy = UserLog.UserKey;
        //    zInfo.Contract_Detail.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Contract_Detail.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Contract_Detail.Code == "200" ||
        //        zInfo.Contract_Detail.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailMinus(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info(AutoKey);
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteMinus(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Contract_Detail_Info zInfo = new Contract_Detail_Info();
        //    zInfo.Contract_Detail.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Contract_Detail_Model zModel = zInfo.Contract_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Contract_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult MinusInfo(int ItemKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Sample_Info zInfo = new Payroll_Sample_Info(ItemKey);
        //    Payroll_Sample_Model zModel = zInfo.Payroll_Sample;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Sample.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //#endregion

        #region [Contract Save]
        [HttpPost]
        public JsonResult SaveContract(
            string ContractKey,
            string ContractID,
            string SubContract,
            string SignDate,
            int ContractType,
            string ContractTypeName,
            string SignBy,
            string SignName,
            int SignPositionKey,
            string SignPositionName,
            string EmployeeKey,
            string EmployeeName,
            string EmployeeGender,
            string EmployeeBirthDay,
            string EmployeeBirthPlace,
            string EmployeePassport,
            string EmployeeIssueDate,
            string EmployeeIssuePlace,
            string EmployeeAdress,
            string ReportToKey,
            string ReportToName,
            string DepartmentKey,
            string DepartmentName,
            string BranchKey,
            string BranchName,
            int PositionKey,
            string PositionName,
            string FromDate,
            string ToDate,
            string DateBeginSalary,
            string Note,
            string BranchAdress
            )
        {
            ServerResult zResult = new ServerResult();
            Contract_Info zInfo = new Contract_Info(ContractKey);

            zInfo.Contract.PartnerNumber = Helper.PartnerNumber;
            zInfo.Contract.ContractKey = ContractKey;
            zInfo.Contract.ContractID = ContractID.Trim();
            zInfo.Contract.SubContract = SubContract.Trim();
            DateTime zSignDate = DateTime.MinValue;
            DateTime.TryParseExact(SignDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zSignDate);

            zInfo.Contract.SignDate = zSignDate;
            zInfo.Contract.ContractType = ContractType;
            zInfo.Contract.ContractTypeName = ContractTypeName;

            zInfo.Contract.SignBy = SignBy;
            zInfo.Contract.SignName = SignName.Trim();
            zInfo.Contract.SignPositionKey = SignPositionKey;
            zInfo.Contract.SignPositionName = SignPositionName.Trim();

            zInfo.Contract.EmployeeKey = EmployeeKey.Trim();
            zInfo.Contract.EmployeeName = EmployeeName.Trim();
            zInfo.Contract.EmployeeGender = EmployeeGender.Trim();
            DateTime zEmployeeBirthDay = DateTime.MinValue;
            DateTime.TryParseExact(EmployeeBirthDay, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zEmployeeBirthDay);

            zInfo.Contract.EmployeeBirthDay = zEmployeeBirthDay;
            zInfo.Contract.EmployeeBirthPlace = EmployeeBirthPlace.Trim();
            zInfo.Contract.EmployeePassport = EmployeePassport.Trim();
            DateTime zEmployeeIssueDate = DateTime.MinValue;
            DateTime.TryParseExact(EmployeeIssueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zEmployeeIssueDate);

            zInfo.Contract.EmployeeIssueDate = zEmployeeIssueDate;
            zInfo.Contract.EmployeeIssuePlace = EmployeeIssuePlace.Trim();
            zInfo.Contract.EmployeeAdress = EmployeeAdress.Trim();

            zInfo.Contract.ReportToKey = ReportToKey.Trim();
            zInfo.Contract.ReportToName = ReportToName.Trim();
            zInfo.Contract.DepartmentKey = DepartmentKey;
            zInfo.Contract.DepartmentName = DepartmentName.Trim();
            zInfo.Contract.PositionKey = PositionKey;
            zInfo.Contract.PositionName = PositionName.Trim();
            zInfo.Contract.BranchAdress = BranchAdress.Trim();
            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Contract.FromDate = zFromDate;
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Contract.ToDate = zToDate;
            DateTime zDateBeginSalary = DateTime.MinValue;
            DateTime.TryParseExact(DateBeginSalary, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateBeginSalary);

            zInfo.Contract.DateBeginSalary = zDateBeginSalary;
            zInfo.Contract.Note = Note.Trim();

            zInfo.Contract.CreatedBy = UserLog.UserKey;
            zInfo.Contract.CreatedName = UserLog.EmployeeName;
            zInfo.Contract.ModifiedBy = UserLog.UserKey;
            zInfo.Contract.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Contract.ContractKey == "")
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Contract.Code == "200" ||
                zInfo.Contract.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = zInfo.Contract.ContractKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Contract.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //Partial view file
        [HttpGet]
        public ActionResult ListFile(string ContractKey = "")
        {
            List<Document_Model> zList = Document_Data.List(Helper.PartnerNumber, ContractKey);
            return PartialView("~/Views/Shared/_DocumentAttach.cshtml", zList);
        }

        [HttpPost]
        public JsonResult UploadFile()
        {
            ServerResult zResult = new ServerResult();
            if (Request.Files.Count > 0)
            {
                try
                {
                    string ContractKey = Request["ContractKey"];
                    HttpFileCollectionBase files = Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fileExt = Path.GetExtension(file.FileName);
                        string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        string fileReName = TN_Utils.ToEnglish(fileName) + fileExt;

                        #region[Upload]
                        string zFilePath = Helper.UploadPath + "/Employee/" + ContractKey + "/";
                        string zFileSave = Path.Combine(Server.MapPath(zFilePath), fileReName);

                        // Check Foder
                        DirectoryInfo zDir = new DirectoryInfo(Server.MapPath(zFilePath));
                        if (!zDir.Exists)
                        {
                            zDir.Create();
                        }
                        else
                        {
                            if (System.IO.File.Exists(zFileSave))
                            {
                                System.IO.File.Delete(zFileSave);
                            }
                        }

                        file.SaveAs(zFileSave);
                        #endregion

                        Document_Model zModel = new Document_Model();
                        zModel.TableKey = ContractKey;
                        zModel.TableJoin = Helper.TableContractEmployee;

                        zModel.FileExt = fileExt;
                        zModel.FileName = fileName;
                        zModel.FilePath = (zFilePath + fileReName);
                        zModel.PartnerNumber = Helper.PartnerNumber;
                        zModel.CreatedBy = UserLog.CreatedBy;
                        zModel.CreatedName = UserLog.CreatedName;
                        zModel.ModifiedBy = UserLog.ModifiedBy;
                        zModel.ModifiedName = UserLog.ModifiedName;

                        Document_Info zInfo = new Document_Info();
                        zInfo.Document = zModel;
                        zInfo.Create_ServerKey();

                        if (zInfo.Document.Code != "200" &&
                            zInfo.Document.Code != "201")
                        {
                            zResult.Message = "Lỗi upload tập tin !." + fileName;
                            zResult.Success = false;
                            return Json(zResult, JsonRequestBehavior.AllowGet);
                        }
                    }

                    zResult.Success = true;
                }
                catch (Exception ex)
                {
                    zResult.Message = ex.Message;
                }
            }
            else
            {
                zResult.Message = "Không có tập tin";
                zResult.Success = false;
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //~/Views/Employee/Salary
        #region[Salary - đề xuất tăng lương]
        public ActionResult Increase()
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListItem = Payroll_Sample_Data.List(Helper.PartnerNumber);
            ViewBag.Increase = Payroll_Increase_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Salary/Increase.cshtml");
        }
        [HttpPost]
        public JsonResult SaveIncrease(int AutoKey, string ID, string ContractID, string EmployeeKey, string EmployeeName, int ItemKey, string ItemName, float Total, string WriteDate, string FromDate, string ToDate, string Description)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Increase_Info zInfo = new Payroll_Increase_Info(AutoKey);

            zInfo.Payroll_Increase.PartnerNumber = Helper.PartnerNumber;
            zInfo.Payroll_Increase.AutoKey = AutoKey;
            zInfo.Payroll_Increase.ID = ID.Trim();
            zInfo.Payroll_Increase.ContractKey = "";
            zInfo.Payroll_Increase.ContractID = ContractID.Trim();
            zInfo.Payroll_Increase.EmployeeKey = EmployeeKey;
            zInfo.Payroll_Increase.EmployeeName = EmployeeName.Trim();
            zInfo.Payroll_Increase.ItemKey = ItemKey;
            zInfo.Payroll_Increase.ItemName = ItemName.Trim();
            zInfo.Payroll_Increase.Total = Total.ToFloat();
            zInfo.Payroll_Increase.Description = Description.Trim();

            DateTime zWriteDate = DateTime.MinValue;
            DateTime.TryParseExact(WriteDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zWriteDate);
            zInfo.Payroll_Increase.WriteDate = zWriteDate;

            DateTime zFromDate = DateTime.MinValue;
            DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);

            zInfo.Payroll_Increase.FromDate = zFromDate;
            DateTime zToDate = DateTime.MinValue;
            DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

            zInfo.Payroll_Increase.ToDate = zToDate;

            zInfo.Payroll_Increase.ItemType = 1;
            zInfo.Payroll_Increase.Quantity = 1;
            zInfo.Payroll_Increase.UnitName = "Tháng";
            zInfo.Payroll_Increase.CreatedBy = UserLog.UserKey;
            zInfo.Payroll_Increase.CreatedName = UserLog.EmployeeName;
            zInfo.Payroll_Increase.ModifiedBy = UserLog.UserKey;
            zInfo.Payroll_Increase.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Payroll_Increase.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Payroll_Increase.Code == "200" ||
                zInfo.Payroll_Increase.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Increase.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailIncrease(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Increase_Info zInfo = new Payroll_Increase_Info(AutoKey);
            Payroll_Increase_Model zModel = zInfo.Payroll_Increase;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Increase.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteIncrease(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Payroll_Increase_Info zInfo = new Payroll_Increase_Info();
            zInfo.Payroll_Increase.AutoKey = AutoKey;
            zInfo.Delete();
            Payroll_Increase_Model zModel = zInfo.Payroll_Increase;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Payroll_Increase.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //~/Views/Employee/Salary
        #region[Bang Khoi tao Luong Ban đầu]
        //public ActionResult SalaryStandard()
        //{
        //    ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
        //    return View("~/Views/Employee/Salary/Standard.cshtml");
        //}

        //public ActionResult StandardEdit(string Key = "")
        //{
        //    Payroll_Person_Info zInfo = new Payroll_Person_Info(Key);
        //    Payroll_Person_Model zModel = zInfo.Payroll_Person;
        //    ViewBag.ListItem = Payroll_Code_Data.List(Helper.PartnerNumber);
        //    ViewBag.ListData = Payroll_Person_Detail_Data.List(Helper.PartnerNumber, Key);
        //    return View("~/Views/Employee/Salary/StandardEdit.cshtml", zModel);
        //}
        ////khởi tạo thông tin
        //[HttpPost]
        //public JsonResult StandardNew(string PayrollKey, string EmployeeKey, string DateWrite)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Person_Info zInfo = new Payroll_Person_Info(PayrollKey);
        //    zInfo.Payroll_Person.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Payroll_Person.PayrollKey = PayrollKey;
        //    zInfo.Payroll_Person.Month = DateWrite.Substring(0, 2).ToInt();
        //    zInfo.Payroll_Person.Year = DateWrite.Substring(3, 4).ToInt();

        //    zInfo.Payroll_Person.EmployeeKey = EmployeeKey;
        //    Employee_Info zEm = new Employee_Info(EmployeeKey);
        //    zInfo.Payroll_Person.EmployeeID = zEm.Employee.EmployeeID;
        //    zInfo.Payroll_Person.EmployeeName = zEm.Employee.LastName + " " + zEm.Employee.FirstName; ;
        //    zInfo.Payroll_Person.BranchKey = zEm.Employee.BranchKey;
        //    zInfo.Payroll_Person.BranchName = zEm.Employee.BranchName;
        //    zInfo.Payroll_Person.DepartmentKey = zEm.Employee.DepartmentKey;
        //    zInfo.Payroll_Person.DepartmentName = zEm.Employee.DepartmentName;
        //    zInfo.Payroll_Person.PositionKey = zEm.Employee.PositionKey;
        //    zInfo.Payroll_Person.PositionName = zEm.Employee.PositionName; ;

        //    zInfo.Payroll_Person.CreatedBy = UserLog.UserKey;
        //    zInfo.Payroll_Person.CreatedName = UserLog.EmployeeName;
        //    zInfo.Payroll_Person.ModifiedBy = UserLog.UserKey;
        //    zInfo.Payroll_Person.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Payroll_Person.PayrollKey == "")
        //    {
        //        zInfo.Create_ClientKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Payroll_Person.Code == "200" ||
        //        zInfo.Payroll_Person.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Message = zInfo.Payroll_Person.PayrollKey;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Person.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult SaveStandardItem(string PayrollKey, int AutoKey, int ItemKey, float Total, string Description)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Person_Detail_Info zInfo = new Payroll_Person_Detail_Info(AutoKey);

        //    zInfo.Payroll_Person_Detail.PartnerNumber = Helper.PartnerNumber;
        //    zInfo.Payroll_Person_Detail.AutoKey = AutoKey;
        //    zInfo.Payroll_Person_Detail.PayrollKey = PayrollKey.Trim();

        //    Payroll_Code_Info zCode = new Payroll_Code_Info(ItemKey);

        //    zInfo.Payroll_Person_Detail.ItemKey = zCode.Payroll_Code.ItemKey;
        //    zInfo.Payroll_Person_Detail.ItemID = zCode.Payroll_Code.ItemID;
        //    zInfo.Payroll_Person_Detail.ItemName = zCode.Payroll_Code.ItemName;
        //    zInfo.Payroll_Person_Detail.ItemType = zCode.Payroll_Code.ItemType;
        //    zInfo.Payroll_Person_Detail.Total = Total;
        //    zInfo.Payroll_Person_Detail.Description = Description.Trim();

        //    zInfo.Payroll_Person_Detail.CreatedBy = UserLog.UserKey;
        //    zInfo.Payroll_Person_Detail.CreatedName = UserLog.EmployeeName;
        //    zInfo.Payroll_Person_Detail.ModifiedBy = UserLog.UserKey;
        //    zInfo.Payroll_Person_Detail.ModifiedName = UserLog.EmployeeName;

        //    if (zInfo.Payroll_Person_Detail.AutoKey == 0)
        //    {
        //        zInfo.Create_ServerKey();
        //    }
        //    else
        //    {
        //        zInfo.Update();
        //    }

        //    if (zInfo.Payroll_Person_Detail.Code == "200" ||
        //        zInfo.Payroll_Person_Detail.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Person_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpGet]
        //public JsonResult DetailStandardItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Person_Detail_Info zInfo = new Payroll_Person_Detail_Info(AutoKey);
        //    Payroll_Person_Detail_Model zModel = zInfo.Payroll_Person_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        zResult.Data = JsonConvert.SerializeObject(zModel);
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Person_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //[HttpPost]
        //public JsonResult DeleteStandardItem(int AutoKey)
        //{
        //    ServerResult zResult = new ServerResult();
        //    Payroll_Person_Detail_Info zInfo = new Payroll_Person_Detail_Info();
        //    zInfo.Payroll_Person_Detail.AutoKey = AutoKey;
        //    zInfo.Delete();
        //    Payroll_Person_Detail_Model zModel = zInfo.Payroll_Person_Detail;
        //    if (zModel.Code == "200" ||
        //        zModel.Code == "201")
        //    {
        //        zResult.Success = true;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        zResult.Success = false;
        //        zResult.Message = zInfo.Payroll_Person_Detail.Message;
        //        return Json(zResult, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        //~/Views/
        #region [Review - đánh giá]
        public ActionResult ReviewList()
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            ViewBag.ListReview = Review_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Review/ReviewList.cshtml");
        }

        [HttpPost]
        public JsonResult SaveReview(string ReviewKey, string DateWrite, string EmployeeKey, string ValuerKey, string ValuerName, string Description)
        {
            ServerResult zResult = new ServerResult();
            Review_Info zInfo = new Review_Info(ReviewKey);

            zInfo.Review.PartnerNumber = Helper.PartnerNumber;
            zInfo.Review.ReviewKey = ReviewKey;
            DateTime zDateWrite = DateTime.MinValue;
            DateTime.TryParseExact(DateWrite, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateWrite);
            zInfo.Review.DateWrite = zDateWrite;

            Employee_Info zEmp = new Employee_Info(EmployeeKey);

            zInfo.Review.EmployeeKey = zEmp.Employee.EmployeeKey;
            zInfo.Review.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
            zInfo.Review.BranchKey = zEmp.Employee.BranchKey;
            zInfo.Review.BranchName = zEmp.Employee.BranchName;
            zInfo.Review.DepartmentKey = zEmp.Employee.DepartmentKey;
            zInfo.Review.DepartmentName = zEmp.Employee.DepartmentName;
            zInfo.Review.Description = Description.Trim();

            zInfo.Review.ValuerKey = ValuerKey;
            zInfo.Review.ValuerName = ValuerName;

            zInfo.Review.CreatedBy = UserLog.UserKey;
            zInfo.Review.CreatedName = UserLog.EmployeeName;
            zInfo.Review.ModifiedBy = UserLog.UserKey;
            zInfo.Review.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Review.ReviewKey == "")
            {
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Review.Code == "200" ||
                zInfo.Review.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = zInfo.Review.ReviewKey;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailReview(string ReviewKey)
        {
            ServerResult zResult = new ServerResult();
            Review_Info zInfo = new Review_Info(ReviewKey);
            Review_Model zModel = zInfo.Review;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteReview(string ReviewKey)
        {
            ServerResult zResult = new ServerResult();
            Review_Info zInfo = new Review_Info();
            zInfo.Review.ReviewKey = ReviewKey;
            zInfo.Delete();
            Review_Model zModel = zInfo.Review;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReviewEdit(string Key = "")
        {
            ViewBag.ListCriteria = Criteria_Data.List(Helper.PartnerNumber);
            ViewBag.ListDetail = Review_Detail_Data.List(Helper.PartnerNumber, Key);
            Review_Info zInfo = new Review_Info(Key);
            Review_Model zModel = zInfo.Review;
            return View("~/Views/Employee/Review/ReviewEdit.cshtml", zModel);
        }
        [HttpPost]
        public JsonResult SaveReviewItem(string ReviewKey, int AutoKey, int MaxPoint, int Point, int CriteriaKey, string CriteriaName, int Rank, string Description)
        {
            ServerResult zResult = new ServerResult();
            Review_Detail_Info zInfo = new Review_Detail_Info(AutoKey);

            zInfo.Review_Detail.PartnerNumber = Helper.PartnerNumber;
            zInfo.Review_Detail.ReviewKey = ReviewKey;
            zInfo.Review_Detail.AutoKey = AutoKey;
            zInfo.Review_Detail.MaxPoint = MaxPoint;
            zInfo.Review_Detail.Point = Point;
            zInfo.Review_Detail.CriteriaKey = CriteriaKey;
            zInfo.Review_Detail.CriteriaName = CriteriaName;
            zInfo.Review_Detail.Rank = Rank;
            zInfo.Review_Detail.Description = Description.Trim();


            zInfo.Review_Detail.CreatedBy = UserLog.UserKey;
            zInfo.Review_Detail.CreatedName = UserLog.EmployeeName;
            zInfo.Review_Detail.ModifiedBy = UserLog.UserKey;
            zInfo.Review_Detail.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Review_Detail.AutoKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Review_Detail.Code == "200" ||
                zInfo.Review_Detail.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review_Detail.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult DetailReviewItem(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Review_Detail_Info zInfo = new Review_Detail_Info(AutoKey);
            Review_Detail_Model zModel = zInfo.Review_Detail;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review_Detail.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult DeleteReviewItem(int AutoKey)
        {
            ServerResult zResult = new ServerResult();
            Review_Detail_Info zInfo = new Review_Detail_Info();
            zInfo.Review_Detail.AutoKey = AutoKey;
            zInfo.Delete();
            Review_Detail_Model zModel = zInfo.Review_Detail;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Review_Detail.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region [Over-Time]
        //~/Views/Employee/Salary/OverTime.cshtml
        public ActionResult OverTime()
        {
            ViewBag.ListData = Over_Time_Data.List(Helper.PartnerNumber);

            return View("~/Views/Employee/Salary/OverTimeList.cshtml");
        }

        public ActionResult OverTimeEdit(string OvertimeKey = "")
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            Over_Time_Info zInfo = new Over_Time_Info(OvertimeKey);
            Over_Time_Model zModel = zInfo.Over_Time;
            zModel.ListItem = Over_Time_Data.ListDetail(Helper.PartnerNumber, OvertimeKey);
            return PartialView("~/Views/Employee/Salary/OverTimeEdit.cshtml", zModel);
        }

        public ActionResult OverTimeRow()
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);
            return PartialView("~/Views/Employee/Salary/OverTimeRow.cshtml");
        }

        [HttpPost]
        public JsonResult SaveOverTime(string OverTimeObj)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                };
                settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                Over_Time_Model zModel = JsonConvert.DeserializeObject<Over_Time_Model>(OverTimeObj, settings);
                Over_Time_Info zInfo = new Over_Time_Info();

                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.OverTimeDate = zModel.OverTimeDate;
                zModel.Paramater = zModel.Paramater;
                zModel.Description = zModel.Description.Trim();
                Employee_Info zEmp = new Employee_Info(UserLog.EmployeeKey);

                zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
                zModel.EmployeeID = zEmp.Employee.EmployeeID;
                zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
                zModel.BranchKey = zEmp.Employee.BranchKey;
                zModel.BranchName = zEmp.Employee.BranchName;
                zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
                zModel.DepartmentName = zEmp.Employee.DepartmentName;
                zModel.PositionKey = zEmp.Employee.PositionKey;
                zModel.PositionName = zEmp.Employee.PositionName;

                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                if (zModel.OvertimeKey.Length >= 36)
                {
                    zInfo.Over_Time = zModel;
                    zInfo.Update();
                }
                else
                {
                    zModel.OvertimeKey = Guid.NewGuid().ToString();
                    zInfo.Over_Time = zModel;
                    zInfo.Create_ClientKey();
                }

                if (zInfo.Over_Time.Code == "200" ||
                    zInfo.Over_Time.Code == "201")
                {
                    zInfo.DeleteDetail();
                    foreach (Over_Time_Detail_Model zItem in zModel.ListItem)
                    {
                        Over_Time_Detail_Info zDetail = new Over_Time_Detail_Info();
                        zDetail.Over_Time_Detail = zItem;

                        zDetail.Over_Time_Detail.PartnerNumber = Helper.PartnerNumber;
                        zDetail.Over_Time_Detail.ParentKey = zModel.OvertimeKey;

                        Employee_Info zEmployee = new Employee_Info(zItem.EmployeeKey);
                        zDetail.Over_Time_Detail.EmployeeKey = zEmployee.Employee.EmployeeKey;
                        zDetail.Over_Time_Detail.EmployeeID = zEmployee.Employee.EmployeeID;
                        zDetail.Over_Time_Detail.EmployeeName = zEmployee.Employee.LastName + " " + zEmp.Employee.FirstName;
                        zDetail.Over_Time_Detail.BranchKey = zEmployee.Employee.BranchKey;
                        zDetail.Over_Time_Detail.BranchName = zEmployee.Employee.BranchName;
                        zDetail.Over_Time_Detail.DepartmentKey = zEmployee.Employee.DepartmentKey;
                        zDetail.Over_Time_Detail.DepartmentName = zEmployee.Employee.DepartmentName;
                        zDetail.Over_Time_Detail.PositionKey = zEmployee.Employee.PositionKey;
                        zDetail.Over_Time_Detail.PositionName = zEmployee.Employee.PositionName;

                        zDetail.Over_Time_Detail.Paramater = zInfo.Over_Time.Paramater;
                        zDetail.Over_Time_Detail.ItemKey = zInfo.Over_Time.ItemKey;
                        zDetail.Over_Time_Detail.ItemID = zInfo.Over_Time.ItemID;
                        zDetail.Over_Time_Detail.ItemName = zInfo.Over_Time.ItemName;
                        zDetail.Over_Time_Detail.DateOverTime = zInfo.Over_Time.OverTimeDate;


                        zDetail.Create_ServerKey();
                    }

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Over_Time.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteOverTime(string OvertimeKey)
        {
            ServerResult zResult = new ServerResult();
            Over_Time_Info zInfo = new Over_Time_Info();
            zInfo.Over_Time.OvertimeKey = OvertimeKey;
            zInfo.Delete();
            Over_Time_Model zModel = zInfo.Over_Time;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Over_Time.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //common function
        [HttpGet]
        public JsonResult GetInfoEmployee(string EmployeeKey)
        {
            ServerResult zResult = new ServerResult();
            Employee_Info zInfo = new Employee_Info(EmployeeKey);
            Employee_Model zModel = zInfo.Employee;
            if (zModel.Code == "200" ||
                zModel.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Employee.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #region [Xử lý tính lương]

        #region [Chấm công]
        [DisplayName("Chấm công tháng")]
        [Route("cham-cong")]
        public ActionResult WorkDayList()
        {
            ViewBag.ListData = Work_Day_Data.List(Helper.PartnerNumber, out _);
            return View("~/Views/Employee/Salary/WorkDayList.cshtml");
        }

        public ActionResult WorkDayEdit(int AutoKey = 0)
        {
            ViewBag.ListSelectEmployee = Employee_Data.List(Helper.PartnerNumber);

            var zModel = new Work_Day_Model();
            var zInfo = new Work_Day_Info(AutoKey);
            if (zInfo.Code == "404")
            {
                zModel.DateWrite = DateTime.Now.ToString("MM/yyyy");
            }
            else
            {
                zModel = zInfo.Work_Day;
            }

            return PartialView("~/Views/Employee/Salary/WorkDayEdit.cshtml", zModel);
        }
        [HttpPost]
        public JsonResult SaveWorkDay(string EmployeeKey, int Days, string DateWrite, string Description, int AutoKey = 0)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                var zModel = new Work_Day_Model();
                zModel.ParentKey = "";
                zModel.PartnerNumber = Helper.PartnerNumber;
                zModel.Days = Days;
                zModel.DateWrite = DateWrite;
                zModel.Description = Description;

                Employee_Info zEmp = new Employee_Info(EmployeeKey);
                zModel.EmployeeKey = zEmp.Employee.EmployeeKey;
                zModel.EmployeeID = zEmp.Employee.EmployeeID;
                zModel.EmployeeName = zEmp.Employee.LastName + " " + zEmp.Employee.FirstName;
                zModel.BranchKey = zEmp.Employee.BranchKey;
                zModel.BranchName = zEmp.Employee.BranchName;
                zModel.DepartmentKey = zEmp.Employee.DepartmentKey;
                zModel.DepartmentName = zEmp.Employee.DepartmentName;
                zModel.PositionKey = zEmp.Employee.PositionKey;
                zModel.PositionName = zEmp.Employee.PositionName;

                zModel.CreatedBy = UserLog.UserKey;
                zModel.CreatedName = UserLog.EmployeeName;
                zModel.ModifiedBy = UserLog.UserKey;
                zModel.ModifiedName = UserLog.EmployeeName;

                Work_Day_Info zInfo = new Work_Day_Info();
                zInfo.Work_Day = zModel;
                if (AutoKey != 0)
                {
                    zInfo.Update();
                }
                else
                {
                    zInfo.Create_KeyAuto();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {

                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message;
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteWorkDay(int AutoKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Work_Day_Info();
            zInfo.Work_Day.AutoKey = AutoKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        public ActionResult ExportExcel(int Type)
        {
            List<Payroll_Item> zListPayroll = new List<Payroll_Item>();
            if (TempData["Payroll"] != null)
            {
                zListPayroll = TempData["Payroll"] as List<Payroll_Item>;
            }

            TempData.Keep();

            DataTable zTable = new DataTable();
            string Filename = "";
            switch (Type)
            {
                case 1:
                    Filename = "BANG_PHAI_TRU.xls";
                    zTable = InsuranceTable(zListPayroll);
                    break;

                case 2:
                    Filename = "BANG_LAM_THEM.xls";
                    zTable = OverTimeTable(zListPayroll);
                    break;

                case 3:
                    Filename = "BANG_PHU_CAP.xls";
                    zTable = SupportTable(zListPayroll);
                    break;

                case 4:
                    Filename = "BANG_THANH_TOAN_LUONG.xls";
                    zTable = PaymentTable(zListPayroll);
                    zTable.Columns.RemoveAt(4);
                    zTable.AcceptChanges();
                    break;
            }

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                // setting style
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("BANG_THANH_TOAN_LUONG");

                #region [Tiêu đề Excel]
                worksheet.Cells[1, 1, 1, 11].Value = "CÔNG TY CỔ PHẦN AN BÌNH";
                worksheet.Cells[1, 1, 1, 11].Merge = true;
                worksheet.Row(1).Style.Font.Bold = true;

                worksheet.Cells[2, 1, 2, 11].Value = "Số 10, Đại lộ Độc Lập, KP Bình Đường 1, P An Bình, Tp Dĩ An, tỉnh Bình Dương";
                worksheet.Cells[2, 1, 2, 11].Merge = true;
                worksheet.Row(2).Style.Font.Bold = true;

                worksheet.Cells[3, 1, 3, 11].Value = "BẢNG THANH TOÁN LƯƠNG";
                worksheet.Cells[3, 1, 3, 11].Style.Font.Size = 16;
                worksheet.Cells[3, 1, 3, 11].Merge = true;
                worksheet.Row(3).Style.Font.Bold = true;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[4, 1, 4, 11].Value = "Ngày Tháng Năm";
                worksheet.Cells[4, 1, 4, 11].Merge = true;
                worksheet.Row(4).Style.Font.Bold = true;
                worksheet.Row(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                #endregion

                #region[Fill data]
                //Tên cột
                var row = 5;
                worksheet.Cells[row, 1].Value = "STT";
                worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                for (int i = 2; i < zTable.Columns.Count; i++)
                {
                    string Name = zTable.Columns[i].ColumnName;
                    worksheet.Cells[row, i].Value = Name;
                    worksheet.Cells[row, i].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }

                worksheet.Row(row).Height = 40;
                worksheet.Row(row).Style.WrapText = true;
                worksheet.Row(row).Style.Font.Bold = true;
                worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(row).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //

                //Dữ liệu
                string Branch = "";
                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    row++;
                    #region [Tiêu đề mỗi chi nhánh]
                    string tmp = r[0].ToString();
                    if (tmp.ToUpper() != Branch.ToUpper())
                    {
                        Branch = tmp;

                        worksheet.Cells[row, 1, row, 3].Value = Branch;
                        worksheet.Cells[row, 1, row, 3].Merge = true;

                        worksheet.Row(row).Style.Font.Bold = true;
                        worksheet.Row(row).Height = 30;
                        worksheet.Row(row).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #region [Sum tổng các cột]
                        for (int i = 4; i < zTable.Columns.Count; i++)
                        {
                            var total = SumColumn(zTable, i, Branch);
                            worksheet.Cells[row, i].Value = total.ToString("n0");
                            worksheet.Cells[row, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            worksheet.Cells[row, i].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        }
                        #endregion

                        worksheet.Cells[row, 1, row, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[row, 1, row, 10].Style.Fill.BackgroundColor.SetColor(Color.LemonChiffon);

                        row++;
                        No = 1;
                    }
                    #endregion

                    #region [Data]
                    worksheet.Cells[row, 1].Value = (No++).ToString();
                    worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                    for (int i = 2; i < zTable.Columns.Count; i++)
                    {
                        string val = r[i].ToString();

                        if (i >= 4)
                        {
                            worksheet.Cells[row, i].Value = r[i].ToDouble().ToString("n0");
                            worksheet.Cells[row, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            worksheet.Column(i).Width = 11;
                        }
                        else
                        {
                            worksheet.Cells[row, i].Value = val;
                        }

                        worksheet.Cells[row, i].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    #endregion
                }


                #endregion
                //Custom
                worksheet.Column(1).Width = 5;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 18;

                //Style Excel
                worksheet.Cells.Style.Font.Name = "Times New Roman";

                package.Save();
            }

            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Filename);
        }

        private double SumColumn(DataTable zTable, int No, string Name)
        {
            double result = 0;
            foreach (DataRow r in zTable.Rows)
            {
                if (r[0].ToString().ToUpper() == Name.ToUpper())
                {
                    result += Convert.ToDouble(r[No]);
                }
            }

            return result;
        }


        public ActionResult SetupPayroll()
        {
            return View("~/Views/Employee/Salary/SetupPayroll.cshtml");
        }
        public ActionResult PayrollStep1()
        {
            ViewBag.ListData = Employee_Data.Ready_Payroll(Helper.PartnerNumber);
            return PartialView("~/Views/Employee/Salary/_Step1.cshtml");
        }
        public ActionResult PayrollStep2()
        {
            ViewBag.ListData = Payroll_Sample_Data.List(Helper.PartnerNumber);
            return PartialView("~/Views/Employee/Salary/_Step2.cshtml");
        }
        public ActionResult PayrollStep3(string Date)
        {
            DateTime zDate = DateTime.Now;
            DateTime.TryParseExact(Date, "dd/MM/yyyy",
               CultureInfo.InvariantCulture,
               DateTimeStyles.None, out zDate);

            List<Payroll_Item> zListPayroll = new List<Payroll_Item>();
            List<Employee_Model> zListEmployee = Employee_Data.List(Helper.PartnerNumber);
            foreach (Employee_Model zEmployee in zListEmployee)
            {
                string EmployeeKey = zEmployee.EmployeeKey;
                string EmployeeName = zEmployee.LastName + " " + zEmployee.FirstName;
                string EmployeeID = zEmployee.EmployeeID;
                string Position = zEmployee.PositionName;
                string Branch = zEmployee.BranchName;
                string Department = zEmployee.DepartmentName;
                string Style = zEmployee.Style;

                zListPayroll.AddRange(CalculatorPayroll(EmployeeKey, EmployeeName, EmployeeID, Position, Department, Branch, zDate, Style.Trim()));
            }

            ViewBag.PivotTable = PopulateGrid(zListPayroll);
            TempData["Payroll"] = zListPayroll;
            return PartialView("~/Views/Employee/Salary/_Step3.cshtml");
        }
        public ActionResult PayrollStep4()
        {
            List<Payroll_Item> zListPayroll = new List<Payroll_Item>();
            if (TempData["Payroll"] != null)
            {
                zListPayroll = TempData["Payroll"] as List<Payroll_Item>;
            }

            ViewBag.InsuranceTable = InsuranceTable(zListPayroll);
            ViewBag.OverTimeTable = OverTimeTable(zListPayroll);
            ViewBag.SupportTable = SupportTable(zListPayroll);
            ViewBag.PaymentTable = PaymentTable(zListPayroll);

            TempData.Keep();
            return PartialView("~/Views/Employee/Salary/_Step4.cshtml");
        }

        public ActionResult PayrollTable()
        {
            ViewBag.ListData = Payroll_Close_Data.List(Helper.PartnerNumber);
            return View("~/Views/Employee/Salary/PayrollTable.cshtml");
        }
        public ActionResult PayrollTableView(string CloseKey)
        {
            Payroll_Close_Model zModel = new Payroll_Close_Model();
            Payroll_Close_Info zInfo = new Payroll_Close_Info(Guid.Parse(CloseKey));

            zModel = zInfo.Payroll_Close;
            zModel.ListItem = Payroll_Close_Detail_Data.List(Helper.PartnerNumber, CloseKey);

            List<Payroll_Item> zListPayroll = new List<Payroll_Item>();
            foreach (Payroll_Close_Detail_Model zItem in zModel.ListItem)
            {
                List<Payroll_Item> zList = JsonConvert.DeserializeObject<List<Payroll_Item>>(zItem.JsonData);
                zListPayroll.AddRange(zList);
            }

            ViewBag.InsuranceTable = InsuranceTable(zListPayroll);
            ViewBag.OverTimeTable = OverTimeTable(zListPayroll);
            ViewBag.SupportTable = SupportTable(zListPayroll);
            ViewBag.PaymentTable = PaymentTable(zListPayroll);

            return View("~/Views/Employee/Salary/PayrollTableView.cshtml", zModel);
        }

        [HttpPost]
        public JsonResult PayrollClose(string CloseDate, string Description)
        {
            ServerResult zResult = new ServerResult();
            try
            {
                List<Payroll_Item> ListData = new List<Payroll_Item>();
                if (TempData["Payroll"] != null)
                {
                    ListData = TempData["Payroll"] as List<Payroll_Item>;
                }

                DateTime zDate = DateTime.Now;
                DateTime.TryParseExact(CloseDate, "dd/MM/yyyy",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.None, out zDate);
                CloseDate = zDate.ToString("MM/yyyy");
                Payroll_Close_Info zInfo = new Payroll_Close_Info(CloseDate);
                if (zInfo.Payroll_Close.CloseKey.Length >= 36)
                {
                    zInfo.Delete_Detail();
                }
                else
                {
                    zInfo.Payroll_Close.CloseKey = Guid.NewGuid().ToString();
                }

                zInfo.Payroll_Close.PartnerNumber = Helper.PartnerNumber;
                zInfo.Payroll_Close.CloseDate = CloseDate;
                zInfo.Payroll_Close.Description = Description;
                zInfo.Payroll_Close.EmployeeKey = UserLog.EmployeeKey;

                zInfo.Payroll_Close.CreatedBy = UserLog.UserKey;
                zInfo.Payroll_Close.CreatedName = UserLog.EmployeeName;
                zInfo.Payroll_Close.ModifiedBy = UserLog.UserKey;
                zInfo.Payroll_Close.ModifiedName = UserLog.EmployeeName;

                zInfo.Create_ClientKey();

                if (ListData.Count > 0)
                {
                    StringBuilder Query = new StringBuilder();

                    var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();
                    foreach (Payroll_Item rec in listEmployee)
                    {
                        var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);

                        string SQL = "";
                        SQL += " INSERT INTO HRM_Payroll_Close_Detail ";
                        SQL += " (ParentKey , CloseDate , EmployeeKey , JsonData , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName )";
                        SQL += " VALUES ";
                        SQL += " (@ParentKey , @CloseDate , @EmployeeKey , @JsonData , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName )";

                        SQL = SQL.Replace("@ParentKey", "'" + zInfo.Payroll_Close.CloseKey + "'");
                        SQL = SQL.Replace("@CloseDate", "'" + zInfo.Payroll_Close.CloseDate + "'");
                        SQL = SQL.Replace("@EmployeeKey", "'" + rec.EmployeeKey + "'");
                        SQL = SQL.Replace("@JsonData", "N'" + JsonConvert.SerializeObject(data) + "'");
                        SQL = SQL.Replace("@PartnerNumber", "'" + Helper.PartnerNumber + "'");
                        SQL = SQL.Replace("@RecordStatus", "0");
                        SQL = SQL.Replace("@CreatedBy", "'" + UserLog.UserKey + "'");
                        SQL = SQL.Replace("@CreatedName", "N'" + UserLog.EmployeeName + "'");
                        SQL = SQL.Replace("@ModifiedBy", "'" + UserLog.UserKey + "'");
                        SQL = SQL.Replace("@ModifiedName", "N'" + UserLog.EmployeeName + "'");

                        Query.AppendLine(SQL);
                    }

                    string Message = "";
                    Helper.RunSQL(Query.ToString(), out Message);
                    if (Message != string.Empty)
                    {
                        zResult.Success = false;
                        zResult.Message = Message;
                    }
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString();
            }
            zResult.Success = true;

            return Json(zResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private string SaveCloseLeave(Leave_Close_Model Item)
        {
            Leave_Close_Info zInfo = new Leave_Close_Info(Item.Reference);
            zInfo.Leave_Close.Reference = Item.Reference;
            zInfo.Leave_Close.EmployeeKey = Item.EmployeeKey;
            zInfo.Leave_Close.EmployeeName = Item.EmployeeName;
            zInfo.Leave_Close.EmployeeID = Item.EmployeeID;
            zInfo.Leave_Close.CloseEnd = Item.CloseEnd;
            zInfo.Leave_Close.CloseYear = Item.CloseYear;
            zInfo.Leave_Close.Description = Item.Description;
            zInfo.Leave_Close.Slug = 1; //mặt định ẩn trong form tiêu chuẩn phép
            zInfo.Leave_Close.PartnerNumber = Helper.PartnerNumber;
            zInfo.Leave_Close.CreatedBy = UserLog.UserKey;
            zInfo.Leave_Close.CreatedName = UserLog.EmployeeName;
            zInfo.Leave_Close.ModifiedBy = UserLog.UserKey;
            zInfo.Leave_Close.ModifiedName = UserLog.EmployeeName;

            if (zInfo.Leave_Close.CloseKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            if (zInfo.Leave_Close.Code == "200" ||
                zInfo.Leave_Close.Code == "201")
            {
                return string.Empty;
            }
            else
            {
                return zInfo.Leave_Close.Message;
            }
        }
        private string GenerateUL(List<Employee_Model> listItem, List<Employee_Model> table, StringBuilder sb)
        {
            if (table.Count > 0)
            {
                sb.AppendLine(" <ul class=''>");

                if (listItem.Count > 0)
                {
                    foreach (Employee_Model dr in listItem)
                    {
                        string line = "<li>" + ItemInfo(dr);
                        sb.Append(line);
                        List<Employee_Model> subMenu = table.FindAll(x => x.ReportToKey == dr.EmployeeKey);
                        if (subMenu.Count > 0)
                        {
                            var subMenuBuilder = new StringBuilder();
                            sb.Append(GenerateUL(subMenu, table, subMenuBuilder));
                        }
                        sb.Append("</li>");
                    }
                }
                sb.Append("</ul>");
                return sb.ToString();
            }
            else
            {
                return sb.ToString();
            }
        }
        private string ItemInfo(Employee_Model Employee)
        {
            string img = Helper.DefaultImage;
            if (Employee.PhotoPath.Length > 0)
            {
                img = Url.Content(Employee.PhotoPath);
            }

            string Html = @"
                        <a href='javascript: void(0); '>
                              <div class='member-view-box'>
                                <div class='member-image'>
                                    <img src = '" + img + @"' alt='Member'>
                                    <span class='btn btn-outline-primary border-0 exInfo' name='" + Employee.EmployeeKey + @"'
                                          data-toggle='tooltip' data-placement='right' title='' data-original-title='Hồ sơ'>
                                        <i class='fas fa-lg fa-info-circle'></i>
                                    </span>
                                    <span class='btn btn-outline-warning border-0 exRate' name='" + Employee.EmployeeKey + @"'
                                          data-toggle='tooltip' data-placement='right' title='' data-original-title='Đánh giá'>
                                        <i class='far fa-star'></i>
                                    </span>                                    
                                    <div class='member-details'>
                                        <h3>" + Employee.LastName + " " + Employee.FirstName + @"</h3>
                                    </div>
                                </div>
                            </div>
                        </a>";

            return Html;
        }
        private string StoreFilePost(HttpPostedFileBase[] files, string Key)
        {
            string zFilePath = "";
            string fileName = "";
            try
            {
                if (files.Length > 0)
                {
                    //Upload hình
                    foreach (HttpPostedFileBase file in files)
                    {
                        //Checking file is available to save.  
                        if (file != null && file.ContentLength > 0)
                        {
                            string strGui = Guid.NewGuid().ToString();
                            fileName = strGui + System.IO.Path.GetExtension(file.FileName);

                            #region[Upload]   
                            zFilePath = Helper.UploadPath + "/Employee/" + Key + "/";
                            string zFileSave = Path.Combine(Server.MapPath(zFilePath), fileName);

                            // Check Foder
                            DirectoryInfo zDir = new DirectoryInfo(Server.MapPath(zFilePath));
                            if (!zDir.Exists)
                            {
                                zDir.Create();
                            }
                            else
                            {
                                if (System.IO.File.Exists(zFileSave))
                                {
                                    System.IO.File.Delete(zFileSave);
                                }
                            }

                            file.SaveAs(zFileSave);
                            #endregion
                        }
                    }
                }

                return zFilePath + fileName;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        private List<Payroll_Item> CalculatorPayroll(string Employee, string EmployeeName, string EmployeeID, string PositionName, string Department, string Branch, DateTime DateCalulator, string Style)
        {
            List<Payroll_Item> zList = Employee_Data.TINHLUONG(Helper.PartnerNumber, Employee, DateCalulator);
            if (zList.Count > 0)
            {
                foreach (Payroll_Item Item in zList)
                {
                    Item.EmployeeKey = Employee;
                    Item.EmployeeName = EmployeeName;
                    Item.EmployeeID = EmployeeID;
                    Item.PositionName = PositionName;
                    Item.DateStart = DateCalulator.ToString("dd/MM/yyyy");
                    Item.DepartmentName = Department;
                    Item.Branch = Branch;
                    Item.Style = Style;

                    // có tham gia bảo hiểm
                    if (Style == "BH" || Style == "")
                    {
                        if (Item.Formula != string.Empty)
                        {
                            switch (Item.Formula)
                            {
                                default:
                                    Item.Amount = Item.Param;
                                    break;

                                case "C=A*B%":
                                    double A = GetData(zList, "A");
                                    double B = GetData(zList, "B");
                                    double C = A * (B / 100);
                                    Item.Amount = C.ToString("n0");
                                    break;

                                case "E=(A+C)/D1":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    double D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    double E = (A + C) / D1;
                                    Item.Amount = E.ToString("n0");
                                    break;

                                case "F=E/8":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    D1 = GetData(zList, "D1");
                                    E = (A + C) / D1;
                                    double F = E / 8;
                                    Item.Amount = F.ToString("n0");
                                    break;

                                case "J1=G+H+I+J":
                                    double G = GetData(zList, "G");
                                    double H = GetData(zList, "H");
                                    double I = GetData(zList, "I");
                                    double J = GetData(zList, "J");
                                    double J1 = G + H + I + J;
                                    Item.Amount = J1.ToString("n0");
                                    break;

                                case "M=K*L":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    D1 = GetData(zList, "D1");
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    double K = GetData(zList, "K");
                                    double L = GetData(zList, "L");
                                    double M = (F * K * L) / 100;
                                    Item.Amount = M.ToString("n0");
                                    break;

                                case "P=N*O":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    double N = GetData(zList, "N");
                                    double O = GetData(zList, "O");
                                    double P = (F * N * O) / 100;
                                    Item.Amount = P.ToString("n0");
                                    break;

                                case "P1=M+P":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    K = GetData(zList, "K");
                                    L = GetData(zList, "L");
                                    M = (F * K * L) / 100;
                                    //
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    N = GetData(zList, "N");
                                    O = GetData(zList, "O");
                                    P = (F * N * O) / 100;
                                    //
                                    double P1 = M + P;
                                    Item.Amount = P1.ToString("n0");
                                    break;

                                case "Q=(A+C+G)*8%":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    double Q = (A + C + G) * 8 / 100;
                                    Item.Amount = Q.ToString("n0");
                                    break;

                                case "R=(A+C+G)*1.5%":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    double R = (A + C + G) * 1.5 / 100;
                                    Item.Amount = R.ToString("n0");
                                    break;

                                case "S=(A+C+G)*1%":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    double S = (A + C + G) * 1 / 100;
                                    Item.Amount = S.ToString("n0");
                                    break;

                                case "T1=Q+R+S+T":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    Q = (A + C + G) * 8 / 100;
                                    R = (A + C + G) * 1.5 / 100;
                                    S = (A + C + G) * 1 / 100;
                                    double T = GetData(zList, "T");
                                    double T1 = Q + R + S + T;
                                    Item.Amount = T1.ToString("n0");
                                    break;

                                case "Z=A+J1+P1-T1":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    H = GetData(zList, "H");
                                    I = GetData(zList, "I");
                                    J = GetData(zList, "J");
                                    M = GetData(zList, "M");
                                    P = GetData(zList, "P");
                                    T = GetData(zList, "T");
                                    Q = (A + C + G) * 8 / 100;
                                    R = (A + C + G) * 1.5 / 100;
                                    S = (A + C + G) * 1 / 100;
                                    T1 = Q + R + S + T;
                                    J1 = G + H + I + J;
                                    P1 = M + P;
                                    double Z = A + J1 + P1 - T1 + C;
                                    Item.Amount = Z.ToString("n0");
                                    break;
                            }
                        }
                        else
                        {
                            Item.Amount = Item.Param;
                        }
                    }
                    // không đóng bảo hiểm
                    else
                    {
                        if (Item.Formula != string.Empty)
                        {
                            switch (Item.Formula)
                            {
                                default:
                                    Item.Amount = Item.Param;
                                    break;

                                case "C=A*B%":
                                    double A = GetData(zList, "A");
                                    double B = GetData(zList, "B");
                                    double C = A * (B / 100);
                                    Item.Amount = C.ToString("n0");
                                    break;

                                case "E=(A+C)/D1":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    double D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    double E = (A + C) / D1;
                                    Item.Amount = E.ToString("n0");
                                    break;

                                case "F=E/8":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    D1 = GetData(zList, "D1");
                                    E = (A + C) / D1;
                                    double F = E / 8;
                                    Item.Amount = F.ToString("n0");
                                    break;

                                case "J1=G+H+I+J":
                                    double G = GetData(zList, "G");
                                    double H = GetData(zList, "H");
                                    double I = GetData(zList, "I");
                                    double J = GetData(zList, "J");
                                    double J1 = G + H + I + J;
                                    Item.Amount = J1.ToString("n0");
                                    break;

                                case "M=K*L":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    D1 = GetData(zList, "D1");
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    double K = GetData(zList, "K");
                                    double L = GetData(zList, "L");
                                    double M = (F * K * L) / 100;
                                    Item.Amount = M.ToString("n0");
                                    break;

                                case "P=N*O":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    double N = GetData(zList, "N");
                                    double O = GetData(zList, "O");
                                    double P = (F * N * O) / 100;
                                    Item.Amount = P.ToString("n0");
                                    break;

                                case "P1=M+P":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    D1 = GetData(zList, "D1");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    K = GetData(zList, "K");
                                    L = GetData(zList, "L");
                                    M = (F * K * L) / 100;
                                    //
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    E = (A + C) / D1;
                                    F = E / 8;
                                    N = GetData(zList, "N");
                                    O = GetData(zList, "O");
                                    P = (F * N * O) / 100;
                                    //
                                    double P1 = M + P;
                                    Item.Amount = P1.ToString("n0");
                                    break;

                                case "Q=(A+C+G)*8%":
                                    double Q = 0;
                                    Item.Amount = Q.ToString("n0");
                                    break;

                                case "R=(A+C+G)*1.5%":
                                    double R = 0;
                                    Item.Amount = R.ToString("n0");
                                    break;

                                case "S=(A+C+G)*1%":
                                    double S = 0;
                                    Item.Amount = S.ToString("n0");
                                    break;

                                case "T1=Q+R+S+T":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    Q = 0;
                                    R = 0;
                                    S = 0;
                                    double T = GetData(zList, "T");
                                    double T1 = Q + R + S + T;
                                    Item.Amount = T1.ToString("n0");
                                    break;

                                case "Z=A+J1+P1-T1":
                                    A = GetData(zList, "A");
                                    B = GetData(zList, "B");
                                    C = A * (B / 100);
                                    G = GetData(zList, "G");
                                    H = GetData(zList, "H");
                                    I = GetData(zList, "I");
                                    J = GetData(zList, "J");
                                    M = GetData(zList, "M");
                                    P = GetData(zList, "P");
                                    T = GetData(zList, "T"); ;
                                    Q = 0;
                                    R = 0;
                                    S = 0;
                                    T1 = Q + R + S + T;
                                    J1 = G + H + I + J;
                                    P1 = M + P;
                                    double Z = A + J1 + P1 - T1 + C;
                                    Item.Amount = Z.ToString("n0");
                                    break;
                            }
                        }
                        else
                        {
                            Item.Amount = Item.Param;
                        }
                    }
                }
            }

            return zList;
        }
        private float GetData(List<Payroll_Item> ListData, string ID)
        {
            var obj = ListData.SingleOrDefault(x => x.ItemID == ID).Param;
            return obj.ToFloat();
        }
        private DataTable PopulateGrid(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Mã số", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));
            dt.Columns.Add(dc);
            dc = new DataColumn("Phòng ban", typeof(string));
            dt.Columns.Add(dc);

            //get header
            var tblLabel = ListData.DistinctBy(o => o.ItemName).ToList();

            //create data column
            foreach (Payroll_Item s in tblLabel)
            {
                dc = new DataColumn(s.ItemName, typeof(string));
                dt.Columns.Add(dc);
            }

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.DepartmentName;
                dr[1] = rec.EmployeeID;
                dr[2] = rec.EmployeeName;
                dr[3] = rec.PositionName;
                dr[4] = rec.DepartmentName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    int no = 5;
                    foreach (Payroll_Item item in data)
                    {
                        dr[no] = item.Amount.ToString();
                        no++;
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable InsuranceTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Mã số", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Phòng ban", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //5  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //6   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //7 C
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp chức vụ", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương đóng BHXH", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("BHYT", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("BHTN", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //13
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.DepartmentName;
                dr[1] = rec.EmployeeID;
                dr[2] = rec.EmployeeName;
                dr[3] = rec.PositionName;
                dr[4] = rec.DepartmentName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "G").Amount;
                    dr[9] = (dr[5].ToDouble() + dr[6].ToDouble() + dr[7].ToDouble() + dr[8].ToDouble()).ToString("n0");
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "Q").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "R").Amount;
                    dr[12] = data.SingleOrDefault(d => d.ItemID == "S").Amount;
                    dr[13] = (dr[10].ToDouble() + dr[11].ToDouble() + dr[12].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable OverTimeTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Mã số", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Phòng ban", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //5  A
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số thâm niên", typeof(string));   //6   B
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //7 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //8 C            
            dt.Columns.Add(dc);
            dc = new DataColumn("Gờ làm thêm ngày thường", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm ngày thường", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //11
            dt.Columns.Add(dc);
            dc = new DataColumn("Giờ làm thêm ngày lễ", typeof(string));   //12
            dt.Columns.Add(dc);
            dc = new DataColumn("Hệ số làm thêm lễ", typeof(string));   //13
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //14
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng tiền trực", typeof(string));   //15
            dt.Columns.Add(dc);
            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.DepartmentName;
                dr[1] = rec.EmployeeID;
                dr[2] = rec.EmployeeName;
                dr[3] = rec.PositionName;
                dr[4] = rec.DepartmentName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "B").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[8] = (dr[5].ToDouble() + dr[7].ToDouble()).ToString("n0");
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "K").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "L").Amount;
                    dr[11] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[12] = data.SingleOrDefault(d => d.ItemID == "N").Amount;
                    dr[13] = data.SingleOrDefault(d => d.ItemID == "O").Amount;
                    dr[14] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[15] = data.SingleOrDefault(d => d.ItemID == "P1").Amount;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable SupportTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Mã số", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Phòng ban", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp chức vụ", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp điện thoại", typeof(string));   //6
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp xăng xe", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp đi lại", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm ngày thường", typeof(string));   //9            
            dt.Columns.Add(dc);
            dc = new DataColumn("Số tiền làm thêm lễ", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng", typeof(string));   //11
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.DepartmentName;
                dr[1] = rec.EmployeeID;
                dr[2] = rec.EmployeeName;
                dr[3] = rec.PositionName;
                dr[4] = rec.DepartmentName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    dr[5] = data.SingleOrDefault(d => d.ItemID == "G").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "H").Amount;
                    dr[7] = data.SingleOrDefault(d => d.ItemID == "I").Amount;
                    dr[8] = data.SingleOrDefault(d => d.ItemID == "J").Amount;
                    dr[9] = data.SingleOrDefault(d => d.ItemID == "M").Amount;
                    dr[10] = data.SingleOrDefault(d => d.ItemID == "P").Amount;
                    dr[11] = (dr[5].ToDouble() + dr[6].ToDouble() + dr[7].ToDouble() + dr[8].ToDouble() + dr[9].ToDouble() + dr[10].ToDouble()).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        private DataTable PaymentTable(List<Payroll_Item> ListData)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Department", typeof(string));   //0
            dt.Columns.Add(dc);
            dc = new DataColumn("Mã số", typeof(string));   //1
            dt.Columns.Add(dc);
            dc = new DataColumn("Họ tên", typeof(string));   //2
            dt.Columns.Add(dc);
            dc = new DataColumn("Chức vụ", typeof(string));   //3
            dt.Columns.Add(dc);
            dc = new DataColumn("Phòng ban", typeof(string));   //4
            dt.Columns.Add(dc);
            dc = new DataColumn("Lương thỏa thuận", typeof(string));   //5
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp thâm niên", typeof(string));   //6
            dt.Columns.Add(dc);
            dc = new DataColumn("Phụ cấp khác", typeof(string));   //7
            dt.Columns.Add(dc);
            dc = new DataColumn("Tổng lương", typeof(string));   //8
            dt.Columns.Add(dc);
            dc = new DataColumn("BHXH, BHYT, BHTN", typeof(string));   //9
            dt.Columns.Add(dc);
            dc = new DataColumn("Tạm ứng", typeof(string));   //10
            dt.Columns.Add(dc);
            dc = new DataColumn("Thực nhận", typeof(string));   //11
            dt.Columns.Add(dc);

            //list employee
            var listEmployee = ListData.DistinctBy(o => o.EmployeeKey).ToList();

            //add data to table
            foreach (Payroll_Item rec in listEmployee)
            {
                // The left columns of the row
                DataRow dr = dt.NewRow();
                dr[0] = rec.Branch;
                dr[1] = rec.EmployeeID;
                dr[2] = rec.EmployeeName;
                dr[3] = rec.PositionName;
                dr[4] = rec.DepartmentName;

                //list data of 1 employee
                var data = ListData.FindAll(x => x.EmployeeKey == rec.EmployeeKey);
                if (data.Count > 0)
                {
                    double J1 = data.SingleOrDefault(d => d.ItemID == "J1").Amount.ToDouble();
                    double P1 = data.SingleOrDefault(d => d.ItemID == "P1").Amount.ToDouble();
                    double TN = data.SingleOrDefault(d => d.ItemID == "C").Amount.ToDouble();
                    double T = data.SingleOrDefault(d => d.ItemID == "T").Amount.ToDouble();
                    double Q = data.SingleOrDefault(d => d.ItemID == "Q").Amount.ToDouble();
                    double R = data.SingleOrDefault(d => d.ItemID == "R").Amount.ToDouble();
                    double S = data.SingleOrDefault(d => d.ItemID == "S").Amount.ToDouble();
                    double Z = Q + R + S;

                    dr[5] = data.SingleOrDefault(d => d.ItemID == "A").Amount;
                    dr[6] = data.SingleOrDefault(d => d.ItemID == "C").Amount;
                    dr[7] = (J1 + P1).ToString("n0");
                    dr[8] = (J1 + P1 + dr[5].ToDouble()).ToString("n0");
                    dr[9] = Z.ToString("n0");
                    dr[10] = T.ToString("n0");
                    dr[11] = (dr[8].ToDouble() - dr[9].ToDouble() - dr[10].ToDouble() + TN).ToString("n0");
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}